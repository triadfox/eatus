# **Triad Fox - Triad Fox Team** #

## 1.Cocos2d-x and Cocos2D Objective-C installation
 ##-Download Cocos2d-x framework from: http://www.cocos2d-x.org/download.
-Download Cocos2d-x C++. Download v2.2.6 by download or clone to Mac.
-Download Cocos2d SpriteBuilder (Objective) from: http://cocos2d.spritebuilder.com
## 2.Bitbucket and SourceTree guidelines ##
### 2.1 Bitbucket and Git ###
-Accept invitation from team, read Git guideline to how to manipulate data with Distributed Source Version Control (DSVC).
-Access team’s repository hosted by Bitbucket at: https://bitbucket.org
-There are three repository included: Implementation, Documents and Tools. 

### 2.2 SourceTree ###
-Download SourceTree from: https://www.sourcetreeapp.com.
-Download and pull it to Application (Mac) from dmg file.
-Open it and clone repository .git from Bitbucket repository.

## 3. Project Environment Setup ##
-At Bitbucket, access Team repository’s project. Click Clone and Choose Clone in SourceTree. SourceTree will automatically clone it to local storage.
-With project, copy HTTPS git link, paste it to Xcode and checkout it from Bitbucket.
Note:
	+Cocos2d-x project was create with initialised library of cocos2d. So please put project to directory of cocos2d-x which is downloaded.
	+Compile and run it. For any concern contact team for details.