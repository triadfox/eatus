//
//  WatchConnectivityManager.swift
//  Eatus
//
//  Created by Phong Nguyen on 11/22/15.
//  Copyright © 2015 Triad Fox. All rights reserved.
//

import Foundation
import WatchKit
import WatchConnectivity

let keyMessageTransfer: String =                     "msgType"

enum MessageTransferType: Int{
    case ReceiptMessage
    case FeedbackMessage
    case UnknownMessage
}

class WatchConnectivityManager: NSObject, WCSessionDelegate{
    var session: WCSession?
    var didPaired: Bool!
    
    class var shareInstance: WatchConnectivityManager{
        struct Singleton {
            static let manager = WatchConnectivityManager()
        }
        return Singleton.manager
    }
    
    func launchConnectivitySession(){
        if WCSession.isSupported(){
            session = WCSession.defaultSession()
            session!.delegate = self
            session!.activateSession()
            
            didPaired = false
        }
    }
    
    func sendMessageToParentDeviceWithApplicationContext(message: [String:AnyObject], completionHandler: (Bool, NSError?) -> Void){
        do{
            try session?.updateApplicationContext(message)
            completionHandler(true, nil)
        }catch let error as NSError{
            completionHandler(false, error)
        }
    }
    
    func sendMessageToParentDeviceInteractiveMessage(message: [String:AnyObject], completionHandler: ([String:AnyObject]) -> Void){
        session?.sendMessage(message, replyHandler: {(replyMessage) ->Void in
            
            }, errorHandler: {(error) ->Void in
        })
    }
    
    func sendMessageDataToParentDeviceInteractiveMessage(messageData: NSData, completionHandler: ([String:AnyObject]) -> Void){
        session?.sendMessageData(messageData, replyHandler: {(data) ->Void in
            
            }, errorHandler: {(error) ->Void in
                
        })
    }
    
    
    func sendUserInfoParentDeviceInBackground(userInfo: [String:AnyObject]){
        session?.transferUserInfo(userInfo)
    }
    
    func isPairedWithParentDevice() -> Bool{
        if session == nil{
            self.launchConnectivitySession()
        }
        return session!.reachable
    }
    
    
    //#MARK: Private methods
    func identifyTranferringMessageType(message: [String:AnyObject]) -> MessageTransferType{
        let msgType: String = message[keyMessageTransfer] as! String
        
        switch msgType{
            case "Receipt":
                return .ReceiptMessage;
            case "Feedback":
                return .FeedbackMessage;
            
        default:
            break
        }
        
        return .UnknownMessage
    }
    
    
    //#MARK: WCSessionDelegate methods
    func session(session: WCSession, didFinishUserInfoTransfer userInfoTransfer: WCSessionUserInfoTransfer, error: NSError?) {
        
    }
    
    func session(session: WCSession, didReceiveMessageData messageData: NSData, replyHandler: (NSData) -> Void) {
        print("Did receive interactive message data from Parent Device")
    }
    
    func session(session: WCSession, didReceiveMessage message: [String : AnyObject], replyHandler: ([String : AnyObject]) -> Void) {
        print("Did receive interactive message from Parent Device")
    }
    
    func session(session: WCSession, didReceiveUserInfo userInfo: [String : AnyObject]) {
        print("Did receive user info from Parent Device")
    }
    
    func session(session: WCSession, didReceiveApplicationContext applicationContext: [String : AnyObject]) {
        print("Did receive application context from Parent Device: \(applicationContext[keyMessageTransfer]!)")
        
        let messageType: MessageTransferType = self.identifyTranferringMessageType(applicationContext)
        if messageType == .ReceiptMessage{
            _UserDefault.setObject(applicationContext, forKey: receiptTransferringFromParentDevice)
        }else if messageType == .FeedbackMessage{
            
        }else if messageType == .UnknownMessage{
            
        }
    }
    
    func session(session: WCSession, didReceiveFile file: WCSessionFile) {
        
    }
    
    //reachabilities and states
    func sessionReachabilityDidChange(session: WCSession) {
        didPaired = session.reachable
    }
    
    func sessionWatchStateDidChange(session: WCSession) {
        
    }
}


