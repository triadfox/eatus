//
//  Promotion.swift
//  Eatus
//
//  Created by Phong Nguyen on 11/23/15.
//  Copyright © 2015 Triad Fox. All rights reserved.
//

import Foundation

class PromotionAvailable {
    let promotionId: String
    let promotionName: String?
    let promotionDiscount: Float?
    let promotionStartDate: NSDate
    let promotionEndDate: NSDate
    let promotionImageUrl: String?
    
    let restaurantName: String
    let longitude: CLLocationDegrees
    let latitude: CLLocationDegrees
    
    init(id: String, name: String?, discountPercentage: Float?, startDate: NSDate, endDate: NSDate, imageUrl: String?, restaurantName: String, longitude: CLLocationDegrees, latitude: CLLocationDegrees){
        self.promotionId = id
        self.promotionName = name
        self.promotionDiscount = discountPercentage
        self.promotionStartDate = startDate
        self.promotionEndDate = endDate
        self.promotionImageUrl = imageUrl
        
        self.restaurantName = restaurantName
        self.longitude = longitude
        self.latitude = latitude
    }
    
    convenience init(dictionary: [String:AnyObject]){
        let id = dictionary["objectId"] as! String
        let name = dictionary["title"] as! String
        let discountPercentage = dictionary["discount"] as! Float
        
        let startDateStr = dictionary["startDate"]!["iso"] as! String
        let endDateStr = dictionary["endDate"]!["iso"] as! String
        let startDate = dateForStringAtCurrentLocaleUTC(startDateStr, format: nil)
        let endDate = dateForStringAtCurrentLocaleUTC(endDateStr, format: nil)
        
        let imageUrl = dictionary["promotionImage"]?["url"] as? String
        
        let restaurantDict = dictionary["restaurant"]!
        
        let restaurantName = restaurantDict["name"] as! String
        
        let geoPointDict = restaurantDict["geoPoint"] as! [String:AnyObject]
        let longitude = geoPointDict["longitude"] as! CLLocationDegrees
        let latitude = geoPointDict["latitude"] as! CLLocationDegrees
        
        self.init(id: id, name: name, discountPercentage: discountPercentage, startDate: startDate!, endDate: endDate!, imageUrl: imageUrl, restaurantName: restaurantName, longitude: longitude, latitude: latitude)
    }
}