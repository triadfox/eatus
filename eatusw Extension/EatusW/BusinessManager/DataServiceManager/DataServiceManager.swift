//
//  DataServiceManager.swift
//  Eatus
//
//  Created by Phong Nguyen on 11/21/15.
//  Copyright © 2015 Triad Fox. All rights reserved.
//

import Foundation

class DataServiceManager: NSObject {
    
    class var shareInstance: DataServiceManager{
        struct Singleton {
            static let manager = DataServiceManager()
        }
        return Singleton.manager
    }
    
    
    //#MARK:- Outlet Store Fetcher
    func getAllOutletStoreFromBackEnd(completionHandler: ([String:AnyObject]?, NSError?) -> Void){
        let apiOutletStore = baseURL + apiGetAllOutletStore
        let request = NSMutableURLRequest(URL: NSURL(string: apiOutletStore)!)
        request.HTTPMethod = "GET"
        request.setValue("application/json; charset=utf-8", forHTTPHeaderField: "Content-Type")
        request.setValue("application/json", forHTTPHeaderField: "Accept")
        
        let outletTask = NSURLSession.sharedSession().dataTaskWithRequest(request, completionHandler: {(data, responseObject, error) -> Void in
            if let errorRet = error{
                completionHandler(nil, errorRet)
            }else{
                if let fetchData = data{
                    do {
                        let anyObj = try NSJSONSerialization.JSONObjectWithData(fetchData, options: []) as! [String:AnyObject]
                        completionHandler(anyObj, nil)
                    } catch let error as NSError {
                        completionHandler(nil, error)
                    }
                }
            }
        })
        
        outletTask.resume()
    }
    
    func outletStoreByNearestDistance(latitude: CLLocationDegrees, longitude: CLLocationDegrees, skip: Int, limit: Int, completionHandler: ([String:AnyObject]?, NSError?) -> Void){
        let apiOutletNearestSotre = baseURL + apiGetOutletByNearestDistance + "?latitude=\(latitude)&longitude=\(longitude)&skip=\(skip)&limit=\(limit)"
        let request = NSMutableURLRequest(URL: NSURL(string: apiOutletNearestSotre)!)
        request.HTTPMethod = "GET"
        request.setValue("application/json; charset=utf-8", forHTTPHeaderField: "Content-Type")
        request.setValue("application/json", forHTTPHeaderField: "Accept")
    
        let nearestOutletTask = NSURLSession.sharedSession().dataTaskWithRequest(request, completionHandler: {(data, responseObject, error) -> Void in
            if let errorRet = error{
                completionHandler(nil, errorRet)
            }else{
                if let fetchData = data{
                    do{
                        let nearestOutlet = try NSJSONSerialization.JSONObjectWithData(fetchData, options: []) as! [String:AnyObject]
                        completionHandler(nearestOutlet, nil)
                    }catch let error as NSError{
                        completionHandler(nil, error)
                    }
                }
            }
        })
        
        nearestOutletTask.resume()
    }

    
    //#MARK:- Feedback Fetcher
    func getRecentlyFeedbackByRange(skip: Int?, limit: Int!, completionHandler: ([String:AnyObject]?, NSError?) -> Void){
        let apiRecentlyFeedback = baseURL + apiGetRecentFeedbackByRange + "?skip=\(skip)&limit=\(limit)"
        let request = NSMutableURLRequest(URL: NSURL(string: apiRecentlyFeedback)!)
        request.HTTPMethod = "GET"
        request.setValue("application/json; charset=utf-8", forHTTPHeaderField: "Content-Type")
        request.setValue("application/json", forHTTPHeaderField: "Accept")
        
        let recentlyTask = NSURLSession.sharedSession().dataTaskWithRequest(request, completionHandler: {(data, responseObject, error) -> Void in
            if let errorRet = error{
                completionHandler(nil, errorRet)
            }else{
                if let fetchData = data{
                    do{
                        let recentlyObj = try NSJSONSerialization.JSONObjectWithData(fetchData, options: []) as! [String:AnyObject]
                        completionHandler(recentlyObj, nil)
                    }catch let error as NSError{
                        completionHandler(nil, error)
                    }
                }
            }
        })
        
        recentlyTask.resume()
    }
    
    func getFeedbackByRestaurant(restaurantId: String!, skip: Int?, limit: Int!, completionHandler: ([String:AnyObject]?, NSError?) -> Void){
        let apiRecentlyFeedback = baseURL + apiGetRecentFeedbackByRange + "?restaurantId=\(restaurantId)" + "skip=\(skip)&limit=\(limit)"
        let request = NSMutableURLRequest(URL: NSURL(string: apiRecentlyFeedback)!)
        request.HTTPMethod = "GET"
        request.setValue("application/json; charset=utf-8", forHTTPHeaderField: "Content-Type")
        request.setValue("application/json", forHTTPHeaderField: "Accept")
        
        let recentlyTask = NSURLSession.sharedSession().dataTaskWithRequest(request, completionHandler: {(data, responseObject, error) -> Void in
            if let errorRet = error{
                completionHandler(nil, errorRet)
            }else{
                if let fetchData = data{
                    do{
                        let recentlyObj = try NSJSONSerialization.JSONObjectWithData(fetchData, options: []) as! [String:AnyObject]
                        completionHandler(recentlyObj, nil)
                    }catch let error as NSError{
                        completionHandler(nil, error)
                    }
                }
            }
        })
        
        recentlyTask.resume()
    }
    
    
    //#MARK:- Promotion Fetcher
    func getAllAvailablePromotion(skip: Int?, limit: Int!, completionHandler: ([String:AnyObject]?, NSError?) -> Void){
        let apiAllPromotion = baseURL + apiGetAvailablePromotion + "?skip\(skip)&limit=\(limit)"
        let request = NSMutableURLRequest(URL: NSURL(string: apiAllPromotion)!)
        request.HTTPMethod = "GET"
        request.setValue("application/json; charset=utf-8", forHTTPHeaderField: "Content-Type")
        request.setValue("application/json", forHTTPHeaderField: "Accept")
        
        let promotionTask = NSURLSession.sharedSession().dataTaskWithRequest(request, completionHandler: {(data, responseObject, error) -> Void in
            if let errorRet = error{
                completionHandler(nil, errorRet)
            }else{
                if let fetchData = data{
                    do{
                        let recentlyObj = try NSJSONSerialization.JSONObjectWithData(fetchData, options: []) as! [String:AnyObject]
                        completionHandler(recentlyObj, nil)
                    }catch let error as NSError{
                        completionHandler(nil, error)
                    }
                }
            }
        })
        
        promotionTask.resume()
    }
    
    
    //#MARK:- Private methods
    func generateParamsDictionaryForGiven(paramNames: [String]!, paramValues: [AnyObject]!)->NSDictionary?{
        if paramNames.count != paramValues.count{
            return nil
        }
        
        let paramKV: NSMutableDictionary = NSMutableDictionary()
        for index in 0..<paramNames.count{
            let value: AnyObject = paramValues[index]
            paramKV.setObject(value, forKey: paramNames[index])
        }
        
        return NSDictionary(dictionary: paramKV)
    }
}
