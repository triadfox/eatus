//
//  LastReceipt.swift
//  Eatus
//
//  Created by Phong Nguyen on 11/29/15.
//  Copyright © 2015 Triad Fox. All rights reserved.
//

import Foundation

class Receipt {
    let receiptId: String
    let receiptCurrency: String
    let receiptDiscountAmount: Double
    let receiptDiscountPercentage: Float
    let receiptSubtotal: Double
    let receiptTax: Double
    let receiptTotal: Double
    let receiptAllDishes: [AnyObject]
    let receiptIssuedDate: NSDate
    let receiptRestaurant: String
    
    var receiptLocale: NSLocale?{
        get{
            return localeByCurrencyMark(receiptCurrency)
        }
    }
    
    var receiptDishes: [String:Double]?{
        get{
             return self.enumerateOnReceiptDishes(receiptAllDishes)
        }
    }
    
    init(receiptId: String, receiptCurrency: String, receiptDiscountAmount: Double, receiptDiscountPercentage: Float, receiptSubtotal: Double, receiptTax: Double, receiptTotal: Double, issuedDate: NSDate, restaurant: String, dishes: [String:AnyObject]){
        self.receiptId = receiptId
        self.receiptCurrency = receiptCurrency
        self.receiptDiscountAmount = receiptDiscountAmount
        self.receiptDiscountPercentage = receiptDiscountPercentage
        self.receiptSubtotal = receiptSubtotal
        self.receiptTax = receiptTax
        self.receiptTotal = receiptTotal
        self.receiptIssuedDate = issuedDate
        self.receiptRestaurant = restaurant
        
        self.receiptAllDishes = dishes["dishes"] as! [AnyObject]
    }
    
    convenience init(dictionary: [String:AnyObject]){
        let id = dictionary["receiptId"] as! String
        let currency = dictionary["currency"] as! String
        let discountAmount = dictionary["discountAmount"] as! Double
        let discountPercentage = dictionary["discountPercentage"] as! Float
        let subTotal = dictionary["subtotal"] as! Double
        let tax = dictionary["tax"] as! Double
        let total = dictionary["total"] as! Double
        let restaurantName = dictionary["restaurantName"] as! String
        
        let issuedDate = dictionary["issuedDate"] as! NSDate
        
        self.init(receiptId: id, receiptCurrency: currency, receiptDiscountAmount: discountAmount, receiptDiscountPercentage: discountPercentage, receiptSubtotal: subTotal, receiptTax: tax, receiptTotal: total, issuedDate:issuedDate, restaurant: restaurantName, dishes: dictionary)
    }
    
    func enumerateOnReceiptDishes(dishes: [AnyObject]) -> [String:Double]{
        var dishDictionary: [String:Double] = [:]
        for dish in dishes{
            if let dishDict = dish as? [String:AnyObject]{
                let dishQty = dishDict["dishQuantity"] as! Int
                let dishName = dishDict["dishName"] as! String + "(\(dishQty))"
                let dishPrice = dishDict["dishPrice"] as! Double * Double(dishQty)
                
                dishDictionary["\(dishName)"] = dishPrice
            }
        }
        return dishDictionary
    }
}