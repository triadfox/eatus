//
//  DataSerializationManager.swift
//  Eatus
//
//  Created by Phong Nguyen on 11/21/15.
//  Copyright © 2015 Triad Fox. All rights reserved.
//

import Foundation
import CoreLocation

class OutletStore {
    
    let outletName: String
    let outletObjectId: String
    
    let outletAddress: String
    
    let logoFileUrl: String
    let menuFileUrl: String?
    
    let closeTime: Int
    let openTime: Int
    
    var distance: CLLocationDistance?
    
    var storeWhetherOpenClosed: Bool{
        get{
            return self.determineStoreWhetherOpenClosed(self.closeTime, openTime: self.openTime)
        }
    }
    
    var servedTime: String{
        get{
            let servedOpenTimeByDate = dayTimeInDayForAllTotalSecond(self.openTime) 
            let servedCloseTimeByDate = dayTimeInDayForAllTotalSecond(self.closeTime)
            
            let calendar = NSCalendar.currentCalendar()
            let dateFormatter = NSDateFormatter()
            dateFormatter.timeZone = calendar.timeZone
            dateFormatter.dateFormat = "HH:mm"
            dateFormatter.locale = NSLocale.currentLocale()
            
            let openStrFromDate = dateFormatter.stringFromDate(servedOpenTimeByDate)
            let closeStrFromDate = dateFormatter.stringFromDate(servedCloseTimeByDate)
            
            return "\(openStrFromDate) - \(closeStrFromDate)"
        }
    }
    
    let outletLocation: CLLocation
    
    init(outletName: String, outletObjectId: String, outletAddress: String, logoFileUrl: String, menuFileUrl: String?, location: CLLocation, closeTime: Int, openTime: Int){
        self.outletName = outletName
        self.outletObjectId = outletObjectId
        self.outletAddress = outletAddress
        self.logoFileUrl = logoFileUrl
        self.menuFileUrl = menuFileUrl
        self.outletLocation = location
        self.closeTime = closeTime
        self.openTime = openTime
    }
    
    convenience init(dictionary: [String:AnyObject]){
        let outletName = dictionary["name"] as? String
        let outletObjectId = dictionary["objectId"] as? String
        let outletAddress = dictionary["address"] as? String
        
        let logoUrl = dictionary["logoImage"]!["url"] as? String
        let menuUrl = dictionary["menu"]!["url"] as? String
        let closeTime = dictionary["closeTime"] as? Int
        let openTime = dictionary["openTime"] as? Int
        
        let geoPointDict = dictionary["geoPoint"] as? [String:AnyObject]
        let longitude = geoPointDict!["longitude"] as! CLLocationDegrees
        let latitude = geoPointDict!["latitude"] as! CLLocationDegrees
        
        let location = CLLocation(latitude: latitude, longitude: longitude)
        
        self.init(outletName: outletName!, outletObjectId: outletObjectId!, outletAddress: outletAddress!, logoFileUrl: logoUrl!, menuFileUrl: menuUrl, location: location, closeTime: closeTime!, openTime: openTime!)
    }
    
    //#MARK: Private method: Convert Total second in a day to time
    func determineStoreWhetherOpenClosed(closedTime: Int, openTime: Int) ->Bool{
        var openTimeAsDay = self.dayTimeForTotalSecond(openTime)
        var closedTimeAsDay = self.dayTimeForTotalSecond(closedTime)
        
        var now = NSDate()
        let sourceTimeZone = NSTimeZone(abbreviation: "GMT")!
        let destinationTimeZone = NSTimeZone.systemTimeZone()
        
        let sourceGTMOffset = sourceTimeZone.secondsFromGMTForDate(now)
        let destinationGTMOffset = destinationTimeZone.secondsFromGMTForDate(now)
        let timeInterval: Double = Double(destinationGTMOffset - sourceGTMOffset)
        
        now = NSDate(timeInterval: timeInterval, sinceDate: now)
        closedTimeAsDay = NSDate(timeInterval: timeInterval, sinceDate: closedTimeAsDay)
        openTimeAsDay = NSDate(timeInterval: timeInterval, sinceDate: openTimeAsDay)
        
        if(now.compare(openTimeAsDay) == .OrderedDescending && now.compare(closedTimeAsDay) == .OrderedAscending){
            return true
        }
        return false
    }
    
    func dayTimeForTotalSecond(totalSecs: Int)->NSDate{
        let minutes: Int = (totalSecs/60) % 60
        let hours: Int = totalSecs / 3600
        let complexity: String = "\(hours):\(minutes)"
        return dayTimeFromMinutesAndHours(complexity)
    }
    
    func dayTimeFromMinutesAndHours(complex: String) -> NSDate{
        let complexity = complex.componentsSeparatedByString(":")
        let calendar: NSCalendar = NSCalendar.currentCalendar()
        calendar.locale = NSLocale.currentLocale()
        
        let components = calendar.components([.Year, .Month, .Day], fromDate: NSDate())
        components.setValue(Int(complexity.first!)!, forComponent: .Hour)
        components.setValue(Int(complexity.last!)!, forComponent: .Minute)
        
        return calendar.dateFromComponents(components)!
    }
}






