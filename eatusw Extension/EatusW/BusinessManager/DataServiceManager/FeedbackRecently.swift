//
//  Feedback.swift
//  Eatus
//
//  Created by Phong Nguyen on 11/23/15.
//  Copyright © 2015 Triad Fox. All rights reserved.
//

import Foundation

class FeedbackRecently: NSObject {
    let feedbackComment: String?
    let feedbackDate: NSDate?
    let feedbackHappy: Bool
    
    let feedbackId: String
    
    let restaurantId: String
    let restaurantName: String
    let restaurantAddress: String
    let latitude: CLLocationDegrees
    let longitude: CLLocationDegrees
    
    let feedbackOwner: String?
    
    init(feedbackId: String!, feedbackComment: String?, feedbackOwner: String?, feedbackDate: NSDate?, feedbackHappy:Bool!, atRestaurantId: String!, restaurantName: String!, restaurantAdd: String!, latitude: CLLocationDegrees!, longitude: CLLocationDegrees!){
        self.feedbackId = feedbackId
        self.feedbackComment = feedbackComment
        self.feedbackDate = feedbackDate
        self.feedbackHappy = feedbackHappy
        
        self.feedbackOwner = feedbackOwner
        
        self.restaurantId = atRestaurantId
        self.restaurantName = restaurantName
        self.restaurantAddress = restaurantAdd
        self.longitude = longitude
        self.latitude = latitude
    }
    
    convenience init(dictionary: [String:AnyObject]!){
        let feedbackId = dictionary["objectId"] as! String
        let feedbackComment = dictionary["comment"] as! String
        
        let feedbackDateStr = dictionary["createdAt"] as! String
        let feedbackDate = dateForStringAtCurrentLocaleUTC(feedbackDateStr, format: nil)
        let feedbackHappy = dictionary["isHappy"] as! Bool
        
        let userDict = dictionary["user"] as! [String:AnyObject]
        let feedbackOwner = userDict["fullname"] as! String
        
        let restaurantDict = dictionary["restaurant"] as! [String:AnyObject]
        let restaurantId = restaurantDict["objectId"] as! String
        let restaurantName = restaurantDict["name"] as! String
        let restaurantAdd = restaurantDict["address"] as! String
        
        let geoPoint = restaurantDict["geoPoint"] as! [String:AnyObject]
        let latitude = geoPoint["latitude"] as! CLLocationDegrees
        let longitude = geoPoint["longitude"] as! CLLocationDegrees
        
        self.init(feedbackId: feedbackId, feedbackComment: feedbackComment, feedbackOwner: feedbackOwner, feedbackDate: feedbackDate, feedbackHappy:feedbackHappy, atRestaurantId: restaurantId, restaurantName: restaurantName, restaurantAdd: restaurantAdd, latitude: latitude, longitude: longitude)
    }
    
    
    //#MARK: Private methods
}

