//
//  LocationManager.swift
//  Eatus
//
//  Created by Phong Nguyen on 11/22/15.
//  Copyright © 2015 Triad Fox. All rights reserved.
//

import WatchKit
import Foundation
import CoreLocation

class LocationManager: NSObject, CLLocationManagerDelegate {
    let locationManager = CLLocationManager()
    
    var currentLocation: CLLocation?
    var parentInterface: WKInterfaceController!
    
    var didCancelLocationError: Bool?
    
    class var shareInstance: LocationManager{
        struct Singleton {
            static let manager = LocationManager()
        }
        return Singleton.manager
    }
    
    func launchMonitorLocation(){
        if CLLocationManager.locationServicesEnabled(){
            locationManager.requestAlwaysAuthorization()
            
            let authorizeStatus = CLLocationManager.authorizationStatus();
            
            switch authorizeStatus{
            case .NotDetermined:
                locationManager.requestAlwaysAuthorization();
                
            case .Restricted:
                locationManager.requestAlwaysAuthorization();
                
            case .AuthorizedAlways:
                locationManager.distanceFilter = 100;
                locationManager.desiredAccuracy = kCLLocationAccuracyBestForNavigation
                locationManager.delegate = self
                locationManager.requestLocation();
                
            default: break
                
            }
        }
    }
    
    func locationManager(manager: CLLocationManager, didChangeAuthorizationStatus status: CLAuthorizationStatus) {
        switch status{
        case .Denied:
            break
            
        case .AuthorizedWhenInUse:
            break
            
        case .NotDetermined:
            break
            
        case .Restricted:
            break
            
        default:
            break
        }
    }
    
    func locationManager(manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        guard !locations.isEmpty else {
            return
        }
        
        currentLocation = locations.first
    }
    
    func locationManager(manager: CLLocationManager, didFailWithError error: NSError) {
        if(self.didCancelLocationError == false){
            let retryAlertAction = WKAlertAction(title: "Retry", style: .Default, handler: {
                self.launchMonitorLocation()
            })
            
            let cancelAlertAction = WKAlertAction(title: "Cancel", style: .Cancel, handler: {
                self.didCancelLocationError = true
            })
            
            dispatch_async(dispatch_get_main_queue(), {
                self.parentInterface.presentAlertControllerWithTitle("Location Not Found", message: "We temporarily can not determine your current position", preferredStyle: .Alert, actions: [retryAlertAction, cancelAlertAction])
            })
        }
    }
    
    func distanceFromCurrentLocationTo(longitude: CLLocationDegrees?, latitude: CLLocationDegrees?, completionHandler: (CLLocationDistance!) ->Void){
        let locationByLongLat = CLLocation(latitude: latitude!, longitude: longitude!)
        
        if currentLocation == nil{
            self.launchMonitorLocation()
        }else{
            let distanceToSpecifiedPosition = currentLocation?.distanceFromLocation(locationByLongLat)
            if distanceToSpecifiedPosition != 0.0000{
                completionHandler(distanceToSpecifiedPosition)
            }
        }
    }
    
    func distanceFromCurrentLocationTo(location: CLLocation, completionHandler: (CLLocationDistance!) ->Void){
        guard (currentLocation != nil) else{
            completionHandler(0.00)
            return
        }
        
        let distanceToSpecifiedPosition: CLLocationDistance! = location.distanceFromLocation(currentLocation!)
        if distanceToSpecifiedPosition != 0.000{
            completionHandler(distanceToSpecifiedPosition/1000)
        }
    }
    
    func currentLocationMomentum() -> CLLocation?{
        if let momentumLocation = currentLocation{
            return momentumLocation
        }
        self.launchMonitorLocation()
        return nil
    }
    
    func currentLocationOnMomentum(completionHandler: (CLLocation) -> Void){
        let momentumGroup = dispatch_group_create();
        
        dispatch_group_enter(momentumGroup)
        self.launchMonitorLocation()
    
        let delaySearching = dispatch_time(DISPATCH_TIME_NOW, Int64(5 * NSEC_PER_SEC))
        dispatch_after(delaySearching, dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0), {
            if self.currentLocation != nil{
                dispatch_group_leave(momentumGroup)
            }
        })
        dispatch_group_notify(momentumGroup, dispatch_get_main_queue(), {
            completionHandler(self.currentLocation!)
        })
    }
}
