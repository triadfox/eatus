//
//  RootInterfaceController.swift
//  Eatus
//
//  Created by Phong Nguyen on 11/22/15.
//  Copyright © 2015 Triad Fox. All rights reserved.
//

import WatchKit
import Foundation

class RootInterfaceController: BaseInterfaceController {
    
    @IBOutlet var logoFoxxieImage: WKInterfaceImage!
    @IBOutlet var nearbyButton: WKInterfaceButton!
    @IBOutlet var trendingButton: WKInterfaceButton!
    
    override func awakeWithContext(context: AnyObject?) {
        super.awakeWithContext(context)
    }
    
    override func willActivate() {
        super.willActivate()
        
        self.nearbyButton.setBackgroundColor(colorMainThemeLightLv1)
        self.trendingButton.setBackgroundColor(colorMainTheme)
    }
    
    override func didDeactivate() {
        super.didDeactivate()
    }
    
    
    //#MARK: IBAction methods
    @IBAction func didTapOnNearbyButton() {
        self.presentControllerWithName("NearbyIndentifier", context: nil)
    }
    
    @IBAction func didTapOnTrendingButton() {
        
    }
}
