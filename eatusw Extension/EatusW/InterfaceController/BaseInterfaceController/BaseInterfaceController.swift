//
//  BaseInterfaceController.swift
//  Eatus
//
//  Created by Phong Nguyen on 11/21/15.
//  Copyright © 2015 Triad Fox. All rights reserved.
//

import WatchKit
import Foundation

class BaseInterfaceController: WKInterfaceController {
    var locationManager: LocationManager!
    var dataServiceManager: DataServiceManager!
    var connectivityManager: WatchConnectivityManager!
    
    override init() {
        super.init()
        locationManager = LocationManager.shareInstance
        dataServiceManager = DataServiceManager.shareInstance
        connectivityManager = WatchConnectivityManager.shareInstance
        
        locationManager.launchMonitorLocation()
        locationManager.parentInterface = self
        
        connectivityManager.launchConnectivitySession()
    }
    
    override func awakeWithContext(context: AnyObject?) {
        
    }
    
    override func willActivate() {
    
    }
    
    override func didDeactivate() {
        
    }
}