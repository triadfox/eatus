//
//  RecentlyInterfaceController.swift
//  Eatus
//
//  Created by Phong Nguyen on 11/23/15.
//  Copyright © 2015 Triad Fox. All rights reserved.
//

import WatchKit
import Foundation

class RecentlyInterfaceController: BaseInterfaceController{
    
    @IBOutlet var recentlyTableInterface: WKInterfaceTable!
    @IBOutlet var recentlySpacerGroup: WKInterfaceGroup!
    @IBOutlet var recentlyActivitiIndicator: WKInterfaceImage!
    @IBOutlet var recentlyMore: WKInterfaceButton!
    
    var skipRecord: Int = 0
    var didLoadOnActive: Bool?
    
    var allFeedback: [FeedbackRecently]?
    var temporaryFeedback: [FeedbackRecently]?
    
    override func awakeWithContext(context: AnyObject?) {
        super.awakeWithContext(context)
        didLoadOnActive = false
    }
    
    override func willActivate() {
        super.willActivate()
    }
    
    override func didAppear() {
        if didLoadOnActive == false{
            
            if self.connectivityManager.isPairedWithParentDevice() == false{
                let pairAlertAction = WKAlertAction(title: "OK", style: .Default, handler: {
                    
                })
                self.presentAlertControllerWithTitle("No Network", message: "Please enable network connection", preferredStyle: .Alert, actions: [pairAlertAction])
            }else{
                self.populateRecentlyTableInterface({
                    self.didLoadOnActive = true
                })
            }
        }
    }
    
    override func didDeactivate() {
        super.didDeactivate()
        self.stopAnimationActivityIndicator()
    }
    
    
    //#MARK: WKInterfaceTable
    //populate
    func populateRecentlyTableInterface(completionHandler: () -> Void){
        self.startAnimateActivityIndicator()
        self.dataServiceManager.getRecentlyFeedbackByRange(skipRecord, limit: 5, completionHandler: {(responseObject, error) -> Void in
            if let errorRet = error{
                print("Error while trying to get recently feedback. Error: \(errorRet.localizedDescription)")
            }else{
                if let objRet = responseObject{
                    
                    if self.allFeedback?.count == nil{
                        self.allFeedback = self.enumerateOnFeedbackRecentlyFetchFromBackEnd(objRet["data"] as? [AnyObject])
                    }else{
                        if let newData = self.enumerateOnFeedbackRecentlyFetchFromBackEnd(objRet["data"] as? [AnyObject]){
                            var shouldAppend = [FeedbackRecently]()
                            var didCompared = [FeedbackRecently]()
                            
                            for fetch in newData{
                                for feedback in self.allFeedback!{
                                    if fetch.feedbackDate?.compare(feedback.feedbackDate!) == .OrderedSame && fetch.feedbackOwner == feedback.feedbackOwner{
                                        didCompared.append(feedback)
                                        break
                                    }else{
                                        if didCompared.contains(feedback){
                                            continue
                                        }else{
                                            shouldAppend.append(fetch)
                                        }
                                    }
                                }
                            }
                            
                            if shouldAppend.count != 0{
                                self.allFeedback?.appendContentsOf(shouldAppend)
                            }
                        }
                    }
                    
                    if self.allFeedback != nil{
                        self.recentlyTableInterface.setNumberOfRows(self.allFeedback!.count, withRowType: "RecentlyRowIdentifier")
                    }
                    
                    for index in 0..<self.recentlyTableInterface.numberOfRows{
                        if let controller = self.recentlyTableInterface.rowControllerAtIndex(index) as? RecentlyRowController{
                            let feedbackForRow = self.allFeedback![index]
                            
                            controller.recentlyFeedback = feedbackForRow
                            
                            let separatorColor: UIColor!
                            if feedbackForRow.feedbackHappy{
                                separatorColor = colorMainThemeOpenTiming
                            }else{
                                separatorColor = colorMainThemeClosedTiming
                            }
                            
                            self.animateWithDuration(0.5, animations: {
                                controller.recentlySeparator.setColor(separatorColor)
                            })
                            
                            controller.recentlyFeedbackOwner.setText(feedbackForRow.feedbackOwner)
                            controller.recentlyFeedbackMoment.setText(downcastNSDateToMomentForDay(feedbackForRow.feedbackDate!))
                        }
                    }
                    
                    completionHandler()
                    
                    dispatch_async(dispatch_get_main_queue(), {
                        self.stopAnimationActivityIndicator()
                    })
                }
            }
        })
    }
    
    
    //#MARK: WKInterfaceTable Delegate method
    override func table(table: WKInterfaceTable, didSelectRowAtIndex rowIndex: Int) {
        if let controller = table.rowControllerAtIndex(rowIndex) as? RecentlyRowController{
            presentControllerWithName("RecentlyDetailIdentifier", context: controller.recentlyFeedback!)
        }
    }
    
    
    //#MARK: Private methods
    func enumerateOnFeedbackRecentlyFetchFromBackEnd(feedbacks: [AnyObject]?)->[FeedbackRecently]?{
        if feedbacks?.count == 0{
            return nil
        }
        
        var feedbacksReturn: [FeedbackRecently] = []
        
        for index in 0..<feedbacks!.count{
            let feedbackDict = feedbacks![index] as! [String:AnyObject]
            let feedbackEntity = FeedbackRecently(dictionary: feedbackDict)
            feedbacksReturn.append(feedbackEntity)
        }
        return feedbacksReturn
    }
    
    
    //#MARK: IBAction methods
    @IBAction func didTapOnMore() {
        skipRecord += 5
        self.populateRecentlyTableInterface({
            
        })
    }

    
    //#MARK: Activity Indicator
    func startAnimateActivityIndicator(){
        self.recentlyActivitiIndicator.setImageNamed("spinner")
        self.recentlyActivitiIndicator.startAnimatingWithImagesInRange(NSMakeRange(0, 41), duration: 1, repeatCount: 100)
    }
    
    func stopAnimationActivityIndicator(){
        self.animateWithDuration(0.5, animations: {
            self.recentlySpacerGroup.setHeight(0)
            self.recentlyActivitiIndicator.stopAnimating()
        })
    }
}