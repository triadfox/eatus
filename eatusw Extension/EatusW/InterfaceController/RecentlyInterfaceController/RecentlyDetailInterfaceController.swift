//
//  RecentlyDetailInterfaceController.swift
//  Eatus
//
//  Created by Phong Nguyen on 11/24/15.
//  Copyright © 2015 Triad Fox. All rights reserved.
//

import WatchKit
import Foundation

class RecentlyDetailInterfaceController: BaseInterfaceController {
    @IBOutlet var recentlyDetailFeedbackOwner: WKInterfaceLabel!
    @IBOutlet var recentlyDetailFeedbackTime: WKInterfaceLabel!
    @IBOutlet var recentlyDetailFeedbackBadge: WKInterfaceImage!
    @IBOutlet var recentlyDetailFeedbackOutletStore: WKInterfaceLabel!
    @IBOutlet var recentlyDetailFeedbackMap: WKInterfaceMap!
    @IBOutlet var recentlyDetailFeedbackDistance: WKInterfaceLabel!
    
    var feedbackRecently: FeedbackRecently?{
        didSet{
            self.recentlyDetailFeedbackOwner.setText(feedbackRecently!.feedbackOwner)
            self.recentlyDetailFeedbackTime.setText(downcastNSDateToMomentForDay(feedbackRecently!.feedbackDate!))
            
            if feedbackRecently!.feedbackHappy{
                self.recentlyDetailFeedbackBadge.setImageNamed("GoodQuality")
            }else{
                self.recentlyDetailFeedbackBadge.setImageNamed("BadQuality")
            }
            
            self.recentlyDetailFeedbackDistance.setTextColor(colorMainTheme)
            self.locationManager.distanceFromCurrentLocationTo(CLLocation(latitude: feedbackRecently!.latitude, longitude: feedbackRecently!.longitude), completionHandler: {(distance) -> Void in
                if distance < 1{
                    self.recentlyDetailFeedbackDistance.setText("About \(String(format: "%.1f", distance * 1000)) m from your current location")
                }else{
                    self.recentlyDetailFeedbackDistance.setText("About \(String(format: "%.2f", distance)) km from your current location")
                }
            })
            
            let momentumLocation = CLLocation(latitude: (feedbackRecently?.latitude)!, longitude: (feedbackRecently?.longitude)!)
            let momentumCoordinate = momentumLocation.coordinate
            let coordinateSpan = MKCoordinateSpanMake(0.005, 0.005)
            self.recentlyDetailFeedbackMap.setRegion(MKCoordinateRegionMake(momentumCoordinate, coordinateSpan))
            self.recentlyDetailFeedbackMap.addAnnotation(momentumCoordinate, withImageNamed: "OutletStoreColor", centerOffset: CGPointMake(1, -1))
        }
    }
    
    override func awakeWithContext(context: AnyObject?) {
        super.awakeWithContext(context)
        
        if let feedback = context as? FeedbackRecently{
            self.feedbackRecently = feedback
        }
    }
    
    override func willActivate() {
        super.willActivate()
    }
    
    override func didDeactivate() {
        super.didDeactivate()
    }
}