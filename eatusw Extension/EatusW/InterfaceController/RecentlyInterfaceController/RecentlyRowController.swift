//
//  RecentlyRowController.swift
//  Eatus
//
//  Created by Phong Nguyen on 11/23/15.
//  Copyright © 2015 Triad Fox. All rights reserved.
//

import WatchKit
import Foundation

class RecentlyRowController: NSObject {
    @IBOutlet var recentlySeparator: WKInterfaceSeparator!
    @IBOutlet var recentlyFeedbackOwner: WKInterfaceLabel!
    @IBOutlet var recentlyFeedbackMoment: WKInterfaceLabel!
    
    var recentlyFeedback: FeedbackRecently?
}