//
//  PromotionInterfaceController.swift
//  Eatus
//
//  Created by Phong Nguyen on 11/23/15.
//  Copyright © 2015 Triad Fox. All rights reserved.
//

import WatchKit
import Foundation

class PromotionInterfaceController: BaseInterfaceController{
    @IBOutlet var promotionTableInterface: WKInterfaceTable!
    @IBOutlet var promotionTopSpacerGroup: WKInterfaceGroup!
    @IBOutlet var promotionActivityIndicator: WKInterfaceImage!
    
    var allPromotions: [PromotionAvailable]?
    var didLoadOnActive: Bool?
    
    override func awakeWithContext(context: AnyObject?) {
        super.awakeWithContext(context)
        didLoadOnActive = false
    }
    
    override func willActivate() {
        super.willActivate()
    }
    
    override func didAppear() {
        if didLoadOnActive == false{
            
            if self.connectivityManager.isPairedWithParentDevice() == false{
                let pairAlertAction = WKAlertAction(title: "OK", style: .Default, handler: {
                    
                })
                self.presentAlertControllerWithTitle("No Network", message: "Please enable network connection", preferredStyle: .Alert, actions: [pairAlertAction])
            }else{
                self.populatePromotionInterfaceTable({
                    self.didLoadOnActive = true
                })
            }
        }
    }
    
    override func didDeactivate() {
        super.didDeactivate()
        self.stopAnimationActivityIndicator()
    }
    
    
    //#MARK: WKInterfaceTable Delegate
    override func table(table: WKInterfaceTable, didSelectRowAtIndex rowIndex: Int) {
        if let rowController = table.rowControllerAtIndex(rowIndex) as? PromotionRowController{
            self.presentControllerWithName("PromotionDetailIdentifier", context: rowController.promotionForRow)
        }
    }
    
    
    //#MARK: Private methods
    func populatePromotionInterfaceTable(completionHandler: () -> Void){
        
        self.startAnimateActivityIndicator()
        
        self.dataServiceManager.getAllAvailablePromotion(0, limit: 5, completionHandler: {(responseObject, error) -> Void in
            if let errorRet = error{
                print("Error while trying to get outlet. Error: \(errorRet.localizedDescription)")
            }else{
                if let objRet = responseObject{
                    self.allPromotions = self.enumerateOnPromotionFetchFromBackend(objRet["data"] as? [AnyObject])
                    
                    self.promotionTableInterface.setNumberOfRows(self.allPromotions!.count, withRowType: "PromotionRowIdentifier")
                    for index in 0..<self.allPromotions!.count{
                        if let controller = self.promotionTableInterface.rowControllerAtIndex(index) as? PromotionRowController{
                            let promotionForRow = self.allPromotions![index]
                            
                            controller.promotionForRow = promotionForRow
                            controller.promotionOutletStoreName.setText(promotionForRow.restaurantName)
                            
                            let complexity = determineExpiryStatus(promotionForRow.promotionEndDate, start: promotionForRow.promotionStartDate)
                            
                            controller.promotionOutletExpiryDate.setText(complexity.expiryStr)
                            controller.promotionSeparator.setColor(complexity.separatorColor)
                        }
                    }
                    
                    completionHandler()
                    self.stopAnimationActivityIndicator()
                }
            }
        })
    }
    
    func enumerateOnPromotionFetchFromBackend(promotions: [AnyObject]?) -> [PromotionAvailable]?{
        if promotions?.count == 0{
            return nil
        }
        
        var promotionsReturn: [PromotionAvailable] = []
        
        for index in 0..<promotions!.count{
            let promotionDict = promotions![index] as! [String:AnyObject]
            let promotionEntity = PromotionAvailable(dictionary: promotionDict)
            promotionsReturn.append(promotionEntity)
        }
        return promotionsReturn
    }
    
    
    //#MARK: Activity Indicator
    func startAnimateActivityIndicator(){
        self.promotionActivityIndicator.setImageNamed("spinner")
        self.promotionActivityIndicator.startAnimatingWithImagesInRange(NSMakeRange(0, 41), duration: 1, repeatCount: 100)
    }
    
    func stopAnimationActivityIndicator(){
        self.animateWithDuration(0.5, animations: {
            self.promotionTopSpacerGroup.setHeight(0)
            self.promotionActivityIndicator.stopAnimating()
        })
    }
}