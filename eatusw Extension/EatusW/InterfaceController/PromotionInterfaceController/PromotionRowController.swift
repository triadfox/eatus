//
//  PromotionRowController.swift
//  Eatus
//
//  Created by Phong Nguyen on 11/23/15.
//  Copyright © 2015 Triad Fox. All rights reserved.
//

import WatchKit
import Foundation

class PromotionRowController: NSObject {
    @IBOutlet var promotionSeparator: WKInterfaceSeparator!
    @IBOutlet var promotionOutletStoreName: WKInterfaceLabel!
    @IBOutlet var promotionOutletExpiryDate: WKInterfaceLabel!
    
    var promotionForRow: PromotionAvailable?
}