//
//  PromotionDetailInterfaceController.swift
//  Eatus
//
//  Created by Phong Nguyen on 11/24/15.
//  Copyright © 2015 Triad Fox. All rights reserved.
//

import WatchKit
import Foundation

class PromotionDetailInterfaceController: BaseInterfaceController {
    @IBOutlet var promotionDetailOutletName: WKInterfaceLabel!
    @IBOutlet var promotionDetailBadgeExpiry: WKInterfaceImage!
    @IBOutlet var promotionDetailDescription: WKInterfaceLabel!
    @IBOutlet var promotionExpiry: WKInterfaceLabel!
    @IBOutlet var promotionStart: WKInterfaceLabel!
    @IBOutlet var promotionExpired: WKInterfaceLabel!
    @IBOutlet var promotionDetailImage: WKInterfaceImage!
    
    var promotion: PromotionAvailable?{
        didSet{
            self.promotionDetailOutletName.setText(promotion!.restaurantName)
            self.promotionStart.setText("From: " + GTMTimeStrForUTCTimeWithoutHours(promotion!.promotionStartDate)!)
            self.promotionExpired.setText("To:   " + GTMTimeStrForUTCTimeWithoutHours(promotion!.promotionEndDate)!)
            
            let complexity = determineExpiryStatus(promotion!.promotionEndDate, start: promotion!.promotionStartDate)
            self.promotionExpiry.setText(complexity.expiryStr)
            self.promotionExpiry.setTextColor(complexity.separatorColor)
            self.promotionDetailDescription.setText(promotion!.promotionName)
        }
    }
    
    override func awakeWithContext(context: AnyObject?) {
        super.awakeWithContext(context)
        if let promotionPassing = context as? PromotionAvailable{
            self.promotion = promotionPassing
        }
        
        addMenuItemWithImageNamed("Guidance", title: "Guide Me To \(self.promotion!.restaurantName)", action: Selector("didTapOnGuidanceToRetaurant"))
    }
    
    override func willActivate() {
        super.willActivate()
    }
    
    override func didDeactivate() {
        super.didDeactivate()
    }
    
    
    //#MARK: Private methods
    func didTapOnGuidanceToRetaurant(){
        let restaurantLocation = CLLocation(latitude: self.promotion!.latitude, longitude: self.promotion!.longitude)
        self.presentControllerWithName("MapGuidanceIdentifier", context: restaurantLocation)
    }

}