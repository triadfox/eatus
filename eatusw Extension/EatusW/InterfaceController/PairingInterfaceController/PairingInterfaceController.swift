//
//  PairingInterfaceController.swift
//  Eatus
//
//  Created by Phong Nguyen on 11/24/15.
//  Copyright © 2015 Triad Fox. All rights reserved.
//

import WatchKit
import Foundation

class PairingInterfaceController: BaseInterfaceController {
    @IBOutlet var foxLogoInterfaceImage: WKInterfaceImage!
    @IBOutlet var pairingGuidance: WKInterfaceLabel!
    @IBOutlet var advantagesInterfaceButton: WKInterfaceButton!
    
    @IBOutlet var pairingCenterSpacerGroup: WKInterfaceGroup!
    @IBOutlet var pairingActivityIndicator: WKInterfaceImage!
    
    override func awakeWithContext(context: AnyObject?) {
        super.awakeWithContext(context)
    }
    
    override func willActivate() {
        super.willActivate()
    }
    
    override func didDeactivate() {
        super.didDeactivate()
    }
    
    
    //#MARK: IBAction methods
    @IBAction func didTapOnAdvantages(){
        if self.connectivityManager.isPairedWithParentDevice(){
            let delayMoment = dispatch_time(DISPATCH_TIME_NOW, Int64(4 * NSEC_PER_SEC))
            
            self.startAnimateActivityIndicator()
            dispatch_after(delayMoment, dispatch_get_main_queue(), {
                self.stopAnimationActivityIndicator()
                self.pushControllerWithName("ProfileIdentifier", context: nil)
            })
        }else{
            let pairingAlertAction = WKAlertAction(title: "OK", style: .Destructive, handler: {
                
            })
            self.presentAlertControllerWithTitle("Pairing", message: "Your watch haven't paired with iPhone. Let pair to discover us with advantages", preferredStyle: .Alert, actions: [pairingAlertAction])
        }
    }
    
    //#MARK: Activity Indicator
    func startAnimateActivityIndicator(){
        self.animateWithDuration(0.5, animations: {
            self.pairingCenterSpacerGroup.setWidth(self.contentFrame.size.width)
        })
        
        self.pairingActivityIndicator.setImageNamed("spinner")
        self.pairingActivityIndicator.startAnimatingWithImagesInRange(NSMakeRange(0, 41), duration: 1, repeatCount: 100)
    }
    
    func stopAnimationActivityIndicator(){
        self.animateWithDuration(0.5, animations: {
            self.pairingCenterSpacerGroup.setWidth(0)
            self.pairingActivityIndicator.stopAnimating()
        })
    }
}