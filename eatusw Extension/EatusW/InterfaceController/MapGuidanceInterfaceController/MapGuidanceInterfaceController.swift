//
//  MapGuidanceInterfaceController.swift
//  Eatus
//
//  Created by Phong Nguyen on 11/24/15.
//  Copyright © 2015 Triad Fox. All rights reserved.
//

import WatchKit
import Foundation

class MapGuidanceInterfaceController: BaseInterfaceController {
    @IBOutlet var guidanceMapInterface: WKInterfaceMap!
    @IBOutlet var guidanceDistance: WKInterfaceLabel!
    
    var didLocateOutletStore: Bool
    var guidanceGeopoint: CLLocation?{
        didSet{
            self.launchMapAsCurrentPosition(guidanceGeopoint!)
            self.guidanceDistance.setTextColor(colorMainTheme)
            self.locationManager.distanceFromCurrentLocationTo(guidanceGeopoint!, completionHandler: {(distance) -> Void in
                if distance < 1{
                    self.guidanceDistance.setText("About \(String(format: "%.1f", distance * 1000)) m from your current position")
                }else{
                    self.guidanceDistance.setText("About \(String(format: "%.2f", distance)) km from your current position")
                }
            })
        }
    }
    
    override init() {
        didLocateOutletStore = false
    }
    
    override func awakeWithContext(context: AnyObject?) {
        super.awakeWithContext(context)
        if let locationPoint = context as? CLLocation{
            self.guidanceGeopoint = locationPoint
        }
        
        addMenuItemWithImageNamed("LocateMe", title: "Locate Me", action: Selector("didTapOnLocateMeButton"))
    }
    
    override func willActivate() {
        super.willActivate()
    }
    
    override func didDeactivate() {
        super.didDeactivate()
    }
    
    
    //#MARK: WkInterfaceMap
    func launchMapAsCurrentPosition(momentumLocation: CLLocation){
        let momentumCoordinate = momentumLocation.coordinate
        let coordinateSpan = MKCoordinateSpanMake(0.005, 0.005)
        self.guidanceMapInterface.setRegion(MKCoordinateRegionMake(momentumCoordinate, coordinateSpan))
        self.guidanceMapInterface.addAnnotation(momentumCoordinate, withImageNamed: "OutletStoreColor", centerOffset: CGPointMake(1, -1))
        
        didLocateOutletStore = true
    }
    
    func didTapOnLocateMeButton() {
        if let momentumLocation = self.locationManager.currentLocationMomentum(){
            let momentumCoordinate = momentumLocation.coordinate
            let coordinateSpan = MKCoordinateSpanMake(1, 1)
            self.guidanceMapInterface.setRegion(MKCoordinateRegionMake(momentumCoordinate, coordinateSpan))
            self.guidanceMapInterface.addAnnotation(momentumCoordinate, withImageNamed: "PeopleMan", centerOffset: CGPointMake(10,-10))
        }
    }
    
}