//
//  AroundInterfaceController.swift
//  Eatus
//
//  Created by Phong Nguyen on 11/22/15.
//  Copyright © 2015 Triad Fox. All rights reserved.
//

import WatchKit
import Foundation

class AroundInterfaceController: BaseInterfaceController {
    
    @IBOutlet weak var aroundInterfaceMap: WKInterfaceMap!
    
    var didLocateCurrentPosition: Bool
    var allNearbyOutletStore: [OutletStore]?
    
    override init() {
        didLocateCurrentPosition = false
    }
    
    override func awakeWithContext(context: AnyObject?) {
        super.awakeWithContext(context)
    }
    
    override func willActivate() {
        super.willActivate()
        
        self.launchMapAsCurrentPosition({(currentLocation) -> Void in
            
            if self.connectivityManager.isPairedWithParentDevice() == false{
            }else{
                self.fetchNearbyOutletStoreByCurrentLocation(currentLocation, completionHandler: {(outlets, error) -> Void in
                    for outlet in outlets!{
                        let momentumCoordinate = outlet.outletLocation.coordinate
                        self.aroundInterfaceMap.addAnnotation(momentumCoordinate, withImageNamed: "OutletStoreColor", centerOffset: CGPointMake(1,-1))
                    }
                })
            }
        })
    }
    
    override func didDeactivate() {
        super.didDeactivate()
    }
    
    
    //#MARK: IBAction methods
    @IBAction func didTapOnLocateMeMenuItem() {
        if didLocateCurrentPosition{
            self.launchMapAsCurrentPosition({(currentLocation) -> Void in
                
            })
        }
    }

    @IBAction func didtapOnTheNearestMeMenuItem() {
        
    }
    
    //#MARK: Private methods
    //WKInterfaceMap (WKInterfaceMap is static, handle manually)
    func launchMapAsCurrentPosition(completionHandler: (currentLocation: CLLocation) -> Void){
        if let momentumLocation = self.locationManager?.currentLocationMomentum(){
            if didLocateCurrentPosition == false{
                let momentumCoordinate = momentumLocation.coordinate
                let coordinateSpan = MKCoordinateSpanMake(0.008, 0.008)
                self.aroundInterfaceMap.setRegion(MKCoordinateRegionMake(momentumCoordinate, coordinateSpan))
                self.aroundInterfaceMap.addAnnotation(momentumCoordinate, withImageNamed: "PeopleMan", centerOffset: CGPointMake(1,-1))
                
                didLocateCurrentPosition = true
                completionHandler(currentLocation: momentumLocation)
            }
        }
    }
    
    //get nearby outlet store
    func fetchNearbyOutletStoreByCurrentLocation(currentLocation: CLLocation?, completionHandler: ([OutletStore]?, NSError?) -> Void){
        self.dataServiceManager.outletStoreByNearestDistance((currentLocation?.coordinate.latitude)!, longitude: (currentLocation?.coordinate.longitude)!, skip: 0, limit: 5, completionHandler: {(responseObject, error) -> Void in
            if let errorRet = error{
                completionHandler(nil, errorRet)
            }else{
                if let objRet = responseObject{
                    let nearbyOutlets = self.enumerateOnOutletStoreFetchFromBackend(objRet["data"] as! [AnyObject])
                    completionHandler(nearbyOutlets, nil)
                }
            }
        })
    }
    
    func enumerateOnOutletStoreFetchFromBackend(outlets: [AnyObject]!)->[OutletStore]?{
        if outlets.count == 0{
            return nil
        }
        var outletsReturn: [OutletStore] = []
        
        for outlet in outlets{
            let outletDict = outlet as! [String:AnyObject]
            outletsReturn.append(OutletStore(dictionary: outletDict))
        }
        return outletsReturn
    }
}

