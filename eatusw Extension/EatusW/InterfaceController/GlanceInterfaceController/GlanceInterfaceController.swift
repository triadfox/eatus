//
//  GlanceInterfaceController.swift
//  Eatus
//
//  Created by Phong Nguyen on 11/29/15.
//  Copyright © 2015 Triad Fox. All rights reserved.
//

import WatchKit
import Foundation

class GlanceInterfaceController: BaseInterfaceController {
    @IBOutlet var foxxieImageInterface: WKInterfaceImage!
    @IBOutlet var totalSpentLabel: WKInterfaceLabel!
    @IBOutlet var receiptDetailLabel: WKInterfaceLabel!
    @IBOutlet var issuedDateLabel: WKInterfaceLabel!
    @IBOutlet var receiptIdLabel: WKInterfaceLabel!
    @IBOutlet var receiptRestaurant: WKInterfaceLabel!
    
    var lastReceipt: Receipt?{
        didSet{
            let totalWithCurrency = priceStrAsCurrencyAtLocaleByPrimitiveNumber((lastReceipt?.receiptLocale)!, number: lastReceipt!.receiptTotal)
            
            self.totalSpentLabel.setText(totalWithCurrency)
            self.issuedDateLabel.setText(downcastNSDateToMomentForDay(lastReceipt!.receiptIssuedDate))
            self.receiptDetailLabel.setText("Your Last Order: \n \n" + self.enumerateToBuildDishes((lastReceipt!.receiptDishes)!, locale: lastReceipt!.receiptLocale!))
            self.receiptIdLabel.setText(lastReceipt!.receiptId)
            self.receiptRestaurant.setText("At " + lastReceipt!.receiptRestaurant)
        }
    }
    
    override func awakeWithContext(context: AnyObject?) {
        super.awakeWithContext(context)
        
        self.receiptIdLabel.setTextColor(colorMainThemeLightLv1)
        self.totalSpentLabel.setTextColor(colorMainTheme)
        self.receiptRestaurant.setTextColor(colorMainThemeDark)
    }
    
    override func willActivate() {
        super.willActivate()
        
        if let lastReceiptTransferred = _UserDefault.objectForKey(receiptTransferringFromParentDevice) as? [String : AnyObject]{
            let receiptEntity = Receipt(dictionary: lastReceiptTransferred)
            self.lastReceipt = receiptEntity
            self.receiptDetailLabel.setTextColor(UIColor.whiteColor())
        }else{
            self.totalSpentLabel.setText("")
            self.receiptDetailLabel.setText("No recent order!")
            self.issuedDateLabel.setText("Don't be waited")
            self.receiptIdLabel.setText("")
            self.receiptRestaurant.setText("")
            
            self.receiptDetailLabel.setTextColor(colorMainThemeClosedTiming)
        }
    }
    
    override func didDeactivate() {
        super.didDeactivate()
    }
    
    
    //#MARK: Private methods
    func enumerateToBuildDishes(allDishes: [String:Double], locale: NSLocale) -> String{
        var dishesBuilder: String = ""
        var notFirstDish: Bool = false
        
        for (dishName, _) in allDishes{
            if notFirstDish{
                dishesBuilder.appendContentsOf("\n" + "\u{25AB}" + dishName)
            }else{
                dishesBuilder.appendContentsOf("\u{25AB}" + dishName)
                notFirstDish = true
            }
        }
        return dishesBuilder
    }
}