//
//  InterfaceController.swift
//  eatusw Extension
//
//  Created by Phong Nguyen on 11/20/15.
//  Copyright © 2015 Triad Fox. All rights reserved.
//

import WatchKit
import Foundation

class NearbyInterfaceController: BaseInterfaceController {

    @IBOutlet var nearbyInterfaceTable: WKInterfaceTable!
    @IBOutlet var nearbySpacerGroup: WKInterfaceGroup!
    @IBOutlet var nearbyActivityIndicator: WKInterfaceImage!
    @IBOutlet var nearbyDetailTitleLabel: WKInterfaceLabel!
    
    var allOutlets: [OutletStore]?
    
    override init() {
        super.init()
    }
    
    override func awakeWithContext(context: AnyObject?) {
        super.awakeWithContext(context)
        
        if self.connectivityManager.isPairedWithParentDevice() == false{
            let pairAlertAction = WKAlertAction(title: "OK", style: .Default, handler: {
                
            })
            
            self.presentAlertControllerWithTitle("No Network", message: "Please enable network connection", preferredStyle: .Alert, actions: [pairAlertAction])
        }else{
            self.populateNearbyInterfaceTable()
        }
    }

    override func willActivate() {
        super.willActivate()
    }

    override func didDeactivate() {
        super.didDeactivate()
    }
    
    //#MARK: WKTableInterface delegate
    override func table(table: WKInterfaceTable, didSelectRowAtIndex rowIndex: Int) {
        //launch outlet store detail
        if let outletForSelectedRow = self.allOutlets?[rowIndex] {
            presentControllerWithName("NearbyDetailIdentifier", context: outletForSelectedRow)
        }
    }
    
    //#MARK: Private methods
    
    func enumerateOnOutletStoreFetchFromBackend(outlets: [AnyObject]!)->[OutletStore]?{
        if outlets.count == 0{
            return nil
        }
        var outletsReturn: [OutletStore] = []
        
        for outlet in outlets{
            let outletDict = outlet as! [String:AnyObject]
            outletsReturn.append(OutletStore(dictionary: outletDict))
        }
        return outletsReturn
    }
    
    
    //#MARK: Activity Indicator
    func startAnimateActivityIndicator(){
        self.nearbyActivityIndicator.setImageNamed("spinner")
        self.nearbyActivityIndicator.startAnimatingWithImagesInRange(NSMakeRange(0, 41), duration: 1, repeatCount: 100)
    }
    
    func stopAnimationActivityIndicator(){
        self.animateWithDuration(0.5, animations: {
            self.nearbySpacerGroup.setHeight(0)
            self.nearbyActivityIndicator.stopAnimating()
        })
    }
    
    func populateNearbyInterfaceTable(){
        
        self.fetchOutletStoreData({()->Void in
            self.nearbyInterfaceTable.setNumberOfRows(self.allOutlets!.count, withRowType: "NearbyRowIdentifier")
            
            for index in 0..<self.nearbyInterfaceTable.numberOfRows{
                if let controller = self.nearbyInterfaceTable.rowControllerAtIndex(index) as? NearbyRowController{
                    
                    let outletForRow: OutletStore = self.allOutlets![index]
                    
                    controller.nearbyStoreNameLabel.setText(outletForRow.outletName)
                    
                    let outletSeparatorColor: UIColor?
                    
                    if outletForRow.storeWhetherOpenClosed{
                        outletSeparatorColor = colorMainThemeOpenTiming
                    }else{
                        outletSeparatorColor = colorMainThemeClosedTiming
                    }
                    
                    self.animateWithDuration(0.5, animations: {
                        controller.nearbyStoreSeparator.setColor(outletSeparatorColor)
                    })
                    
                    controller.nearbyStoreDistance.setText("Calculating...")
                    self.locationManager.distanceFromCurrentLocationTo(outletForRow.outletLocation, completionHandler: {(distance) -> Void in
                        
                        if distance < 1{
                            controller.nearbyStoreDistance.setText("\(String(format: "%.1f", distance * 1000)) m")
                        }else{
                            controller.nearbyStoreDistance.setText("\(String(format: "%.2f", distance)) km")
                        }
                    })
                }
            }
        })
    }
    
    func fetchOutletStoreData(completionHandler: () -> Void){
        self.startAnimateActivityIndicator()
        
        self.dataServiceManager.getAllOutletStoreFromBackEnd({(retDict, error) -> Void in
            if let objRet = retDict{
                self.allOutlets = self.enumerateOnOutletStoreFetchFromBackend(objRet["data"] as! [AnyObject])
            }
            
            let enumerateGroup: dispatch_group_t = dispatch_group_create()
            
            dispatch_group_enter(enumerateGroup)
            for outlet in self.allOutlets!{
                self.locationManager.distanceFromCurrentLocationTo(outlet.outletLocation, completionHandler: {(distance) -> Void in
                    outlet.distance = distance
                    let indexOutlet = self.allOutlets!.indexOf({
                        $0 === outlet
                    })
                    
                    if indexOutlet == self.allOutlets!.count - 1{
                        dispatch_group_leave(enumerateGroup)
                    }
                })
            }
            
            //sort all outlets by ascending distance
            dispatch_group_notify(enumerateGroup, dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0), {
                self.allOutlets!.sortInPlace({
                    $0.distance < $1.distance
                })
                
                completionHandler()
            })
            
            self.stopAnimationActivityIndicator()
        })
    }

}
