//
//  NearbyRowController.swift
//  Eatus
//
//  Created by Phong Nguyen on 11/21/15.
//  Copyright © 2015 Triad Fox. All rights reserved.
//

import Foundation
import WatchKit

class NearbyRowController: NSObject {
    @IBOutlet var nearbyStoreNameLabel: WKInterfaceLabel!
    @IBOutlet var nearbyStoreSeparator: WKInterfaceSeparator!
    @IBOutlet var nearbyStoreDistance: WKInterfaceLabel!

}