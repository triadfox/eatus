//
//  NearbyDetailInterfaceController.swift
//  Eatus
//
//  Created by Phong Nguyen on 11/22/15.
//  Copyright © 2015 Triad Fox. All rights reserved.
//

import WatchKit
import Foundation

let defaultNumberOfRowOutletStoreDetail: Int = 3

class NearbyDetailInfaceController: BaseInterfaceController{
    
    @IBOutlet var nearbyDetailTopSpacerGroup: WKInterfaceGroup!
    @IBOutlet var nearbyDetailLogoImage: WKInterfaceImage!
    @IBOutlet var nearbyDetailOutletName: WKInterfaceLabel!
    @IBOutlet var nearbyDetailTableInterface: WKInterfaceTable!
    
    var outletStore: OutletStore?{
        didSet{
            if let outletStoreDetail = outletStore{
                self.setTitle(outletStoreDetail.outletName)
                self.nearbyDetailOutletName.setText(outletStoreDetail.outletName)
                
                dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0), {
                    if let logoUrl = NSURL(string: outletStoreDetail.logoFileUrl){
                        if let logoData = NSData(contentsOfURL: logoUrl){
                            
                            let delay = dispatch_time(DISPATCH_TIME_NOW, Int64(0.5 * Double(NSEC_PER_SEC)))
                            dispatch_after(delay, dispatch_get_main_queue(), {
                                self.animateWithDuration(0.5, animations: {
                                    self.nearbyDetailTopSpacerGroup .setHeight(0)
                                    })
                                self.nearbyDetailLogoImage.setImage(UIImage(data: logoData))
                            })
                        }
                    }
                })
            }
        }
    }
    
    override func awakeWithContext(context: AnyObject?) {
        super.awakeWithContext(context)
        
        if let outletStoreDetail = context as? OutletStore{
            self.outletStore = outletStoreDetail
        }
        
        addMenuItemWithImageNamed("MenuBook", title: "View Menu", action: Selector("didTapOnViewMenu"))
        addMenuItemWithImageNamed("Guidance", title: "Guide Me To \(self.outletStore!.outletName)", action: Selector("didtapOnGuideWay"))
    }
    
    override func willActivate() {
        super.willActivate()
        
        self.populateNearbyDetailTable()
    }
    
    override func didDeactivate() {
        super.didDeactivate()
    }
    
    
    //#MARK: WKInterfaceTable
    func populateNearbyDetailTable(){
        nearbyDetailTableInterface.setNumberOfRows(defaultNumberOfRowOutletStoreDetail, withRowType: "NearbyDetailInformationRowIdentifier")
        
        for index in 0...2{
            
            if let controllerInformation = nearbyDetailTableInterface.rowControllerAtIndex(index) as? NearbyDetailInformationRowController {
                
                var rowTitleBuilder: String?
                var rowImageBuilder: UIImage?
                
                switch index{
                case 0:
                    if self.outletStore!.storeWhetherOpenClosed{
                        rowTitleBuilder = "Opend Now"
                        controllerInformation.nearbyDetailInformationLabel.setTextColor(colorMainThemeOpenTiming)
                    }else{
                        rowTitleBuilder = "Closed"
                        controllerInformation.nearbyDetailInformationLabel.setTextColor(colorMainThemeClosedTiming)
                    }
                    
                case 1:
                    //row 2: address (info)
                    rowTitleBuilder = self.outletStore!.outletAddress //change later
                    rowImageBuilder = UIImage(named: "Pin")!
                    
                case 2:
                    //row 3: rating (info)
                    rowTitleBuilder = self.outletStore!.servedTime
                    rowImageBuilder = UIImage(named: "ServeTime")
                    
                default:
                    break
                }
                
                controllerInformation.nearbyDetailInformationLabel.setText(rowTitleBuilder)
                controllerInformation.nearbyDeailInformatinImage.setImage(rowImageBuilder)
            }
        }
    }
    
    
    //#MARK: IBOutlet methods
    func didTapOnViewMenu() {
        if self.connectivityManager.isPairedWithParentDevice(){
            let syncDataAlertAction = WKAlertAction(title: "Sync", style: .Destructive, handler: {
                
            })
            
            let cancelDataAlertAction = WKAlertAction(title: "Cancel", style: .Cancel, handler: {
                
            })
            
            self.presentAlertControllerWithTitle("Pairing", message: "Watch will synchronize data from your iPhone now to collect menu information!", preferredStyle: .ActionSheet, actions: [syncDataAlertAction, cancelDataAlertAction])
        }else{
            
        }
    }
    
    func didtapOnGuideWay() {
        self.locationManager.launchMonitorLocation()
        self.presentControllerWithName("MapGuidanceIdentifier", context: self.outletStore!.outletLocation)
    }
}