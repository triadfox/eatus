//
//  NearbyDetailRowController.swift
//  Eatus
//
//  Created by Phong Nguyen on 11/22/15.
//  Copyright © 2015 Triad Fox. All rights reserved.
//

import WatchKit
import Foundation

class NearbyDetailInformationRowController: NSObject {
    @IBOutlet var nearbyDetailInformationLabel: WKInterfaceLabel!
    @IBOutlet var nearbyDeailInformatinImage: WKInterfaceImage!
}