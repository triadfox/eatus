//
//  AppGroupSharingManager.swift
//  Eatus
//
//  Created by Phong Nguyen on 11/21/15.
//  Copyright © 2015 Triad Fox. All rights reserved.
//

import Foundation

let userDefault = NSUserDefaults.init(suiteName: "group.eatus.defaults")!

func shareObject(shareObject: AnyObject!, _ key: NSString!)->Bool{
    userDefault.setObject(shareObject, forKey: key as String)
    return userDefault.synchronize()
}

func retrieveSharingObjectForKey(key: NSString?)->AnyObject?{
    let retrieval: AnyObject?
    retrieval = userDefault.objectForKey(key as! String)
    return retrieval
}

func removeSharingObjectForKey(key: NSString?){
    userDefault .removeObjectForKey(key as! String)
}
