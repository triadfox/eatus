//
//  Util.swift
//  Eatus
//
//  Created by Phong Nguyen on 11/21/15.
//  Copyright © 2015 Triad Fox. All rights reserved.
//

import Foundation
import WatchKit

func colorWithHexString(hexString: String!) ->UIColor{
    var cString:String = hexString.stringByTrimmingCharactersInSet(NSCharacterSet.whitespaceAndNewlineCharacterSet()).uppercaseString
    
    if (cString.hasPrefix("#")) {
        cString = (cString as NSString).substringFromIndex(1)
    }
    
    if (cString.characters.count != 6) {
        return UIColor.grayColor()
    }
    
    let rString = (cString as NSString).substringToIndex(2)
    let gString = ((cString as NSString).substringFromIndex(2) as NSString).substringToIndex(2)
    let bString = ((cString as NSString).substringFromIndex(4) as NSString).substringToIndex(2)
    
    var r:CUnsignedInt = 0, g:CUnsignedInt = 0, b:CUnsignedInt = 0;
    NSScanner(string: rString).scanHexInt(&r)
    NSScanner(string: gString).scanHexInt(&g)
    NSScanner(string: bString).scanHexInt(&b)
    
    
    return UIColor(red: CGFloat(r) / 255.0, green: CGFloat(g) / 255.0, blue: CGFloat(b) / 255.0, alpha: CGFloat(1))
}

func colorFromRGB(rgb: Int, alpha: Float) ->UIColor {
    let red = CGFloat(Float(((rgb>>16) & 0xFF)) / 255.0)
    let green = CGFloat(Float(((rgb>>8) & 0xFF)) / 255.0)
    let blue = CGFloat(Float(((rgb>>0) & 0xFF)) / 255.0)
    let alpha = CGFloat(alpha)
    
    return UIColor(red: red, green: green, blue: blue, alpha: alpha)
}


//#MARK: Date Time Manipulation
func dayTimeInDayForAllTotalSecond(totalSecs: Int)->NSDate{
    let minutes: Int = (totalSecs/60) % 60
    let hours: Int = totalSecs / 3600
    let complexity: String = "\(hours):\(minutes)"
    return dayTimeFromMinutesAndHours(complexity)
}

func dayTimeFromMinutesAndHours(complex: String) -> NSDate{
    let complexity = complex.componentsSeparatedByString(":")
    let calendar: NSCalendar = NSCalendar.currentCalendar()
    calendar.locale = NSLocale.currentLocale()
    
    let components = calendar.components([.Year, .Month, .Day], fromDate: NSDate())
    components.setValue(Int(complexity.first!)!, forComponent: .Hour)
    components.setValue(Int(complexity.last!)!, forComponent: .Minute)
    
    return calendar.dateFromComponents(components)!
}

func dateForStringAtCurrentLocale(dateStr: String!, format: String?) -> NSDate{
    let dateFormatter = NSDateFormatter()
    if format == nil{
        dateFormatter.dateFormat = "HH:mm MMMM dd yyyy"
    }else{
        dateFormatter.dateFormat = format
    }
    
    dateFormatter.locale = NSLocale.currentLocale()
    return dateFormatter.dateFromString(dateStr)!
}

func dateForStringAtCurrentLocaleUTC(dateStr: String!, format: String?) -> NSDate?{
    let dateFormatter = NSDateFormatter()
    if format == nil{
        dateFormatter.dateFormat = "EEE, MMM d, yyyy - HH:mm"
    }else{
        dateFormatter.dateFormat = format
    }
    
    dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss"
    dateFormatter.timeZone = NSTimeZone.localTimeZone()
    
    let dateNeeded = dateStr.componentsSeparatedByString(".").first!;
    return dateFormatter.dateFromString(dateNeeded)
}

func GMTTimeStrForUTCTime(utcTime: NSDate!) -> String?{
    let dateFormatter = NSDateFormatter()
    dateFormatter.dateFormat = "EEE, MMM dd, yyyy - HH:mm"
    
    let timeZone = NSTimeZone(abbreviation: "GMT")
    dateFormatter.timeZone = timeZone
    return dateFormatter .stringFromDate(utcTime)
}

func GTMTimeStrForUTCTimeWithoutHours(utcTime: NSDate!) ->String?{
    let dateFormatter = NSDateFormatter()
    dateFormatter.dateFormat = "MMM dd, yyyy"
    
    let timeZone = NSTimeZone(abbreviation: "GMT")
    dateFormatter.timeZone = timeZone
    return dateFormatter.stringFromDate(utcTime)
}

func GMTTimeStrForUTCTimeWithouthYear(utc: NSDate!) -> String?{
    let dateFormatter = NSDateFormatter()
    dateFormatter.dateFormat = "MMM dd"
    
    let timeZone = NSTimeZone(abbreviation: "GMT")
    dateFormatter.timeZone = timeZone
    return dateFormatter.stringFromDate(utc)
}

func downcastNSDateToMomentForDay(date: NSDate) -> String{
    let calendar = NSCalendar.currentCalendar()
    calendar.locale = NSLocale.currentLocale()
    
    let componentsToday = calendar.components([.Year, .Month, .Day, .Hour, .Minute, .Second], fromDate: NSDate())
    let todayYear = componentsToday.year
    let todayMonth = componentsToday.month
    let todayDate = componentsToday.day
    let todayHour = componentsToday.hour
    let todayMin = componentsToday.minute
    
    let componentsDate = calendar.components([.Year, .Month, .Day, .Hour, .Minute], fromDate: date)
    let dateYear = componentsDate.year
    let dateMonth = componentsDate.month
    let dateDay = componentsDate.day
    let dateHour = componentsDate.hour
    let dateMin = componentsDate.minute
    
    let dateStr: String = GTMTimeStrForUTCTimeWithoutHours(date)!
    
    if(todayYear == dateYear){
        if(todayMonth == dateMonth){
            if(todayDate == dateDay){
                if(todayHour == dateHour){
                    if(todayMin == dateMin){
                        return "a couple secs ago"
                    }else{
                        return "\(todayMin - dateMin) mins ago"
                    }
                }else{
                    return "\(todayHour - dateHour) hours ago"
                }
            }else{
                if(todayDate - dateDay == 1){
                    return "a day ago"
                }
                return "\(dateStr)"
            }
        }else{
            return "\(GMTTimeStrForUTCTimeWithouthYear(date)!)"
        }
    }else{
        return "\(dateStr)"
    }
}

func determineExpiryStatus(expiry: NSDate, start: NSDate) -> (separatorColor: UIColor, expiryStr: String, expired: Bool){
    let calendar = NSCalendar.currentCalendar()
    calendar.timeZone = NSTimeZone.localTimeZone()
    calendar.locale = NSLocale.currentLocale()
    
    let todayComponents = calendar.components([.Year, .Month, .Day, .Hour], fromDate: NSDate())
    let expiryComponents = calendar.components([.Year, .Month, .Day, .Hour], fromDate: expiry)
    
    let todayYear = todayComponents.year
    let todayMonth = todayComponents.month
    let todayDate = todayComponents.day
    let todayHour = todayComponents.hour
    let todayMin = todayComponents.minute
    
    let expiryYear = expiryComponents.year
    let expiryMonth = expiryComponents.month
    let expiryDate = expiryComponents.day
    let expiryHour = expiryComponents.hour
    let expiryMin = expiryComponents.minute
    
    let dateStr: String! = GTMTimeStrForUTCTimeWithoutHours(expiry)
    var expiryDateStr: String!
    var separatorColor: UIColor!
    var expired: Bool!
    
    if !didPromotionEnd(start, endDate: expiry){
        separatorColor = colorMainThemeClosedTiming
        expiryDateStr = "promotion end"
        expired = true
    }else{
        if(todayYear == expiryYear){
            if(todayMonth == expiryMonth){
                if(todayDate == expiryDate){
                    if(todayHour == expiryHour){
                        if(todayMin != expiryMin){
                            expiryDateStr = "end in \(abs(expiryMin - todayMin)) mins"
                            separatorColor = colorMainThemeClosedTiming
                        }
                    }else{
                        expiryDateStr = "end in \(abs(expiryHour - todayHour)) hours"
                        separatorColor = colorMainThemeClosedTiming
                    }
                }else{
                    expiryDateStr = "end in \(abs(expiryDate - todayDate)) days"
                    separatorColor = colorMainThemeOpenTiming
                }
            }else{
                expiryDateStr = "end in \(abs(expiryMonth - todayMonth)) months"
                separatorColor = colorMainThemeOpenTiming
            }
        }else{
            expiryDateStr = "end on" + dateStr
            separatorColor = colorMainTheme
        }
        
    }
    
    return (separatorColor, expiryDateStr, expired)
}

func didPromotionEnd(startDate: NSDate, endDate: NSDate) -> Bool{
    var now = NSDate()
    let sourceTimeZone = NSTimeZone(abbreviation: "GMT")!
    let destinationTimeZone = NSTimeZone.systemTimeZone()
    
    let sourceGTMOffset = sourceTimeZone.secondsFromGMTForDate(now)
    let destinationGTMOffset = destinationTimeZone.secondsFromGMTForDate(now)
    let timeInterval: Double = Double(destinationGTMOffset - sourceGTMOffset)
    
    now = NSDate(timeInterval: timeInterval, sinceDate: now)
    let startDateGMT = NSDate(timeInterval: timeInterval, sinceDate: startDate)
    let endDateGMT = NSDate(timeInterval: timeInterval, sinceDate: endDate)
    
    if now.compare(startDateGMT) == .OrderedDescending && now.compare(endDateGMT) == .OrderedAscending{
        return true
    }
    return false
}

func priceStrAsCurrencyAtLocale(locale: NSLocale, number: NSNumber) -> String{
    let numberFormmater = NSNumberFormatter()
    numberFormmater.locale = locale
    numberFormmater.numberStyle = NSNumberFormatterStyle.CurrencyStyle
    numberFormmater.usesGroupingSeparator = true
    return numberFormmater.stringFromNumber(number)!
}

func priceStrAsCurrencyAtLocaleByPrimitiveNumber(locale: NSLocale, number: Double) -> String{
    return priceStrAsCurrencyAtLocale(locale, number: NSNumber(double: number))
}

func localeByCurrencyMark(currency: String) -> NSLocale?{
    switch currency{
        case "VND":
            return NSLocale(localeIdentifier: vietnamLocaleCode)
        case "USD":
            return NSLocale(localeIdentifier: usLocaleCode)
        
    default:
        break
    }
    return nil
}





