//
//  Constant.swift
//  Eatus
//
//  Created by Phong Nguyen on 11/21/15.
//  Copyright © 2015 Triad Fox. All rights reserved.
//

import Foundation
import UIKit
import WatchKit
import WatchConnectivity


//#MARK: ENUMs

enum WatchTransferType: Int{
    case kWatchInteractiveMessage
    case kWatchBackgroundTransfer
}


//#MARK: API
let baseURL: String =                                       "http://peentri-lightorder.herokuapp.com"

let apiGetAllOutletStore: String =                          "/api/restaurantGetAll"
let apiGetOutletByNearestDistance: String =                 "/api/restaurantGetNear"
let apiGetRecentFeedbackByRange: String =                   "/api/feedbackGetAll"
let apiGetAvailablePromotion: String =                      "/api/promotionGetAll"


//#MARK: USER DEFAULT
let _UserDefault: NSUserDefaults =                           NSUserDefaults.standardUserDefaults()
let receiptTransferringFromParentDevice: String =            "receiptTransferringFromDevice"
let feedbackTransferringFromParentDevice: String =           "feedbackTransferringFromDevice"


//#MARK: NOTIFICATION
let _NotificationCenter: NSNotificationCenter =              NSNotificationCenter.defaultCenter()


//#MARK: SEGUE IDENTIFIER
//from RootInterfaceController
let segueFromRootToNearby: String =                         "SEGUE_FROM_ROOT_TO_NEARBY"
let segueFromRootToTrending: String =                       "SEGUE_FROM_ROOT_TO_TRENDING"

//from NearbyInterfaceController
let segueFromOutletStoreToOutletDetail:String =             "SEGUE_FROM_OUTLET_STORE_OUTLET_DETAIL"


//#MARK: Locale & Currency
let vietnamLocaleCode: String =                             "vi_VN"
let usLocaleCode: String =                                  "en_US"

let vietnamDongCurrency: String =                           "VND"
let usDollarCurrency: String =                              "USD"
let europeanCurrency: String =                              "EUR"


//#MARK: Them & Colour
let mainThemeColorLightLv1: String = "ff9000"
let mainThemeColor: String = "ff7b00"
let mainThemeColorDark: String = "ff6500"

let mainThemeColorOpenTiming: String = "00e600"
let mainThemeColorClosedTiming: String = "e60000"
let mainThemeColorDeepBlue: String = "1e90ff"

let mainThemeColorSilver: String = "a3a3a3"
let mainThemeColorSilverDark: String = "9b9b9b"


var colorMainThemeLightLv1: UIColor!{
    get{
        return colorWithHexString(mainThemeColorLightLv1)
    }
}

var colorMainTheme: UIColor!{
    get{
        return colorWithHexString(mainThemeColor)
    }
}

var colorMainThemeDark: UIColor!{
    get{
        return colorWithHexString(mainThemeColorDark)
    }
}

var colorMainThemeOpenTiming: UIColor!{
    get{
        return colorWithHexString(mainThemeColorOpenTiming)
    }
}

var colorMainThemeClosedTiming: UIColor!{
    get{
        return colorWithHexString(mainThemeColorClosedTiming)
    }
}

var colorMainThemeDeepBlue: UIColor!{
    get{
        return colorWithHexString(mainThemeColorDeepBlue)
    }
}

var colorMainThemeSilver: UIColor!{
    get{
        return colorWithHexString(mainThemeColorSilver)
    }
}

var colorMainThemeSilverDark: UIColor!{
    get{
        return colorWithHexString(mainThemeColorSilverDark)
    }
}
