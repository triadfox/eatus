//
//  main.m
//  Example
//
//  Created by Phong Nguyen on 8/12/13.
//  Copyright Triad Fox 2013. All rights reserved.
//

#import <UIKit/UIKit.h>

int main(int argc, char *argv[]) {
    
    @autoreleasepool {
        int retVal = UIApplicationMain(argc, argv, nil, nil);
        return retVal;
    }
}
