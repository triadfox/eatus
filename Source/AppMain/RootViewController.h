//
//  RootViewController.h
//  MiPoBeacon
//
//  Created by Phong Nguyen on 9/1/15.
//  Copyright (c) 2015 Triad Fox. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "NavBarController.h"
#import "MainOrderViewController.h"
#import "UIBarButtonItem+Badge.h"
#import "UIButton+Badge.h"
#import "UIButton+EdgeInset.h"

@interface RootViewController : UIViewController

@property(nonatomic, strong) NavBarController *navbarController;

@property(nonatomic, weak) IBOutlet UIButton *loginWithFacebook;
@property(nonatomic, weak) IBOutlet UIButton *continueWithoutLogin;
@property(nonatomic, strong) IBOutlet UIImageView *triadfoxImageView;
@property(nonatomic, strong) IBOutlet UILabel *eatusLoganTitle;

- (IBAction)loginWithFacebook:(UIButton *)sender;
- (IBAction)continueWithoutLogin:(id)sender;

- (IBAction)animateFoxxieLogo:(id)sender;
@end
