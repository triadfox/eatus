/*
 * Triad Fox 2015
 *
 * Copyright (c) Triad Fox.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

#import "AppDelegate.h"
#import "Util.h"
#import "ParseModel.h"
#import "MenuSideViewController.h"
#import "PaymentManager.h"
#import "WatchConnectivityManager.h"
#import "MenuJsonSerializerConstant.h"

#import "OrderPlacingViewController.h"

#import "UIFont+SystemFontOverride.h"

#import "RKDropdownAlert.h"
#import "iRate.h"

#import <Parse/Parse.h>
#import <FBSDKCoreKit/FBSDKCoreKit.h>
#import <ParseFacebookUtilsV4/PFFacebookUtils.h>

@implementation AppDelegate
@synthesize window, viewController, revealViewController;

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    //trigger notification when app is killed and background
    //beacon notifier
    if([launchOptions objectForKey:UIApplicationLaunchOptionsLocationKey]){
        [[KontaktBeaconServiceManager shareBeacon] launchKontaktBeaconMonitoring];
    }
    
    //stripe registration
    [PaymentManager registerStripeTest];
    
    //first time use app handler
    [self firstTimeUseAppHandler];
    
    //Parse
    [self integrateParseWithOptions:launchOptions];
    
    [self registerPushNotification:application];
    
    [[FBSDKApplicationDelegate sharedInstance] application:application didFinishLaunchingWithOptions:launchOptions];
    
    //Screen bounds settings
    CGRect screenBounds = [[UIScreen mainScreen] bounds];
#if __has_feature(objc_arc)
        self.window = [[UIWindow alloc] initWithFrame:screenBounds];
#else
        self.window = [[[UIWindow alloc] initWithFrame:screenBounds] autorelease];
#endif
    self.window.autoresizesSubviews = YES;
    
    [self initViewControllers];
    
    if ([User currentUser]) {
        self.window.rootViewController = self.revealViewController;
    } else {
        self.window.rootViewController = self.viewController;
    }
    
    [self.window makeKeyAndVisible];
    
    return YES;
}

-(void) applicationDidEnterBackground:(UIApplication*)application {
    
}

-(void) applicationWillEnterForeground:(UIApplication*)application {
    
}

- (void)applicationWillTerminate:(UIApplication *)application {
    
}

- (void)applicationDidBecomeActive:(UIApplication *)application {
    [FBSDKAppEvents activateApp];
    
    [_NotificationCenter addObserver:self selector:@selector(initViewControllers) name:kNotificationSignOut object:nil];
}

- (void)application:(UIApplication *)app didRegisterForRemoteNotificationsWithDeviceToken:(NSData *)deviceToken {
    NSString *token = [[deviceToken description] stringByTrimmingCharactersInSet: [NSCharacterSet characterSetWithCharactersInString:@"<>"]];
    token = [token stringByReplacingOccurrencesOfString:@" " withString:@""];
    
    [UserDefaultManager setDeviceTokenInNSData:deviceToken];
    [UserDefaultManager setDeviceToken:token];
    
    PFInstallation *currentInstallation = [PFInstallation currentInstallation];
    [currentInstallation setDeviceTokenFromData:deviceToken];
    currentInstallation.channels = @[@"global"];
    
    if ([User currentUser]) {
        currentInstallation[@"user"] = [User currentUser];
    }
    
    [currentInstallation saveInBackground];
    
    [PFPush subscribeToChannelInBackground:@"" block:^(BOOL succeeded, NSError *error) {
        if (succeeded) {
            NSLog(@"Successfully subscribed to push notifications on the broadcast channel.");
        } else {
            NSLog(@"Failed to subscribe to push notifications on the broadcast channel.");
        }
    }];
}

- (void)application:(UIApplication *)application didFailToRegisterForRemoteNotificationsWithError:(NSError *)error {
    if(error){
        NSLog(@"Did fail to register for remote notification with errror: %@", error.localizedDescription);
    }
}

- (BOOL)application:(UIApplication *)application openURL:(NSURL *)url sourceApplication:(NSString *)sourceApplication annotation:(id)annotation {
    return [[FBSDKApplicationDelegate sharedInstance] application:application openURL:url sourceApplication:sourceApplication annotation:annotation ];
}

- (void)application:(UIApplication *)application didReceiveLocalNotification:(UILocalNotification *)notification{
    UIApplicationState state = [application applicationState];
    NSDictionary *userInfo = notification.userInfo;
    
    NSString *localNotificationCategory = [userInfo objectForKey:@"category"];
    if([localNotificationCategory isEqualToString:kNotificationBeacon]){
        
        if(state == UIApplicationStateActive){
            UIColor *themeColor;
            NSString *title;
            
            if([[userInfo objectForKey:@"action"] boolValue]){
                themeColor = COLOR_MAIN_THEME_PENDING_BLUE;
                title = @"Welcome";
            }else{
                themeColor = COLOR_MAIN_THEME_FAILURE_RED;
                title = @"Goodbye";
            }
            
            [RKDropdownAlert title:title message:notification.alertBody backgroundColor:themeColor textColor:[UIColor whiteColor]];
        }
    }
    application.applicationIconBadgeNumber = 0;
}

-(void)application:(UIApplication *)application didReceiveRemoteNotification:(NSDictionary *)userInfo fetchCompletionHandler:(void (^)(UIBackgroundFetchResult))completionHandler{
    
    UIApplicationState state = [application applicationState];
    NSDictionary *params = [[userInfo objectForKey:@"aps"] objectForKey:@"params"];
    
    NSString *orderId = [params objectForKey:@"orderId"];
    NSString *receiptId = [params objectForKey:@"receiptId"];
    NSString *restaurantName = [params objectForKey:@"restaurantName"];
    
    if(state == UIApplicationStateActive){
        OrderConfirmationStatus confirmationStatus = [[params objectForKey:@"action"] unsignedIntegerValue];
        
        UIImage *confirmationImage;
        NSString *builderStr;
        switch (confirmationStatus) {
            case kOrderConfirmed:{
                builderStr = @"%@ received your order. Enjoy!";
                confirmationImage = [UIImage imageNamed:@"Check White"];
            }
                break;
                
            case kOrderCancelled:{
                builderStr = @"%@ cancelled your order. See more in detail";
                confirmationImage = [UIImage imageNamed:@"Cancel"];
            }
            default:
                break;
        }
        
        AMSmoothAlertView *alertView = [AlertViewManager smoothDropAlertViewWithTitle:[NSString stringWithFormat:@"Order %@", receiptId] content:[NSString stringWithFormat:builderStr, restaurantName] cancelButton:YES color:COLOR_MAIN_THEME alertType:AlertInfo onCompletion:^(AMSmoothAlertView *alert, UIButton *button){
            
            if([button isEqual:alert.defaultButton]){
                [_Shared showOverlayIndetermineSmalProgressViewWithTitle:[NSString stringWithFormat:@"Loading"] style:kOverlayElegantStyle tintColor:COLOR_MAIN_THEME onView:self.window.rootViewController.view];
                
                [self presentOrderDetailOnOrderId:orderId onCompletion:^(OrderPlacingViewController *orderPlacingViewController, NSError *error){
                    if(!error){
                        [self.window.rootViewController presentViewController:orderPlacingViewController animated:YES completion:^{
                            [alert dismissAlertView];
                            
                            [_Shared hideOverlayIndetermineProgressViewAfter:1.0f];
                        }];
                    }
                }];
            }else if([button isEqual:alert.cancelButton]){
                [alert dismissAlertView];
            }
        }];
        [alertView.defaultButton setTitle:@"Detail" forState:UIControlStateNormal];
        [alertView.cancelButton setTitle:@"Dismiss" forState:UIControlStateNormal];
        [alertView.logoView setImage:confirmationImage];
        
        [alertView show];
    }else if(state == UIApplicationStateInactive || state == UIApplicationStateBackground){
        [self handleReceiptDetailBy:userInfo];
    }
    
    completionHandler(UIBackgroundFetchResultNewData);
}


#pragma mark - Private methods
-(void)handleLogoutFromApp{
    [self.window setRootViewController:nil];
    [self.window setRootViewController:self.viewController];
}

- (void)registerPushNotification:(UIApplication *)application  {
    if ([application respondsToSelector:@selector(isRegisteredForRemoteNotifications)]) {
        [application registerForRemoteNotifications];
    }
}

- (void)initViewControllers {
    MenuSideViewController *sidebarViewController = [[MenuSideViewController alloc] initWithNibName:@"MenuSideView" bundle:nil];
    MainOrderViewController *mainMenuViewController = [[MainOrderViewController alloc] initWithNibName:@"MainOrderView" bundle:nil];
    
    NavBarController *navbarController = [[NavBarController alloc] initWithRootViewController:mainMenuViewController];
    
    [sidebarViewController preserveMainNavController:navbarController];
    
    self.revealViewController = [[SWRevealViewController alloc] initWithRearViewController:sidebarViewController
                                                                frontViewController:navbarController];
}

- (void)firstTimeUseAppHandler{
    if(![_UserDefault objectForKey:kFirstUseApp]){
        // first time open
        [_UserDefault setObject:[NSNumber numberWithBool:NO] forKey:kShouldAuthTouchIdForPlaceOrder];
        [_UserDefault setObject:[NSNumber numberWithBool:YES] forKey:kShouldSurveillanceBeacons];
        
        [_UserDefault setObject:[NSNumber numberWithBool:NO] forKey:kFirstUseApp];
    }
}

- (void)initializeAppRating{
    iRate *rating = [iRate sharedInstance];
    rating.daysUntilPrompt = 2;
    rating.usesUntilPrompt = 1;
    rating.daysUntilPrompt = 0.5;
    
    [rating setAppStoreID:000];
    [rating setAppStoreGenreID:000];
    [rating setAppStoreCountry:@"VI"];
    [rating setApplicationName:@"Eatus"];
    [rating setApplicationBundleID:@"com.triadfox.eatus"];
    
    [rating setMessageTitle:@""];
    [rating setMessage:@""];
    
}

- (void)integrateParseWithOptions:(NSDictionary *)launchOptions {
    //have to call this before setApplicationId
    [self registerParseSubclass];
    
    NSString *parseAppID = [[NSBundle mainBundle] objectForInfoDictionaryKey:@"ParseAppID"];
    NSString *parseClientKey = [[NSBundle mainBundle] objectForInfoDictionaryKey:@"ParseClientKey"];
    
    // Initialize Parse.
    [Parse setApplicationId:parseAppID
                  clientKey:parseClientKey];
    [PFFacebookUtils initializeFacebookWithApplicationLaunchOptions:launchOptions];

    // [Optional] Track statistics around application opens.
    [PFAnalytics trackAppOpenedWithLaunchOptions:launchOptions];
}

- (void)registerParseSubclass {
    [Promotion registerSubclass];
    [Restaurant registerSubclass];
    [Beacon registerSubclass];
    [User registerSubclass];
    [CardPayment registerSubclass];
    [Trending registerSubclass];
    [RestaurantTable registerSubclass];
    [Order registerSubclass];
    [Feedback registerSubclass];
}

-(void)handleReceiptDetailBy: (NSDictionary *)userInfo{
    NSDictionary *params = [[userInfo objectForKey:@"aps"] objectForKey:@"params"];
    NSString *orderId = [params objectForKey:@"orderId"];
    
    [_Shared showOverlayIndetermineSmalProgressViewWithTitle:[NSString stringWithFormat:@"Loading"] style:kOverlayElegantStyle tintColor:COLOR_MAIN_THEME onView:self.window.rootViewController.view];
    
    [self presentOrderDetailOnOrderId:orderId onCompletion:^(OrderPlacingViewController *orderPlacingViewController, NSError *error){
        if(!error){
            [self.window.rootViewController presentViewController:orderPlacingViewController animated:YES completion:^{
                [_Shared hideOverlayIndetermineProgressViewAfter:1.0f];
            }];
        }
    }];
}

-(void)presentOrderDetailOnOrderId: (NSString *)orderId onCompletion: (void (^)(OrderPlacingViewController *orderPlacingViewController, NSError *error))completion{
    [ParseDataManager orderForIdOfCurrentUser:orderId onCompletion:^(Order *order, NSError *error){
        if(!error){
            NSDictionary *neededData = [MenuJsonSerializer allNeededOrderDetailGotJsonFromBackend:order.orderDetail];
            
            __block NSLocale *localeByCurrencyGotJson = [neededData objectForKey:KEY_RECEIPT_LOCALE];
            __block NSString *tableNameWithReceipt;
            
            [order fetchIfNeededInBackgroundWithBlock:^(PFObject *obj, NSError *error){
                if(!error){
                    RestaurantTable *table = obj[@"restaurantTable"];
                    
                    [table fetchIfNeededInBackgroundWithBlock:^(PFObject *object, NSError *error){
                        if(!error){
                            tableNameWithReceipt = object[@"name"];
                            
                            OrderPlacingViewController *orderPlacingViewController = [[OrderPlacingViewController alloc] initWithNibName:@"OrderPlacingView" bundle:nil mode:kOrderPlacingReceiptReview];
                            [orderPlacingViewController prepareAllCalculationByReceiptDataAtTableWithOrder:order locale:localeByCurrencyGotJson tableName:tableNameWithReceipt];
                            
                            completion(orderPlacingViewController, nil);
                        }else{
                            completion(nil, error);
                        }
                    }];
                }else{
                    completion(nil, error);
                }
            }];
        }else{
            completion(nil, error);
        }
    }];
}

@end
