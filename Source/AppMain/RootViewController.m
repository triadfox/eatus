//
//  RootViewController.m
//  MiPoBeacon
//
//  Created by Phong Nguyen on 9/1/15.
//  Copyright (c) 2015 Triad Fox. All rights reserved.
//

#import "RootViewController.h"
#import "AppDelegate.h"

#import <ParseFacebookUtilsV4/ParseFacebookUtilsV4.h>
#import <FBSDKCoreKit/FBSDKCoreKit.h>

#import "UIView+DCAnimationKit.h"

@interface RootViewController (){
    NSTimer *foxxieTimer;
}

@end

@implementation RootViewController
@synthesize loginWithFacebook;
@synthesize continueWithoutLogin;
@synthesize navbarController;

- (void)viewDidLoad {
    [super viewDidLoad];
    [self prepareInterface];
    
    [self.loginWithFacebook setTitle:NSLocalizedString(@"com.eatus.auth.login.facebook", @"FBAuth") forState:UIControlStateNormal];
    [self.continueWithoutLogin setTitle:NSLocalizedString(@"com.eatus.auth.login.continue", @"AnonymousAuth") forState:UIControlStateNormal];
}

-(void)viewDidAppear:(BOOL)animated{
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1.0f * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        [self animateFoxxieImageView];
    });
}

-(void)prepareInterface{
    self.navbarController.navigationBarHidden = YES;
    
    [self.loginWithFacebook setBackgroundColor:[Util colorWithHexString:MAIN_FACEBOOK_PALETTE]];
    [self.continueWithoutLogin setBackgroundColor:[Util colorWithHexString:MAIN_THEME_COLOR]];
    
    [self.loginWithFacebook spacingBetweenImageAndTitleWith:15.0f];
    [self.continueWithoutLogin spacingBetweenImageAndTitleWith:10.0f];
    
    [self.view setBackgroundColor:[UIColor colorWithPatternImage:[UIImage imageNamed:@"Background"]]];
    
    UITapGestureRecognizer *logoGestureTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(animateFoxxieImageView)];
    [self.triadfoxImageView addGestureRecognizer:logoGestureTap];

    
    UIView *dimView = [[UIView alloc] initWithFrame:self.view.bounds];
    [dimView setBackgroundColor:[UIColor blackColor]];
    [dimView setAlpha:0.5];
    [self.view insertSubview:dimView atIndex:0];
}

-(void)animateFoxxieImageView{
    NSDateComponents *todayComponents = [Util dateTimeComponentsAtLocale:[NSLocale currentLocale] date:[NSDate date]];
    NSUInteger minutes = [todayComponents minute];
    
    if(minutes % 2 == 0){
        [self.triadfoxImageView tada:nil];
        [self.eatusLoganTitle tada:nil];
    }else if(minutes % 3 == 0){
        [self.triadfoxImageView pulse:nil];
        [self.eatusLoganTitle pulse:nil];
    }else{
        [self.triadfoxImageView bounce:nil];
        [self.eatusLoganTitle bounce:nil];
    }
    
}

-(UIStatusBarStyle)preferredStatusBarStyle{
    return UIStatusBarStyleLightContent;
}

-(void)moveToMainOrder{
    [self presentViewController:_AppDelegate.revealViewController animated:YES completion:nil];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

- (IBAction)continueWithoutLogin:(id)sender {
    [self signInWithDeviceIdentity];
}

- (IBAction)loginWithFacebook:(UIButton *)sender {
    [self signInWithFacebook];
}


#pragma mark - Private methods
-(void)signInWithDeviceIdentity{
    [_Shared showOverlayIndetermineSmalProgressViewWithTitle:NSLocalizedString(@"com.eatus.hud.process", @"Processing") style:kOverlayElegantStyle tintColor:COLOR_MAIN_THEME onView:self.view];
    [_Shared loginOrCreateUser:^(BOOL isLoggedIn, NSError *err) {
        [_Shared hideOverlayIndetermineProgressViewAfter:0.3];
        if (isLoggedIn) {
            NSLog(@"Logged in as user: %@", [User currentUser]);
        }
        [self moveToMainOrder];
    }];
}

- (void)signInWithFacebook{
    [PFFacebookUtils logInInBackgroundWithReadPermissions:kFacebookPermissions block:^(PFUser *user, NSError *error) {
        if (!user) {
            NSLog(@"Uh oh. The user cancelled the Facebook login.");
        } else if (user.isNew) {
            NSLog(@"User signed up and logged in through Facebook!");
            
            [_Shared updateInstallationUser];
            
            [self getFacebookProfile];
            
        } else {
            [_Shared updateInstallationUser];
            
            [self getFacebookProfile];
        }
    }];
}

- (void)animateFoxxieLogo:(id)sender{
    [self animateFoxxieImageView];
    
}

- (void)getFacebookProfile {
    [_Shared showOverlayIndetermineSmalProgressViewWithTitle:NSLocalizedString(@"com.eatus.notification.profile.signingIn.title", @"SigningIn") style:kOverlayElegantStyle tintColor:COLOR_MAIN_THEME onView:self.view];
    
    [_Shared getFacebookProfile:^(BOOL succeeded, NSError *err) {
        if (succeeded) {
            [_Shared getFacebookAvatar:^(BOOL succeeded, NSError *err) {
                if (succeeded) {
                    User *currentUser = [User currentUser];
                    currentUser.deviceUUID = [Util getDeviceUUID];
                    
                    [currentUser saveInBackgroundWithBlock:^(BOOL succeeded, NSError *error) {
                        [_Shared hideOverlayIndetermineProgressViewAfter:0.3];
                        [self moveToMainOrder];
                    }];
                } else {
                    [_Shared hideOverlayIndetermineProgressViewAfter:0.3];
                    [self moveToMainOrder];
                }
            }];
        } else {
            [_Shared hideOverlayIndetermineProgressViewAfter:0.3];
            [self moveToMainOrder];
        }
        
    }];
}

@end
