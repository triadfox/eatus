//
//  UIButton+EdgeInset.h
//  Eatus
//
//  Created by Phong Nguyen on 10/29/15.
//  Copyright © 2015 Triad Fox. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface UIButton (UIButtonExt)

- (void)centerImageAndTitle:(float)space;
- (void)spacingBetweenImageAndTitleWith: (CGFloat)space;
- (void)spacingBetweenImageAndTitleWith:(CGFloat)space sizeToFit: (BOOL)determine;
- (void)alignRightTheImageOfButtonWithSpace: (CGFloat)space;
- (void)moveImageUnderTitleWithSpace: (CGFloat)space;
- (void)moveTitleUnderImageWithSpace: (CGFloat)space;
- (void)moveImageToRightSide;
- (void)centerImageAndTitle;

@end