//
//  UIFont+SystemFontOverride.h
//  Eatus
//
//  Created by Phong Nguyen on 11/15/15.
//  Copyright © 2015 Triad Fox. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIFont (SystemFontOverride)

@end
