//
//  UIFont+SystemFontOverride.m
//  Eatus
//
//  Created by Phong Nguyen on 11/15/15.
//  Copyright © 2015 Triad Fox. All rights reserved.
//

#import "UIFont+SystemFontOverride.h"

@implementation UIFont (SystemFontOverride)

#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Wobjc-protocol-method-implementation"

+ (UIFont *)boldSystemFontOfSize:(CGFloat)fontSize {
    return [UIFont fontWithName:@"Trebuchet MS" size:fontSize];
}

+ (UIFont *)systemFontOfSize:(CGFloat)fontSize {
    return [UIFont fontWithName:@"Trebuchet MS" size:fontSize];
}

#pragma clang diagnostic pop

@end