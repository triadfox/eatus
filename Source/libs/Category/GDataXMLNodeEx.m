//
//  GDataXMLNodeExtension.m
//  IPLibrary
//
//  Created by Phong Nguyen on 11/17/14.
//  Copyright (c) 2014 Phong Nguyen. All rights reserved.
//

#import "GDataXMLNodeEx.h"

#pragma GDataXMLNode

@implementation GDataXMLNode (Category)

- (GDataXMLElement*)singleNodeForXPath:(NSString *)xpath
{
    NSError* error;
    NSArray* nodes = [self nodesForXPath:xpath error:&error];
    if ([nodes count] > 0)
        return (GDataXMLElement*)[nodes firstObject];
    
    return nil;
}

- (GDataXMLElement*)singleNodeForXPath:(NSString *)xpath nodeId:(NSString*)nodeId
{
    NSError* error;
    NSArray* nodes = [self nodesForXPath:xpath error:&error];
    if ([nodes count] > 0)
    {
        for (GDataXMLElement* node in nodes)
        {
            NSString* findId = [node attributeValueForName:@"id"];
            if (findId != nil && [findId isEqualToString:nodeId])
                return node;
        }
    }

    return nil;
}

- (NSArray *)nodesForXPath:(NSString *)xpath
{
    NSError* error;
    return [self nodesForXPath:xpath error:&error];
}

// just support simple xPath such as abc/xyz/a/b/c
- (GDataXMLElement*)createNodeForXPath:(NSString *)xpath
{
    NSString* currentPath = @"";
    NSArray* tagList = [xpath componentsSeparatedByString:@"/"];
    GDataXMLElement* currentNode = (GDataXMLElement*)self;
    for (NSString* tag in tagList)
    {
        if ([tag isEqualToString:@""] && [currentPath isEqualToString:@""])
        {
            currentPath = @"/";
            continue;
        }
        
        if ([currentPath isEqualToString:@""] || [currentPath isEqualToString:@"/"])
            currentPath = [currentPath stringByAppendingString:tag];
        else
            currentPath = [currentPath stringByAppendingPathComponent:tag];
        
        GDataXMLElement* node = [currentNode singleNodeForXPath:currentPath];
        if (node == nil)
        {
            [currentNode addChild:[GDataXMLElement elementWithName:tag]];
            currentNode = [currentNode singleNodeForXPath:tag];
        }
        else
        {
            currentNode = node;
        }
    }
    
    return currentNode;

}

- (GDataXMLElement*)createLatestNode:(NSString*)xpath nodeId:(NSString*)nodeId
{
    NSArray* tagList = [xpath componentsSeparatedByString:@"/"];
    GDataXMLElement* node = (GDataXMLElement*)self;
    if ([tagList count] > 1)
    {
        NSString* parentXPath = @"";
        for (int i=0; i < [tagList count] -1; i++)
            parentXPath = [parentXPath stringByAppendingPathComponent:[tagList objectAtIndex:i]];
        node = [self createNodeForXPath:parentXPath];
    }

    GDataXMLElement* element = [GDataXMLElement elementWithName:[tagList lastObject]];
    [element setAttributeValue:@"id" value:nodeId];
                                
    [node addChild:element];
    
    return [node singleNodeForXPath:xpath nodeId:nodeId];
}

- (NSArray*)stringArray
{
    NSArray *datas;
    
    NSArray *children = [self children];
    if([children count] == 0)
        return nil;
    
    NSMutableArray *valueReturn = [[NSMutableArray alloc] init];
    
    for(GDataXMLElement *child in children)
        [valueReturn addObject:[child stringValue]];
    datas = [NSArray arrayWithArray:valueReturn];
    
    return datas;
}

- (NSArray*)dicArrayWithTagValue
{
    NSArray *datas;
    
    NSArray *children = [self children];
    if([children count] == 0)
        return nil;
    
    NSMutableDictionary *data = [[NSMutableDictionary alloc] init];
    NSMutableArray *arrayData = [[NSMutableArray alloc] init];
    for(GDataXMLElement *child in children)
    {
        NSArray *siblings = [child children];
        for(GDataXMLElement *sibling in siblings)
        {
            if([sibling isKindOfClass:[GDataXMLElement class]])
                [data setObject:[sibling stringValue] forKey:[sibling name]];
        }
        [arrayData addObject:data];
    }
    datas = [NSArray arrayWithArray:arrayData];
    
    return datas;
}

- (NSArray*)dicArrayWithKeyAttribute:(NSString*)attributeName  valueType:(GDataXMLType)valueType
{
    NSArray *datas;
    
    NSArray *children = [self children];
    NSMutableArray *arrayData = [[NSMutableArray alloc] init];
    if([children count] == 0)
        return nil;
    
    for(GDataXMLElement *child in children)
    {
        NSMutableDictionary *data = [[NSMutableDictionary alloc] init];
        
        GDataXMLNode *attribute = [child attributeForName:attributeName];
        
        if(valueType == GDataXMLTypeTagValue)
            [data setObject:[child stringValue] forKey:[attribute name]];
        else if(valueType == GDataXMLTypeAttributeValue)
            [data setObject:[attribute stringValue] forKey:[attribute name]];
        [arrayData addObject:data];
    }
    datas = [NSArray arrayWithArray:arrayData];
    
    return datas;
}

- (NSDictionary*)dictionaryWithTagValue
{
    NSDictionary *datas;
    
    NSMutableDictionary *dictData = [[NSMutableDictionary alloc] init];
    
    NSArray *children = [self children];
    if([children count] == 0)
        return nil;

    for(GDataXMLElement *child in children)
        [dictData setObject:[child stringValue] forKey:[child name]];
    datas = [NSMutableDictionary dictionaryWithDictionary:dictData];
    
    return  datas;
}

@end

#pragma GDataXMLElement

@implementation GDataXMLElement (Category)

- (NSString*)attributeValueForName:(NSString*)name
{
    return [(GDataXMLNode*)[self attributeForName:name] stringValue];
}

- (void)setAttributeValue:(NSString*)name value:(NSString*)value
{
    GDataXMLNode* attribNode = [self attributeForName:name];
    if (attribNode == nil)
    {
        attribNode = [GDataXMLElement attributeWithName:name stringValue:value];
        [self addAttribute:attribNode];
    }
    else{
        [attribNode setStringValue:value];
    }
}

- (NSDictionary*)dictionaryFromAttributes
{
    NSDictionary *datas;
    NSMutableDictionary *dictData = [[NSMutableDictionary alloc] init];
    
    NSArray *attributes = [self attributes];
    for(GDataXMLNode *attribute in attributes)
        [dictData setObject:[attribute stringValue] forKey:[attribute name]];
    datas = [NSDictionary dictionaryWithDictionary:dictData];
    
    return datas;
}

@end

#pragma GDataXMLDocument

@implementation GDataXMLDocument (Category)

- (GDataXMLElement*)singleNodeForXPath:(NSString *)xpath
{
    NSError* error;
    NSArray* nodes = [self nodesForXPath:xpath error:&error];
    if ([nodes count] > 0)
        return (GDataXMLElement*)[nodes firstObject];
    
    return nil;
}

- (NSArray *)nodesForXPath:(NSString *)xpath
{
    NSError* error;
    return [self nodesForXPath:xpath error:&error];
}

- (id)initWithFile:(NSString*)file
{
    NSError* error;
    NSData* xmlData = [[NSMutableData alloc] initWithContentsOfFile:file];
    return [[GDataXMLDocument alloc] initWithData:xmlData options:0 error:&error];
}

@end
