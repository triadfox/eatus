//
//  GDataXMLNodeExtension.h
//  IPLibrary
//
//  Created by Phong Nguyen on 11/17/14.
//  Copyright (c) 2014 Phong Nguyen. All rights reserved.
//


#import <Foundation/Foundation.h>
#import "GDataXMLNode.h"

typedef NS_ENUM(NSInteger,GDataXMLType)
{
    GDataXMLTypeTagValue = 0,
    GDataXMLTypeAttributeValue = 1
};

#pragma GDataXMLNode

@interface GDataXMLNode (Category)

- (GDataXMLElement*)singleNodeForXPath:(NSString *)xpath;
- (NSArray *)nodesForXPath:(NSString *)xpath;
- (GDataXMLElement*)createNodeForXPath:(NSString *)xpath;
- (GDataXMLElement*)createLatestNode:(NSString*)xpath nodeId:(NSString*)nodeId;
- (GDataXMLElement*)singleNodeForXPath:(NSString *)xpath nodeId:(NSString*)nodeId;
- (NSArray*)stringArray;
- (NSArray*)dicArrayWithTagValue;
- (NSArray*)dicArrayWithKeyAttribute:(NSString*)attributeName  valueType:(GDataXMLType)valueType;
- (NSDictionary*)dictionaryWithTagValue;

@end

#pragma GDataXMLElement

@interface GDataXMLElement (Category)

- (NSString *)attributeValueForName:(NSString *)name;
- (void)setAttributeValue:(NSString*)name value:(NSString*)value;
- (NSDictionary *)dictionaryFromAttributes;
@end

#pragma GDataXMLDocument

@interface GDataXMLDocument (Category)

- (GDataXMLElement*)singleNodeForXPath:(NSString *)xpath;
- (NSArray *)nodesForXPath:(NSString *)xpath;
- (id)initWithFile:(NSString*)file;

@end