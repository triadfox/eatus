//
//  KontaktBeaconService.h
//  Eatus
//
//  Created by Phong Nguyen on 11/6/15.
//  Copyright © 2015 Triad Fox. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreLocation/CoreLocation.h>
#import <CoreBluetooth/CoreBluetooth.h>

#import "KontaktSDK.h"

@interface KontaktBeaconServiceManager : NSObject

+(instancetype)shareBeacon;

-(void)launchKontaktBeaconMonitoring;

-(void)launchKontatkBeaconSurveillance;
-(void)evictKontaktBeaconSurveillance;

-(void)evictBeaconManager;

-(void)nearestBeaconInRange: (void (^)(CLBeacon *beacon))completion;
-(void)nearestBeaconRegionInRange: (void (^)(CLBeaconRegion *beacon))completion;

@end
