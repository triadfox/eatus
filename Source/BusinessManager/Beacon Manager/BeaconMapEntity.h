//
//  BeaconMapEntity.h
//  Light House
//
//  Created by Phong Nguyen on 9/25/15.
//  Copyright © 2015 Triad Fox. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface BeaconMapEntity : NSObject

@property(nonatomic, assign) NSInteger mapMajorId;
@property(nonatomic, strong) NSMutableArray *mapCollection;
@property(nonatomic, strong) NSString *mapBeaconMapping;

@end
