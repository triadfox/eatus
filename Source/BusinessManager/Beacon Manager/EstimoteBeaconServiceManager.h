//
//  BeaconEngine.h
//  Light House
//
//  Created by Phong Nguyen on 9/24/15.
//  Copyright © 2015 Triad Fox. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <EstimoteSDK/EstimoteSDK.h>
#import <CoreLocation/CoreLocation.h>
#import <CoreBluetooth/CoreBluetooth.h>

#import "KontaktSDK.h"

@protocol BeaconEngineProtocols <NSObject>

@required
-(void)didEnterRegion: (CLBeaconRegion *)beaconRegion;
-(void)didExitRegion: (CLBeaconRegion *)beaconRegion;

@optional
-(void)didRangingBeacons: (NSArray *)beacons inRegion:(CLBeaconRegion *)region;
-(void)didRangingBeaconsWithTheSameMajor: (NSArray *)beacons;
-(void)didFailBeaconLauncherWithError: (NSError *)error;
-(void)didFailMonitorBeaconWithError: (NSError *)error;
-(void)didFailRangingBeaconWithError: (NSError *)error;

//determine whether user's being in beacon region included: Unknow (value = 0), Inside (value = 1) and Outside (value = 2)
-(void)didStayInSideBeaconRegion: (CLBeaconRegion *)region;
-(void)didStayOutsideBeaconRegion: (CLBeaconRegion *)region;
-(void)didStayUnknowBeaconRegion: (CLBeaconRegion *)region;

@end

@interface EstimoteBeaconServiceManager : NSObject <ESTBeaconManagerDelegate>

@property (nonatomic, strong) id delegate;

+(id)shareBeacon;
+(void)getNearestBeaconInZoneOnCompletion: (void (^)(CLBeacon *beacon))completion;
+(void)getNearestBeaconRegionInZoneOnCompletion: (void (^)(CLBeaconRegion *beaconRegion))completion;

-(void)launchBeaconMonitoring;
-(void)evictBeaconMonitoring;
-(void)swapCurrentDelegateWith: (id)newDelegate;

@end
