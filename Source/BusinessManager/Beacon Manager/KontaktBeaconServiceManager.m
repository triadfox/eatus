//
//  KontaktBeaconService.m
//  Eatus
//
//  Created by Phong Nguyen on 11/6/15.
//  Copyright © 2015 Triad Fox. All rights reserved.
//

#import "KontaktBeaconServiceManager.h"

#define EATUS_BEACON_UUID                       @"29b5ab2f-87f4-4293-9eb3-7decb3659d23"

@interface KontaktBeaconServiceManager()<KTKLocationManagerDelegate, KTKBluetoothManagerDelegate>{
    NSMutableArray *beaconsInRange;
    NSMutableSet *devicesInRange;
    
    BOOL didSurveillance;
    BOOL didMonitor;
    BOOL didEnterRegion;
    
    BOOL shouldPostLocalOnEnterRegionNotification;
    BOOL shouldPostLocalOnExitRegionNotification;
    
    NSNumber *previousMajorId;
}

@property(nonatomic, strong) KTKLocationManager *locationManager;
@property(nonatomic, strong) KTKBluetoothManager *beaconManager;
@property(nonatomic, strong) KTKRegion *locationRegion;

@end

@implementation KontaktBeaconServiceManager
@synthesize locationManager, beaconManager, locationRegion;

+(instancetype)shareBeacon{
    static KontaktBeaconServiceManager *manager;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        manager = [[KontaktBeaconServiceManager alloc] init];
    });
    return manager;
}

-(void)launchKontaktBeaconMonitoring{
    locationManager = [[KTKLocationManager alloc] init];
    
    if([KTKLocationManager canMonitorBeacons]){
        locationRegion = [[KTKRegion alloc] initWithUUID:EATUS_BEACON_UUID];
        
        [locationManager setRegions:@[locationRegion]];
        locationManager.delegate = self;
        
        shouldPostLocalOnEnterRegionNotification = YES;
        shouldPostLocalOnExitRegionNotification = YES;
        
        [_NotificationCenter addObserver:self selector:@selector(cleanDataTriggerBluetoothOff) name:kNotificationBluetoothOff object:nil];
        [_NotificationCenter addObserver:self selector:@selector(cleanDataTriggerBluetoothOff) name:kNotificationNoBeaconsInRange object:nil];
        [_NotificationCenter addObserver:self selector:@selector(startMonitoringBeaconsInRange) name:kNotificationBluetoothOn object:nil];
        
        [self startMonitoringBeaconsInRange];
    }
}

-(void)launchKontatkBeaconSurveillance{
    [_NotificationCenter addObserver:self selector:@selector(evictKontaktBeaconSurveillance) name:kNotificationEnableBeaconSurveillanceDidChange object:nil];
    
    if([[_UserDefault objectForKey:kShouldSurveillanceBeacons] boolValue]){
        beaconManager = [[KTKBluetoothManager alloc] init];
        beaconManager.delegate = self;
        
        [beaconManager startFindingDevices];
        
        //first time initializing
        didSurveillance = YES;
    }else{
        return;
    }
}

-(void)evictKontaktBeaconSurveillance{
    if(didSurveillance){
        //surveillance is on
        [beaconManager stopFindingDevices];
        didSurveillance = NO;
        [self cleanDataTriggerBeaconSurveillance];
        
        [_UserDefault setObject:[NSNumber numberWithBool:NO] forKey:kShouldSurveillanceBeacons];
    }else{
        //surveillance is off, need to turn on
        [_NotificationCenter removeObserver:self name:kNotificationEnableBeaconSurveillanceDidChange object:nil];
        [self launchKontatkBeaconSurveillance];
    }
}

-(void)startMonitoringBeaconsInRange{
    [locationManager startMonitoringBeacons];
}

-(void)cleanDataTriggerBluetoothOff{
    if(beaconsInRange){
        [beaconsInRange removeAllObjects];
    }
}

-(void)cleanDataTriggerBeaconSurveillance{
    if(devicesInRange){
        [devicesInRange removeAllObjects];
    }
}

-(void)evictBeaconManager{
    [locationManager stopMonitoringBeacons];
}

-(void)nearestBeaconInRange:(void (^)(CLBeacon *))completion{
    if([beaconsInRange count] != 0){
        [self sortAllBeaconsByDistance:beaconsInRange];
        
        if(completion){
            completion([beaconsInRange firstObject]);
        }
    }else{
        if(completion){
            completion(nil);
        }
    }
}

-(void)nearestBeaconRegionInRange:(void (^)(CLBeaconRegion *))completion{
    //implement later
}


#pragma mark - KTKLocationManager Delegate methods
-(void)locationManager:(KTKLocationManager *)locationManager didChangeState:(KTKLocationManagerState)state withError:(NSError *)error{
    switch (state) {
        case KTKLocationManagerStateMonitoring:
            break;
            
        case KTKLocationManagerStateFailed:
            break;
            
        case KTKLocationManagerStateInactive:
            break;
            
        case KTKLocationManagerStateInitializing:
            break;
            
        default:
            break;
    }
}

-(void)locationManager:(KTKLocationManager *)locationManager didEnterRegion:(KTKRegion *)region{
    NSLog(@"Enter region: %@", region.uuid);
    
    if(shouldPostLocalOnEnterRegionNotification){
        didEnterRegion = YES;
        UILocalNotification* localNotification = [[UILocalNotification alloc] init];
        localNotification.fireDate = [NSDate date];
        localNotification.alertBody = NSLocalizedString(@"com.eatus.notification.enterRegion.body", @"Enter");
        localNotification.alertAction = NSLocalizedString(@"com.eatus.notification.action", @"Action");
        localNotification.userInfo = @{@"action":[NSNumber numberWithBool:YES], @"category":kNotificationBeacon};
        
        localNotification.soundName = UILocalNotificationDefaultSoundName;
        localNotification.timeZone = [NSTimeZone localTimeZone];
        
        [[UIApplication sharedApplication] scheduleLocalNotification:localNotification];
        [[NSNotificationCenter defaultCenter] postNotificationName:kNotificationBeacon object:self];
    }
}

-(void)locationManager:(KTKLocationManager *)locationManager didExitRegion:(KTKRegion *)region{
    NSLog(@"Exit region: %@", region.uuid);
    
    if(shouldPostLocalOnExitRegionNotification){
        UILocalNotification* localNotification = [[UILocalNotification alloc] init];
        localNotification.fireDate = [NSDate date];
        localNotification.alertBody = NSLocalizedString(@"com.eatus.notification.exitRegion.body", @"Exit");
        localNotification.alertAction = @"";
        localNotification.soundName = UILocalNotificationDefaultSoundName;
        localNotification.timeZone = [NSTimeZone localTimeZone];
        localNotification.userInfo = @{@"action":[NSNumber numberWithBool:NO], @"category":kNotificationBeacon};
        
        [[UIApplication sharedApplication] scheduleLocalNotification:localNotification];
        [[NSNotificationCenter defaultCenter] postNotificationName:kNotificationBeacon object:self];
    }
    
    [self cleanTriggerDataPostLocalNotificationOnExitRegion];
}

-(void)locationManager:(KTKLocationManager *)locationManager didRangeBeacons:(NSArray *)beacons{
    if([beacons count] == 0){
        [_NotificationCenter postNotificationName:kNotificationNoBeaconsInRange object:nil];
        
        CLBeacon *beacon = [beacons firstObject];
        previousMajorId = beacon.major;
    }
    
    [self shouldPostLocalOnExterRegionNotificationWithBeacons:beacons];
    
    beaconsInRange = [beacons mutableCopy];
}


#pragma mark - KTKBluetoothManager delegate methods
-(void)bluetoothManager:(KTKBluetoothManager *)bluetoothManager didChangeDevices:(NSSet *)devices{
    BOOL permission = [[_UserDefault objectForKey:kShouldSurveillanceBeacons] boolValue];
    if(!permission){
        return;
    }
    else if([devicesInRange count] != 0){
        if([devices count] == 0){
            return;
        }
        if([devicesInRange isEqualToSet:devices]){
            return;
        }else{
            devicesInRange = [devices mutableCopy];;
        }
    }else if(!devicesInRange && [devices count] != 0){
        devicesInRange = [devices mutableCopy];
    }else if([devices count] == 0){
        return;
    }
    
    if([devices count] != 0){
        devicesInRange = [devices mutableCopy];
        [devices enumerateObjectsUsingBlock:^(id obj, BOOL *stop){
            __block KTKBeaconDevice *device = (KTKBeaconDevice *)obj;
            
            [ParseDataManager getBeaconByIdentifier:device.uniqueID onCompletion:^(Beacon *beacon, NSError *error){
                if(!error){
                    if([self shouldUpdateBeaconInformationAsLastUpdateDate:beacon.updatedAt]){
                        [self updateNewStatusOfBeacon:beacon device:device onCompletion:^(BOOL succeeded, NSError *error){
                            if(!error){
                                NSLog(@"Succeeded");
                            }else{
                                NSLog(@"Failed");
                            }
                        }];
                    }else{
                        return;
                    }
                }
            }];
        }];
    }
}

-(void)bluetoothManager:(KTKBluetoothManager *)bluetoothManager didChangeEddystones:(NSSet *)eddystones{
    //doesn't need to implement
}


#pragma mark - Utilities

-(void)shouldPostLocalOnExterRegionNotificationWithBeacons: (NSArray *)beacons{
    if([beacons count] == 0){
        if(didEnterRegion){
            shouldPostLocalOnEnterRegionNotification = NO;
        }else{
            shouldPostLocalOnEnterRegionNotification = YES;
        }
        
        shouldPostLocalOnExitRegionNotification = YES;
        return;
    }
    
    if(!previousMajorId){
        //previous major id is nil, it means this's the first region user entered
        CLBeacon *beacon = [beacons firstObject];
        previousMajorId = beacon.major;
        
        shouldPostLocalOnEnterRegionNotification = YES;
        shouldPostLocalOnExitRegionNotification = NO;
    }else{
        //previous is not nil, it means user did stay in other region before get in this new region
        for(CLBeacon *beacon in beacons){
            if(![previousMajorId isEqual:beacon.major]){
                //there's a beacons with other major id of new region
                shouldPostLocalOnEnterRegionNotification = YES;
                shouldPostLocalOnExitRegionNotification = NO;
                
                continue;
            }else{
                //there'are no new beacons with new region, user stay in the as same previous region
                shouldPostLocalOnEnterRegionNotification = NO;
                shouldPostLocalOnExitRegionNotification = YES;
            }
        }
    }
}

-(void)cleanTriggerDataPostLocalNotificationOnExitRegion{
    previousMajorId = nil;
    
    shouldPostLocalOnEnterRegionNotification = YES;
    shouldPostLocalOnExitRegionNotification = NO;
}

-(void)sortAllBeaconsByDistance: (NSMutableArray *)beacons{
    [beacons sortUsingDescriptors:@[[NSSortDescriptor sortDescriptorWithKey:@"accuracy" ascending:NO]]];
}

-(void)updateNewStatusOfBeacon: (Beacon *)beacon device: (KTKBeaconDevice *)device onCompletion: (void (^)(BOOL succeeded, NSError *error))completion{
    NSDictionary *dataByKey = @{kBeaconBBatteryStatus:[NSNumber numberWithUnsignedInteger:device.batteryLevel], kBeaconBfirmwareVersion:[NSString stringWithFormat:@"%@", device.firmwareVersion]};
    [ParseDataManager updateBeaconStatusByIdentifier:device.uniqueID dataByKey:dataByKey onCompletion:^(BOOL succeeded, NSError *error){
        if(completion){
            completion(succeeded, error);
        }
    }];
}


-(BOOL)shouldUpdateBeaconInformationAsLastUpdateDate: (NSDate *)lastUpdate{
    NSDate *currentDate = [NSDate date];
    if([currentDate compare:lastUpdate] == NSOrderedDescending){
        //today is later than last update. For eg: today is Nov 2th, last update is Oct 31th
        NSCalendarUnit components = NSCalendarUnitYear | NSCalendarUnitMonth | NSCalendarUnitDay;
        NSCalendar *calendar = [NSCalendar currentCalendar];
        NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
        dateFormatter.locale = [NSLocale currentLocale];
        dateFormatter.timeZone = calendar.timeZone;
        [dateFormatter setDateFormat:[NSDateFormatter dateFormatFromTemplate:@"ddMMyyyy" options:0 locale:[NSLocale currentLocale]]];
        
        NSDateComponents *lastUpdateDateComponents = [calendar components:components fromDate:lastUpdate];
        NSDateComponents *currentDateComponents = [calendar components:components fromDate:currentDate];
        
        NSUInteger lastUpdateYear = [lastUpdateDateComponents year];
        NSUInteger lastUpdateMonth = [lastUpdateDateComponents month];
        NSUInteger lastUpdateDate = [lastUpdateDateComponents valueForComponent:NSCalendarUnitDay];
        
        NSUInteger currentYear = [currentDateComponents year];
        NSUInteger currentMonth = [currentDateComponents month];
        NSUInteger currentDate = [currentDateComponents valueForComponent:NSCalendarUnitDay];
        
        if(lastUpdateYear < currentYear){
            return YES;
        }else if((lastUpdateYear == currentYear) && (lastUpdateMonth < currentMonth)){
            return YES;
        }else if((lastUpdateYear == currentYear) && (lastUpdateMonth == currentMonth) && (lastUpdateDate < currentDate)){
            if((currentDate - lastUpdateDate) >= 5){
                return YES;
            }
            return NO;
        }
    }
    return NO;
}

@end
