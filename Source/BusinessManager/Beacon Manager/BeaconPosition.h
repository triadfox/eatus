//
//  BeaconPosition.h
//  MiPoBeacon
//
//  Created by Phong Nguyen on 8/22/15.
//  Copyright (c) 2015 Triad Fox. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface BeaconPosition : NSObject

@property(nonatomic, weak) NSString *beaconPosition;
@property(nonatomic, assign) float beaconAccuracy;
@property(nonatomic, assign) NSInteger beaconRSSI;
@end
