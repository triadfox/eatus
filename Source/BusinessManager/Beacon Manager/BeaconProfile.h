//
//  BeaconProfile.h
//  MiPoBeacon
//
//  Created by Phong Nguyen on 8/18/15.
//  Copyright (c) 2015 Triad Fox. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface BeaconProfile : NSObject

@property(nonatomic, weak) NSString *beaconName;
@property(nonatomic, assign) NSInteger beaconX;
@property(nonatomic, assign) NSInteger beaconY;
@property(nonatomic, assign) float beaconDistanceToDevice;
@property(nonatomic, assign) NSInteger beaconRSSI;

@end
