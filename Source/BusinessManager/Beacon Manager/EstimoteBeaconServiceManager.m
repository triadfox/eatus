//
//  BeaconEngine.m
//  Light House
//
//  Created by Phong Nguyen on 9/24/15.
//  Copyright © 2015 Triad Fox. All rights reserved.
//

#import "EstimoteBeaconServiceManager.h"

#define PEENTRI_BEACON_IDENTIFIER                   @"PeentriBeaconIdentifier"

@interface EstimoteBeaconServiceManager()

@property(nonatomic, strong) ESTBeaconManager *estBeaconManager;
@property(nonatomic, strong) ESTSecureBeaconManager *estSecuredBeaconManager;
@property(nonatomic, strong) CLBeaconRegion *beaconRegion;
@property(nonatomic, strong) ESTCloudManager *estCloudManager;

@property(nonatomic, copy) CLBeacon *nearestBeaconInRange;
@property(nonatomic, copy) CLBeaconRegion *nearestBeaconRegionInRange;
@end

@implementation EstimoteBeaconServiceManager{
    BOOL _downloadMap;
}

@synthesize delegate;
@synthesize nearestBeaconInRange, nearestBeaconRegionInRange;

+(id)shareBeacon{
    static EstimoteBeaconServiceManager *shareBeacon;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        shareBeacon = [[EstimoteBeaconServiceManager alloc] init];
    });
    return shareBeacon;
}


#pragma mark - Public methods
-(void)launchBeaconMonitoring{
    [ESTCloudManager setupAppID:@"light-house" andAppToken:@"5ee954c06f297fe09054632e43593d7c"];
    [ESTCloudManager enableRangingAnalytics:YES];
    [ESTCloudManager enableMonitoringAnalytics:YES];
    
    self.estBeaconManager = [[ESTBeaconManager alloc] init];
    self.estBeaconManager.delegate = self;
    self.beaconRegion =  [[CLBeaconRegion alloc] initWithProximityUUID:ESTIMOTE_PROXIMITY_UUID identifier:@"PeentriBeacon"];
    
    self.beaconRegion.notifyEntryStateOnDisplay = YES;
    [self startMonitorBeaconRegion];
}

-(void)evictBeaconMonitoring{
    [self.estBeaconManager stopMonitoringForRegion:self.beaconRegion];
    [self.estBeaconManager stopRangingBeaconsInRegion:self.beaconRegion];
}

+(void)getNearestBeaconInZoneOnCompletion:(void (^)(CLBeacon *))completion{
    EstimoteBeaconServiceManager *manager = [EstimoteBeaconServiceManager shareBeacon];
    if(manager.nearestBeaconInRange){
        if(completion){
            completion(manager.nearestBeaconInRange);
        }
    }else{
        [manager startMonitorBeaconRegion];
    }
}

+(void)getNearestBeaconRegionInZoneOnCompletion:(void (^)(CLBeaconRegion *))completion{
    EstimoteBeaconServiceManager *manager = [EstimoteBeaconServiceManager shareBeacon];
    if(manager.nearestBeaconRegionInRange){
        dispatch_async(dispatch_get_main_queue(), ^{
            if(completion){
                completion(manager.nearestBeaconRegionInRange);
            }
        });
    }else{
        [manager startMonitorBeaconRegion];
    }
}


#pragma mark - Private methods
-(void)startMonitorBeaconRegion{
    CLAuthorizationStatus authorizeStatus = [ESTBeaconManager authorizationStatus];
    
    [self.estBeaconManager requestAlwaysAuthorization];
    
    if(authorizeStatus == kCLAuthorizationStatusAuthorizedAlways){
        [self.estBeaconManager startMonitoringForRegion:self.beaconRegion];
    }else if(authorizeStatus == kCLAuthorizationStatusNotDetermined){
        [self.estBeaconManager startMonitoringForRegion:self.beaconRegion];
    }else if(authorizeStatus == kCLAuthorizationStatusRestricted){
        [UIAlertController alertControllerWithTitle:@"Location's not available" message:@"You have no access to location service" preferredStyle:UIAlertControllerStyleAlert];
    }else if(authorizeStatus == kCLAuthorizationStatusDenied){
        [UIAlertController alertControllerWithTitle:@"Location access denied" message:@"You have denied access to location service" preferredStyle:UIAlertControllerStyleAlert];
    }
}

-(void)preserveNearestBeaconInRange: (CLBeacon *)beacon{
    nearestBeaconInRange = beacon;
}

-(void)preserveNearestBeaconRegionInRange: (CLBeaconRegion *)beaconRegion{
    nearestBeaconRegionInRange = beaconRegion;
}


#pragma mark - Core Location Beacon Delegate methods
-(void)beaconManager:(id)manager didDetermineState:(CLRegionState)state forRegion:(CLBeaconRegion *)region{
    if(state == CLRegionStateInside){
        [self.estBeaconManager startRangingBeaconsInRegion:self.beaconRegion];
        [self.delegate didStayInSideBeaconRegion:region];
        NSLog(@"State: Inside");
    }else if(state == CLRegionStateOutside){
//        [self.delegate didStayOutsideBeaconRegion:region];
//        [self.mapDelegate didStayOutsideBeaconRegion:region];
        NSLog(@"State: Outside");
    }else if(state == CLRegionStateUnknown){
//        [self.delegate didStayUnknowBeaconRegion:region];
//        [self.mapDelegate didStayUnknowBeaconRegion:region];
        NSLog(@"State: Unknown");
    }
}

-(void)beaconManager:(id)manager didStartMonitoringForRegion:(CLBeaconRegion *)region{
    NSLog(@"Did start monitoring region UUID: %@", region.proximityUUID.UUIDString);
}

-(void)beaconManager:(id)manager didEnterRegion:(CLBeaconRegion *)region{
    NSLog(@"Enter region: %@", region.proximityUUID.UUIDString);
    
//    UILocalNotification* localNotification = [[UILocalNotification alloc] init];
//    localNotification.fireDate = [NSDate date];
//    localNotification.alertBody = @"Enter region";
//    localNotification.alertAction = @"Wooh hoo! You entered our proximity";
//    localNotification.soundName = UILocalNotificationDefaultSoundName;
//    localNotification.timeZone = [NSTimeZone localTimeZone];
//    localNotification.applicationIconBadgeNumber = [[UIApplication sharedApplication] applicationIconBadgeNumber] + 1;
//    
//    [[UIApplication sharedApplication] scheduleLocalNotification:localNotification];
//    [[NSNotificationCenter defaultCenter] postNotificationName:@"BeaconNotifier" object:self];
    
    [self.delegate didEnterRegion:region];
}

-(void)beaconManager:(id)manager didExitRegion:(CLBeaconRegion *)region{
    NSLog(@"Exit region: %@", region.proximityUUID.UUIDString);
    
//    UILocalNotification* localNotification = [[UILocalNotification alloc] init];
//    localNotification.fireDate = [NSDate date];
//    localNotification.alertBody = @"Exit region";
//    localNotification.alertAction = @"Ops! You move out our proximity";
//    localNotification.soundName = UILocalNotificationDefaultSoundName;
//    localNotification.timeZone = [NSTimeZone localTimeZone];
//    localNotification.applicationIconBadgeNumber = [[UIApplication sharedApplication] applicationIconBadgeNumber] + 1;
//    
//    [[UIApplication sharedApplication] scheduleLocalNotification:localNotification];
//    [[NSNotificationCenter defaultCenter] postNotificationName:@"BeaconNotifier" object:self];
//    
    [self.delegate didExitRegion:region];
}

//error delegate methods
-(void)beaconManager:(id)manager didFailWithError:(NSError *)error{
    NSLog(@"Did fail with error: %@", error.description);
}

-(void)beaconManager:(id)manager rangingBeaconsDidFailForRegion:(CLBeaconRegion *)region withError:(NSError *)error{
    NSLog(@"Did fail ranging beacon for region UUID: %@", region.proximityUUID.UUIDString);
    [self.delegate didFailRangingBeaconWithError:error];
}

-(void)beaconManager:(id)manager monitoringDidFailForRegion:(CLBeaconRegion *)region withError:(NSError *)error{
    NSLog(@"Did fail monitoring for region UUID: %@", region.proximityUUID.UUIDString);
    [self.delegate didFailMonitorBeaconWithError:error];
}

-(void)beaconManager:(id)manager didRangeBeacons:(NSArray *)beacons inRegion:(CLBeaconRegion *)region{
    //perform get nearest beacon in range
    [self performSelectorInBackground:@selector(preserveNearestBeaconInRange:) withObject:[beacons firstObject]];
    [self performSelectorInBackground:@selector(preserveNearestBeaconRegionInRange:) withObject:region];
}

-(void)swapCurrentDelegateWith:(id)newDelegate{
    delegate = newDelegate;
}
@end
