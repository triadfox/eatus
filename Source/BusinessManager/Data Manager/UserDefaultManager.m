//
//  UserDefaultManager.m
//  MiPoBeacon
//
//  Created by phuc on 9/2/15.
//  Copyright (c) 2015 MiPo Consolidate LLC. All rights reserved.
//

#import "UserDefaultManager.h"

#define kAppSessionFirstTimeKey @"kAppSessionFirstTimeKey"
#define kAppSessionDeviceToken @"kAppSessionDeviceToken"
#define kAppSessionDeviceTokenInNSData @"kAppSessionDeviceTokenInNSData"
#define kAppSessionUserNameKey @"kAppSessionUserNameKey"
#define kAppSessionUserPasswordKey @"kAppSessionUserPasswordKey"

@implementation UserDefaultManager

+ (BOOL)isFirstTimeUsingApp {
    if ([[NSUserDefaults standardUserDefaults] objectForKey:kAppSessionFirstTimeKey]) {
        return [[NSUserDefaults standardUserDefaults] boolForKey:kAppSessionFirstTimeKey];
    }
    return YES;
}

+ (void)setFirstTimeUsingApp:(BOOL)firstTime {
    [[NSUserDefaults standardUserDefaults] setBool:firstTime forKey:kAppSessionFirstTimeKey];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

#pragma mark - Device Token
+ (void)setDeviceToken:(NSString*)deviceToken{
    [[NSUserDefaults standardUserDefaults] setObject:deviceToken forKey:kAppSessionDeviceToken];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

+ (NSString*)getDeviceToken {
    if ([[NSUserDefaults standardUserDefaults] objectForKey:kAppSessionDeviceToken]) {
        return [[NSUserDefaults standardUserDefaults] stringForKey:kAppSessionDeviceToken];
    }
    return nil;
}

+ (void)setDeviceTokenInNSData:(NSData*)deviceToken {
    [[NSUserDefaults standardUserDefaults] setObject:deviceToken forKey:kAppSessionDeviceTokenInNSData];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

+ (NSData*)getDeviceTokenInNSData {
    if ([[NSUserDefaults standardUserDefaults] objectForKey:kAppSessionDeviceTokenInNSData]) {
        return [[NSUserDefaults standardUserDefaults] dataForKey:kAppSessionDeviceTokenInNSData];
    }
    return nil;
}

#pragma mark - User
+ (NSString*)getCurrentUserName {
    if ([[NSUserDefaults standardUserDefaults] objectForKey:kAppSessionUserNameKey]) {
        return [[NSUserDefaults standardUserDefaults] stringForKey:kAppSessionUserNameKey];
    }
    return nil;
}

+ (void)setCurrentUserName:(NSString *)userName {
    [[NSUserDefaults standardUserDefaults] setObject:userName forKey:kAppSessionUserNameKey];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

+ (NSString*)getCurrentUserPassword {
    if ([[NSUserDefaults standardUserDefaults] objectForKey:kAppSessionUserPasswordKey]) {
        return [[NSUserDefaults standardUserDefaults] stringForKey:kAppSessionUserPasswordKey];
    }
    return nil;
}

+ (void)setCurrentUserPassword:(NSString *)password {
    [[NSUserDefaults standardUserDefaults] setObject:password forKey:kAppSessionUserPasswordKey];
    [[NSUserDefaults standardUserDefaults] synchronize];
}
@end
