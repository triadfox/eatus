//
//  ServiceAPIManager.m
//  Light Order
//
//  Created by phuc on 10/18/15.
//  Copyright (c) 2015 Triad Fox. All rights reserved.
//

#import "ServiceAPIManager.h"
#import "AFNetworking.h"

// Server Domain
NSString* const kServiceDomainURL = @"http://peentri-lightorder.herokuapp.com";
//NSString* const kServiceDomainURL = @"http://localhost:5000";

// APIs
NSString* const kServiceOrderRequest = @"/api/orderRequest";
NSString* const kServiceRestaurantFeedback = @"/api/feedbackCreate";


#define STATUS_RESPONSE                 @"status"

@implementation ServiceAPIManager


+ (void)createOrderRequest:(NSString *)orderJson withRestaurantId:(NSString *)restaurantId atTableId:(NSString *)tableId withTableName:(NSString *)tableName paymentType:(NSUInteger)payment onCompletion:(void (^)(BOOL, NSError *))completion {
    
    NSString *requestURL = [NSString stringWithFormat:@"%@%@", kServiceDomainURL, kServiceOrderRequest];
    
    User *currentUser = [User currentUser];
    
    NSDictionary *json = @{
                           @"userId": currentUser.objectId,
                           @"username": currentUser.username,
                           @"restaurantId": restaurantId,
                           @"tableId":tableId,
                           @"tableName":tableName,
                           @"paymentType":[NSNumber numberWithUnsignedInteger:payment],
                           @"orderJson": orderJson
                           };
    
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    manager.requestSerializer = [AFJSONRequestSerializer serializer];
    
    [manager POST:requestURL parameters:json success:^(NSURLSessionDataTask *task, id responseObject) {
        [manager invalidateSessionCancelingTasks:YES];
        if([[responseObject objectForKey:STATUS_RESPONSE] isEqualToString:@"OK"]){
            completion(YES, nil);
        }
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        [manager invalidateSessionCancelingTasks:YES];
        completion(NO, error);
    }];
}

+ (void)createFeedbackAtRestaurant:(Restaurant*)restaurant isHappy:(BOOL)isHappy comment:(NSString *)comment onCompletion:(void (^)(BOOL succeeded, NSError *error))completion {
    
    NSString *requestURL = [NSString stringWithFormat:@"%@%@", kServiceDomainURL, kServiceRestaurantFeedback];
    
    User *currentUser = [User currentUser];
    
    NSDictionary *json = @{
                           @"userId": currentUser.objectId,
                           @"restaurantId": restaurant.objectId,
                           @"isHappy": [NSNumber numberWithBool:isHappy],
                           @"comment": comment
                           };
    
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    manager.requestSerializer = [AFJSONRequestSerializer serializer];
    
    [manager POST:requestURL parameters:json success:^(NSURLSessionDataTask *task, id responseObject) {
        [manager invalidateSessionCancelingTasks:YES];
        if([[responseObject objectForKey:STATUS_RESPONSE] isEqualToString:@"OK"]){
            completion(YES, nil);
        }
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        [manager invalidateSessionCancelingTasks:YES];
        completion(NO, error);
    }];
}
@end
