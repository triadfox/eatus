//
//  ParseDataManager.m
//  MiPoBeacon
//
//  Created by phuc on 9/9/15.
//  Copyright (c) 2015 Triad Fox. All rights reserved.
//

#import "ParseDataManager.h"

@implementation ParseDataManager

#pragma mark - Helper methods
+ (NSString*)getDisplayNameOfPFFileName:(NSString*)pfFileName {
    //pfFileName is kind of :tffs-xxxxxxxx-xxxx-xxxx-xxxx-xxxxxxxx-displayName.zip
    NSArray* parts = [pfFileName componentsSeparatedByString: @"-"];
    return [parts lastObject];
}

#pragma mark - Restaurant
+ (void)getRestaurantById:(NSString*)restaurantId onCompletion:(void (^)(Restaurant*, NSError*))completion {
    
    PFQuery *query = [Restaurant query];
    [query getObjectInBackgroundWithId:restaurantId block:^(PFObject *object, NSError *error) {
        if (!error) {
            if (completion) {
                completion((Restaurant*)object, nil);
            }
        } else {
            // Log details of the failure
            NSLog(@"Error: %@ %@", error, [error userInfo]);
            if (completion) {
                completion(nil, error);
            }
        }
    }];
}

+ (void)getRestaurantByMajorId:(NSNumber*)majorId onCompletion:(void (^)(Restaurant*, NSError*))completion {
    
    PFQuery *query = [Restaurant query];
    [query whereKey:kBeaconMajorId equalTo:majorId];
    [query findObjectsInBackgroundWithBlock:^(NSArray *objects, NSError *error) {
        if (!error) {
            if (completion) {
                completion((Restaurant*)[objects firstObject], nil);
            }
        } else {
            // Log details of the failure
            NSLog(@"Error: %@ %@", error, [error userInfo]);
            if (completion) {
                completion(nil, error);
            }
        }
    }];
}

/* Parse just limit maximum 1000 records per query, default is 100
 https://parse.com/questions/fetch-all-data-in-a-table-using-pfquery
 If you want to get all, use recursion to do that job
 */
+ (void)getAllRestaurantsOnCompletion:(void (^)(NSArray*, NSError*))completion {
    
    PFQuery *query = [Restaurant query];
    [query setLimit:1000];
    [query findObjectsInBackgroundWithBlock:^(NSArray *objects, NSError *error) {
        if (!error) {
            if (completion) {
                completion(objects, nil);
            }
        } else {
            // Log details of the failure
            NSLog(@"Error: %@ %@", error, [error userInfo]);
            if (completion) {
                completion(nil, error);
            }
        }
    }];
}

/*
 This query is case sensitive and may slow in some cases. Look https://parse.com/docs/ios/guide#performance-regular-expressions
 Consider to use findPlacesHasPrefix which is fast 'cause of indices
 */
+ (void)getRestaurantsContainName:(NSString*)name onCompletion:(void (^)(NSArray*, NSError*))completion {
    
    PFQuery *query = [Restaurant query];
    [query setLimit:1000];
    [query whereKey:kName containsString:name];
    [query findObjectsInBackgroundWithBlock:^(NSArray *objects, NSError *error) {
        if (!error) {
            if (completion) {
                completion(objects, nil);
            }
        } else {
            // Log details of the failure
            NSLog(@"Error: %@ %@", error, [error userInfo]);
            if (completion) {
                completion(nil, error);
            }
        }
    }];
}

/* This query is case sensitive
 Example how to use:
 [Restaurant findPlacesHasPrefix:@"new" onCompletion:^(NSArray *restaurants, NSError *error) {
 if (error != nil) {
 NSLog(@"%d", restaurants.count);
 }
 }];
 */
+ (void)getRestaurantsHavePrefix:(NSString*)name onCompletion:(void (^)(NSArray*, NSError*))completion {
    
    PFQuery *query = [Restaurant query];
    [query whereKey:kName hasPrefix:name];
    [query findObjectsInBackgroundWithBlock:^(NSArray *objects, NSError *error) {
        if (!error) {
            if (completion) {
                completion(objects, nil);
            }
        } else {
            // Log details of the failure
            NSLog(@"Error: %@ %@", error, [error userInfo]);
            if (completion) {
                completion(nil, error);
            }
        }
    }];
}

+ (void)downloadOrUpdateMenuAtRestaurant:(Restaurant*)restaurant onCompletion:(void (^)(NSString*, NSError*))completion {
    
    if (!restaurant) {
        if (completion) {
            NSError *error = [NSError errorWithDomain:@"Undefined Restaurant" code:404 userInfo:nil];
            completion(@"", error);
        }
        return;
    }
    
    if (restaurant.menu == nil || [restaurant.menu isEqual:[NSNull null]]) {
        if (completion) {
            NSError *error = [NSError errorWithDomain:@"Restaurant hasn't had menu yet." code:404 userInfo:nil];
            completion(@"", error);
        }
        return;
    }
    
    //Create Menus dir if new
    NSString *menusDir = [Util getMenusDirWithMajor:restaurant.beaconMajorId];
    [Util createDirIfNeeded:menusDir];
    
    //read all files in path
    NSArray *files = [Util fileFromDirectory:menusDir];
    NSString *newFileName = [ParseDataManager getDisplayNameOfPFFileName:restaurant.menu.name];
    NSString *newMenuWithoutExt = [newFileName stringByDeletingPathExtension];
    
    if (files.count == 0) {
        //download file
        [_Shared downloadThenUncompressFile:restaurant.menu toPath:menusDir onCompletion:^(NSString *path, BOOL error) {
            if (completion) {
                completion([NSString stringWithFormat:@"%@/%@.%@", path, newMenuWithoutExt, EXTENSION_XML], nil);
            }
        }];
    } else {
        NSString *menuFound = @"";
        for (NSString *file in files) {
            NSString *extension = [file pathExtension];
            if ([extension caseInsensitiveCompare:EXTENSION_XML] == NSOrderedSame) {
                
                menuFound = file;
                
                NSString *oldMenuWithoutExt = [file stringByDeletingPathExtension];
                
                
                //no change, just return
                if ([oldMenuWithoutExt caseInsensitiveCompare:newMenuWithoutExt] == NSOrderedSame) {
                    if (completion) {
                        completion([NSString stringWithFormat:@"%@/%@", menusDir, file], nil);
                    }
                } else {
                    //remove old file
                    [Util deleteFileWithPath:[NSString stringWithFormat:@"%@/%@", menusDir, file]];
                    
                    //then download new file
                    [_Shared downloadThenUncompressFile:restaurant.menu toPath:menusDir onCompletion:^(NSString *path, BOOL error) {
                        if (completion) {
                            completion([NSString stringWithFormat:@"%@/%@.%@", path, newMenuWithoutExt, EXTENSION_XML], nil);
                        }
                    }];
                }
                
                break;
            }
        }
        
        if (menuFound.length == 0) {
            //download file
            [_Shared downloadThenUncompressFile:restaurant.menu toPath:menusDir onCompletion:^(NSString *path, BOOL error) {
                if (completion) {
                    completion([NSString stringWithFormat:@"%@/%@.%@", path, newMenuWithoutExt, EXTENSION_XML], nil);
                }
            }];
        }
    }
}

#pragma mark - Promotion
+ (void)getPromotionsByRestaurant:(Restaurant*)restaurant onCompletion:(void (^)(NSArray*, NSError*))completion {
    
    if (!restaurant) {
        if (completion) {
            NSError *error = [NSError errorWithDomain:@"Undefined Restaurant" code:404 userInfo:nil];
            completion(nil, error);
        }
        return;
    }
    
    PFQuery *query = [Promotion query];
    [query setLimit:1000];
    [query includeKey:kRestaurant];
    [query whereKey:kRestaurant equalTo:restaurant];
    [query findObjectsInBackgroundWithBlock:^(NSArray *promotions, NSError *error) {
        if (!error) {
            if (completion) {
                completion(promotions, nil);
            }
        } else {
            if (completion) {
                completion(nil, error);
            }
        }
    }];
}

+ (void)getPromotionsByRestaurantId:(NSString*)restaurantId onCompletion:(void (^)(NSArray*, NSError*))completion {
    
    if (restaurantId == nil || restaurantId.length == 0) {
        if (completion) {
            NSError *error = [NSError errorWithDomain:@"Undefined Restaurant" code:404 userInfo:nil];
            completion(nil, error);
        }
        return;
    }
    
    PFQuery *query = [Promotion query];
    [query setLimit:1000];
    [query includeKey:kRestaurant];
    [query whereKey:kRestaurant equalTo:[PFObject objectWithoutDataWithClassName:[Restaurant parseClassName] objectId:restaurantId]];
    
    [query findObjectsInBackgroundWithBlock:^(NSArray *promotions, NSError *error) {
        if (!error) {
            if (completion) {
                completion(promotions, nil);
            }
        } else {
            if (completion) {
                completion(nil, error);
            }
        }
    }];
}

+ (void)getAllPromotionOnCompletion:(void (^)(NSArray *, NSError *))completion{
    PFQuery *query = [Promotion query];
    [query setLimit:1000];
    [query findObjectsInBackgroundWithBlock:^(NSArray *promotions, NSError *error){
        if(!error){
            if(completion){
                completion(promotions, nil);
            }
        }else{
            if(completion){
                completion(nil, error);
            }
        }
    }];
}

+ (void)getReferedRestaurantByPromotion:(Promotion *)promotion completion:(void (^)(Restaurant *, NSError *))completion{
    [promotion.restaurant fetchIfNeededInBackgroundWithBlock:^(PFObject *restaurant, NSError *error){
        if(!error){
            if(completion){
                completion((Restaurant *)restaurant, nil);
            }
        }else{
            completion(nil, error);
        }
    }];
}

+(void)getAvailableTodayPromotionByRestaurantId:(NSString *)restaurantId onCompletion:(void (^)(NSArray *, NSError *))completion{
    if(restaurantId){
        PFQuery *query = [Promotion query];
        [query includeKey:kRestaurant];
        [query whereKey:kRestaurant equalTo:[PFObject objectWithoutDataWithClassName:[Restaurant parseClassName] objectId:restaurantId]];
        
        [query findObjectsInBackgroundWithBlock:^(NSArray *promotions, NSError *error){
            if(error){
                if(completion){
                    completion(nil, error);
                }
            }else{
                NSCalendar *calendar = [NSCalendar currentCalendar];
                NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
                dateFormatter.locale = [NSLocale currentLocale];
                dateFormatter.timeZone = calendar.timeZone;
                [dateFormatter setDateFormat:[NSDateFormatter dateFormatFromTemplate:@"ddMMyyyy hh:mm" options:0 locale:[NSLocale currentLocale]]];
                NSCalendarUnit calendarUnits = NSCalendarUnitYear | NSCalendarUnitMonth | NSCalendarUnitDay;
                NSDateComponents *components = [calendar components:calendarUnits fromDate:[NSDate date]];
                
                
                NSDate *currentDate = [[NSCalendar currentCalendar] dateFromComponents:components];
                
                NSMutableArray *availablePromotion = [[NSMutableArray alloc] init];
                
                for(Promotion *promotion in promotions){
                    NSDate *startDate = promotion.startDate;
                    NSDate *endDate = promotion.endDate;
                    
                    if(([currentDate compare:startDate] == NSOrderedDescending || NSOrderedSame) && (([currentDate compare:endDate] == NSOrderedAscending) | NSOrderedSame)){
                        [availablePromotion addObject:promotion];
                    }
                }
                
                if(completion){
                    completion(availablePromotion, nil);
                }
            }
        }];
    }
}

+ (void)getAvailableTodayPromotionByRestaurant:(Restaurant *)restaurant onCompletion:(void (^)(NSArray *, NSError *))completion{
    if(!restaurant){
        return;
    }
    
    PFQuery *query = [Promotion query];
    [query includeKey:kRestaurant];
    [query whereKey:kRestaurant equalTo:restaurant];
    
    [query findObjectsInBackgroundWithBlock:^(NSArray *promotions, NSError *error){
        if(error){
            if(completion){
                completion(nil, error);
            }
        }else{
            NSCalendar *calendar = [NSCalendar currentCalendar];
            NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
            dateFormatter.locale = [NSLocale currentLocale];
            dateFormatter.timeZone = calendar.timeZone;
            [dateFormatter setDateFormat:[NSDateFormatter dateFormatFromTemplate:@"ddMMyyyy hh:mm" options:0 locale:[NSLocale currentLocale]]];
            NSCalendarUnit calendarUnits = NSCalendarUnitYear | NSCalendarUnitMonth | NSCalendarUnitDay;
            NSDateComponents *components = [calendar components:calendarUnits fromDate:[NSDate date]];
            
            
            NSDate *currentDate = [[NSCalendar currentCalendar] dateFromComponents:components];
            
            NSMutableArray *availablePromotion = [[NSMutableArray alloc] init];
            
            for(Promotion *promotion in promotions){
                NSDate *startDate = promotion.startDate;
                NSDate *endDate = promotion.endDate;
                
                if(([currentDate compare:startDate] == NSOrderedDescending || NSOrderedSame) && (([currentDate compare:endDate] == NSOrderedAscending) | NSOrderedSame)){
                    [availablePromotion addObject:promotion];
                }
            }
            
            if(completion){
                completion(availablePromotion, nil);
            }
        }

    }];
}


#pragma mark - Trending
+ (void)logMeAtRestaurantInBackground:(Restaurant*)restaurant { //log current user
    User *currentUser = [User currentUser];
    if (currentUser != nil && restaurant != nil) {
        Trending *trending = [Trending object];
        [trending setUser:currentUser];
        [trending setRestaurant:restaurant];
        [trending saveInBackground];
    }
}

+ (void)getMyVisitsAtRestaurant:(Restaurant*)restaurant onCompletion:(void (^)(NSArray*, NSError*))completion {
    
    User *currentUser = [User currentUser];
    if (currentUser == nil || restaurant == nil) {
        if (completion) {
            NSError *error = [NSError errorWithDomain:@"Undefined User or Restaurant" code:404 userInfo:nil];
            completion(nil, error);
        }
        return;
    }
    
    PFQuery *query = [Trending query];
    [query setLimit:1000];
    [query includeKey:kUser];
    [query includeKey:kRestaurant];
    [query whereKey:kUser equalTo:currentUser];
    [query whereKey:kRestaurant equalTo:restaurant];
    
    [query findObjectsInBackgroundWithBlock:^(NSArray *logs, NSError *error) {
        if (!error) {
            if (completion) {
                completion(logs, nil);
            }
        } else {
            if (completion) {
                completion(nil, error);
            }
        }
    }];
}

+ (void)getMyVisitsAtRestaurantId:(NSString*)restaurantId onCompletion:(void (^)(NSArray*, NSError*))completion {
    
    User *currentUser = [User currentUser];
    if (currentUser == nil || restaurantId == nil || restaurantId.length == 0) {
        if (completion) {
            NSError *error = [NSError errorWithDomain:@"Undefined User or Restaurant" code:404 userInfo:nil];
            completion(nil, error);
        }
        return;
    }
    
    PFQuery *query = [Trending query];
    [query setLimit:1000];
    [query includeKey:kUser];
    [query includeKey:kRestaurant];
    [query whereKey:kUser equalTo:currentUser];
    [query whereKey:kRestaurant equalTo:[PFObject objectWithoutDataWithClassName:[Restaurant parseClassName] objectId:restaurantId]];
    
    [query findObjectsInBackgroundWithBlock:^(NSArray *logs, NSError *error) {
        if (!error) {
            if (completion) {
                completion(logs, nil);
            }
        } else {
            if (completion) {
                completion(nil, error);
            }
        }
    }];
}
+ (void)getMyVisitsAtRestaurantsOnCompletion:(void (^)(NSArray*, NSError*))completion {
    
    User *currentUser = [User currentUser];
    if (currentUser == nil) {
        if (completion) {
            NSError *error = [NSError errorWithDomain:@"Undefined User" code:404 userInfo:nil];
            completion(nil, error);
        }
        return;
    }
    
    PFQuery *query = [Trending query];
    [query setLimit:1000];
    [query includeKey:kUser];
    [query includeKey:kRestaurant];
    [query whereKey:kUser equalTo:currentUser];
    
    [query findObjectsInBackgroundWithBlock:^(NSArray *logs, NSError *error) {
        if (!error) {
            if (completion) {
                completion(logs, nil);
            }
        } else {
            if (completion) {
                completion(nil, error);
            }
        }
    }];
}
+ (void)getAllVisitsAtRestaurant:(Restaurant*)restaurant onCompletion:(void (^)(NSArray*, NSError*))completion {
    
    if (!restaurant) {
        if (completion) {
            NSError *error = [NSError errorWithDomain:@"Undefined Restaurant" code:404 userInfo:nil];
            completion(nil, error);
        }
        return;
    }
    
    PFQuery *query = [Trending query];
    [query setLimit:1000];
    [query includeKey:kUser];
    [query includeKey:kRestaurant];
    [query whereKey:kRestaurant equalTo:restaurant];
    
    [query findObjectsInBackgroundWithBlock:^(NSArray *logs, NSError *error) {
        if (!error) {
            if (completion) {
                completion(logs, nil);
            }
        } else {
            if (completion) {
                completion(nil, error);
            }
        }
    }];
}

+ (void)getAllVisitsAtRestaurantId:(NSString*)restaurantId onCompletion:(void (^)(NSArray*, NSError*))completion {
    
    if (restaurantId == nil || restaurantId.length == 0) {
        if (completion) {
            NSError *error = [NSError errorWithDomain:@"Undefined Restaurant" code:404 userInfo:nil];
            completion(nil, error);
        }
        return;
    }
    
    PFQuery *query = [Trending query];
    [query setLimit:1000];
    [query includeKey:kUser];
    [query includeKey:kRestaurant];
    [query whereKey:kRestaurant equalTo:[PFObject objectWithoutDataWithClassName:[Restaurant parseClassName] objectId:restaurantId]];
    
    [query findObjectsInBackgroundWithBlock:^(NSArray *logs, NSError *error) {
        if (!error) {
            if (completion) {
                completion(logs, nil);
            }
        } else {
            if (completion) {
                completion(nil, error);
            }
        }
    }];
}

#pragma mark - Feedback

+ (void)getFeedbacksAtRestaurant:(Restaurant*)restaurant onCompletion:(void (^)(NSArray*, NSError*))completion {
    
    if (!restaurant) {
        if (completion) {
            NSError *error = [NSError errorWithDomain:@"Undefined Restaurant" code:404 userInfo:nil];
            completion(nil, error);
        }
        return;
    }
    
    PFQuery *query = [Feedback query];
    [query whereKey:kRestaurant equalTo:restaurant];
    [query includeKey:kUser];
    [query includeKey:kRestaurant];
    [query findObjectsInBackgroundWithBlock:^(NSArray *feedbacks, NSError *error) {
        if (!error) {
            if (completion) {
                completion(feedbacks, nil);
            }
        } else {
            if (completion) {
                completion(nil, error);
            }
        }
    }];
}

+ (void)getFeedbacksAtRestaurantId:(NSString*)restaurantId onCompletion:(void (^)(NSArray*, NSError*))completion {
    
    if (restaurantId == nil || restaurantId.length == 0) {
        if (completion) {
            NSError *error = [NSError errorWithDomain:@"Undefined Restaurant" code:404 userInfo:nil];
            completion(nil, error);
        }
        return;
    }
    
    PFQuery *query = [Feedback query];
    [query includeKey:kUser];
    [query includeKey:kRestaurant];
    [query whereKey:kRestaurant equalTo:[PFObject objectWithoutDataWithClassName:[Restaurant parseClassName] objectId:restaurantId]];
    [query findObjectsInBackgroundWithBlock:^(NSArray *feedbacks, NSError *error) {
        if (!error) {
            if (completion) {
                completion(feedbacks, nil);
            }
        } else {
            if (completion) {
                completion(nil, error);
            }
        }
    }];
}

#pragma mark - Get tables at restaurant
+ (void)getTablesAtRestaurant:(Restaurant*)restaurant onCompletion:(void (^)(NSArray*, NSError*))completion {
    
    if (restaurant != nil) {
        
        PFQuery *query = [RestaurantTable query];
        [query setLimit:1000];
        [query includeKey:kRestaurant];
        [query includeKey:kBeacon];
        [query whereKey:kRestaurant equalTo:restaurant];
        
        [query findObjectsInBackgroundWithBlock:^(NSArray *tables, NSError *error) {
            if (!error) {
                if (completion) {
                    completion(tables, nil);
                }
            } else {
                if (completion) {
                    completion(nil, error);
                }
            }
        }];
        
    } else {
        if (completion) {
            NSError *error = [NSError errorWithDomain:@"Undefined Restaurant" code:404 userInfo:nil];
            completion(nil, error);
        }
        return;
    }
}
+ (void)getTablesAtRestaurantId:(NSString*)restaurantId onCompletion:(void (^)(NSArray*, NSError*))completion {
    
    if (restaurantId != nil && restaurantId.length > 0) {
        
        PFQuery *query = [RestaurantTable query];
        [query setLimit:1000];
        [query includeKey:kRestaurant];
        [query includeKey:kBeacon];
        [query whereKey:kRestaurant equalTo:[PFObject objectWithoutDataWithClassName:[Restaurant parseClassName] objectId:restaurantId]];
        
        [query findObjectsInBackgroundWithBlock:^(NSArray *tables, NSError *error) {
            if (!error) {
                if (completion) {
                    completion(tables, nil);
                }
            } else {
                if (completion) {
                    completion(nil, error);
                }
            }
        }];
        
    } else {
        if (completion) {
            NSError *error = [NSError errorWithDomain:@"Undefined Restaurant" code:404 userInfo:nil];
            completion(nil, error);
        }
        return;
    }
}
+ (void)getTableByBeaconMinorId:(NSNumber*)minorId onCompletion:(void (^)(NSArray*, NSError*))completion {
    
    if (minorId != nil) {
        
        PFQuery *query = [RestaurantTable query];
        [query setLimit:1000];
        [query includeKey:kRestaurant];
        [query includeKey:kBeacon];
        
        PFQuery *nonBlockedBeaconInnerQuery = [Beacon query];
        [nonBlockedBeaconInnerQuery setLimit:1000];
        [nonBlockedBeaconInnerQuery whereKey:@"minorId" equalTo:minorId];
        
        [query whereKey:kBeacon matchesQuery:nonBlockedBeaconInnerQuery];
        [query setLimit:1000];
        [query findObjectsInBackgroundWithBlock:^(NSArray *tables, NSError *error) {
            if (!error) {
                if (completion) {
                    completion(tables, nil);
                }
            } else {
                if (completion) {
                    completion(nil, error);
                }
            }
        }];
        
    } else {
        if (completion) {
            NSError *error = [NSError errorWithDomain:@"Undefined Beacon" code:404 userInfo:nil];
            completion(nil, error);
        }
        return;
    }
}

+(void)gettableByBeaconMinorid:(NSNumber *)minorId atRestaurant:(Restaurant *)restaurant onCompletion:(void (^)(RestaurantTable *, NSError *))completion{
    
    if(minorId){
        PFQuery *query = [RestaurantTable query];
        [query setLimit:1000];
        [query includeKey:kRestaurant];
        
        [query whereKey:kRestaurant equalTo:restaurant];
        [query whereKey:kRestaurantMinorId equalTo:minorId];
        
        [query findObjectsInBackgroundWithBlock:^(NSArray *tables, NSError *error){
            if(!error){
                if(completion){
                    completion([tables firstObject], nil);
                }
            }else{
                if(completion){
                    completion(nil, error);
                }
            }
        }];
    }
}

+(void)gettableByBeaconMinorid:(NSNumber *)minorId atRestaurantId:(NSString *)restaurantId onCompletion:(void (^)(RestaurantTable *, NSError *))completion{
    
}

#pragma mark - Beacon
+ (void)getBeaconByUUID:(NSString*)beaconUUID onCompletion:(void (^)(Beacon*, NSError*))completion {
    
    PFQuery *query = [Beacon query];
    [query whereKey:kProximityUUID equalTo:beaconUUID];
    [query findObjectsInBackgroundWithBlock:^(NSArray *beacons, NSError *error) {
        if (!error) {
            if (completion) {
                completion([beacons firstObject], nil);
            }
        } else {
            // Log details of the failure
            NSLog(@"Error: %@ %@", error, [error userInfo]);
            if (completion) {
                completion(nil, error);
            }
        }
    }];
}

+ (void)getBeaconByIdentifier:(NSString *)identifier onCompletion:(void (^)(Beacon *, NSError *))completion{
    PFQuery *query = [Beacon query];
    [query whereKey:kBeaconIdentifier equalTo:identifier];
    [query findObjectsInBackgroundWithBlock:^(NSArray *beacons, NSError *error) {
        if (!error) {
            if (completion) {
                completion([beacons firstObject], nil);
            }
        } else {
            // Log details of the failure
            NSLog(@"Error: %@ %@", error, [error userInfo]);
            if (completion) {
                completion(nil, error);
            }
        }
    }];
}

+ (void)updateBeaconStatusById:(NSString *)beaconId dataByKey:(NSDictionary *)dataByKey onCompletion:(void (^)(BOOL, NSError *))completion{
    if(!isNilOrEmpty(beaconId) && [dataByKey count] != 0){
        PFQuery *query = [Beacon query];
        [query getObjectInBackgroundWithId:beaconId block:^(PFObject *beacon, NSError *error){
            if(!error){
                for(NSString *key in dataByKey){
                    [beacon setObject:key forKey:[dataByKey objectForKey:key]];
                }
                [beacon saveInBackgroundWithBlock:^(BOOL succeeded, NSError *error){
                    if(completion){
                        completion(succeeded, error);
                    }
                }];
            }else{
                if(completion){
                    completion(NO, error);
                }
            }
        }];
    }
}

+(void)updateBeaconStatusByIdentifier:(NSString *)identifier dataByKey:(NSDictionary *)dataByKey onCompletion:(void (^)(BOOL, NSError *))completion{
    if(!isNilOrEmpty(identifier) && [dataByKey count] != 0){
        PFQuery *query = [Beacon query];
        [query whereKey:kBeaconIdentifier equalTo:identifier];
        
        [query findObjectsInBackgroundWithBlock:^(NSArray *beacons, NSError *error){
            if(!error){
                Beacon *beacon = [beacons firstObject];
                for(NSString *key in dataByKey){
                    [beacon setObject:[dataByKey objectForKey:key] forKey:key];
                }
                [beacon saveInBackgroundWithBlock:^(BOOL succeeded, NSError *error){
                    if(completion){
                        completion(succeeded, error);
                    }
                }];
            }else{
                if(completion){
                    completion(NO, error);
                }
            }
        }];
    }
}

+ (void)updateBeaconStatusByUUID:(NSString *)beaconUUID majorId:(NSString *)majorId minorId:(NSString *)minorId dataByKey:(NSDictionary *)dataByKey onCompletion:(void (^)(BOOL, NSError *))completion {
    
    if(!isNilOrEmpty(beaconUUID) && [dataByKey count] != 0){
        PFQuery *query = [Beacon query];
        [query whereKey:kProximityUUID equalTo:beaconUUID];
        [query whereKey:kBeaconBMajorId equalTo:majorId];
        [query whereKey:kBeaconBMinorId equalTo:minorId];
        
        [query findObjectsInBackgroundWithBlock:^(NSArray *beacons, NSError *error){
            if(!error){
                Beacon *beacon = [beacons firstObject];
                for(NSString *key in dataByKey){
                    [beacon setObject:key forKey:[dataByKey objectForKey:key]];
                }
                [beacon saveInBackgroundWithBlock:^(BOOL succeeded, NSError *error){
                    if(completion){
                        completion(succeeded, error);
                    }
                }];
            }else{
                if(completion){
                    completion(NO, error);
                }
            }
        }];
    }
}


#pragma mark - Order & Order details
+ (void)createOrderAtTable:(RestaurantTable*)table withOrderDetail:(NSString*)orderDetail onCompletion:(void (^)(Order*, NSError*))completion {
    
    User *currentUser = [User currentUser];
    
    if (table != nil && currentUser != nil) {
        Order *order = [Order object];
        order.orderDetail = orderDetail;
        order.status = Pending;
        [order setUser:currentUser];
        [order setRestaurantTable:table];
        
        [order saveInBackgroundWithBlock:^(BOOL succeeded, NSError *error) {
            if (succeeded) {
                if (completion) {
                    completion(order, nil);
                }
            }
            else if (completion) {
                completion(nil, error);
            }
        }];
    }
}

+(void)getAllOrderOfCurrentUserOnCompletion:(void (^)(NSArray *, NSError *))completion{
    User *currentUser = [User currentUser];
    
    if(currentUser){
        PFQuery *query = [Order query];
        [query includeKey:kUser];
        [query whereKey:kUser equalTo:currentUser];
        
        [query findObjectsInBackgroundWithBlock:^(NSArray *receipts, NSError *error){
            if(!error){
                if(completion){
                    completion(receipts, nil);
                }
            }else{
                if(completion){
                    completion(nil, error);
                }
            }
        }];
    }
}

+(void)getAllOrderOfCurrentUserWithStatus:(OrderStatus)status onCompletion:(void (^)(NSArray *, NSError *))completion{
    User *currentUser = [User currentUser];
    
    if(currentUser){
        PFQuery *query = [Order query];
        [query includeKey:kUser];
        [query includeKey:kReceiptStatus];
        
        [query whereKey:kUser equalTo:currentUser];
        [query whereKey:kReceiptStatus equalTo:[NSNumber numberWithInteger:status]];
        
        [query findObjectsInBackgroundWithBlock:^(NSArray *recetips, NSError *error){
            if(!error){
                if(completion){
                    completion(recetips, nil);
                }
            }else{
                if(completion){
                    completion(nil, error);
                }
            }
        }];
    }
}

+(void)orderForIdOfCurrentUser:(NSString *)orderId onCompletion:(void (^)(Order *, NSError *))completion{
    User *currentUser = [User currentUser];
    
    if(currentUser){
        PFQuery *query = [Order query];
        [query getObjectInBackgroundWithId:orderId block:^(PFObject *order, NSError *error){
            if(!error){
                if(completion){
                    completion((Order *)order, nil);
                }
            }else{
                if(completion){
                    completion(nil, error);
                }
            }
        }];
    }
}


#pragma mark - User
+ (void)updateAvatar:(UIImage*)image onCompletion:(void (^)(BOOL, NSError*))completion {
    
    [[PFUser currentUser] fetchIfNeededInBackgroundWithBlock:^(PFObject *curUser, NSError *error) {
        
        PFFile *imageFile;
        
        if(image != nil) {
            NSData *imageData = UIImagePNGRepresentation(image);
            if(imageData) {
                imageFile = [PFFile fileWithName:[NSString stringWithFormat:@"%f.png",[[NSDate date] timeIntervalSince1970]] data:imageData];
                [curUser setObject:imageFile forKey:kAvatar];
            }
        } else {
            [curUser removeObjectForKey:kAvatar];
        }
        
        
        [curUser saveInBackgroundWithBlock:^(BOOL succeeded, NSError *error) {
            completion(succeeded, error);
        }];
    }];
}

+ (void)updatePaymentTokenId:(NSString *)tokenId last4Id: (NSString *)last4Id onCompletion:(void (^)(BOOL, NSError *))completion{
    [[PFUser currentUser] fetchIfNeededInBackgroundWithBlock:^(PFObject *currentUser, NSError *error){
        if(tokenId && last4Id){
            [currentUser setObject:tokenId forKey:kPaymentTokenId];
            [currentUser setObject:last4Id forKey:kPayment4LastId];
        }else{
            [currentUser removeObjectForKey:kPaymentTokenId];
            [currentUser removeObjectForKey:kPayment4LastId];
        }

        [currentUser saveInBackgroundWithBlock:^(BOOL succeeded, NSError *error){
            completion(succeeded, error);
        }];
    }];
}

+ (void)logoutWithOnCompletion:(void (^)(BOOL, NSError*))completion{
//    PFInstallation *currentInstallation = [PFInstallation currentInstallation];
//    [currentInstallation setObject:[NSNull null] forKey:kUser];
//    [currentInstallation saveInBackgroundWithBlock:^(BOOL succeeded, NSError *error) {
//        completion(succeeded, error);
//    }];
    
    [PFUser logOutInBackgroundWithBlock:^(NSError *error){
        if(error){
            if(completion){
                completion(NO, error);
            }
        }else{
            if(completion) {
                completion(YES, nil);
            }
        }
    }];
}

+ (void)getAvatarFromUser:(User *)user onCompletion:(void (^)(UIImage *, NSError *))completion{
    [user fetchIfNeededInBackgroundWithBlock:^(PFObject *user, NSError *error){
        [user[kAvatar] getDataInBackgroundWithBlock:^(NSData *data, NSError *error){
            if(!error){
                if(data){
                    completion([UIImage imageWithData:data], nil);
                }else{
                    completion(nil, error);
                }
            }
        }];
    }];
}


#pragma mark - Card Payment
+ (void)registerMyCardWithTokenId:(NSString *)tokenId last4Id:(NSString *)last4Id onCompletion:(void (^)(BOOL, NSError *))completion{
    User *currentUser = [User currentUser];
    if(!currentUser){
        return;
    }
    
    if(!isNilOrEmpty(tokenId) && !isNilOrEmpty(last4Id)){
        CardPayment *card = [CardPayment object];
        [card setTokenId:tokenId];
        [card setLast4Id:last4Id];
        [card setUser:currentUser];
        
        [card saveInBackgroundWithBlock:^(BOOL succeeded, NSError *error){
            completion(succeeded, error);
        }];
    }
}

+ (void)getAllMyCardLast4IdOnCompletion:(void (^)(NSArray *, NSError *))completion{
    PFQuery *cardQuery = [CardPayment query];
    [cardQuery setLimit:1000];
    [cardQuery includeKey:kUser];
    [cardQuery whereKey:kUser equalTo:[User currentUser]];
    
    [cardQuery findObjectsInBackgroundWithBlock:^(NSArray *cards, NSError *error){
        if(!error){
            completion(cards, error);
        }else{
            completion(nil, error);
        }
    }];
}

+ (void)stripeTokenWithLast4Id:(NSString *)last4Id onCompletion:(void (^)(NSString *, NSError *))completion{
    if(!last4Id){
        return;
    }
    
    PFQuery *query = [CardPayment query];
    [query setLimit:1000];
    [query includeKey:kUser];
    [query includeKey:kLast4Id];
    
    [query whereKey:kUser equalTo:[User currentUser]];
    [query whereKey:kLast4Id equalTo:last4Id];
    
    [query findObjectsInBackgroundWithBlock:^(NSArray *cards, NSError *error){
        if(!error){
            if(completion){
                completion([cards firstObject], error);
            }
        }else{
            completion(nil, error);
        }
    }];
}
@end
