//
//  CloudinaryShared.h
//  Eatus
//
//  Created by Phong Nguyen on 11/9/15.
//  Copyright © 2015 Triad Fox. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "CLCloudinary.h"

@interface CloudinaryShared : NSObject

+(CLCloudinary *)shareCloudinary;

@end
