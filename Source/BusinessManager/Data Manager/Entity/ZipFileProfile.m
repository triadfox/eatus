//
//  ZipFileProfile.m
//  Light House
//
//  Created by Phong Nguyen on 9/21/15.
//  Copyright © 2015 Triad Fox. All rights reserved.
//

#import "ZipFileProfile.h"

@implementation ZipFileProfile

-(id)initWithZipFileName:(NSString *)zipFileName createdDate:(NSDate *)zipFileDate filedSize:(NSUInteger)fileSize compressionLevel:(ZipCompressionLevel)zipFileLevel{
    ZipFileProfile *profile = [[ZipFileProfile alloc] init];
    profile.zipFileName = zipFileName;
    profile.zipFileDate = zipFileDate;
    profile.zipFileSize = fileSize;
    profile.zipFileLevel = zipFileLevel;
    
    return profile;
}
@end
