//
//  ZipFileProfile.h
//  Light House
//
//  Created by Phong Nguyen on 9/21/15.
//  Copyright © 2015 Triad Fox. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ZipFile.h"

@interface ZipFileProfile : NSObject

@property (nonatomic, strong) NSString *zipFileName;
@property (nonatomic, strong) NSDate *zipFileDate;
@property (nonatomic, assign) NSUInteger zipFileSize;
@property (nonatomic, assign) ZipCompressionLevel zipFileLevel;

-(id)initWithZipFileName: (NSString *)zipFileName createdDate: (NSDate *)zipFileDate filedSize: (NSUInteger)fileSize compressionLevel: (ZipCompressionLevel)zipFileLevel;
@end
