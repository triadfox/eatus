//
//  CloudinaryShared.m
//  Eatus
//
//  Created by Phong Nguyen on 11/9/15.
//  Copyright © 2015 Triad Fox. All rights reserved.
//

#import "CloudinaryShared.h"

#define CLOUDINARY_NAME_PREFIX                     @"cloud_name"
#define CLOUDINARY_API_KEY_PREFIX                      @"api_key"
#define CLOUDINARY_API_SECRET_PREFIX                   @"api_secret"

#define CLOUDINARY_NAME                         @"triad-fox"
#define CLOUDINARY_API_KEY                      @"887442219142747"
#define CLOUDINARY_API_SECRET                   @"hBmFw4fufdRYDWkrojdoFD_G2t8"
#define CLOUDINARY_URL_BASED_DELIVERY           @"http://res.cloudinary.com/triad-fox"
#define CLOUDINARY_URL_SECURED_DELIVERY         @"https://res.cloudinary.com/triad-fox"
#define CLOUDINARY_URL_BASED_API                @"https://api.cloudinary.com/v1_1/triad-fox"

@implementation CloudinaryShared

+(CLCloudinary *)shareCloudinary{
    static CLCloudinary *cloudinary;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        cloudinary = [[CLCloudinary alloc] init];
        [cloudinary.config setValue:CLOUDINARY_NAME forKey:CLOUDINARY_NAME_PREFIX];
        [cloudinary.config setValue:CLOUDINARY_API_KEY forKey:CLOUDINARY_API_KEY_PREFIX];
        [cloudinary.config setValue:CLOUDINARY_API_SECRET forKey:CLOUDINARY_API_SECRET_PREFIX];
    });
    return cloudinary;
}
@end
