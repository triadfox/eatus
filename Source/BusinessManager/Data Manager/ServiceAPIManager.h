//
//  ServiceAPIManager.h
//  Light Order
//
//  Created by phuc on 10/18/15.
//  Copyright (c) 2015 Triad Fox. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ServiceAPIManager : NSObject

+ (void)createOrderRequest:(NSString*)orderJson withRestaurantId:(NSString*)restaurantId atTableId: (NSString *)tableId withTableName: (NSString *)tableName paymentType: (NSUInteger)payment onCompletion:(void (^)(BOOL succeeded, NSError* error))completion;
+ (void)createFeedbackAtRestaurant:(Restaurant*)restaurant isHappy:(BOOL)isHappy comment:(NSString *)comment onCompletion:(void (^)(BOOL succeeded, NSError *error))completion;

@end
