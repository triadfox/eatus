//
//  ParseDataManager.h
//  MiPoBeacon
//
//  Created by phuc on 9/9/15.
//  Copyright (c) 2015 Triad Fox. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ParseModel.h"

@interface ParseDataManager : NSObject

#pragma mark - Helper methods
+ (NSString*)getDisplayNameOfPFFileName:(NSString*)pfFileName;

#pragma mark - Restaurant
+ (void)getRestaurantById:(NSString*)restaurantId onCompletion:(void (^)(Restaurant*, NSError*))completion;
+ (void)getRestaurantByMajorId:(NSNumber*)majorId onCompletion:(void (^)(Restaurant*, NSError*))completion;
+ (void)getRestaurantsContainName:(NSString*)name onCompletion:(void (^)(NSArray*, NSError*))completion;
+ (void)getRestaurantsHavePrefix:(NSString*)name onCompletion:(void (^)(NSArray*, NSError*))completion;
+ (void)getAllRestaurantsOnCompletion:(void (^)(NSArray*, NSError*))completion;
+ (void)downloadOrUpdateMenuAtRestaurant:(Restaurant*)restaurant onCompletion:(void (^)(NSString*, NSError*))completion;

#pragma mark - Promotion
+ (void)getPromotionsByRestaurant:(Restaurant*)restaurant onCompletion:(void (^)(NSArray*, NSError*))completion;
+ (void)getPromotionsByRestaurantId:(NSString*)restaurantId onCompletion:(void (^)(NSArray*, NSError*))completion;
+ (void)getAllPromotionOnCompletion: (void (^)(NSArray *, NSError *))completion;
+ (void)getReferedRestaurantByPromotion: (Promotion *)promotion completion: (void (^)(Restaurant *restaurant, NSError *error))completion;
+ (void)getAvailableTodayPromotionByRestaurantId: (NSString *)restaurantId onCompletion: (void (^)(NSArray *promotions, NSError *error))completion;
+ (void)getAvailableTodayPromotionByRestaurant: (Restaurant *)restaurant onCompletion: (void (^)(NSArray *promotions, NSError *error))completion;
//+ (void)getAvailableTodayPromotionByRestaurantMajorId: (NSNumber *)majorId onCompletion: (void (^)(NSArray *promotions, NSError *error))completion;

#pragma mark - Log current user at restaurant (Trending)
+ (void)logMeAtRestaurantInBackground:(Restaurant*)restaurant; //log current user
+ (void)getMyVisitsAtRestaurant:(Restaurant*)restaurant onCompletion:(void (^)(NSArray*, NSError*))completion;
+ (void)getMyVisitsAtRestaurantId:(NSString*)restaurantId onCompletion:(void (^)(NSArray*, NSError*))completion;
+ (void)getMyVisitsAtRestaurantsOnCompletion:(void (^)(NSArray*, NSError*))completion;
+ (void)getAllVisitsAtRestaurant:(Restaurant*)restaurant onCompletion:(void (^)(NSArray*, NSError*))completion;
+ (void)getAllVisitsAtRestaurantId:(NSString*)restaurantId onCompletion:(void (^)(NSArray*, NSError*))completion;

#pragma mark - Feedback
+ (void)getFeedbacksAtRestaurant:(Restaurant*)restaurant onCompletion:(void (^)(NSArray*, NSError*))completion;
+ (void)getFeedbacksAtRestaurantId:(NSString*)restaurantId onCompletion:(void (^)(NSArray*, NSError*))completion;

#pragma mark - Get tables at restaurant
+ (void)getTablesAtRestaurant:(Restaurant*)restaurant onCompletion:(void (^)(NSArray*, NSError*))completion;
+ (void)getTablesAtRestaurantId:(NSString*)restaurantId onCompletion:(void (^)(NSArray*, NSError*))completion;
+ (void)getTableByBeaconMinorId:(NSNumber*)minorId onCompletion:(void (^)(NSArray*, NSError*))completion;
+ (void)gettableByBeaconMinorid:(NSNumber *)minorId atRestaurant: (Restaurant *)restaurant onCompletion: (void (^)(RestaurantTable *table, NSError *error))completion;
+ (void)gettableByBeaconMinorid:(NSNumber *)minorId atRestaurantId: (NSString *)restaurantId onCompletion: (void (^)(RestaurantTable *table, NSError *error))completion;


#pragma mark - Beacon
+ (void)getBeaconByUUID:(NSString*)beaconUUID onCompletion:(void (^)(Beacon*, NSError*))completion;
+ (void)getBeaconByIdentifier: (NSString *)identifier onCompletion: (void (^)(Beacon *beacon, NSError *error))completion;
+ (void)updateBeaconStatusByUUID: (NSString *)beaconUUID majorId: (NSString *)majorId minorId: (NSString *)minorId dataByKey: (NSDictionary *)dataByKey onCompletion: (void (^)(BOOL succeeded, NSError *error))completion;
+ (void)updateBeaconStatusById: (NSString *)beaconId dataByKey: (NSDictionary *)dataByKey onCompletion: (void (^)(BOOL succeeded, NSError *error))completion;
+ (void)updateBeaconStatusByIdentifier: (NSString *)identifier dataByKey: (NSDictionary *)dataByKey onCompletion: (void (^)(BOOL succeeded, NSError *error))completion;

#pragma mark - Order
+ (void)createOrderAtTable:(RestaurantTable*)table withOrderDetail:(NSString*)orderDetail onCompletion:(void (^)(Order*, NSError*))completion;
+ (void)getAllOrderOfCurrentUserOnCompletion: (void (^)(NSArray *orders, NSError *error))completion;
+ (void)getAllOrderOfCurrentUserWithStatus: (OrderStatus)status onCompletion: (void (^)(NSArray *receipts, NSError *error))completion;
+ (void)orderForIdOfCurrentUser: (NSString *)orderId onCompletion: (void (^)(Order *order, NSError *error))completion;


#pragma mark - User
+ (void)updateAvatar:(UIImage*)image onCompletion:(void (^)(BOOL, NSError*))completion;
+ (void)logoutWithOnCompletion:(void (^)(BOOL, NSError*))completion;
+ (void)getAvatarFromUser: (User *)user onCompletion: (void (^)(UIImage *image, NSError *error))completion;


#pragma mark - Card Payment
+ (void)registerMyCardWithTokenId: (NSString *)tokenId last4Id: (NSString *)last4Id onCompletion: (void (^)(BOOL succeeded, NSError *error))completion;
+ (void)getAllMyCardLast4IdOnCompletion: (void (^)(NSArray *cards, NSError *error))completion;
+ (void)stripeTokenWithLast4Id: (NSString *)last4Id onCompletion: (void (^)(NSString *tokenId, NSError *error))completion;
@end
