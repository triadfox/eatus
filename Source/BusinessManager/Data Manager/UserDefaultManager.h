//
//  UserDefaultManager.h
//  MiPoBeacon
//
//  Created by phuc on 9/2/15.
//  Copyright (c) 2015 MiPo Consolidate LLC. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface UserDefaultManager : NSObject

+ (BOOL)isFirstTimeUsingApp;
+ (void)setFirstTimeUsingApp:(BOOL)firstTime;

+ (void)setDeviceToken:(NSString*)deviceToken;
+ (NSString*)getDeviceToken;
+ (void)setDeviceTokenInNSData:(NSData*)deviceToken;
+ (NSData*)getDeviceTokenInNSData;

+ (NSString*)getCurrentUserName;
+ (void)setCurrentUserName:(NSString *)userName;
+ (NSString*)getCurrentUserPassword;
+ (void)setCurrentUserPassword:(NSString *)password;

@end
