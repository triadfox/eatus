//
//  SessionManager.m
//  MiPoBeacon
//
//  Created by phuc on 9/2/15.
//  Copyright (c) 2015 MiPo Consolidate LLC. All rights reserved.
//

#import "SharedManager.h"

#import <ParseFacebookUtilsV4/ParseFacebookUtilsV4.h>
#import <FBSDKCoreKit/FBSDKCoreKit.h>

#import "ZipFileProfile.h"
#import "FileInZipInfo.h"
#import "CloudinaryShared.h"

#define DEFAULT_DATA_LENGTH                     5242880

#define CLOUDINARY_NAME_PREFIX                     @"cloud_name"
#define CLOUDINARY_API_KEY_PREFIX                      @"api_key"
#define CLOUDINARY_API_SECRET_PREFIX                   @"api_secret"

#define CLOUDINARY_NAME                         @"triad-fox"
#define CLOUDINARY_API_KEY                      @"887442219142747"
#define CLOUDINARY_API_SECRET                   @"hBmFw4fufdRYDWkrojdoFD_G2t8"
#define CLOUDINARY_URL_BASED_DELIVERY           @"http://res.cloudinary.com/triad-fox"
#define CLOUDINARY_URL_SECURED_DELIVERY         @"https://res.cloudinary.com/triad-fox"
#define CLOUDINARY_URL_BASED_API                @"https://api.cloudinary.com/v1_1/triad-fox"

@interface SharedManager()

@property (strong, nonatomic) MRActivityIndicatorView *activityIndicatorView;
@property (nonatomic, strong) MRCircularProgressView *circularProgressView;
@property (nonatomic, strong) MRProgressOverlayView *progressOverlayView;
@property (nonatomic, strong) MRNavigationBarProgressView *navbarProgressView;
@property(nonatomic, strong) CLCloudinary *cloudinary;

@end

@implementation SharedManager

- (instancetype)init {
    if (self = [super init]) {
        
    }
    return self;
}

+ (SharedManager*)sharedInstance {
    static SharedManager* sharedInstance = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedInstance = [SharedManager new];
    });
    return sharedInstance;
}


#pragma mark - MRProgress View
-(MRActivityIndicatorView *)showIndependentActivityIndicatorViewWithTintColor:(UIColor *)tintcolor frame:(CGRect)frame{
    MRActivityIndicatorView *activityIndicator = [[MRActivityIndicatorView alloc] initWithFrame:frame];
    [activityIndicator setTintColor:tintcolor];
    [activityIndicator startAnimating];
    return activityIndicator;
}

-(void)showOverlayIndetermineProgressViewWithTitle:(NSString *)title tintColor:(UIColor *)tintColor onView:(UIView *)view{
    if(_progressOverlayView){
        [_progressOverlayView hide:YES];
    }
    if(view){
        _progressOverlayView = [MRProgressOverlayView showOverlayAddedTo:view title:title mode:MRProgressOverlayViewModeIndeterminate animated:YES];
        [_progressOverlayView setTintColor:tintColor];
    }
}

-(void)hideOverlayIndetermineProgressViewAfter:(float)delays{
    if(_progressOverlayView){
        [self performBlock:^{
            [_progressOverlayView dismiss:YES completion:^{
                
            }];
        }afterDelay:delays];
    }
}

-(void)showOverlayIndetermineSmalProgressViewWithTitle:(NSString *)title style:(OverlayStyle)style tintColor:(UIColor *)tintColor onView:(UIView *)view{
    if(_progressOverlayView){
        [_progressOverlayView dismiss:YES completion:nil];
    }
    
    if(view){
        _progressOverlayView = [MRProgressOverlayView showOverlayAddedTo:view title:title mode:MRProgressOverlayViewModeIndeterminate animated:YES];
        if(style == kOverlayDefaultStyle){
            _progressOverlayView.mode = MRProgressOverlayViewModeIndeterminateSmallDefault;
        }else{
            _progressOverlayView.mode = MRProgressOverlayViewModeIndeterminateSmall;
        }
        [_progressOverlayView setTintColor:tintColor];
    }
}

-(void)showSucceedCheckMarkProcessViewWithTitle:(NSString *)title tintColor:(UIColor *)tintColor onView:(UIView *)view{
    if(_progressOverlayView){
        [_progressOverlayView dismiss:YES completion:nil];
    }
    
    if(view){
        _progressOverlayView = [MRProgressOverlayView showOverlayAddedTo:view animated:YES];
        _progressOverlayView.mode = MRProgressOverlayViewModeCheckmark;
        _progressOverlayView.tintColor = tintColor;
        _progressOverlayView.titleLabelText = title;
    }
    
    [self performBlock:^{
        [_progressOverlayView dismiss:YES];
    }afterDelay:2.0f];
}

-(void)showFailureCrossProcessViewWithTitle:(NSString *)title tintColor:(UIColor *)tintColor onView:(UIView *)view{
    if(_progressOverlayView){
        [_progressOverlayView dismiss:YES completion:nil];
    }
    
    if(view){
        _progressOverlayView = [MRProgressOverlayView showOverlayAddedTo:view animated:YES];
        _progressOverlayView.mode = MRProgressOverlayViewModeCross;
        _progressOverlayView.tintColor = tintColor;
        _progressOverlayView.titleLabelText = title;
    }
    [self performBlock:^{
        [_progressOverlayView dismiss:YES];
    }afterDelay:2.0f];
}

- (void)performBlock:(void(^)())block afterDelay:(NSTimeInterval)delay {
    dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, (int64_t)(delay * NSEC_PER_SEC));
    dispatch_after(popTime, dispatch_get_main_queue(), block);
}


#pragma mark - User
- (void)loginOrCreateUser:(void (^)(BOOL, NSError*))completion {

    //User already logged in
    if ([User currentUser]) {
        if (completion) {
            completion(YES, nil);
        }
    } else {
        NSString *username = [UserDefaultManager getCurrentUserName];
        NSString *password = [UserDefaultManager getCurrentUserPassword];
        
        if (username && password) {
            //If user exists but not logged in -> log him/her in
            [User logInWithUsernameInBackground:username password:password block:^(PFUser *user, NSError *error) {
                if (!error) {
                    if (completion) {
                        completion(YES, nil);
                    }
                }
                else {
                    if (completion) {
                        NSLog(@"Failed: %@", [error localizedFailureReason]);
                        completion(NO, error);
                    }
                }
            }];
        } else {
            //If user doesn't exist, temporarily create one for him/her
            NSString *deviceUUID = [Util getDeviceUUID];
            [UserDefaultManager setCurrentUserName:deviceUUID];
            [UserDefaultManager setCurrentUserPassword:deviceUUID];
            
            User *user = [User object];
            user.username = deviceUUID;
            user.password = deviceUUID;
            user.deviceUUID = deviceUUID;
            
            [user signUpInBackgroundWithBlock:^(BOOL succeeded, NSError *err) {
                if (succeeded) {
                    
                    [_Shared updateInstallationUser];
                    
                    if (completion) {
                        completion(YES, nil);
                    }
                }
                else {
                    if (completion) {
                        NSLog(@"Failed: %@", [err localizedFailureReason]);
                        completion(NO, err);
                    }
                }
            }];
        }
    }
}

- (void)updateInstallationUser {
    if ([User currentUser]) {
        PFInstallation *currentInstallation = [PFInstallation currentInstallation];
        if (currentInstallation[@"user"] == nil) {
            currentInstallation[@"user"] = [User currentUser];
            [currentInstallation saveInBackground];
        }
    }
}

#pragma mark - Facebook profile
- (void)getFacebookAvatar:(void (^)(BOOL, NSError*))completion {
    NSString *accessToken = [FBSDKAccessToken currentAccessToken].tokenString;
    __block NSData *received_data = [[NSMutableData alloc] init];
    NSString *avatarUrl = [NSString stringWithFormat:kFacebookPictureUrl, accessToken];
    
    NSURLSession *session = [NSURLSession sharedSession];
    [[session dataTaskWithURL:[NSURL URLWithString:avatarUrl]
            completionHandler:^(NSData *data,
                                NSURLResponse *response,
                                NSError *error) {
                // handle response
                if (error == nil) {
                    
                    received_data = data;
                    PFFile *imageFile = [PFFile fileWithName:[NSString stringWithFormat:@"%f.png",[[NSDate date] timeIntervalSince1970]] data:data];
                    [[User currentUser] setObject:imageFile forKey:kAvatar];
                    completion(YES, nil);
                } else {
                    completion(NO, error);
                }
                
            }] resume];
    
}


- (void)getFacebookProfile: (void (^)(BOOL, NSError*))completion {
    FBSDKGraphRequest *request = [[FBSDKGraphRequest alloc] initWithGraphPath:@"me"
                                                                   parameters:kProfileParameters];
    [request startWithCompletionHandler:^(FBSDKGraphRequestConnection *connection, id result, NSError *errorFacebook) {
        
        if (!errorFacebook) {
            NSLog(@"result: %@",result);
            NSString *birthDay = [result objectForKey:kBirthday];
            NSString *fullName = [result objectForKey:kFBName];
            NSString *facebookId = [result objectForKey:@"id"];
            
            NSString *yearString = [birthDay substringFromIndex:birthDay.length - 4];
            NSNumber *year = [NSNumber numberWithInt:[yearString intValue]];
            
            [[User currentUser]setYearOfBirth:year];
            [[User currentUser]setFullname:fullName];
            [[User currentUser]setFacebookId:facebookId];
            
            completion(YES, nil);
        } else {
            completion(NO, errorFacebook);
        }
    }];
}

#pragma mark - Download
- (void)downloadThenUncompressFile:(PFFile*)file toPath:(NSString*)path onCompletion:(void (^)(NSString*, BOOL error))completion {
    
    if (file == nil) {
        if (completion) {
            completion(@"", YES);
        }
        return;
    }
    
    // save map into file system
    [file getDataInBackgroundWithBlock:^(NSData *data, NSError *err) {
        
        // Writing NSData into a file is an IO operation that may block the main thread. Especially if the data object is large.
        // Use GCD's background queue
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0), ^{

            [Util createDirIfNeeded:path];
            
            NSString *fileName = [ParseDataManager getDisplayNameOfPFFileName:file.name];
            
            // Generate the file path
            NSString *dataPath = [NSString stringWithFormat:@"%@/%@", path, fileName];
            
            // save map file
            [data writeToFile:dataPath atomically:YES];
            
            // unzip
            [_Shared uncompressZipFileAtPath:dataPath deleteZip:YES completion:^(NSString *pathToDirectory, BOOL error) {
                if (completion) {
                    completion(pathToDirectory, error);
                }
            }];
        });
    }];
}

#pragma mark - Compress & Uncompress
-(void)uncompressZipFileAtPath:(NSString *)filePath deleteZip:(BOOL)deleteZip completion:(UnCompressCompletion)completion{
    if(isNilOrEmpty(filePath)){
        completion(nil, YES);
        return;
    }
    
    //first create a directory named after zip file
    NSString *zipFileWithExtension = [[filePath componentsSeparatedByString:@"/"] lastObject];
    //NSString *zipFileName = [[zipFileWithExtension componentsSeparatedByString:@"."] firstObject];
    
    NSString *pathToDirectory = [filePath stringByReplacingOccurrencesOfString:zipFileWithExtension withString:@""];
    [Util createDirIfNeeded:pathToDirectory];
    
    //append block to concurrent queue background
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0), ^{
        dispatch_group_t uncompressGroup = dispatch_group_create();
        
        NSMutableArray *arrayOfFile = [[NSMutableArray alloc] init];
        
        ZipFile *fileToUncompress = [[ZipFile alloc] initWithFileName:filePath mode:ZipFileModeUnzip];
        
        dispatch_group_enter(uncompressGroup);
        NSArray *filesProfile = [self enumerateOnZipFileInfo:[fileToUncompress listFileInZipInfos]];
        
        [fileToUncompress goToFirstFileInZip];
        NSData *data = [self zipStreamReadWithFile:fileToUncompress];
        [arrayOfFile addObject:data];
        
        int i = 1;
        while ([fileToUncompress goToNextFileInZip]) {
            NSData *data = [self zipStreamReadWithFile:fileToUncompress];
            [arrayOfFile addObject:data];
            i += 1;
        }
        NSDictionary *fileWithNameIncoporate = [self incorporateFileWithName:arrayOfFile names:filesProfile];
        
        if([fileWithNameIncoporate count] > 0){
            dispatch_group_leave(uncompressGroup);
        }
        
        dispatch_group_notify(uncompressGroup, dispatch_get_main_queue(), ^{
            for(NSString *fileName in fileWithNameIncoporate.allKeys){
                NSData *data = [fileWithNameIncoporate objectForKey:fileName];
                [data writeToFile:[NSString stringWithFormat:@"%@/%@", pathToDirectory, fileName] atomically:NO];
            }
            if(deleteZip){
                [Util deleteFileWithPath:filePath];
            }
            completion(pathToDirectory, NO);
        });
    });
}

-(void)compressZipFileWithData:(NSData *)data toPath:(NSString *)filePath completion:(CompressCompletion)completion{
    //implement if needed
}

-(NSData *)zipStreamReadWithFile: (ZipFile *)zipFile{
    ZipReadStream *zipStreamReader = [zipFile readCurrentFileInZip];
    NSData *data = [zipStreamReader readDataOfLength:DEFAULT_DATA_LENGTH];
    [zipStreamReader finishedReading];
    return data;
}

-(NSArray *)enumerateOnZipFileInfo: (NSArray *)infos{
    NSMutableArray *fileInZipInfos = [[NSMutableArray alloc] init];
    for(FileInZipInfo *info in infos){
        ZipFileProfile *profile = [[ZipFileProfile alloc] initWithZipFileName:info.name createdDate:info.date filedSize:info.size compressionLevel:info.level];
        [fileInZipInfos addObject:profile];
    }
    return [NSArray arrayWithArray:fileInZipInfos];
}

-(NSDictionary *)incorporateFileWithName: (NSArray *)files names: (NSArray *)names{
    NSMutableDictionary *fileWithName = [[NSMutableDictionary alloc] init];
    for(NSData *file in files){
        NSUInteger index = [files indexOfObject:file];
        ZipFileProfile *zipProfile = [names objectAtIndex:index];
        [fileWithName setObject:file forKey:zipProfile.zipFileName];
    }
    return [NSDictionary dictionaryWithDictionary:fileWithName];
}


#pragma mark - Cloudinary
-(UIImage *)imageFromCloudinaryWithURL: (NSString *)URL{
    CLCloudinary *cloudinary = [CloudinaryShared shareCloudinary];
    NSString *URLToImage = [cloudinary url:URL];
    NSError *error = nil;
    return [UIImage imageWithData:[NSData dataWithContentsOfURL:[NSURL URLWithString:URLToImage] options:NSDataReadingMappedIfSafe error:&error]];
}

-(void)imageFromCloudinaryWithURLAsync:(NSString *)URL completionBlock:(void (^)(UIImage *))completion{
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_LOW, 0), ^{
        NSError *error = nil;
        NSData *imageData = [NSData dataWithContentsOfURL:[NSURL URLWithString:URL] options:NSDataReadingMappedIfSafe error:&error];
        
        if(imageData){
            completion([UIImage imageWithData:imageData]);
        }
    });
}

@end

