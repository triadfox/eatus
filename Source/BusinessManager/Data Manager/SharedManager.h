//
//  SessionManager.h
//  MiPoBeacon
//
//  Created by phuc on 9/2/15.
//  Copyright (c) 2015 MiPo Consolidate LLC. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ZipFile.h"
#import "ZipException.h"
#import "FileInZipInfo.h"
#import "ZipWriteStream.h"
#import "ZipReadStream.h"
#import "MRProgress.h"
#import "CLCloudinary.h"

typedef void (^UnCompressCompletion)(NSString *pathToDirectory, BOOL error);
typedef void (^CompressCompletion)(NSError *error);

typedef NS_ENUM(NSInteger, OverlayStyle) {
    kOverlayDefaultStyle = 0,
    kOverlayElegantStyle = 1
};

@interface SharedManager : NSObject

+ (SharedManager*)sharedInstance;


//MRprocess
- (MRActivityIndicatorView *)showIndependentActivityIndicatorViewWithTintColor: (UIColor *)tintcolor frame: (CGRect)frame;
- (void)showOverlayIndetermineProgressViewWithTitle: (NSString *)title tintColor: (UIColor *)tintColor onView: (UIView *)view;
- (void)hideOverlayIndetermineProgressViewAfter: (float)delays;
- (void)showOverlayIndetermineSmalProgressViewWithTitle: (NSString *)title style: (OverlayStyle)style tintColor: (UIColor *)tintColor onView: (UIView *)view;
- (void)showSucceedCheckMarkProcessViewWithTitle: (NSString *)title tintColor: (UIColor *)tintColor onView: (UIView *)view;
- (void)showFailureCrossProcessViewWithTitle: (NSString *)title tintColor: (UIColor *)tintColor onView: (UIView *)view;

- (void)updateInstallationUser;
- (void)loginOrCreateUser:(void (^)(BOOL, NSError*))completion;
- (void)getFacebookAvatar:(void (^)(BOOL, NSError*))completion;
- (void)getFacebookProfile: (void (^)(BOOL, NSError*))completion;

//download
- (void)downloadThenUncompressFile:(PFFile*)file toPath:(NSString*)path onCompletion:(void (^)(NSString*, BOOL error))completion;

//compress and uncompress
-(void)uncompressZipFileAtPath: (NSString *)filePath deleteZip: (BOOL)deleteZip completion: (UnCompressCompletion)completion;
//-(void)uncompressZipFileAtPath:(NSString *)filePath toPath: (NSString *)toPath deleteZip: (BOOL)deleteZip completion: (UnCompressCompletion)completion;
-(void)compressZipFileWithData: (NSData *)data toPath: (NSString *)filePath completion: (CompressCompletion)completion;

//Cloudinary
-(UIImage *)imageFromCloudinaryWithURL: (NSString *)URL;
-(void)imageFromCloudinaryWithURLAsync: (NSString *)URL completionBlock: (void (^)(UIImage *image))completion;

@end
