//
//  LocationManager.h
//  Eatus
//
//  Created by Phong Nguyen on 11/19/15.
//  Copyright © 2015 Triad Fox. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreLocation/CoreLocation.h>

@interface LocationManager : NSObject<CLLocationManagerDelegate>{
    CLLocation *_currentLocation;
}

@property(nonatomic, strong) CLLocationManager *locationManager;
@property(nonatomic, strong) CLGeocoder *locationGeoCoder;
@property(nonatomic, strong) CLPlacemark *locationPlaceMark;

+(instancetype)defaultLocation;
-(CLLocationDistance)calculateDistanceFrom: (CLLocation *)currentLocation toStoreLocation: (CLLocation *)storeLocation;
-(void)calculateDistanceFromCurrentPlaceToStoreAddress: (NSString *)address completion: (void (^)(CLLocationDistance distance))completion;
-(void)calculateDistanceFromCurrentPlaceToLatitude: (CLLocationDegrees)latitude longitude: (CLLocationDegrees)longitude completion: (void (^)(CLLocationDistance distance))completion;

@end
