//
//  LocationManager.m
//  Eatus
//
//  Created by Phong Nguyen on 11/19/15.
//  Copyright © 2015 Triad Fox. All rights reserved.
//

#import "LocationManager.h"

@implementation LocationManager
@synthesize locationManager;
@synthesize locationGeoCoder;

+(instancetype)defaultLocation{
    static LocationManager *manager;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        manager = [[LocationManager alloc] init];
        [manager startMonitorLocation];
    });
    return manager;
}

-(CLLocationDistance)calculateDistanceFrom: (CLLocation *)currentLocation toStoreLocation: (CLLocation *)storeLocation{
    return [currentLocation distanceFromLocation:storeLocation];
}

-(void)calculateDistanceFromCurrentPlaceToStoreAddress: (NSString *)address completion: (void (^)(CLLocationDistance distance))completion{
    [locationGeoCoder geocodeAddressString:address completionHandler:^(NSArray *placeMarks, NSError *error){
        if(placeMarks && placeMarks.count > 0){
            CLPlacemark *firstPlace = [placeMarks firstObject];
            CLLocationDistance distanceToStore = [_currentLocation distanceFromLocation:firstPlace.location]/1000;
            completion(distanceToStore);
        }
    }];
}

-(void)calculateDistanceFromCurrentPlaceToLatitude: (CLLocationDegrees)latitude longitude: (CLLocationDegrees)longitude completion: (void (^)(CLLocationDistance distance))completion{
    CLLocation *storeLocation = [[CLLocation alloc] initWithLatitude:latitude longitude:longitude];
    
    if(!_currentLocation){
        [locationManager startUpdatingLocation];
    }else{
        CLLocationDistance distance = [_currentLocation distanceFromLocation:storeLocation]/1000;
        if(completion){
            if(distance != 0.00000){
                completion(distance);
            }
        }
    }
}



#pragma mark - Location Delegate methods
-(void)locationManager:(CLLocationManager *)manager didFailWithError:(NSError *)error{
    NSLog(@"Did fail location manager with error: %@", error.description);
}

-(void)locationManager:(CLLocationManager *)manager didUpdateLocations:(NSArray<CLLocation *> *)locations{
    _currentLocation = [locations firstObject];
}


#pragma mark - Private methods
-(void)startMonitorLocation{
    locationManager = [[CLLocationManager alloc] init];
    locationGeoCoder = [[CLGeocoder alloc] init];
    
    locationManager.delegate = self;
    locationManager.distanceFilter = 100.0f;
    locationManager.desiredAccuracy = kCLLocationAccuracyBestForNavigation;
    [locationManager startUpdatingLocation];
}

@end
