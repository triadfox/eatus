//
//  PermissionManager.h
//  Eatus
//
//  Created by Phong Nguyen on 11/1/15.
//  Copyright © 2015 Triad Fox. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <PermissionScope/PermissionScope-Swift.h>

@interface PermissionScope (Manager)

+(instancetype)shareScope;

-(void)setHeaderScopeView: (NSString *)headerScope;
-(void)setBodyScopeView: (NSString *)bodyScope;

@end
