//
//  ConnectionManager.m
//  Eatus
//
//  Created by Phong Nguyen on 10/29/15.
//  Copyright © 2015 Triad Fox. All rights reserved.
//

#import "ConnectionManager.h"
#import "AlertViewManager.h"
#import "KontaktBeaconServiceManager.h"

@interface ConnectionManager()<CBCentralManagerDelegate>

@property(nonatomic, strong) CBCentralManager *bluetoothManager;
@property(nonatomic, strong) Reachability *reachability;
@property(nonatomic, strong) UIColor *mainTheme;

@end

@implementation ConnectionManager
@synthesize bluetoothManager;
@synthesize reachability;
@synthesize mainTheme;

+(instancetype)defaultConnection{
    static ConnectionManager *manager;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        manager = [[ConnectionManager alloc] init];
        manager.mainTheme = COLOR_MAIN_THEME;
        
    });
    return manager;
}


#pragma mark - Public methods
+(void)currentBluetoothStatus{
    [[ConnectionManager defaultConnection] startDetectBluetoothConnection];
}

+(void)currentConnectionStatus{
    [[ConnectionManager defaultConnection] startDetectCellularAndWifiConnection];
}


#pragma mark - Private methods
-(void)startDetectCellularAndWifiConnection{
    reachability = [Reachability reachabilityForInternetConnection];
    UIImage *networkImage = [UIImage imageNamed:@"Network"];
    
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0), ^{
        [reachability startNotifier];
        NetworkStatus status = [reachability currentReachabilityStatus];
        
        switch (status) {
            case NotReachable:{
                dispatch_async(dispatch_get_main_queue(), ^{
                    [_NotificationCenter postNotificationName:kNotificationNetworkOff object:nil];
                    
                    AMSmoothAlertView *alert = [AlertViewManager smoothDropAlertViewWithTitle:@"Enable Network" content:@"Turn on wifi or cellular network" cancelButton:NO color:mainTheme alertType:AlertInfo];
                    [alert.logoView setImage:networkImage];
                    [alert.defaultButton setTitle:@"Settings" forState:UIControlStateNormal];
                    
                    alert.completionBlock = ^void(AMSmoothAlertView *alert, UIButton *button){
                        if([button isEqual:alert.defaultButton]){
                            [[UIApplication sharedApplication] openURL:[NSURL URLWithString:PREFS_GENERAL_NETWORK]];
                        }
                    };
                    
                    [alert show];
                });
            }
                break;
                
            case ReachableViaWiFi:{
                [_NotificationCenter postNotificationName:kNotificationNetworkOn object:nil];
                
                return;
            }
                break;
                
            case ReachableViaWWAN:{
                [_NotificationCenter postNotificationName:kNotificationNetworkOn object:nil];
                
                return;
            }
                
            default:
                break;
        }
    });

}

-(void)startDetectBluetoothConnection{
    if(!bluetoothManager){
        NSDictionary *options = [NSDictionary dictionaryWithObjectsAndKeys:[NSNumber numberWithBool:NO], CBCentralManagerOptionShowPowerAlertKey, nil];
        
        bluetoothManager = [[CBCentralManager alloc] initWithDelegate:self queue:dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0) options:options];
    }
}

-(void)centralManagerDidUpdateState:(CBCentralManager *)central{
    UIImage *bluetoothImage = [UIImage imageNamed:@"Bluetooth"];
    
    switch (central.state) {
        case CBCentralManagerStatePoweredOff:{
            dispatch_async(dispatch_get_main_queue(), ^{
                [_NotificationCenter postNotificationName:kNotificationBluetoothOff object:nil];
                
                AMSmoothAlertView *alert = [AlertViewManager smoothDropAlertViewWithTitle:@"Enable Bluetooth" content:@"Turn on bluetooth to accurate your location" cancelButton:NO color:mainTheme alertType:AlertInfo];

                [alert.defaultButton setTitle:@"Settings" forState:UIControlStateNormal];
                [alert.logoView setImage:bluetoothImage];
                
                alert.completionBlock = ^void(AMSmoothAlertView *alert, UIButton *button){
                    if([button isEqual:alert.defaultButton]){
                        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:PREFS_GENERAL_BLUETOOTH]];
                    }

                };
                
                [alert show];
            });
        }
            break;
            
        case CBCentralManagerStatePoweredOn:{
            [_NotificationCenter postNotificationName:kNotificationBluetoothOn object:nil];
            [[KontaktBeaconServiceManager shareBeacon] launchKontatkBeaconSurveillance];
        }
            break;
            
        case CBCentralManagerStateUnauthorized:{
            [_NotificationCenter postNotificationName:kNotificationBluetoothOff object:nil];
            
            dispatch_async(dispatch_get_main_queue(), ^{
                AMSmoothAlertView *alert = [AlertViewManager smoothDropAlertViewWithTitle:@"Authorize Bluetooth" content:@"Bluetooth status is not authorized yet" cancelButton:NO color:mainTheme alertType:AlertInfo];
                [alert.logoView setImage:bluetoothImage];
                
                alert.completionBlock = ^void(AMSmoothAlertView *alert, UIButton *button){
                    
                };
                
                [alert show];
            });
        }
            break;
            
        case CBCentralManagerStateResetting:{
            [_NotificationCenter postNotificationName:kNotificationBluetoothOn object:nil];
            [[KontaktBeaconServiceManager shareBeacon] launchKontatkBeaconSurveillance];
        }
            break;
            
        case CBCentralManagerStateUnknown:{
            dispatch_async(dispatch_get_main_queue(), ^{
                AMSmoothAlertView *alert = [AlertViewManager smoothDropAlertViewWithTitle:@"Unknown Bluetooth" content:@"Your device seem have problem with bluetooth" cancelButton:NO color:mainTheme alertType:AlertInfo];
                [alert.logoView setImage:bluetoothImage];
                alert.completionBlock = ^void(AMSmoothAlertView *alert, UIButton *button){
                    if(button == alert.defaultButton){
                        [alert dismissAlertView];
                    }
                };
                
                [alert show];
            });
        }
            break;
            
        case CBCentralManagerStateUnsupported:{
            dispatch_async(dispatch_get_main_queue(), ^{
                AMSmoothAlertView *alert = [AlertViewManager smoothDropAlertViewWithTitle:@"No Bluetooth" content:@"Bluetooth is not available on your device" cancelButton:NO color:mainTheme alertType:AlertInfo];
                [alert.logoView setImage:bluetoothImage];
                [alert.defaultButton setTitle:@"OK" forState:UIControlStateNormal];
                
                alert.completionBlock = ^void(AMSmoothAlertView *alert, UIButton *button){
                    if(button == alert.defaultButton){
                        [alert dismissAlertView];
                    }
                };
                
                [alert show];
            });
        }
            break;
            
        default:
            break;
    }
    
    [central stopScan];
}

@end
