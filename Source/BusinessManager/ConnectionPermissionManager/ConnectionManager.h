//
//  ConnectionManager.h
//  Eatus
//
//  Created by Phong Nguyen on 10/29/15.
//  Copyright © 2015 Triad Fox. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreBluetooth/CoreBluetooth.h>

#import "Reachability.h"

#define PREFS_GENERAL_BLUETOOTH                         @"prefs:root=Bluetooth"
#define PREFS_GENERAL_NETWORK                           @"prefs:root=WIFI"
#define PREFS_APP_PERMISSION                            @"prefs:root=Settings&path=Eatus"

@interface ConnectionManager : NSObject

+(instancetype)defaultConnection;
+(void)currentConnectionStatus;
+(void)currentBluetoothStatus;

@end
