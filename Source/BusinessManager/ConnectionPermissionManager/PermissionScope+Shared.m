//
//  PermissionManager.m
//  Eatus
//
//  Created by Phong Nguyen on 11/1/15.
//  Copyright © 2015 Triad Fox. All rights reserved.
//

#import "PermissionScope+Shared.h"

@implementation PermissionScope(Manager)

+(instancetype)shareScope{
    static PermissionScope *manager;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        manager = [[PermissionScope alloc] init];
        [manager settingUpAppeareance];
    });
    return manager;
}

-(void)settingUpAppeareance{
    [self setCloseButtonTextColor:COLOR_MAIN_THEME_LIGHT_LV_1];
    [self setPermissionButtonBorderColor:COLOR_MAIN_THEME_LIGHT_LV_1];
    
    [self setAuthorizedButtonColor:COLOR_MAIN_THEME_LIGHT_LV_1];
    [self setUnauthorizedButtonColor:COLOR_MAIN_THEME_FAILURE_RED];
    
    [self setPermissionButtonTextColor:COLOR_MAIN_THEME_LIGHT_LV_1];
    
    [self setHeaderScopeView:@"Eatus"];
    [self setBodyScopeView:NSLocalizedString(@"com.eatus.permission.body.content", @"PermissionRequest")];
}

-(void)setHeaderScopeView:(NSString *)headerScope{
    self.headerLabel.text = headerScope;
}

-(void)setBodyScopeView:(NSString *)bodyScope{
    self.bodyLabel.text = bodyScope;
}

@end
