//
//  FeedbackManager.h
//  Eatus
//
//  Created by Phong Nguyen on 10/29/15.
//  Copyright © 2015 Triad Fox. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface FeedbackManager : NSObject

+(instancetype)defaultFeedback;

+(void)ratingPointForRestaurantId: (NSString *)restaurantId onCompletion: (void (^)(float point))completion;
+(void)ratingPointForRestaurant: (Restaurant *)restaurant onCompletion: (void (^)(float point))completion;

+(void)ratingEmoticonForRestaurantId: (NSString *)restaurantId onCompletion: (void (^)(UIImage *emoticon))completion;
+(void)ratingEmoticonForRestaurant: (Restaurant *)restaurant onCompletion: (void (^)(UIImage *emoticon))completion;

+(void)ratingUnicodeEmoticonForRestaurantId: (NSString *)restaurantId onCompletion: (void (^)(NSString *emoticon))completion;
+(void)ratingUnicodeEmoticonForRestaurant: (Restaurant *)restaurant onCompletion: (void (^)(NSString *emoticon))completion;


-(void)startCountToFireMomentOfFeedbackFromNow;
-(void)getTheRestTimeToTheFireMoment;

@end
