//
//  FeedbackManager.m
//  Eatus
//
//  Created by Phong Nguyen on 10/29/15.
//  Copyright © 2015 Triad Fox. All rights reserved.
//

#import "FeedbackManager.h"

@implementation FeedbackManager

+(instancetype)defaultFeedback{
    static FeedbackManager *manager;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        manager = [[FeedbackManager alloc] init];
    });
    return manager;
}

+(void)ratingPointForRestaurant:(Restaurant *)restaurant onCompletion:(void (^)(float))completion{
    [ParseDataManager getFeedbacksAtRestaurant:restaurant onCompletion:^(NSArray *feedbacks, NSError *error){
        if(completion){
            NSInteger point = [[FeedbackManager defaultFeedback] accurateOnFeedbacksByAveragePoint:feedbacks];
            completion(point);
        }
    }];
}

+(void)ratingEmoticonForRestaurant:(Restaurant *)restaurant onCompletion:(void (^)(UIImage *))completion{
    [ParseDataManager getFeedbacksAtRestaurant:restaurant onCompletion:^(NSArray *feedbacks, NSError *error){
        if([feedbacks count] > 0){
            if(completion){
                FeedbackManager *manager = [FeedbackManager defaultFeedback];
                float point = [manager accurateOnFeedbacksByAveragePoint:feedbacks];
                completion([manager ratingEmoticonByPoint:point]);
            }
        }
    }];
}

+(void)ratingPointForRestaurantId:(NSString *)restaurantId onCompletion:(void (^)(float))completion{
    [ParseDataManager getFeedbacksAtRestaurantId:restaurantId onCompletion:^(NSArray *feedbacks, NSError *error){
        if(completion && [feedbacks count] > 0){
            NSInteger point = [[FeedbackManager defaultFeedback] accurateOnFeedbacksByAveragePoint:feedbacks];
            completion(point);
        }
    }];
}

+(void)ratingUnicodeEmoticonForRestaurant:(Restaurant *)restaurant onCompletion:(void (^)(NSString *))completion{
    [ParseDataManager getFeedbacksAtRestaurant:restaurant onCompletion:^(NSArray *feedbacks, NSError *error){
        if(completion && [feedbacks count] > 0){
            FeedbackManager *manager = [FeedbackManager defaultFeedback];
            NSInteger point = [manager accurateOnFeedbacksByAveragePoint:feedbacks];
            completion([manager ratingUnicodeEmoticonByPoint:point]);
        }
    }];
}

+(void)ratingUnicodeEmoticonForRestaurantId:(NSString *)restaurantId onCompletion:(void (^)(NSString *))completion{
    [ParseDataManager getFeedbacksAtRestaurantId:restaurantId onCompletion:^(NSArray *feedbacks, NSError *error){
        if(completion && [feedbacks count] > 0){
            FeedbackManager *manager = [FeedbackManager defaultFeedback];
            NSInteger point = [manager accurateOnFeedbacksByAveragePoint:feedbacks];
            completion([manager ratingUnicodeEmoticonByPoint:point]);
        }
    }];
}

+(void)ratingEmoticonForRestaurantId:(NSString *)restaurantId onCompletion:(void (^)(UIImage *))completion{
    [ParseDataManager getFeedbacksAtRestaurantId:restaurantId onCompletion:^(NSArray *feedbacks, NSError *error){
        if(completion && [feedbacks count] > 0){
            FeedbackManager *manager = [FeedbackManager defaultFeedback];
            float point = [manager accurateOnFeedbacksByAveragePoint:feedbacks];
            completion([manager ratingEmoticonByPoint:point]);
        }
    }];
}

-(void)startCountToFireMomentOfFeedbackFromNow{
    
}

-(void)getTheRestTimeToTheFireMoment{
    
}


#pragma mark - Private methods
-(float)accurateOnFeedbacksByAveragePoint: (NSArray *)feedbacks{
    float happyPoint  = 0.0f;
    float dislikePoint = 0.0f;
    
    for(Feedback *feedback in feedbacks){
        if(feedback.isHappy){
            happyPoint += 1;
        }else{
            dislikePoint += 1;
        }
    }
    
    /* after get happy and dislike record, we multiple it as formula:
    AveragePoint = Happy/Dislike
     AveragePoint > 1 -->> Outstanding conslusion
     AveragePoint == 1 ->> Good conclusion
     AveragePoint < 1 && AveragePoint > 0.5 -->> Poor conslusion
     AveragePoint < 0.5 -->> Bad conclusion
     */
    return happyPoint/dislikePoint;
}

-(UIImage *)ratingEmoticonByPoint: (float)point{
    if(point > 1){
        return [UIImage imageNamed:@"Outstanding"];
    }else if(point == 1){
        return [UIImage imageNamed:@"Good"];
    }else if(point < 1){
        return [UIImage imageNamed:@"Poor"];
    }else{
        return [UIImage imageNamed:@"Bad"];
    }
}

-(NSString *)ratingUnicodeEmoticonByPoint: (float)point{
    if(point > 1){
        return @"\U0001F606";
    }else if(point == 1){
        return @"\U0001F604";
    }else if(point < 1){
        return @"\U0001F614";
    }else{
        return @"\U0001F616";
    }

}
@end
