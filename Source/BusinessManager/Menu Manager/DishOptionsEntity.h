//
//  DishItemEntity.h
//  Light Order
//
//  Created by Phong Nguyen on 10/4/15.
//  Copyright © 2015 Triad Fox. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface DishOptionsEntity : NSObject<NSCopying>

@property(nonatomic, copy) NSString *optionName;
@property(nonatomic, strong) NSArray *options;
@property(nonatomic, assign) BOOL optionExclusiveSelection;
@property(nonatomic, assign) BOOL optionAddOn;

@end
