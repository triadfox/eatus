//
//  MenuEngineDAC.m
//  Light Order
//
//  Created by Phong Nguyen on 10/4/15.
//  Copyright © 2015 Triad Fox. All rights reserved.
//

#import "MenuEngineDAC.h"

@implementation MenuEngineDAC

+(GDataXMLDocument *)menuFileForPath:(NSString *)filePath{
    NSError *error = nil;
    NSData *data = [NSData dataWithContentsOfFile:filePath];
    if(!data){
        NSLog(@"Menu file is nil. File not found");
    }
    return [[GDataXMLDocument alloc] initWithData:data options:0 error:&error];
}

@end
