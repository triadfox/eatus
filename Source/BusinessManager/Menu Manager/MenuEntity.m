//
//  MenuEntity.m
//  Light Order
//
//  Created by Phong Nguyen on 10/4/15.
//  Copyright © 2015 Triad Fox. All rights reserved.
//

#import "MenuEntity.h"

@implementation MenuEntity

-(id)copyWithZone:(NSZone *)zone{
    id copy = [[[self class] alloc] init];
    if(copy) {
        [copy setMajorId:self.majorId];
        [copy setVersion:[self.version copyWithZone:zone]];
        [copy setDishes:[self.dishes copyWithZone:zone]];
        [copy setCategories:[self.categories copyWithZone:zone]];
        [copy setQuantityLimit:self.quantityLimit];
    }
    return copy;
}
@end
