//
//  MenuJsonSerializer.h
//  Light Order
//
//  Created by Phong Nguyen on 10/14/15.
//  Copyright © 2015 Triad Fox. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "DishEntity.h"

@interface MenuJsonSerializer : NSObject

+(instancetype)shareSerial;
+(NSArray *)totalPriceAndReceiptIdGotJsonFromBackend: (NSString *)jsonString;
+(NSArray *)allDishesGotJsonFromBackend: (NSString *)jsonString;
+(NSDictionary *)allDishesAsDictionaryGotJsonFromBackend: (NSString *)jsonString;

+(NSString *)taxGotJsonFromBackend: (NSString *)jsonString;
+(NSString *)discountPercentageGotJsonFromBackend: (NSString *)jsonString;
+(NSArray *)subTotalTaxAndDiscountPercentageGotJsonFromBackend: (NSString *)jsonString;
+(NSDictionary *)allNeededOrderDetailGotJsonFromBackend: (NSString *)jsonString;

-(void)smartAppendDishToOrders:(DishEntity *)dish;
-(void)smartAppendDishInEditMode: (DishEntity *)dish;

-(NSData *)orderJsonByDataWithPromotionPercentage: (float)percentage tax: (float)tax;
-(NSDictionary *)orderJsonByDictionary;
-(NSArray *)deserializeToDishEntity;

-(DishEntity *)dishEntityForId: (NSString *)dishId;
-(NSDictionary *)dishJsonForId: (NSString *)dishId;
-(double)getCurrentTotal;
-(void)removeDishAtIndex: (NSInteger)index;
-(void)removeDishWithName: (NSString *)dishName;

-(NSDictionary *)packedJsonWithPromotionPercentage: (float)percentage tax: (float)tax;
-(NSString *)packJsonByStringWithPromotionPercentage: (float)perentage tax: (float)tax;
-(NSDictionary *)packedJsonWithPromotionPercentageForTransferWatch: (float)percentage tax: (float)tax atRestaurant: (NSString *)restaurant;
+(void)cleanUpAndSweepAll;
@end
