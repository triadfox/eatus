//
//  DishItemEntity.m
//  Light Order
//
//  Created by Phong Nguyen on 10/4/15.
//  Copyright © 2015 Triad Fox. All rights reserved.
//

#import "DishOptionsEntity.h"

@implementation DishOptionsEntity

-(id)copyWithZone:(NSZone *)zone{
    id copy = [[[self class] alloc] init];
    if(copy){
        [copy setOptionName:[self.optionName copyWithZone:zone]];
        [copy setOptions:[self.options copyWithZone:zone]];
    }
    return copy;
}
@end
