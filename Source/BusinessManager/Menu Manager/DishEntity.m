//
//  DishEntity.m
//  Light Order
//
//  Created by Phong Nguyen on 10/4/15.
//  Copyright © 2015 Triad Fox. All rights reserved.
//

#import "DishEntity.h"

@implementation DishEntity

-(id)copyWithZone:(NSZone *)zone{
    id copy = [[[self class] allocWithZone:zone] init];
    if(copy){
        [copy setDishId:self.dishId];
        [copy setDishName:[self.dishName copyWithZone:zone]];
        [copy setDishDescription:[self.dishDescription copyWithZone:zone]];
        [copy setDishPrice:self.dishPrice];
        [copy setDishCategory:[self.dishCategory copyWithZone:zone]];
        [copy setDishOptions:[self.dishOptions copyWithZone:zone]];
        [copy setDishQuantity:self.dishQuantity];
    }
    return copy;
}

@end
