//
//  MenuJsonSerializer.m
//  Light Order
//
//  Created by Phong Nguyen on 10/14/15.
//  Copyright © 2015 Triad Fox. All rights reserved.
//

#import "MenuJsonSerializer.h"
#import "DishOptionsEntity.h"
#import "DishOptionEntity.h"
#import "MenuEngineManager.h"
#import "MenuJsonSerializerConstant.h"

@implementation MenuJsonSerializer{
    NSMutableDictionary *dishes;
    NSMutableArray *dishesInReadMode;
    
    NSString *preservedReceiptId;
    
    BOOL editMode;
}

+(instancetype)shareSerial{
    static MenuJsonSerializer *serial;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        serial = [[MenuJsonSerializer alloc] init];
    });
    return serial;
}

+(NSArray *)totalPriceAndReceiptIdGotJsonFromBackend:(NSString *)jsonString{
    NSError *error = nil;
    NSData *jsonData = [jsonString dataUsingEncoding:NSUTF8StringEncoding];
    NSDictionary *jsonObject = [NSJSONSerialization JSONObjectWithData:jsonData options:0 error:&error];
    NSString *total = [jsonObject objectForKey:KEY_TOTAL];
    NSString *currency = [jsonObject objectForKey:KEY_CURRENCY];
    NSString *receiptId = [jsonObject objectForKey:KEY_RECEIPT_ID];
    NSLocale *localeByCurrency = [MenuEngineManager localeByCurrencyInput:currency];
    
    NSArray *totalPriceWithReceiptId = @[[Util stringForNumberWithLocale:localeByCurrency number:[NSNumber numberWithDouble:[total doubleValue]]], receiptId, localeByCurrency];
    return totalPriceWithReceiptId;
}

+(NSArray *)allDishesGotJsonFromBackend:(NSString *)jsonString{
    NSError *error = nil;
    NSData *jsonData = [jsonString dataUsingEncoding:NSUTF8StringEncoding];
    NSDictionary *jsonObject = [NSJSONSerialization JSONObjectWithData:jsonData options:0 error:&error];
    
    MenuJsonSerializer *serializer = [MenuJsonSerializer shareSerial];
    return [serializer deserializeAllDishFromJsonToEntity:jsonObject];
}

+(NSDictionary *)allDishesAsDictionaryGotJsonFromBackend:(NSString *)jsonString{
    NSError *error = nil;
    NSData *jsonData = [jsonString dataUsingEncoding:NSUTF8StringEncoding];
    NSDictionary *jsonObject = [NSJSONSerialization JSONObjectWithData:jsonData options:0 error:&error];
    return [jsonObject objectForKey:KEY_DISHES];
}

+(NSString *)taxGotJsonFromBackend:(NSString *)jsonString{
    NSError *error = nil;
    NSData *jsonData = [jsonString dataUsingEncoding:NSUTF8StringEncoding];
    NSDictionary *jsonObject = [NSJSONSerialization JSONObjectWithData:jsonData options:0 error:&error];
    return priceByCurrentAlongCurrentLocale([[[jsonObject objectForKey:KEY_DISHES] objectForKey:KEY_RECEIPT_TAX] floatValue]);
}

+(NSString *)discountPercentageGotJsonFromBackend: (NSString *)jsonString{
    NSError *error = nil;
    NSData *jsonData = [jsonString dataUsingEncoding:NSUTF8StringEncoding];
    NSDictionary *jsonObject = [NSJSONSerialization JSONObjectWithData:jsonData options:0 error:&error];
    return [[jsonObject objectForKey:KEY_DISHES] objectForKey:KEY_RECEIPT_DISCOUNT_PERCENTAGE];
}

+(NSArray *)subTotalTaxAndDiscountPercentageGotJsonFromBackend:(NSString *)jsonString{
    NSError *error = nil;
    NSData *jsonData = [jsonString dataUsingEncoding:NSUTF8StringEncoding];
    NSDictionary *jsonObject = [NSJSONSerialization JSONObjectWithData:jsonData options:0 error:&error];
    
    NSNumber *taxNumber = [jsonObject objectForKey:KEY_RECEIPT_TAX];
    NSNumber *subTotalNumber = [jsonObject objectForKey:KEY_SUB_TOTAL];
    NSNumber *discountPercentageNumber = [jsonObject objectForKey:KEY_RECEIPT_DISCOUNT_PERCENTAGE];
    NSNumber *discountAmountNumber = [jsonObject objectForKey:KEY_RECEIPT_DISCOUNT_AMOUNT];

    return @[subTotalNumber, taxNumber, discountPercentageNumber, discountAmountNumber];
}

+(NSDictionary *)allNeededOrderDetailGotJsonFromBackend:(NSString *)jsonString{
    NSError *error = nil;
    NSData *jsonData = [jsonString dataUsingEncoding:NSUTF8StringEncoding];
    NSDictionary *jsonObject = [NSJSONSerialization JSONObjectWithData:jsonData options:0 error:&error];
    
    NSNumber *taxNumber = [jsonObject objectForKey:KEY_RECEIPT_TAX];
    NSNumber *subTotalNumber = [jsonObject objectForKey:KEY_SUB_TOTAL];
    NSNumber *discountPercentageNumber = [jsonObject objectForKey:KEY_RECEIPT_DISCOUNT_PERCENTAGE];
    NSNumber *discountAmountNumber = [jsonObject objectForKey:KEY_RECEIPT_DISCOUNT_AMOUNT];
    
    NSNumber *total = [jsonObject objectForKey:KEY_TOTAL];
    NSString *receiptId = [jsonObject objectForKey:KEY_RECEIPT_ID];
    
    NSString *currency = [jsonObject objectForKey:KEY_CURRENCY];
    NSLocale *localeByCurrency = [MenuEngineManager localeByCurrencyInput:currency];
    
    return @{KEY_RECEIPT_ID:receiptId,
             KEY_TOTAL:total,
             KEY_SUB_TOTAL:subTotalNumber,
             KEY_RECEIPT_LOCALE:localeByCurrency,
             KEY_RECEIPT_TAX:taxNumber,
             KEY_SUB_TOTAL:subTotalNumber,
             KEY_RECEIPT_DISCOUNT_PERCENTAGE:discountPercentageNumber,
             KEY_RECEIPT_DISCOUNT_AMOUNT:discountAmountNumber};
}


#pragma mark - Public Methods
-(void)smartAppendDishToOrders:(DishEntity *)dish{
    [self initWithDishEntity:dish];
}

-(void)smartAppendDishInEditMode:(DishEntity *)dish{
    editMode = YES;
    [self initWithDishEntity:dish];
    editMode = NO;
}

-(DishEntity *)dishEntityForId:(NSString *)dishId{
    NSDictionary *dictD = [self dishForDishId:dishId];
    return [self deserializeJsonToDishEntity:dictD];
}

-(NSDictionary *)dishJsonForId:(NSString *)dishId{
    return [self dishForDishId:dishId];
}

-(NSData *)orderJsonByDataWithPromotionPercentage:(float)percentage tax:(float)tax{
    MenuJsonSerializer *serializer = [MenuJsonSerializer shareSerial];
    NSError *error = nil;
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:[serializer packJsonObjectWithDiscountPercentage:percentage tax:tax] options:NSJSONWritingPrettyPrinted error:&error];
    return jsonData;
}

-(NSDictionary *)orderJsonByDictionary{
    MenuJsonSerializer *serializer = [MenuJsonSerializer shareSerial];
    return [serializer getDishesJsonDict];
}

-(NSArray *)deserializeToDishEntity{
    MenuJsonSerializer *serializer = [MenuJsonSerializer shareSerial];
    return [serializer deserializeAllDishToEntity];
}

-(double)getCurrentTotal{
    return [self currentTotalForOrder];
}

-(void)removeDishAtIndex:(NSInteger)index{
    [self removeDishByIndexInDishes:index];
}

-(void)removeDishWithName:(NSString *)dishName{
    [self removeDishByName:dishName];
}

-(NSDictionary *)packedJsonWithPromotionPercentage:(float)percentage tax:(float)tax{
    return [self packJsonObjectWithDiscountPercentage:percentage tax:tax];
}

-(NSString *)packJsonByStringWithPromotionPercentage:(float)perentage tax:(float)tax{
    NSError *error = nil;
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:[self packedJsonWithPromotionPercentage:perentage tax:tax] options:NSJSONWritingPrettyPrinted error:&error];
    if(jsonData){
        return [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
    }
    return nil;
}

-(NSDictionary *)packedJsonWithPromotionPercentageForTransferWatch:(float)percentage tax:(float)tax atRestaurant:(NSString *)restaurant{
    NSMutableDictionary *dict = [self packJsonObjectWithDiscountPercentage:percentage tax:tax];
    
    if(preservedReceiptId){
        [dict setObject:preservedReceiptId forKey:KEY_RECEIPT_ID];
    }
    
    [dict setObject:@"Receipt" forKey:KEY_MSG_TRANSFERRING];
    [dict setObject:[NSDate date] forKey:@"issuedDate"];
    [dict setObject:restaurant forKey:@"restaurantName"];
    return [[NSDictionary alloc] initWithDictionary:dict];
}

+(void)cleanUpAndSweepAll{
    [[MenuJsonSerializer shareSerial] cleanUpDishes];
}


#pragma mark - Private Methods
-(void)cleanUpDishes{
    [dishes removeAllObjects];
    dishes = nil;
}

-(NSDictionary *)getDishesJsonDict{
    return dishes;
}

-(void)initWithDishEntity: (DishEntity *)dish{
    if([dishes count] == 0){
        dishes = [[NSMutableDictionary alloc] init];
        [self prepareDishesCollection];
    }
    [self determineShouldAddOrAppend:dish];
}

-(void)prepareDishesCollection{
    [dishes setObject:[[NSMutableArray alloc] init] forKey:KEY_DISHES];
}


#pragma mark - Pack Json Object
-(NSMutableDictionary *)packJsonObjectWithDiscountPercentage: (float)percentage tax: (float)tax{
    NSArray *jsonObject = [self allDishFromOrder];
    double total = [self currentTotalForOrder];
    NSMutableDictionary *pDict = [[NSMutableDictionary alloc] init];
    [pDict setObject:jsonObject forKey:KEY_DISHES];
    [pDict setObject:[NSNumber numberWithDouble:total] forKey:KEY_SUB_TOTAL];
    [pDict setObject:[NSNumber numberWithDouble:(total + tax - total * percentage)] forKey:KEY_TOTAL];
    [pDict setObject:[NSNumber numberWithFloat:tax] forKey:KEY_RECEIPT_TAX];
    [pDict setObject:[NSNumber numberWithDouble:(total * percentage)] forKey:KEY_RECEIPT_DISCOUNT_AMOUNT];
    [pDict setObject:[NSNumber numberWithFloat:percentage] forKey:KEY_RECEIPT_DISCOUNT_PERCENTAGE];
    
    [pDict setObject:[[MenuEngineManager shareManager] localCurrency] forKey:KEY_CURRENCY];
    
    preservedReceiptId = [self receiptRandomCode];
    [pDict setObject:preservedReceiptId forKey:KEY_RECEIPT_ID];
    
    return pDict;
}

-(NSString *)receiptRandomCode{
    NSString *letters = @"ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
    NSMutableString *randomString = [NSMutableString stringWithCapacity: RECEIPT_CODE_LENGTH];
    
    for (int i=0; i<RECEIPT_CODE_LENGTH; i++) {
        [randomString appendFormat: @"%C", [letters characterAtIndex: arc4random_uniform((u_int32_t)[letters length])]];
    }
    return [NSString stringWithFormat:@"#%@", randomString];
}


#pragma mark - Deserializer
-(NSArray *)deserializeAllDishToEntity{
    NSMutableArray *dishesCollection = [[NSMutableArray alloc] init];
    NSArray *allDishes = [dishes objectForKey:KEY_DISHES];
    for(NSDictionary *dictD in allDishes){
        [dishesCollection addObject:[self deserializeJsonToDishEntity:dictD]];
    }
    return [[NSArray alloc] initWithArray:dishesCollection];
}

-(NSArray *)deserializeAllDishFromJsonToEntity: (NSDictionary *)dishJson{
    NSMutableArray *dishesCollection = [[NSMutableArray alloc] init];
    NSArray *alldishes = [dishJson objectForKey:KEY_DISHES];
    for(NSDictionary *dictD in alldishes){
        [dishesCollection addObject:[self deserializeJsonToDishEntity:dictD]];
    }
    return [[NSArray alloc] initWithArray:dishesCollection];
}

-(DishEntity *)deserializeJsonToDishEntity: (NSDictionary *)dictD{
    DishEntity *dishEntity = [[DishEntity alloc] init];
    dishEntity.dishId = [dictD objectForKey:DISH_ID];
    dishEntity.dishName = [dictD objectForKey:DISH_NAME];
    dishEntity.dishPrice = [[dictD objectForKey:DISH_PRICE] doubleValue];
    dishEntity.dishQuantity = [[dictD objectForKey:DISH_QUANTITY] integerValue];
    dishEntity.dishOptions = [self deserializeAllOptionsGroupFromDishDict:[dictD objectForKey:DISH_OPTIONS]];
    return dishEntity;
}

-(NSArray *)deserializeAllOptionsGroupFromDishDict: (NSArray *)allOptions{
    DishOptionsEntity *optionsEntity;
    NSMutableArray *allOptionsReturn = [[NSMutableArray alloc] init];
    for(NSDictionary *dicOpt in allOptions){
        optionsEntity = [[DishOptionsEntity alloc] init];
        optionsEntity.optionName = [dicOpt.allKeys firstObject];
        optionsEntity.options = [self deserializeAllOptionFromOptArray:[dicOpt.allValues firstObject]];
        [allOptionsReturn addObject:optionsEntity];
    }
    return [[NSArray alloc] initWithArray:allOptionsReturn];
}

-(NSArray *)deserializeAllOptionFromOptArray: (NSArray *)optArray{
    DishOptionEntity *optionEntity;
    NSMutableArray *optionCollection = [[NSMutableArray alloc] init];
    
    for(NSArray *opt in optArray){
        optionEntity = [[DishOptionEntity alloc] init];
        [self enumerateOnArrayOfOptionAndAddOnPrice:opt on:optionEntity];
        optionEntity.selected = YES;
        [optionCollection addObject:optionEntity];
    }
    return [[NSArray alloc] initWithArray:optionCollection];
}

-(void)enumerateOnArrayOfOptionAndAddOnPrice: (NSArray *)complex on: (DishOptionEntity *)optionEntity{
    for(id objc in complex){
        if([objc isKindOfClass:[NSString class]]){
            optionEntity.dishOptionName = (NSString *)objc;
        }else if([objc isKindOfClass:[NSNumber class]]){
            NSNumber *priceAddOn = (NSNumber *)objc;
            optionEntity.dishOptionAddOnPrice = [priceAddOn doubleValue];
        }
    }
}


#pragma mark - Calculation & Finding
-(double)currentTotalForOrder{
    //first get all dishes by deserialized way
    NSArray *allDishesEntity = [self deserializeAllDishToEntity];
    double total = 0.0f;
    
    for(DishEntity *dishEntity in allDishesEntity){
        double subTotal = dishEntity.dishQuantity * dishEntity.dishPrice;
        total = total + subTotal;
    }
    return total;
}

-(void)removeDishByIndexInDishes: (NSInteger)index{
    [[self allDishFromOrder] removeObjectAtIndex:index];
}

-(void)removeDishByName: (NSString *)dishName{
    NSMutableArray *allDishes = [self allDishFromOrder];
    
    NSMutableArray *removeDish = [NSMutableArray array];
    for(NSDictionary *dish in allDishes){
        if([dishName isEqualToString:[dish objectForKey:DISH_NAME]]){
            [removeDish addObject:dish];
            break;
        }
    }
    
    if([removeDish count] > 0){
        [allDishes removeObjectsInArray:removeDish];
    }
}


#pragma mark - Determine Engine: Append or Add new
-(void)determineShouldAddOrAppend: (DishEntity *)dish{
    NSString *dishId = dish.dishId;
    
    //first serialize the dish entity to nsdictionary to comparison more convenient
    NSDictionary *dishByDict = [self serializeJsonFromDishEntity:dish];
    
    NSMutableArray *allDishes = [self allDishFromOrder];
    if([allDishes count] == 0){
        //no dishes in order, add it not append
        [allDishes addObject:dishByDict];
    }else{
        //dishes is existed in order, let walkthrough each of them and check id, options
        //first check the id, whether the same dish is existed
        
        //citrus a fres NSArrayM, add all needed to it, then we will add it later to allDishes to avoid: "<__NSArrayM: 0x742a580> was mutated while being enumerated."
        NSMutableArray *needToAddArray = [NSMutableArray array];
        
        for(NSMutableDictionary *dishDict in allDishes){
            if([self compareExistDishId:dishDict withId:dishId]){
                //check whether dish has options
                if(![self dishDictHasOptions:dishByDict]){
                    NSInteger quantity = [self determineQuantityInCaseFromOne:[[dishDict objectForKey:DISH_QUANTITY] integerValue] withTwo:[[dishByDict objectForKey:DISH_QUANTITY] integerValue]];
                    [dishDict setObject:[NSNumber numberWithInteger:quantity] forKey:DISH_QUANTITY];
                    break;
                }else{
                    if([self compareAllOptionsOf:[dishDict objectForKey:DISH_OPTIONS] with:[dishByDict objectForKey:DISH_OPTIONS]]){
                        //if there're a same dish is existed, let check whether options is the same
                        //then compare all options
                        //append, let get quantity first, then increase it
                        NSInteger quantity = [self determineQuantityInCaseFromOne:[[dishDict objectForKey:DISH_QUANTITY] integerValue] withTwo:[[dishByDict objectForKey:DISH_QUANTITY] integerValue]];
                        //finally set it again to dictionary
                        [dishDict setObject:[NSNumber numberWithInteger:quantity] forKey:DISH_QUANTITY];
                        break;
                    }else{
                        if([allDishes indexOfObject:dishDict] == allDishes.count -1){
                            //no dishes in the order is the same, let add new
                            [needToAddArray addObject:dishByDict];
                        }
                    }
                }
            }else{
                if([allDishes indexOfObject:dishDict] == allDishes.count -1){
                    //no dishes in the order is the same, let add new
                    [needToAddArray addObject:dishByDict];
                }
            }
        }
        
        if([needToAddArray count] != 0){
            [allDishes addObjectsFromArray:needToAddArray];
        }
    }
}

-(NSInteger)determineQuantityInCaseFromOne: (NSInteger)quantityOne withTwo: (NSInteger)quantityTwo{
    if(editMode){
        return quantityTwo;
    }else{
        return quantityOne + quantityTwo;
    }
}

-(NSMutableArray *)allDishFromOrder{
    NSMutableArray *allDishes = [dishes objectForKey:KEY_DISHES];
    return allDishes;
}

-(NSArray *)optionsOfDishById: (NSString*)dishId optionsName: (NSString *)name{
    NSArray *allDishes = [self allDishFromOrder];
    
    for(NSDictionary *dish in allDishes){
        //compare with existed dish by ID
        if([self compareExistDishId:dish withId:dishId]){
            //if dish ID is match together, get options by name
            NSDictionary *optionsCollection = [dish objectForKey:DISH_OPTIONS];
            NSArray *allOptionsByName = [optionsCollection objectForKey:name];
            return allOptionsByName;
        }
    }
    return nil;
}

-(NSDictionary *)dishForDishId: (NSString *)dishId{
    NSArray *allDishes = [[NSArray alloc] initWithArray:[self allDishFromOrder]];
    NSArray *ret = predicateArray(allDishes, @"SELF.dishId = %@", dishId);
    if([ret count] != 0){
        return [ret firstObject];
    }
    return nil;
}

-(NSDictionary *)dishForIndex: (NSInteger)index{
    return [dishes.allValues objectAtIndex:index];
}

#pragma mark - Comparison and Checking Data
-(BOOL)dishDictHasOptions: (NSDictionary *)dishDict{
    NSArray *dishOptions = [dishDict objectForKey:DISH_OPTIONS];
    if([dishOptions count] > 0){
        return YES;
    }
    return NO;
}

-(BOOL)compareExistDishId:(NSDictionary *)dish withId: (NSString*)dishId{
    if([[dish objectForKey:DISH_ID] isEqualToString:dishId]){
        return YES;
    }
    return NO;
}

-(BOOL)compareAllOptionsOf: (NSArray *)fromOne with: (NSArray *)toTwo{
    BOOL flag = NO;
    
    for(NSDictionary *optionsDictOne in fromOne){
        for(NSDictionary *optionsDictTwo in toTwo){
            //check the name of it: Sauce, Vegetable, this only has one key
            if([[optionsDictOne.allKeys firstObject] isEqualToString:[optionsDictTwo.allKeys firstObject]]){
                //if the name is the same, let check all option inside it
                if([self compareIndividualOption:[optionsDictOne.allValues firstObject] two:[optionsDictTwo.allValues firstObject]]){
                    flag =YES;
                }else{
                    flag = NO;
                    break;
                }
            }
        }
        if(!flag){
            break;
        }
    }
    return flag;
}

-(BOOL)compareIndividualOption: (NSArray *)optionsOne two: (NSArray *)optionsTwo{
    BOOL flag = NO;
    if([optionsOne count] != [optionsTwo count]){
        return flag;
    }else if([optionsOne count] == [optionsTwo count]){
        if([optionsOne count] == 0 && [optionsTwo count] == 0){
            flag = YES;
        }else{
            for(NSArray *fromOne in optionsOne){
                for(NSArray *fromTwo in optionsTwo){
                    if([self compareIndividualByArray:fromOne two:fromTwo]){
                        flag = YES;
                    }else{
                        flag = NO;
                    }
                }
            }
        }
    }
    return flag;
}

-(BOOL)compareIndividualByArray: (NSArray *)fromOne two: (NSArray *)fromTwo{
    NSString *fromOneStr = [fromOne firstObject];
    NSString *fromTwoStr = [fromTwo firstObject];
    
    if([fromOneStr isEqualToString:fromTwoStr]){
        return YES;
    }else{
        return NO;
    }
}


#pragma mark - Serializer
-(NSDictionary *)serializeJsonFromDishEntity: (DishEntity *)dish{
    NSNumber *addOn;
    NSMutableDictionary *dictD = [[NSMutableDictionary alloc] init];
    [dictD setObject:dish.dishId forKey:DISH_ID];
    [dictD setObject:dish.dishName forKey:DISH_NAME];
    [dictD setObject:[NSNumber numberWithInteger:dish.dishQuantity] forKey:DISH_QUANTITY];
    
    NSMutableArray *arrayOpts = [self serializeOptionsGroup:dish.dishOptions];
    
    if([arrayOpts count] > 0){
        if([arrayOpts lastObject]){
            addOn = [arrayOpts lastObject];
            [arrayOpts removeLastObject];
        }
        
        [dictD setObject:arrayOpts forKey:DISH_OPTIONS];
        
        if(addOn){
            [dictD setObject:[NSNumber numberWithDouble:(dish.dishPrice + [addOn doubleValue])] forKey:DISH_PRICE];
        }else{
            [dictD setObject:[NSNumber numberWithDouble:dish.dishPrice] forKey:DISH_PRICE];
        }
    }else{
        [dictD setObject:[NSNumber numberWithDouble:dish.dishPrice] forKey:DISH_PRICE];
    }
    return dictD;
}

-(NSMutableArray *)serializeOptionsGroup: (NSArray *)optionsEntity{
    NSMutableArray *arrayOpts = [[NSMutableArray alloc] init];
    NSNumber *addOnPrice;
    
    for(DishOptionsEntity *options in optionsEntity){
        NSMutableDictionary *dictOpts = [[NSMutableDictionary alloc] init];
        NSMutableArray *optionCollection = [self serializeIndividualOption:options.options];
        addOnPrice = [optionCollection lastObject];
        [optionCollection removeLastObject];
        
        [dictOpts setObject:optionCollection forKey:options.optionName];
        NSDictionary *dict = [[NSDictionary alloc] initWithDictionary:dictOpts];
        [arrayOpts addObject:dict];
    }
    addOnPrice?[arrayOpts addObject:addOnPrice]:nil;
    
    return arrayOpts;
}

-(NSMutableArray *)serializeIndividualOption: (NSArray *)optionsEntity{
    NSMutableArray *optionCollection = [[NSMutableArray alloc] init];
    double addOn = 0.0f;
    
    for(DishOptionEntity *option in optionsEntity){
        if(option.selected){
            if(option.dishOptionAddOnPrice > 0){
                addOn += option.dishOptionAddOnPrice;
            }
            
            NSArray *arrayOpt = @[option.dishOptionName, [NSNumber numberWithDouble:option.dishOptionAddOnPrice]];
            [optionCollection addObject:arrayOpt];
        }
    }
    [optionCollection addObject:[NSNumber numberWithDouble:addOn]];

    return optionCollection;
}
@end
