//
//  MenuConstant.h
//  Light Order
//
//  Created by Phong Nguyen on 10/4/15.
//  Copyright © 2015 Triad Fox. All rights reserved.
//

#import <Foundation/Foundation.h>

extern NSString *const OPTION_ITEM_KIND_OPTIONAL;
extern NSString *const OPTION_ITEM_KIND_DEFAUL;

typedef NS_ENUM(NSInteger, DishOptionItemType) {
    kOptionItemOptional = 0,
    kOptionItemDefault =1
};

typedef NS_ENUM(NSInteger, BitValueType) {
    kBitYes = 1,
    kBitNo = 0
};

typedef NS_ENUM(NSInteger, MenuCurrency){
    kMenuCurrencyVND = 0,
    kMenuCurrencyUSD = 1,
    kMenuCurrencyEURO = 2
};
