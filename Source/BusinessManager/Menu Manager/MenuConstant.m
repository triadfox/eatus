//
//  MenuConstant.m
//  Light Order
//
//  Created by Phong Nguyen on 10/4/15.
//  Copyright © 2015 Triad Fox. All rights reserved.
//

#import "MenuConstant.h"

NSString *const OPTION_ITEM_KIND_OPTIONAL = @"Optional";
NSString *const OPTION_ITEM_KIND_DEFAUL = @"Default";