//
//  MenuEngineManager.h
//  Light Order
//
//  Created by Phong Nguyen on 10/4/15.
//  Copyright © 2015 Triad Fox. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "GDataXMLNodeEx.h"

@interface MenuEngineManager : NSObject

+(id)shareManager;
-(void)initWithMenuFile: (NSString *)filePath;

-(NSArray *)getAllDishes;
-(NSArray *)getAllCategory;
-(NSInteger)getQuantityLimited;
-(void)cleanAndSweepAll;

-(float)taxPercentageForCurrentMenu;
-(NSLocale *)localeByCurrentCurrency;
+(NSLocale *)localeByCurrencyInput: (NSString *)currency;
-(NSString *)localCurrency;
@end
