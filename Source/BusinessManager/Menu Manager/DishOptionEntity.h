//
//  DishOptionEntity.h
//  Light Order
//
//  Created by Phong Nguyen on 10/6/15.
//  Copyright © 2015 Triad Fox. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "MenuConstant.h"

@interface DishOptionEntity : NSObject<NSCopying>

@property(nonatomic, strong) NSString *dishOptionName;
@property(nonatomic, assign) DishOptionItemType dishOptionItemType;
@property(nonatomic, assign) BOOL selected;
@property(nonatomic, assign) double dishOptionAddOnPrice;

@end
