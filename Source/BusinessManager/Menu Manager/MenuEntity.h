//
//  MenuEntity.h
//  Light Order
//
//  Created by Phong Nguyen on 10/4/15.
//  Copyright © 2015 Triad Fox. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "MenuConstant.h"
#import "DishEntity.h"

@interface MenuEntity : NSObject<NSCopying>

@property(nonatomic, strong) NSNumber *majorId;
@property(nonatomic, assign) float taxPercentage;
@property(nonatomic, weak) NSString *version;
@property(nonatomic, strong) NSArray *dishes;
@property(nonatomic, strong) NSArray *categories;
@property(nonatomic, assign) NSInteger quantityLimit;
@property(nonatomic, assign) MenuCurrency currency;
@end
