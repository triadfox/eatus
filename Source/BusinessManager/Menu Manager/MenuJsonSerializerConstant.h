//
//  MenuJsonSerializerConstant.h
//  Eatus
//
//  Created by Phong Nguyen on 11/18/15.
//  Copyright © 2015 Triad Fox. All rights reserved.
//

#define DISH_ID                         @"dishId"
#define DISH_NAME                       @"dishName"
#define DISH_PRICE                      @"dishPrice"
#define DISH_OPTIONS                    @"dishOptions"
#define DISH_QUANTITY                   @"dishQuantity"

#define RECEIPT_ID                      @"receiptId"
#define SUB_TOTAL                       @"subTotal"

#define KEY_RECEIPT_LOCALE              @"locale"
#define KEY_ORDER                       @"order"
#define KEY_DISHES                      @"dishes"
#define KEY_RECEIPT_ID                  @"receiptId"
#define KEY_TOTAL                       @"total"
#define KEY_SUB_TOTAL                   @"subtotal"
#define KEY_CURRENCY                    @"currency"
#define KEY_PAYMENT_TYPE                @"paymentType"
#define KEY_RECEIPT_DISCOUNT_PERCENTAGE @"discountPercentage"
#define KEY_RECEIPT_DISCOUNT_AMOUNT     @"discountAmount"
#define KEY_RECEIPT_TAX                 @"tax"

#define KEY_MSG_TRANSFERRING            @"msgType"

#define RECEIPT_CODE_LENGTH             6