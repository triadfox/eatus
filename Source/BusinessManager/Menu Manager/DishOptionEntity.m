//
//  DishOptionEntity.m
//  Light Order
//
//  Created by Phong Nguyen on 10/6/15.
//  Copyright © 2015 Triad Fox. All rights reserved.
//

#import "DishOptionEntity.h"

@implementation DishOptionEntity

-(id)copyWithZone:(NSZone *)zone{
    id copy = [[[self class] alloc] init];
    if(copy){
        [copy setDishOptionName:[self.dishOptionName copyWithZone:zone]];
        [copy setDishOptionItemType:self.dishOptionItemType];
    }
    return copy;
}
@end
