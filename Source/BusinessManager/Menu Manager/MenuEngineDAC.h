//
//  MenuEngineDAC.h
//  Light Order
//
//  Created by Phong Nguyen on 10/4/15.
//  Copyright © 2015 Triad Fox. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "GDataXMLNodeEx.h"

@interface MenuEngineDAC : NSObject

+ (GDataXMLDocument *)menuFileForPath: (NSString *)filePath;

@end
