//
//  MenuEngineManager.m
//  Light Order
//
//  Created by Phong Nguyen on 10/4/15.
//  Copyright © 2015 Triad Fox. All rights reserved.
//

#import "MenuEngineManager.h"
#import "MenuEngineDAC.h"
#import "DishEntity.h"
#import "MenuEntity.h"
#import "DishOptionsEntity.h"
#import "DishOptionEntity.h"

#define CURRENCY_VND                            @"VND"
#define CURRENCY_USD                            @"USD"
#define CURRENCY_EURO                           @"EURO"

#define NODE_CATEGORIES                         @"categories"
#define NODE_DISHES                             @"dishes"

//dish node, attributes
#define ATTRIBUTE_DISH_ID                       @"id"
#define ATTRIBUTE_DISH_CATEGORY                 @"category"
#define ATTRIBUTE_DISH_QUANTITY                 @"quantity"
#define ATTRIBUTE_OPTIONS_NAME                  @"name"
#define NODE_DISH_TITLE                         @"name"
#define NODE_DISH_DESCRIPTION                   @"description"
#define NODE_DISH_PRICE                         @"price"
#define NODE_DISH_IMAGE                         @"image"
#define NODE_DISH_CATEGORY                      @"category"
#define NODE_DISH_ITEMS                         @"options"

//option attribute
#define ATTRIBUTE_OPTION_TYPE                   @"type"
#define ATTRIBUTE_OPTION_ADDONS_TYPE            @"addOn"
#define ATTRIBUTE_OPTION_EXCLUSIVE_SELECTION    @"exclusive"
#define ATTRIBUTE_OPTION_ADDONS_PRICE           @"price"

//menu attribute, node
#define ATTRIBUTE_MENU_MAJOR_ID                 @"majorId"
#define ATTRIBUTE_MENU_VERSION                  @"version"
#define ATTRIBUTE_MENU_CURRENCY                 @"currency"
#define ATTRIBUTE_MENU_TAX_PERCENTAGE           @"tax-percentage"

#define NODE_MENU_QUANTITY_LIMIT                @"quantity-limit"

@interface MenuEngineManager()

@property(nonatomic, strong) NSArray *dishes;
@property(nonatomic, strong) NSArray *categories;
@property(nonatomic, strong) MenuEntity *menu;

@end

@implementation MenuEngineManager

+(id)shareManager{
    static MenuEngineManager *shareManager;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        shareManager = [[MenuEngineManager alloc] init];
    });
    return shareManager;
}

-(float)taxPercentageForCurrentMenu{
    return self.menu.taxPercentage;
}

- (NSLocale *)localeByCurrentCurrency{
    switch (self.menu.currency) {
        case kMenuCurrencyVND:
            return [[NSLocale alloc] initWithLocaleIdentifier:LOCALE_VIE_VN];
            break;
            
        case kMenuCurrencyUSD:
            return [[NSLocale alloc] initWithLocaleIdentifier:LOCALE_EN_US];
            
        case kMenuCurrencyEURO:
            return [[NSLocale alloc] initWithLocaleIdentifier:LOCALE_EN_EUROPE];
            
        default:
            break;
    }
}

+(NSLocale *)localeByCurrencyInput:(NSString *)currency{
    NSLocale *locale;
    
    if([currency isEqualToString:@"VND"]){
        locale = [[NSLocale alloc] initWithLocaleIdentifier:LOCALE_VIE_VN];
    }else if([currency isEqualToString:@"USD"]){
        locale = [[NSLocale alloc] initWithLocaleIdentifier:LOCALE_EN_US];
    }else if([currency isEqualToString:@"EUR"]){
        locale = [[NSLocale alloc] initWithLocaleIdentifier:LOCALE_EN_EUROPE];
    }
    
    return locale;
}

-(NSString *)localCurrency{
    switch (self.menu.currency) {
        case kMenuCurrencyVND:
            return @"VND";
            break;
            
        case kMenuCurrencyUSD:
            return @"USD";
            break;
            
            case kMenuCurrencyEURO:
            return @"EUR";
            
        default:
            break;
    }
}



-(void)cleanAndSweepAll{
    self.menu = [[MenuEntity alloc] init];
}

-(void)initWithMenuFile:(NSString *)filePath{
    [self startParseMenuFile:filePath];
}

-(void)startParseMenuFile: (NSString *)filePath{
    GDataXMLDocument *menuDocument = [MenuEngineDAC menuFileForPath:filePath];
    
    NSArray *categoriesAndDishes = [[[menuDocument nodesForXPath:@"child::*"] firstObject] children];
    if([categoriesAndDishes count] == 0){
        NSLog(@"Categories and Dishes nodes is nil");
        return;
    }
    
    MenuEntity *menuEntity = [self startParseToGetMenuData:[menuDocument singleNodeForXPath:@"menu"]];
    
    GDataXMLElement *categoriesNode = [categoriesAndDishes objectAtIndex:1];
    GDataXMLElement *dishesNode = [categoriesAndDishes lastObject];
    
    menuEntity.categories = [self startParseToGetCategories:categoriesNode];
    menuEntity.dishes = [self startParseToGetDishes:dishesNode];
    self.menu = menuEntity;
}

-(MenuEntity *)startParseToGetMenuData: (GDataXMLElement *)menuToParse{
    MenuEntity *menu = [[MenuEntity alloc] init];
    NSArray *attributes = [menuToParse attributes];
    
    for(GDataXMLNode *attribute in attributes){
        NSString *attributeName = attribute.name;
        NSString *attributeValue = attribute.stringValue;
        
        if([attributeName isEqualToString:ATTRIBUTE_MENU_MAJOR_ID]){
            menu.majorId = [NSNumber numberWithInteger:[attributeValue integerValue]];
        }else if([attributeName isEqualToString:ATTRIBUTE_MENU_VERSION]){
            menu.version = attributeValue;
        }else if([attributeName isEqualToString:ATTRIBUTE_MENU_CURRENCY]){
            NSString *currencyStr = attributeValue;
            if([currencyStr isEqualToString:CURRENCY_VND]){
                menu.currency = kMenuCurrencyVND;
            }else if([currencyStr isEqualToString:CURRENCY_USD]){
                menu.currency = kMenuCurrencyUSD;
            }else if([currencyStr isEqualToString:CURRENCY_EURO]){
                menu.currency = kMenuCurrencyEURO;
            }
        }else if([attributeName isEqualToString:ATTRIBUTE_MENU_TAX_PERCENTAGE]){
            menu.taxPercentage = [attributeValue floatValue];
        }
    }
    
    GDataXMLElement *limitQuantityElement = [menuToParse singleNodeForXPath:@"quantity-limit"];
    if(limitQuantityElement){
        menu.quantityLimit = [[limitQuantityElement stringValue] integerValue];
    }
    return menu;
}

-(NSArray *)startParseToGetCategories: (GDataXMLElement *)categoriesToParse{
    NSArray *allCategories = [categoriesToParse children];
    NSMutableArray *categoriesFound = [[NSMutableArray alloc] init];
    
    if([allCategories count] == 0){
        NSLog(@"No categories found in menu file");
        return nil;
    }
    
    for(GDataXMLElement *category in allCategories){
        [categoriesFound addObject:[category stringValue]];
    }
    return [NSArray arrayWithObject:categoriesFound];
}

-(NSArray *)startParseToGetDishes: (GDataXMLElement *)dishesFound{
    NSArray *allDishes = [dishesFound children];
    NSMutableArray *dishesCollection = [[NSMutableArray alloc] init];
    
    if([allDishes count] == 0){
        NSLog(@"No dishes found in menu file");
        return nil;
    }
    
    for(GDataXMLElement *dish in allDishes){
        [dishesCollection addObject:[self startParseToGetDishDetail:dish]];
    }
    return [NSArray arrayWithArray:dishesCollection];
}

-(DishEntity *)startParseToGetDishDetail: (GDataXMLElement *)dishElement{
    DishEntity *dishEntity = [[DishEntity alloc] init];
    dishEntity.dishId = [[dishElement attributeForName:ATTRIBUTE_DISH_ID] stringValue];
    dishEntity.dishCategory = [[dishElement attributeForName:ATTRIBUTE_DISH_CATEGORY] stringValue];
    dishEntity.dishQuantity = [[[dishElement attributeForName:ATTRIBUTE_DISH_QUANTITY] stringValue] integerValue];
    
    NSArray *dishDetails = [dishElement children];
    if([dishDetails count] == 0){
        NSLog(@"Details of dish is nil");
        return nil;
    }
    
    NSArray *optionsCollection = [dishElement elementsForName:NODE_DISH_ITEMS];
    NSMutableArray *allOptions = [[NSMutableArray alloc] init];
    for(GDataXMLElement *options in optionsCollection){
        DishOptionsEntity *optionsEntity = [[DishOptionsEntity alloc] init];
        optionsEntity.optionName = [[options attributeForName:ATTRIBUTE_OPTIONS_NAME] stringValue];
        NSArray *optionsGot = [self startParseToGetOptionFromOptions:options];
        
        //determine exclusive, addOn of option group (options)
        NSInteger bitExclusiveStr = [[[options attributeForName:ATTRIBUTE_OPTION_EXCLUSIVE_SELECTION] stringValue] integerValue];
        NSInteger bitAddOnStr = [[[options attributeForName:ATTRIBUTE_OPTION_ADDONS_TYPE] stringValue] integerValue];
        
        if([self determineBitValueString:bitExclusiveStr]){
            optionsEntity.optionExclusiveSelection = YES;
        }
        
        if([self determineBitValueString:bitAddOnStr]){
            optionsEntity.optionAddOn = YES;
        }
        
        optionsEntity.options = (optionsGot.count > 0)?[NSArray arrayWithArray:optionsGot]:nil;
        
        if([optionsEntity.options count] > 0){
            [allOptions addObject:optionsEntity];
        }
    }
    
    for(GDataXMLElement *detail in dishDetails){
        NSString *detailValue = [detail stringValue];
        NSString *detailName = [detail name];
        
        dishEntity.dishOptions = [NSArray arrayWithArray:allOptions];
        
        if([detailName isEqualToString:NODE_DISH_TITLE]){
            dishEntity.dishName = detailValue;
        }else if([detailName isEqualToString:NODE_DISH_IMAGE]){
            dishEntity.dishImageURL = detailValue;
        }else if([detailName isEqualToString:NODE_DISH_DESCRIPTION]){
            dishEntity.dishDescription = detailValue;
        }else if([detailName isEqualToString:NODE_DISH_PRICE]){
            dishEntity.dishPrice = [detailValue doubleValue];
        }else if([detailName isEqualToString:NODE_DISH_ITEMS]){
            continue;
        }
    }
    return dishEntity;
}

-(NSArray *)startParseToGetOptionFromOptions: (GDataXMLElement *)optionsElement{
    DishOptionEntity *optionEntity;
    
    NSMutableArray *optionsCollection = [[NSMutableArray alloc ] init];
    
    NSArray *allOptions = [optionsElement children];
    
    if([allOptions count] == 0){
        return nil;
    }
    
    for(GDataXMLElement *option in allOptions){
        optionEntity = [[DishOptionEntity alloc] init];
        optionEntity.dishOptionName = [option stringValue];
        
        GDataXMLNode *typeAttribute = [option attributeForName:ATTRIBUTE_OPTION_TYPE];
        if(!typeAttribute){
            NSLog(@"Option attribute type is nil");
            continue;
        }
        
        //get type (default/optional)
        NSInteger typeAttributeInNumber = [[typeAttribute stringValue] integerValue];
        if(typeAttributeInNumber == kOptionItemDefault){
            optionEntity.dishOptionItemType = kOptionItemDefault;
            optionEntity.selected = YES;
        }else{
            optionEntity.dishOptionItemType = kOptionItemOptional;
            optionEntity.selected = NO;
        }
        
        //determine addon price
        GDataXMLNode *addOnPriceAttribute = [option attributeForName:ATTRIBUTE_OPTION_ADDONS_PRICE];
        
        if(addOnPriceAttribute){
            double addOnPrice = [[addOnPriceAttribute stringValue] doubleValue];
            optionEntity.dishOptionAddOnPrice = addOnPrice;
        }
        
        [optionsCollection addObject:optionEntity];
    }
    return [NSArray arrayWithArray:optionsCollection];
}

-(NSArray *)getAllCategory{
    if(self.menu){
        return self.menu.categories;
    }
    return nil;
}

-(NSInteger)getQuantityLimited{
    if(self.menu){
        return self.menu.quantityLimit;
    }
    return 0;
}

-(NSArray *)getAllDishes{
    if(self.menu){
        return self.menu.dishes;
    }
    return nil;
}

-(BOOL)determineBitValueString: (BitValueType)bitValue{
    if(bitValue == kBitYes){
        return YES;
    }else{
        return NO;
    }
}
@end
