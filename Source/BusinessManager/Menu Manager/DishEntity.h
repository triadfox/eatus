//
//  DishEntity.h
//  Light Order
//
//  Created by Phong Nguyen on 10/4/15.
//  Copyright © 2015 Triad Fox. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "DishOptionEntity.h"
#import "DishOptionsEntity.h"

@interface DishEntity : NSObject<NSCopying>

@property(nonatomic, strong) NSString *dishId;
@property(nonatomic, strong) NSString *dishName;
@property(nonatomic, strong) NSString *dishImageURL;
@property(nonatomic, strong) NSString *dishDescription;
@property(nonatomic, assign) double dishPrice;
@property(nonatomic, strong) NSString *dishCategory;
@property(nonatomic, strong) NSArray *dishOptions;
@property(nonatomic, assign) NSInteger dishQuantity;

@end
