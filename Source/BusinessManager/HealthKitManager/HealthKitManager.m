//
//  HealthKitManager.m
//  Eatus
//
//  Created by Phong Nguyen on 12/4/15.
//  Copyright © 2015 Triad Fox. All rights reserved.
//

#import "HealthKitManager.h"
#import "AlertViewManager.h"

@interface HealthKitManager()

@property(nonatomic, strong) HKHealthStore *healthStore;

@end

@implementation HealthKitManager
@synthesize healthStore;

+(instancetype)standardManager{
    static HealthKitManager *manager;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        manager = [[HealthKitManager alloc] init];
    });
    return manager;
}


#pragma mark - Private methods
-(void)launchHealthKitManager{
    if([HKHealthStore isHealthDataAvailable]){
        dispatch_sync(dispatch_get_main_queue(), ^{
            AMSmoothAlertView *alertView = [AlertViewManager smoothDropAlertViewWithTitle:@"Health" content:@"Your device doesn't support Health Measurement" cancelButton:NO color:COLOR_MAIN_THEME alertType:AlertInfo onCompletion:^(AMSmoothAlertView *alert, UIButton *button){
                
            }];
            [alertView.logoView setImage:[UIImage imageNamed:@"Health"]];
            
            [alertView show];
        });
    }else{
        NSArray *readTypes = @[[HKObjectType characteristicTypeForIdentifier:HKCharacteristicTypeIdentifierDateOfBirth], [HKObjectType characteristicTypeForIdentifier:HKCharacteristicTypeIdentifierBiologicalSex], [HKObjectType quantityTypeForIdentifier:HKQuantityTypeIdentifierActiveEnergyBurned]];
        [healthStore requestAuthorizationToShareTypes:[NSSet setWithArray:readTypes] readTypes:nil completion:^(BOOL succeeded, NSError *error){
            NSLog(@"Did activate health kit");
        }];
    }
}
@end
