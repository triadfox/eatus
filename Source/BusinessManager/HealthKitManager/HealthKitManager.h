//
//  HealthKitManager.h
//  Eatus
//
//  Created by Phong Nguyen on 12/4/15.
//  Copyright © 2015 Triad Fox. All rights reserved.
//

#import <Foundation/Foundation.h>

#import <HealthKit/HealthKit.h>

@interface HealthKitManager : NSObject

+(instancetype)standardManager;

@end
