//
//  WatchConnectivityManager.m
//  Eatus
//
//  Created by Phong Nguyen on 11/22/15.
//  Copyright © 2015 Triad Fox. All rights reserved.
//

#import "WatchConnectivityManager.h"
#import <WatchConnectivity/WatchConnectivity.h>

@interface WatchConnectivityManager()<WCSessionDelegate>

@property(nonatomic, strong) WCSession *session;

@end

@implementation WatchConnectivityManager
@synthesize session;

+(instancetype)standardConnectivity{
    static WatchConnectivityManager *manager;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        manager = [[WatchConnectivityManager alloc] init];
        [manager launchConnectivityManager];
    });
    return manager;
}


#pragma mark - Private methods
-(void)launchConnectivityManager{
    if([WCSession isSupported]){
        session = [WCSession defaultSession];
        session.delegate = self;
        [session activateSession];
    }
}

#pragma mark -Public Methods
-(BOOL)didPairWithParentDevice{
    if(session){
        return session.reachable;
    }
    return NO;
}

-(void)sendMessageToWatchWithApplicationContextWithMessage:(NSDictionary *)message onCompletion:(void (^)(BOOL, NSError *))completionHandler{
    NSError *error = nil;
    [session updateApplicationContext:message error:&error];
    
    if(error){
        completionHandler(NO, error);
    }else{
        completionHandler(YES, nil);
    }
}

-(void)sendMessageToWatchInteractiveMessage:(NSDictionary *)message onCompletion:(void (^)(NSDictionary *, NSError *))completionHandler{
    [session sendMessage:message replyHandler:^(NSDictionary *replyMessage) {
        completionHandler(replyMessage, nil);
    }errorHandler:^(NSError *error){
        completionHandler(nil, error);
    }];
}

-(void)sendMessageDataToWatchInteractiveMessageData:(NSData *)messageData onCompletion:(void (^)(NSData *, NSError *))completionHandler{
    [session sendMessageData:messageData replyHandler:^(NSData *data){
        completionHandler(data, nil);
    }errorHandler:^(NSError *error){
        completionHandler(nil, error);
    }];
}

-(WCSessionFileTransfer *)transferFileToWatchWithFileURL:(NSURL *)fileURL metadata:(NSDictionary *)metadata{
    WCSessionFileTransfer *fileTransferSession = [session transferFile:fileURL metadata:metadata];
    if(fileTransferSession){
        return fileTransferSession;
    }else{
        return nil;
    }
}

-(WCSessionUserInfoTransfer *)transferUserInfo:(NSDictionary *)userInfo{
    WCSessionUserInfoTransfer *userInfoTransferSession = [session transferUserInfo:userInfo];
    if(userInfoTransferSession){
        return userInfoTransferSession;
    }else{
        return nil;
    }
}


#pragma mark - WCSession Delegate methods
-(void)session:(WCSession *)session didFinishFileTransfer:(WCSessionFileTransfer *)fileTransfer error:(NSError *)error{
    
}

-(void)session:(WCSession *)session didFinishUserInfoTransfer:(WCSessionUserInfoTransfer *)userInfoTransfer error:(NSError *)error{
    
}

-(void)session:(WCSession *)session didReceiveApplicationContext:(NSDictionary<NSString *,id> *)applicationContext{
    
}

-(void)session:(WCSession *)session didReceiveFile:(WCSessionFile *)file{
    
}

-(void)session:(WCSession *)session didReceiveMessage:(NSDictionary<NSString *,id> *)message{
    
}

-(void)session:(WCSession *)session didReceiveMessage:(NSDictionary<NSString *,id> *)message replyHandler:(void (^)(NSDictionary<NSString *,id> * _Nonnull))replyHandler{
    
}

-(void)session:(WCSession *)session didReceiveMessageData:(NSData *)messageData replyHandler:(void (^)(NSData * _Nonnull))replyHandler{
    
}

-(void)session:(WCSession *)session didReceiveUserInfo:(NSDictionary<NSString *,id> *)userInfo{
    
}

-(void)sessionReachabilityDidChange:(WCSession *)session{
    
}

-(void)sessionWatchStateDidChange:(WCSession *)session{
    
}
@end
