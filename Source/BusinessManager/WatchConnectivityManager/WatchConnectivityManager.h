//
//  WatchConnectivityManager.h
//  Eatus
//
//  Created by Phong Nguyen on 11/22/15.
//  Copyright © 2015 Triad Fox. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <WatchConnectivity/WatchConnectivity.h>

@interface WatchConnectivityManager : NSObject

+(instancetype)standardConnectivity;
-(BOOL)didPairWithParentDevice;
-(void)sendMessageToWatchWithApplicationContextWithMessage: (NSDictionary *)message onCompletion: (void (^)(BOOL, NSError*))completionHandler;
-(void)sendMessageToWatchInteractiveMessage: (NSDictionary *)message onCompletion: (void (^)(NSDictionary*, NSError*))completionHandler;
-(void)sendMessageDataToWatchInteractiveMessageData: (NSData *)messageData onCompletion: (void (^)(NSData*, NSError *))completionHandler;
-(WCSessionFileTransfer *)transferFileToWatchWithFileURL: (NSURL *)fileURL metadata: (NSDictionary *)metadata;
-(WCSessionUserInfoTransfer *)transferUserInfo: (NSDictionary *)userInfo;
@end
