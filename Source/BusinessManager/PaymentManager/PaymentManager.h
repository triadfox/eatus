//
//  PaymentManager.h
//  Eatus
//
//  Created by Phong Nguyen on 10/23/15.
//  Copyright © 2015 Triad Fox. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Stripe.h"

@interface PaymentManager : NSObject

+(instancetype)defaultPayment;
+(void)registerStripeTest;
+(void)registerStripeLive;

+(STPCardParams *)generateCardParamWithNameCardHolder: (NSString *)name cardNumber: (NSString *)number cvcNumber: (NSString *)cvcNumber expMonth: (NSInteger)expMonth expYear: (NSInteger)expYear;
+(STPCardBrand)cardBrandNameForNumber: (NSString *)carNumber;

+(void)mycardWithBrandName: (NSString *)last4Id cardBrand: (STPCardBrand)brandName;
+(STPCardBrand)mycardBrandNameForLast4Id: (NSString *)last4Id;

+(void)mycardKeychainWithBrandName: (NSString *)last4Id cardBrand: (STPCardBrand)brandName;
+(STPCardBrand)mycardBrandNameKeychainForLast4Id: (NSString *)last4Id;

+(STPCardBrand)cardBrandNameForTokenId: (NSString *)tokenId;

+(UIImage *)imageForSTPCardBrand: (STPCardBrand)cardBrand;


-(void)paymentWithCardHolderName: (NSString *)name cardId: (NSString *)cardId CVCNumber: (NSString *)CVCNumber expMonth: (NSInteger)expMonth expYear: (NSInteger)expYear onCompletion: (void (^)(BOOL succeeded, NSError *error))completion;

-(void)registerCardWithHolderName: (NSString *)name cardNumber: (NSString *)cardNumber cvcNumber: (NSString *)cvc expMonth: (NSInteger)expMonth expYear:(NSInteger)expYear onCompletion: (void (^)(STPToken *token, NSError *error))completion;

-(BOOL)validateCardWithInformation: (NSString *)name cardId: (NSString *)cardId CVCNumber: (NSString *)CVCNumber expMonth: (NSInteger)expMonth expYear: (NSInteger)expYear;
@end
