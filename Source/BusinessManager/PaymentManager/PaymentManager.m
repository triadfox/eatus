//
//  PaymentManager.m
//  Eatus
//
//  Created by Phong Nguyen on 10/23/15.
//  Copyright © 2015 Triad Fox. All rights reserved.
//

#import "PaymentManager.h"

#import "Stripe.h"
#import "UICKeyChainStore.h"
#import "AFNetworking.h"

#define STRIPE_TEST                                         @"StripeTest"
#define STRIPE_TEST_SECRET_KEY                              @"TestSecretKey"
#define STRIPE_TEST_PUBLISH_KEY                             @"TestPublishableKey"

#define STRIPE_LIVE                                         @"StripeLive"
#define STRIPE_LIVE_SECRET_KET                              @"LiveSecretKey"
#define STRIPE_LIVE_PUBLISH_KEY                             @"LivePublishableKey"

#define STRIPE_CHARGE_WITH_TOKEN                            @"https://example.com/token"
#define EATUS_CHARGE_WITH_TOKEN                             @""

#define PAYMENT_TOKEN_ID_PARAM                              @"stripeToken"

#define EATUS_LAST4_WITH_BRAND_NUMBER_SERVICE                          @"com.triadfox.eatus.last4BrandService"
#define EATUS_LAST4_WITH_BRAND_NUMBER_ACCESS_GROUP                     @"com.triadfox.eatus.last4BrandAccessGroup"

@interface PaymentManager()

@property(nonatomic, strong) UICKeyChainStore *keychainStore;
@property(nonatomic, copy) NSError *keychainError;

@end

@implementation PaymentManager
@synthesize keychainError;
@synthesize keychainStore;

+(instancetype)defaultPayment{
    static PaymentManager *manager;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        manager = [[PaymentManager alloc] init];
        [manager instantiateKeyChainStore];
    });
    return manager;
}

+(void)registerStripeLive{
    NSDictionary *stripeLive = [[NSBundle mainBundle] objectForInfoDictionaryKey:STRIPE_LIVE];
    if(stripeLive){
        [Stripe setDefaultPublishableKey:[stripeLive objectForKey:STRIPE_LIVE_PUBLISH_KEY]];
    }
}

+(void)registerStripeTest{
    NSDictionary *stripeTest = [[NSBundle mainBundle] objectForInfoDictionaryKey:STRIPE_TEST];
    if(stripeTest){
        [Stripe setDefaultPublishableKey:[stripeTest objectForKey:STRIPE_TEST_PUBLISH_KEY]];
    }
}


+(STPCardParams *)generateCardParamWithNameCardHolder:(NSString *)name cardNumber:(NSString *)number cvcNumber:(NSString *)cvcNumber expMonth:(NSInteger)expMonth expYear:(NSInteger)expYear{
    PaymentManager *payment = [PaymentManager defaultPayment];
    return [payment generateCardParamsWith:name carId:number CVCNumber:cvcNumber expMonth:expMonth expYear:expYear];
}

+(STPCardBrand)cardBrandNameForNumber:(NSString *)carNumber{
    return [STPCardValidator brandForNumber:carNumber];
}

+(void)mycardWithBrandName:(NSString *)last4Id cardBrand:(STPCardBrand)brandName{
    NSMutableDictionary *brandNameWithLast4 = [[_UserDefault objectForKey:kCardBrandNameWithLast4] mutableCopy];
    
    if(!brandNameWithLast4){
        brandNameWithLast4 = [[NSMutableDictionary alloc] initWithObjects:@[[NSNumber numberWithInteger:brandName]] forKeys:@[last4Id]];
    }else{
        NSNumber *brandNameByNumber = [brandNameWithLast4 objectForKey:last4Id];
        if(!brandNameByNumber){
            [brandNameWithLast4 setObject:[NSNumber numberWithInteger:brandName] forKey:last4Id];
        }
    }
    [_UserDefault setObject:brandNameWithLast4 forKey:kCardBrandNameWithLast4];
}

+(void)mycardKeychainWithBrandName:(NSString *)last4Id cardBrand:(STPCardBrand)brandName{
    PaymentManager *manager = [PaymentManager defaultPayment];
    [manager setCardInKeychainWithLast4:last4Id brandName:brandName];
}

+(STPCardBrand)mycardBrandNameKeychainForLast4Id:(NSString *)last4Id{
    PaymentManager *manager = [PaymentManager defaultPayment];
    return [manager getCardBrandnameWithLast4:last4Id];
}

+(STPCardBrand)cardBrandNameForTokenId:(NSString *)tokenId{
    return STPCardBrandUnknown;
}

+(STPCardBrand)mycardBrandNameForLast4Id:(NSString *)last4Id{
    NSMutableDictionary *complexity = [_UserDefault objectForKey:kCardBrandNameWithLast4];
    if(!complexity){
        return STPCardBrandUnknown;
    }
    
    NSNumber *cardBrandByNumber = [complexity objectForKey:last4Id];
    if(!cardBrandByNumber){
        return STPCardBrandUnknown;
    }
    
    return [cardBrandByNumber integerValue];
}

+(UIImage *)imageForSTPCardBrand:(STPCardBrand)cardBrand{
    switch (cardBrand) {
        case STPCardBrandAmex:
            return [UIImage imageNamed:@"Amex Colored"];
            break;
            
        case STPCardBrandMasterCard:{
            return [UIImage imageNamed:@"Mastercard Colored"];
        }
            break;
            
        case STPCardBrandVisa:
            return [UIImage imageNamed:@"Visa Colored"];
            break;
            
        case STPCardBrandJCB:
            return [UIImage imageNamed:@"JCB Colored"];
            break;
            
        case STPCardBrandUnknown:
            return [UIImage imageNamed:@"Card Orange"];
            
        default:
            break;
    }
    return nil;
}

-(BOOL)validateCardWithInformation:(NSString *)name cardId:(NSString *)cardId CVCNumber:(NSString *)CVCNumber expMonth:(NSInteger)expMonth expYear:(NSInteger)expYear{
    
    if([STPCardValidator validationStateForNumber:cardId validatingCardBrand:YES] != STPCardValidationStateValid){
        return NO;
    }else if([STPCardValidator validationStateForCVC:CVCNumber cardBrand:[STPCardValidator brandForNumber:cardId]] != STPCardValidationStateValid){
        return NO;
    }else if([STPCardValidator validationStateForExpirationMonth:[@(expMonth) stringValue]] != STPCardValidationStateValid){
        return NO;
    }else if([STPCardValidator validationStateForExpirationYear:[@(expYear) stringValue] inMonth:[@(expMonth) stringValue]] != STPCardValidationStateValid){
        return NO;
    }
    return YES;
}

-(void)paymentWithCardHolderName:(NSString *)name cardId:(NSString *)cardId CVCNumber:(NSString *)CVCNumber expMonth:(NSInteger)expMonth expYear:(NSInteger)expYear onCompletion:(void (^)(BOOL, NSError *))completion{
    
    [self registerCardWithHolderName:name cardNumber:cardId cvcNumber:CVCNumber expMonth:expMonth expYear:expYear onCompletion:^(STPToken *token, NSError *error){
        if(!error){
            [self paymentChargeWithToken:token onCompletion:^(NSError *error, id response){
                
            }];
        }
    }];
}

-(void)registerCardWithHolderName:(NSString *)name cardNumber:(NSString *)cardNumber cvcNumber:(NSString *)cvc expMonth:(NSInteger)expMonth expYear:(NSInteger)expYear onCompletion:(void (^)(STPToken *, NSError *))completion{
    STPCardParams *params = [self generateCardParamsWith:name carId:cardNumber CVCNumber:cvc expMonth:expMonth expYear:expYear];
    if(params){
        [[STPAPIClient sharedClient] createTokenWithCard:params completion:^(STPToken *token, NSError *error){
            if(error){
                NSLog(@"Error while create token from card: %@", error.localizedDescription);
            }else{
                completion(token, error);
            }
        }];
    }
}


#pragma mark - Private Methods
-(void)instantiateKeyChainStore{
    if(self.keychainStore){
        return;
    }else{
        self.keychainStore = [UICKeyChainStore keyChainStoreWithService:EATUS_LAST4_WITH_BRAND_NUMBER_SERVICE accessGroup:EATUS_LAST4_WITH_BRAND_NUMBER_ACCESS_GROUP];
        self.keychainStore.accessibility = UICKeyChainStoreAccessibilityAfterFirstUnlock;
        self.keychainStore.synchronizable = YES;
    }
}

-(void)setCardInKeychainWithLast4: (NSString *)last4 brandName: (STPCardBrand)brand{
    [self instantiateKeyChainStore];
    
    NSMutableDictionary *last4WithBrandName = [[Util dictionaryWithData:[keychainStore dataForKey:kCardBrandNameWithLast4]] mutableCopy];
    
    if(!last4WithBrandName){
        last4WithBrandName = [[NSMutableDictionary alloc] init];
    }
    
    NSDictionary *last4Complex;
    if(![last4WithBrandName objectForKey:last4]){
        last4Complex = [[NSDictionary alloc] initWithObjects:@[[NSNumber numberWithInteger:brand]] forKeys:@[last4]];
    }
    
    if([last4Complex count] > 0){
        [last4WithBrandName addEntriesFromDictionary:last4Complex];
        [keychainStore setData:[Util dataWithDictionary:last4WithBrandName] forKey:kCardBrandNameWithLast4];
    }
}

-(STPCardBrand)getCardBrandnameWithLast4: (NSString *)last4{
    NSMutableDictionary *last4WithBrandName = [[Util dictionaryWithData:[keychainStore dataForKey:kCardBrandNameWithLast4]] mutableCopy];
    NSNumber *brandNameNumber = [last4WithBrandName objectForKey:last4];
    if(brandNameNumber){
        return [brandNameNumber integerValue];
    }else{
        return STPCardBrandUnknown;
    }
}

-(STPCardParams *)generateCardParamsWith: (NSString *)cardNameHolder carId: (NSString *)cardId CVCNumber: (NSString *)CVCNumber expMonth: (NSInteger)expMonth expYear: (NSInteger)expYear{
    STPCardParams *params = [[STPCardParams alloc] init];
    [params setName:cardNameHolder];
    [params setNumber:cardId];
    [params setCvc:CVCNumber];
    [params setExpMonth:expMonth];
    [params setExpYear:expYear];
    return params;
}

-(void)paymentChargeWithToken: (STPToken *)token onCompletion: (void (^)(NSError *error, id response))completion{
    AFHTTPSessionManager *manager = [[AFHTTPSessionManager alloc] init];
    manager.responseSerializer = [AFJSONResponseSerializer serializer];
    
    NSMutableDictionary *params = [[NSMutableDictionary alloc] init];
    [params setObject:token.tokenId forKey:PAYMENT_TOKEN_ID_PARAM];
    
    [manager POST:STRIPE_CHARGE_WITH_TOKEN parameters:params success:^(NSURLSessionDataTask *sessionTask, id response){
        completion(nil, response);
    }failure:^(NSURLSessionDataTask *dataTask, NSError *error){
        if(error){
            NSLog(@"Error while charge by token ID to server: %@", error.localizedDescription);
        }
    }];
}

@end
