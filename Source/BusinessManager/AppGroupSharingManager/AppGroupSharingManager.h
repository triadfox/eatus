//
//  AppGroupSharingManager.h
//  Eatus
//
//  Created by Phong Nguyen on 11/21/15.
//  Copyright © 2015 Triad Fox. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface AppGroupSharingManager : NSObject

+(BOOL)shareObject:(id)object forKey :(NSString *)key;
+(id)retrieveSharingObjectForKey: (NSString *)key;
+(void)removeSharingObjectForKey: (NSString *)key;

@end
