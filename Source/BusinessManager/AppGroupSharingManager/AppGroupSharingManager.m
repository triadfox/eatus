//
//  AppGroupSharingManager.m
//  Eatus
//
//  Created by Phong Nguyen on 11/21/15.
//  Copyright © 2015 Triad Fox. All rights reserved.
//

#import "AppGroupSharingManager.h"

#define SUITE_GROUP_NAME                        @"group.eatus.defaults"

@implementation AppGroupSharingManager

+(BOOL)shareObject:(id)object forKey :(NSString *)key{
    NSUserDefaults *defaults = [[NSUserDefaults alloc] initWithSuiteName:SUITE_GROUP_NAME];
    [defaults setObject:object forKey:key];
    return [defaults synchronize];
}

+(id)retrieveSharingObjectForKey:(NSString *)key{
    NSUserDefaults *defaults = [[NSUserDefaults alloc] initWithSuiteName:SUITE_GROUP_NAME];
    return [defaults objectForKey:key];
}

+(void)removeSharingObjectForKey:(NSString *)key{
    NSUserDefaults *defaults = [[NSUserDefaults alloc] initWithSuiteName:SUITE_GROUP_NAME];
    [defaults removeObjectForKey:key];
}


@end
