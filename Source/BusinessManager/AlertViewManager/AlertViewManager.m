//
//  AlertViewManager.m
//  Eatus
//
//  Created by Phong Nguyen on 11/1/15.
//  Copyright © 2015 Triad Fox. All rights reserved.
//

#import "AlertViewManager.h"

@implementation AlertViewManager

+(void)dropSmoothAlertViewWithTitle:(NSString *)title content:(NSString *)content cancelButton:(BOOL)canceled color:(UIColor *)color alertType:(AlertType)type{
    AMSmoothAlertView *alertView = [[AMSmoothAlertView alloc] initDropAlertWithTitle:title andText:content andCancelButton:canceled forAlertType:type andColor:color];
    [alertView setCornerRadius:5.0f];
    [alertView setTitleFont:[UIFont systemFontOfSize:22.0f]];
    [alertView.defaultButton setTitle:@"OK" forState:UIControlStateNormal];
    
    [alertView show];
}

+(void)dropSmoothAlertViewWithTitle:(NSString *)title content:(NSString *)content cancelButton:(BOOL)canceled color:(UIColor *)color alertType:(AlertType)type onCompletion:(dismissAlertWithButton)completion{
    AMSmoothAlertView *alertView = [[AMSmoothAlertView alloc] initDropAlertWithTitle:title andText:content andCancelButton:canceled forAlertType:type andColor:color withCompletionHandler:completion];
    [alertView setCornerRadius:5.0f];
    [alertView setTitleFont:[UIFont systemFontOfSize:22.0f]];
    [alertView.defaultButton setTitle:@"OK" forState:UIControlStateNormal];
    
    [alertView show];
}

+(void)fadeSmoothAlertViewWithTitle:(NSString *)title content:(NSString *)content cancelButton:(BOOL)canceled color:(UIColor *)color alertType:(AlertType)type{
    AMSmoothAlertView *alertView = [[AMSmoothAlertView alloc] initFadeAlertWithTitle:title andText:content andCancelButton:canceled forAlertType:type andColor:color];
    [alertView setCornerRadius:5.0f];
    [alertView setTitleFont:[UIFont systemFontOfSize:22.0f]];
    [alertView.defaultButton setTitle:@"OK" forState:UIControlStateNormal];
    
    [alertView show];
}

+(void)fadeSmoothAlertViewWithTitle:(NSString *)title content:(NSString *)content cancelButton:(BOOL)canceled color:(UIColor *)color alertType:(AlertType)type onCompletion:(dismissAlertWithButton)completion{
    AMSmoothAlertView *alertView = [[AMSmoothAlertView alloc] initFadeAlertWithTitle:title andText:content andCancelButton:canceled forAlertType:type andColor:color withCompletionHandler:completion];
    [alertView setCornerRadius:5.0f];
    [alertView setTitleFont:[UIFont systemFontOfSize:22.0f]];
    [alertView.defaultButton setTitle:@"OK" forState:UIControlStateNormal];
    
    [alertView show];
}

+(AMSmoothAlertView *)smoothDropAlertViewWithTitle:(NSString *)title content:(NSString *)content cancelButton:(BOOL)canceled color:(UIColor *)color alertType:(AlertType)type{
    AMSmoothAlertView *alertView = [[AMSmoothAlertView alloc] initDropAlertWithTitle:title andText:content andCancelButton:canceled forAlertType:type andColor:color];
    [alertView setCornerRadius:5.0f];
    [alertView setTitleFont:[UIFont systemFontOfSize:22.0f]];
    [alertView.defaultButton setTitle:@"OK" forState:UIControlStateNormal];
    
    return alertView;
}

+(AMSmoothAlertView *)smoothFadeAlertViewWithTitle:(NSString *)title content:(NSString *)content cancelButton:(BOOL)canceled color:(UIColor *)color alertType:(AlertType)type{
    AMSmoothAlertView *alertView =  [[AMSmoothAlertView alloc] initFadeAlertWithTitle:title andText:content andCancelButton:canceled forAlertType:type andColor:color];
    [alertView setCornerRadius:5.0f];
    [alertView setTitleFont:[UIFont systemFontOfSize:22.0f]];
    [alertView.defaultButton setTitle:@"OK" forState:UIControlStateNormal];
    
    return alertView;
}

+(AMSmoothAlertView *)smoothDropAlertViewWithTitle:(NSString *)title content:(NSString *)content cancelButton:(BOOL)canceled color:(UIColor *)color alertType:(AlertType)type onCompletion:(dismissAlertWithButton)completion{
    AMSmoothAlertView *alertView = [[AMSmoothAlertView alloc] initDropAlertWithTitle:title andText:content andCancelButton:canceled forAlertType:type andColor:color withCompletionHandler:completion];
    [alertView setCornerRadius:5.0f];
    [alertView setTitleFont:[UIFont systemFontOfSize:22.0f]];
    [alertView.defaultButton setTitle:@"OK" forState:UIControlStateNormal];
    
    return alertView;
}

+(AMSmoothAlertView *)smoothFadeAlertViewWithTitle:(NSString *)title content:(NSString *)content cancelButton:(BOOL)canceled color:(UIColor *)color alertType:(AlertType)type onCompletion:(dismissAlertWithButton)completion{
    AMSmoothAlertView *alertView = [[AMSmoothAlertView alloc] initFadeAlertWithTitle:title andText:content andCancelButton:canceled forAlertType:type andColor:color withCompletionHandler:completion];
    [alertView setCornerRadius:5.0f];
    [alertView setTitleFont:[UIFont systemFontOfSize:22.0f]];
    [alertView.defaultButton setTitle:@"OK" forState:UIControlStateNormal];
    
    return alertView;
}

@end
