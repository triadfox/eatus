//
//  AlertViewManager.h
//  Eatus
//
//  Created by Phong Nguyen on 11/1/15.
//  Copyright © 2015 Triad Fox. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "AMSmoothAlertView.h"

@interface AlertViewManager : NSObject

+(void)dropSmoothAlertViewWithTitle: (NSString *)title content: (NSString *)content cancelButton: (BOOL)canceled color: (UIColor *)color alertType: (AlertType)type;
+(void)fadeSmoothAlertViewWithTitle: (NSString *)title content: (NSString *)content cancelButton: (BOOL)canceled color: (UIColor *)color alertType: (AlertType)type;

+(void)dropSmoothAlertViewWithTitle: (NSString *)title content: (NSString *)content cancelButton: (BOOL)canceled color: (UIColor *)color alertType: (AlertType)type onCompletion: (dismissAlertWithButton)completion;
+(void)fadeSmoothAlertViewWithTitle: (NSString *)title content: (NSString *)content cancelButton: (BOOL)canceled color: (UIColor *)color alertType: (AlertType)type onCompletion: (dismissAlertWithButton)completion;

+(AMSmoothAlertView *)smoothDropAlertViewWithTitle: (NSString *)title content: (NSString *)content cancelButton: (BOOL)canceled color: (UIColor *)color alertType: (AlertType)type;
+(AMSmoothAlertView *)smoothFadeAlertViewWithTitle: (NSString *)title content: (NSString *)content cancelButton: (BOOL)canceled color: (UIColor *)color alertType: (AlertType)type;

+(AMSmoothAlertView *)smoothDropAlertViewWithTitle: (NSString *)title content: (NSString *)content cancelButton: (BOOL)canceled color: (UIColor *)color alertType: (AlertType)type onCompletion: (dismissAlertWithButton)completion;
+(AMSmoothAlertView *)smoothFadeAlertViewWithTitle: (NSString *)title content: (NSString *)content cancelButton: (BOOL)canceled color: (UIColor *)color alertType: (AlertType)type onCompletion: (dismissAlertWithButton)completion;


@end
