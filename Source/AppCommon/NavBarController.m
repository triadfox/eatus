//
//  NavBarController.m
//  MiPoBeacon
//
//  Created by Phong Nguyen on 9/3/15.
//  Copyright (c) 2015 Triad Fox. All rights reserved.
//

#import "NavBarController.h"

@interface NavBarController ()

@end

@implementation NavBarController

- (void)viewDidLoad {
    [super viewDidLoad];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(id)initWithRootViewController:(UIViewController *)rootViewController{
    self = [super initWithRootViewController:rootViewController];
    if(self){
        [self settingUpNavbar];
    }
    return self;
}

-(void)settingUpNavbar{
    [[UINavigationBar appearance] setTintColor:[UIColor whiteColor]];
    [[UINavigationBar appearance] setBarTintColor:[Util colorWithHexString:NAV_BAR_BACKGROUND_BAR_TINT_COLOR]];
    [[UINavigationBar appearance] setBarTintColor:[Util colorWithHexString:NAV_BAR_BACKGROUND_BAR_TINT_COLOR]];
    
    [self.navigationController.navigationBar
     setTitleTextAttributes:@{NSForegroundColorAttributeName : [UIColor whiteColor]}];
    self.navigationBar.translucent = NO;
}

@end
