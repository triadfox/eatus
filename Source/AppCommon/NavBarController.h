//
//  NavBarController.h
//  MiPoBeacon
//
//  Created by Phong Nguyen on 9/3/15.
//  Copyright (c) 2015 Triad Fox. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface NavBarController : UINavigationController

-(id)initWithRootViewController:(UIViewController *)rootViewController;

@end
