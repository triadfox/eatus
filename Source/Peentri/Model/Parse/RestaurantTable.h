//
//  RestaurantTable.h
//  ParseStarterProject
//
//  Created by Phuc Luong on 10/5/15.
//
//

#import <Parse/Parse.h>

#define kRestaurantTable @"restaurantTable"
#define kRestaurantMinorId @"minorId"

@class Restaurant, Beacon;

@interface RestaurantTable : PFObject <PFSubclassing>

@property (nonatomic, copy) NSString *name;
@property (nonatomic, copy) NSNumber *minorId;

- (Restaurant*)restaurant;
- (Beacon*)beacon;

+ (NSString *)parseClassName;

@end
