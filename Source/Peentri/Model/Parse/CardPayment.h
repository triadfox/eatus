//
//  CardPayment.h
//  Eatus
//
//  Created by Phong Nguyen on 10/24/15.
//  Copyright © 2015 Triad Fox. All rights reserved.
//

#import <Parse/Parse.h>

#define kLast4Id                    @"last4Id"

@interface CardPayment : PFObject <PFSubclassing>

@property(nonatomic, copy) NSString *tokenId;
@property(nonatomic, copy) NSString *last4Id;

-(User *)user;
-(void)setUser: (User *)user;

@end
