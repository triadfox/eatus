//
//  Promotion.h
//  ParseStarterProject
//
//  Created by Phuc Luong on 10/5/15.
//
//

#import <Parse/Parse.h>

@class Restaurant;

@interface Promotion : PFObject <PFSubclassing>

@property (nonatomic, copy) NSNumber *discount;
@property (nonatomic, copy) NSDate *startDate;
@property (nonatomic, copy) NSDate *endDate;
@property (nonatomic, copy) NSString *title;

- (Restaurant*)restaurant;
- (PFFile *)promotionImage;

+ (NSString *)parseClassName;

@end
