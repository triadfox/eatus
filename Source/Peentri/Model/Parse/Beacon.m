//
//  Beacon.m
//  ParseStarterProject
//
//  Created by Phuc Luong on 10/5/15.
//
//

#import "Beacon.h"

@implementation Beacon

@dynamic proximityUUID, identifier, majorId, minorId, beaconType, firmwareVersion, batteryStatus;

+ (NSString *)parseClassName {
    return @"Beacon";
}


@end
