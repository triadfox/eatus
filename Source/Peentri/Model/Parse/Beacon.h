//
//  Beacon.h
//  ParseStarterProject
//
//  Created by Phuc Luong on 10/5/15.
//
//

#import <Parse/Parse.h>

#define kBeacon @"beacon"
#define kBeaconIdentifier @"identifier"

@interface Beacon : PFObject <PFSubclassing>

+ (NSString *)parseClassName;

@property (nonatomic, copy) NSString *proximityUUID; //url
@property (nonatomic, copy) NSString *identifier;
@property (nonatomic, copy) NSNumber *majorId;
@property (nonatomic, copy) NSNumber *minorId;
@property (nonatomic, copy) NSString *beaconType;
@property (nonatomic, copy) NSString *firmwareVersion;
@property (nonatomic, copy) NSNumber *batteryStatus;

@end