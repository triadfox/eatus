//
//  ParseModel.h
//  ParseStarterProject
//
//  Created by Phuc Luong on 10/5/15.
//
//

#ifndef ParseModel_h
#define ParseModel_h

#import "Restaurant.h"
#import "Promotion.h"
#import "Beacon.h"
#import "User.h"
#import "CardPayment.h"
#import "Trending.h"
#import "RestaurantTable.h"
#import "Order.h"
#import "Feedback.h"

#endif /* ParseModel_h */
