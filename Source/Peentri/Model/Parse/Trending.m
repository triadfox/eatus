//
//  UserRestaurant.m
//  ParseStarterProject
//
//  Created by Phuc Luong on 10/5/15.
//
//

#import "Trending.h"
#import "User.h"
#import "Restaurant.h"

@implementation Trending

- (User*)user {
    return self[kUser];
}

- (Restaurant*)restaurant {
    return self[kRestaurant];
}

- (void)setUser:(User*)user {
    self[kUser] = user;
}

- (void)setRestaurant:(Restaurant*)restaurant {
    self[kRestaurant] = restaurant;
}

+ (NSString *)parseClassName {
    return @"Trending";
}

@end
