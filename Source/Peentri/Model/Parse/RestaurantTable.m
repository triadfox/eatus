//
//  RestaurantTable.m
//  ParseStarterProject
//
//  Created by Phuc Luong on 10/5/15.
//
//

#import "RestaurantTable.h"
#import "Restaurant.h"
#import "Beacon.h"

@implementation RestaurantTable

@dynamic name;

- (Restaurant*)restaurant {
    return self[kRestaurant];
}
- (Beacon*)beacon {
    return self[kBeacon];
}

+ (NSString *)parseClassName {
    return @"RestaurantTable";
}

@end
