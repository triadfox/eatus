//
//  UserRestaurant.h
//  ParseStarterProject
//
//  Created by Phuc Luong on 10/5/15.
//
//

#import <Parse/Parse.h>

@class User, Restaurant;

@interface Trending : PFObject <PFSubclassing>

- (User*)user;
- (Restaurant*)restaurant;
- (void)setUser:(User*)user;
- (void)setRestaurant:(Restaurant*)restaurant;

+ (NSString *)parseClassName;

@end
