//
//  Promotion.m
//  ParseStarterProject
//
//  Created by Phuc Luong on 10/5/15.
//
//

#import "Promotion.h"

@implementation Promotion

@dynamic discount, startDate, endDate, title;

- (Restaurant*)restaurant {
    return self[@"restaurant"];
}

- (PFFile *)promotionImage{
    return self[@"promotionImage"];
}

+ (NSString *)parseClassName {
    return @"Promotion";
}

@end
