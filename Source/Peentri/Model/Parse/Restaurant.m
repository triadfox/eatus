//
//  Restaurant.m
//  ParseStarterProject
//
//  Created by Phuc Luong on 10/5/15.
//
//

#import "Restaurant.h"

@implementation Restaurant

@dynamic name, openTime, closeTime, address, description, beaconMajorId;

- (PFFile*)logoImage {
    return self[@"logoImage"];
}
- (PFFile*)backgroundImage {
    return self[@"backgroundImage"];
}
- (PFFile*)menu {
    return self[@"menu"];
}
- (PFFile*)geoPoint {
    return self[@"geoPoint"];
}

+ (NSString *)parseClassName {
    return @"Restaurant";
}

@end
