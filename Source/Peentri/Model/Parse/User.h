//
//  User.h
//  ParseStarterProject
//
//  Created by Phuc Luong on 10/5/15.
//
//

/*
 https://parse.com/docs/osx/api/Classes/PFUser.html
 
 // Example how to register a user:
 User *user = [User new];
 user.username = @"phucluong";
 user.password = @"phucluong";
 user.fullname = @"Luong Van Phuc";
 user.email = @"phuc@mail.cc";
 user.yearOfBirth = [NSNumber numberWithInt:1990];
 user.deviceUUID = @"example device UUID";
 user.facebookId = @"123456789";
 
 [user signUpInBackgroundWithBlock:^(BOOL succeeded, NSError *err) {
 if (succeeded) {
 NSLog(@"Succeeded");
 NSLog(@"%@", [User currentUser]);
 } else {
 NSLog(@"Failed: %@", [err localizedFailureReason]);
 }
 }];
 
 // Example how to login. Note: should check [User currentUser] before logging in
 [User logInWithUsernameInBackground:@"phuc.luong" password:@"123456" block:^(PFUser *user, NSError *error) {
 if(!error)
 NSLog(@"Login Successful.");
 else {
 NSLog(@"Error");
 }
 }];
 
 */

#import <Parse/Parse.h>

#define kUser @"user"


@interface User : PFUser <PFSubclassing>

// Return the current user
+ (User *)currentUser;

@property (nonatomic, copy) NSString *fullname;
@property (nonatomic, copy) NSNumber *yearOfBirth;
@property (nonatomic, copy) NSString *facebookId;
@property (nonatomic, copy) NSString *deviceUUID;
@property (nonatomic, copy) PFFile *avatar;

@end