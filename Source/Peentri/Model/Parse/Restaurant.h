//
//  Restaurant.h
//  ParseStarterProject
//
//  Created by Phuc Luong on 10/5/15.
//
//

#import <Parse/Parse.h>

#define kRestaurant @"restaurant"
#define kBeaconMajorId @"beaconMajorId"
#define kName @"name"

#define kLogoImage @"logoImage"
#define kBackgroundImage @"backgroundImage"

@interface Restaurant : PFObject <PFSubclassing>

@property (nonatomic, copy) NSString *name;
@property (nonatomic, copy) NSString *address;
@property (nonatomic, copy) NSString *description;
@property (nonatomic, assign) double distance;
@property int beaconMajorId;
@property int openTime;
@property int closeTime;

- (PFFile*)logoImage;
- (PFFile*)backgroundImage;
- (PFFile*)menu;
- (PFGeoPoint*)geoPoint;

+ (NSString *)parseClassName;

@end