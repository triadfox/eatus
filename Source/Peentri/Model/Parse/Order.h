//
//  Order.h
//  ParseStarterProject
//
//  Created by Phuc Luong on 10/5/15.
//
//

#import <Parse/Parse.h>

#define kReceiptStatus      @"status"
#define kPaymentType        @"paymentType"

@class User, RestaurantTable;

typedef enum {
    Pending = 1,
    Cancelled,
    Confirmed,
    Delivered
} OrderStatus;

@interface Order : PFObject <PFSubclassing>

@property (nonatomic, copy) NSString *orderDetail;
@property int status;

- (User*)user;
- (RestaurantTable*)restaurantTable;
- (void)setUser:(User*)user;
- (void)setRestaurantTable:(RestaurantTable*)restaurantTable;
+ (NSString *)parseClassName;

@end
