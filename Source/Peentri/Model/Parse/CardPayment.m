//
//  CardPayment.m
//  Eatus
//
//  Created by Phong Nguyen on 10/24/15.
//  Copyright © 2015 Triad Fox. All rights reserved.
//

#import "CardPayment.h"

@implementation CardPayment

@dynamic tokenId, last4Id;

+(NSString *)parseClassName{
    return @"CardPayment";
}

-(User *)user{
    return self[kUser];
}

-(void)setUser: (User *)user{
    self[kUser] = user;
}

@end
