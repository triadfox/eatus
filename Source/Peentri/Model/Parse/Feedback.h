//
//  Feedback.h
//  Light Order
//
//  Created by phuc on 10/6/15.
//  Copyright (c) 2015 Triad Fox. All rights reserved.
//

#import <Parse/Parse.h>

@class User, Restaurant;

@interface Feedback : PFObject <PFSubclassing>

@property (nonatomic, copy) NSString *comment;
@property BOOL isHappy;

- (User*)user;
- (Restaurant*)restaurant;
- (void)setUser:(User*)user;
- (void)setRestaurant:(Restaurant*)restaurant;

+ (NSString *)parseClassName;

@end