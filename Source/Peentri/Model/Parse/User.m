//
//  User.m
//  ParseStarterProject
//
//  Created by Phuc Luong on 10/5/15.
//
//

#import "User.h"

@implementation User

@dynamic fullname, yearOfBirth, facebookId, deviceUUID, avatar;

// Return the current user
+ (User *)currentUser {
    return (User *)[PFUser currentUser];
}

@end
