//
//  Order.m
//  ParseStarterProject
//
//  Created by Phuc Luong on 10/5/15.
//
//

#import "Order.h"
#import "User.h"
#import "RestaurantTable.h"

@implementation Order

@dynamic status, orderDetail;

- (User*)user {
    return self[kUser];
}
- (RestaurantTable*)restaurantTable {
    return self[kRestaurantTable];
}

- (void)setUser:(User*)user {
    self[kUser] = user;
}

- (void)setRestaurantTable:(RestaurantTable*)restaurantTable {
    self[kRestaurantTable] = restaurantTable;
}

+ (NSString *)parseClassName {
    return @"Order";
}

@end
