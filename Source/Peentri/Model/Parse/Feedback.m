//
//  Feedback.m
//  Light Order
//
//  Created by phuc on 10/6/15.
//  Copyright (c) 2015 Triad Fox. All rights reserved.
//

#import "Feedback.h"
#import "User.h"
#import "Restaurant.h"

@implementation Feedback

@dynamic isHappy, comment;

- (User*)user {
    return self[kUser];
}

- (Restaurant*)restaurant {
    return self[kRestaurant];
}

- (void)setUser:(User*)user {
    self[kUser] = user;
}

- (void)setRestaurant:(Restaurant*)restaurant {
    self[kRestaurant] = restaurant;
}

+ (NSString *)parseClassName {
    return @"Feedback";
}

@end
