//
//  PromotionViewController.h
//  Light Order
//
//  Created by Phong Nguyen on 10/18/15.
//  Copyright © 2015 Triad Fox. All rights reserved.
//

#import "BaseViewController.h"

@interface PromotionViewController : BaseViewController<UITableViewDataSource, UITableViewDelegate>

@property(nonatomic, strong) IBOutlet UITableView *promotionTableView;

@end
