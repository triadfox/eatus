//
//  PromotionViewController.m
//  Light Order
//
//  Created by Phong Nguyen on 10/18/15.
//  Copyright © 2015 Triad Fox. All rights reserved.
//

#import "PromotionViewController.h"
#import "PromotionTableViewCell.h"
#import "StoreViewController.h"

#define HEIGHT_FOR_SECTION              16

@interface PromotionViewController (){
    NSArray *allPromotions;
}

@end

@implementation PromotionViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self prepareNavBar];
}

- (void)viewDidAppear:(BOOL)animated{
    [self preparePromotionData];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    
}

-(void)prepareNavBar{
    [self prepareSideBarMenu];
    [self setNavCenterTitle:NSLocalizedString(@"com.eatus.nav.center.title.promotion", @"Promotion")];
}

-(void)preparePromotionData{
    [_Shared showOverlayIndetermineSmalProgressViewWithTitle:NSLocalizedString(@"com.eatus.hud.load", @"Loading") style:kOverlayElegantStyle tintColor:COLOR_MAIN_THEME onView:self.view];
    
    [ParseDataManager getAllPromotionOnCompletion:^(NSArray *promotions, NSError *error){
        allPromotions = promotions;
        
        self.promotionTableView.delegate = self;
        self.promotionTableView.dataSource = self;
        [self.promotionTableView reloadData];
        
        [_Shared hideOverlayIndetermineProgressViewAfter:0.3f];
    }];
}

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return [allPromotions count];
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return 1;
}

-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    return HEIGHT_FOR_SECTION;
}

-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    UIView *headerView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, self.promotionTableView.bounds.size.width, HEIGHT_FOR_SECTION)];
    [headerView setBackgroundColor:[UIColor clearColor]];
    return headerView;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    NSArray *nibArray = [[NSBundle mainBundle] loadNibNamed:@"PromotionViewCell" owner:self options:nil];
    PromotionTableViewCell *cell = [nibArray firstObject];
    
    Promotion *promotion = [allPromotions objectAtIndex:indexPath.section];
    
    cell.promotionTitle.text = promotion.title;
    
    if(promotion.promotionImage.url){
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_LOW, 0), ^{
            UIImage *image = [Util getImageSyncFromURL:promotion.promotionImage.url];
            
            dispatch_async(dispatch_get_main_queue(), ^{
                if(image){
                    [cell.promotionImageView setImage:image];
                }
            });
        });
    }
    
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    Promotion *promotion = [allPromotions objectAtIndex:indexPath.section];
    [ParseDataManager getReferedRestaurantByPromotion:promotion completion:^(Restaurant *restaurant, NSError *error){
        StoreViewController *storeViewController = [[StoreViewController alloc] initWithNibName:@"StoreView" bundle:nil];
        storeViewController.storeMajorId = restaurant.beaconMajorId;
        storeViewController.storeName = restaurant.name;
        [self.navigationController pushViewController:storeViewController animated:YES];
    }];
    
}

@end
