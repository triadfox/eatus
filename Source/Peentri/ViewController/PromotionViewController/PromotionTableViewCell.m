//
//  PromotionTableViewCell.m
//  Light Order
//
//  Created by Phong Nguyen on 10/18/15.
//  Copyright © 2015 Triad Fox. All rights reserved.
//

#import "PromotionTableViewCell.h"

@implementation PromotionTableViewCell

- (void)awakeFromNib {
//    [Util roundView:self.promotionImageView background:nil borderColor:[UIColor clearColor] borderWidth:0.1f];
    self.selectionStyle = UITableViewCellSelectionStyleNone;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
}

@end
