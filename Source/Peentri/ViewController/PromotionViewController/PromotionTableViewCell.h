//
//  PromotionTableViewCell.h
//  Light Order
//
//  Created by Phong Nguyen on 10/18/15.
//  Copyright © 2015 Triad Fox. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PromotionTableViewCell : UITableViewCell

@property(nonatomic, weak) IBOutlet UIImageView *promotionImageView;
@property(nonatomic, weak) IBOutlet UILabel *promotionTitle;

@end
