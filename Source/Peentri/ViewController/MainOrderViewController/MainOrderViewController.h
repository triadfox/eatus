//
//  MainOrderViewController.h
//  Light Order
//
//  Created by Phong Nguyen on 10/2/15.
//  Copyright © 2015 Triad Fox. All rights reserved.
//

#import "BaseViewController.h"
#import <CoreLocation/CoreLocation.h>

#define STORE_CLOSED                @"Closed"
#define STORE_OPEN                  @"Open Now"

@interface MainOrderViewController : BaseViewController<UITableViewDataSource, UITableViewDelegate>

@property(nonatomic, strong) IBOutlet UITableView *storeTableView;
@property(nonatomic, strong) IBOutlet UIView *activityIndicatorView;
@property(nonatomic, strong) IBOutlet UIView *activityOverlayView;
@property(nonatomic, strong) IBOutlet UILabel *activityLoadingTitle;

@end
