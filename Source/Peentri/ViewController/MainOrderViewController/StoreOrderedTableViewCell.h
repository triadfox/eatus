//
//  StoreOrderedTableViewCell.h
//  Light Order
//
//  Created by Phong Nguyen on 10/3/15.
//  Copyright © 2015 Triad Fox. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface StoreOrderedTableViewCell : UITableViewCell

@property(nonatomic, strong) IBOutlet UIView *orderedBackgroundView;
@property(nonatomic, strong) IBOutlet UIImageView *orderedImageView;
@property(nonatomic, strong) IBOutlet UILabel *orderedLabel;

@end
