//
//  StoreOrderedViewController.m
//  Light Order
//
//  Created by Phong Nguyen on 10/3/15.
//  Copyright © 2015 Triad Fox. All rights reserved.
//

#import "StoreOrderedViewController.h"
#import "StoreOrderedTableViewCell.h"

@interface StoreOrderedViewController (){
    NSArray *_images;
    NSArray *_titles;
}

@end

@implementation StoreOrderedViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self prepareForTableView];
}

-(void)prepareForTableView{
    _images =  [NSArray arrayWithObjects:[UIImage imageNamed:@"Length"], [UIImage imageNamed:@"Question"], [UIImage imageNamed:@"Clock"], nil];
    _titles = [NSArray arrayWithObjects:STORE_SORT_BY_DISTANCE, STORE_SORT_BY_RATING, STORE_SORT_BY_OPENING_TIME, nil];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return [_images count];
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    static NSString *identifier = @"StoreOrderedCellIdentifier";
    StoreOrderedTableViewCell *cell = (StoreOrderedTableViewCell *)[tableView dequeueReusableCellWithIdentifier:identifier];
    if(!cell){
        NSArray *nibArray = [[NSBundle mainBundle] loadNibNamed:@"StoreOrderedTableViewCell" owner:self options:nil];
        cell = [nibArray firstObject];
    }
    cell.orderedLabel.text = _titles[indexPath.row];
    cell.orderedImageView.image = _images[indexPath.row];
    return cell;
}

@end
