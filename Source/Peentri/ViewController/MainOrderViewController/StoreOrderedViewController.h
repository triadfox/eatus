//
//  StoreOrderedViewController.h
//  Light Order
//
//  Created by Phong Nguyen on 10/3/15.
//  Copyright © 2015 Triad Fox. All rights reserved.
//

#import "BaseViewController.h"

#define STORE_SORT_BY_DISTANCE                  @"Stores by distance"
#define STORE_SORT_BY_RATING                    @"Stores by rating"
#define STORE_SORT_BY_OPENING_TIME              @"Stores by opening time"

@interface StoreOrderedViewController : BaseViewController<UITableViewDataSource, UITableViewDelegate>

@property(nonatomic, strong) IBOutlet UITableView *storeOrderedTableView;

@end
