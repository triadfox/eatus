//
//  MainOrderTableViewCell.m
//  Light Order
//
//  Created by Phong Nguyen on 10/3/15.
//  Copyright © 2015 Triad Fox. All rights reserved.
//

#import "MainOrderTableViewCell.h"

@implementation MainOrderTableViewCell

- (void)awakeFromNib {
    [Util roundView:self.smallBackgroundView background:nil borderColor:[Util colorWithHexString:MAIN_THEME_COLOR] borderWidth:0.3f];
    [Util roundView:self.smallImageView background:nil borderColor:[Util colorWithHexString:MAIN_THEME_COLOR] borderWidth:1.0f];
    [Util dropShadowForView:self.smallBackgroundView];
    [self.horizontalSeparator setBackgroundColor:[Util colorWithHexString:MAIN_THEME_COLOR_DARK]];
    
    self.storeRatingUnicode.text = @"\U0001F609";
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
}

-(void)firstTimeToolTipOnRating{
    if(![_UserDefault objectForKey:kFirstAccessToolTipsMainOrder] || ![[_UserDefault objectForKey:kFirstAccessToolTipsMainOrder] boolValue]){
        
        toolTipView = [[JDFTooltipView alloc] initWithTargetPoint:self.storeRatingUnicode.center hostView:self tooltipText:NSLocalizedString(@"com.eatus.feedback.tooltip.content", "toolTipMsg") arrowDirection:JDFTooltipViewArrowDirectionDown width:300.0f];
        [toolTipView setTooltipBackgroundColour:COLOR_MAIN_THEME_LIGHT_LV_1];
        [toolTipView show];
        [_UserDefault setObject:[NSNumber numberWithBool:YES] forKey:kFirstAccessToolTipsMainOrder];
        
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(3.0f * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
            [toolTipView hideAnimated:YES];
        });
    }
}

@end
