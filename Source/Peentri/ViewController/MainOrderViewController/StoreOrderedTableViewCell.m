//
//  StoreOrderedTableViewCell.m
//  Light Order
//
//  Created by Phong Nguyen on 10/3/15.
//  Copyright © 2015 Triad Fox. All rights reserved.
//

#import "StoreOrderedTableViewCell.h"

@implementation StoreOrderedTableViewCell

- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
