//
//  MainOrderViewController.m
//  Light Order
//
//  Created by Phong Nguyen on 10/2/15.
//  Copyright © 2015 Triad Fox. All rights reserved.
//

#import "MainOrderViewController.h"
#import "MainOrderTableViewCell.h"
#import "StoreOrderedViewController.h"
#import "StoreViewController.h"
#import "StoreSwipeViewController.h"
#import "FeedbackManager.h"

#import "YALSunnyRefreshControl.h"
#import "MenuSideViewController.h"
#import "SWRevealViewController.h"
#import "MRActivityIndicatorView.h"

#define DATE_FORMAT                 @"HH:mm:ss"

@interface MainOrderViewController (){
    BOOL _isRefreshIconOverlap;
    BOOL _isRefreshAnimating;
    
    BOOL _shouldReload;
    
    NSMutableArray *_allRestaurant;
    
    CLLocation *_currentLocation;
}

@property (nonatomic, strong) StoreSwipeViewController *storeSwipeViewController;
@property (nonatomic, retain) StoreViewController *storeViewController;
@property (nonatomic, retain) StoreOrderedViewController *storeOrderedViewController;
@property (nonatomic, strong) YALSunnyRefreshControl *refreshController;

//refresh control and animations
@property (nonatomic, retain) UIRefreshControl *refreshControl;
@property (nonatomic, retain) UIView *refreshView;
@property (nonatomic, retain) UIView *refreshColorView;
@property (nonatomic, strong) UIImageView *sunImageView;
@property (nonatomic, strong) UIImageView *skyImageView;

@property (nonatomic, strong) MRActivityIndicatorView *activityIndicator;

@end

@implementation MainOrderViewController
@synthesize storeTableView;

- (void)viewDidLoad {
    [super viewDidLoad];
    [self prepareNavbar];
    [self prepareRefreshController];
}

-(void)viewDidAppear:(BOOL)animated{
    //parser get data
    [self preparePermissionOnCompletion:^{
        [self getAllRestaurant:YES];
    }];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

-(void)prepareRefreshController{
    self.refreshController = [[YALSunnyRefreshControl alloc] init];
    [self.refreshController addTarget:self action:@selector(startRefreshing) forControlEvents:UIControlEventValueChanged];
    [self.refreshController attachToScrollView:self.storeTableView];
}

-(void)startRefreshing{
    //start to refresh data
    _isRefreshAnimating = YES;
    [self getAllRestaurant:NO];
}

-(void)stopRefreshing{
    [self.refreshController endRefreshing];
}

-(void)prepareNavbar{
    [self prepareSideBarMenu];
    [self setNavCenterTitle:NSLocalizedString(@"com.eatus.nav.center.title.nearby", "NEARBY")];
}


#pragma mark - Parser Get Data
-(void)getAllRestaurant: (BOOL)firstTime{
    [self launchNetworkMonitoring];
    
    if(firstTime){
        [self showActivityIndicatorForLoading];
    }
    
    [ParseDataManager getAllRestaurantsOnCompletion:^(NSArray *restaurants, NSError *error){
        if(error){
            NSLog(@"Error occurs while trying to get all restaurant from Parse: %@", error.description);
        }else{
            
            if(_isRefreshAnimating){
                [self stopRefreshing];
            }
            
            [self enumerateOnRestaurantToEstimateDistance:[restaurants mutableCopy] onCompletion:^(NSMutableArray *restaurants){
                _allRestaurant = restaurants;
                
                if(firstTime){
                    self.storeTableView.delegate = self;
                    self.storeTableView.dataSource = self;
                }
                
                if(_shouldReload){
                    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1.5f * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                        [self dismissActivityIndicatorForLoadingCompletion:^{
                            [self.storeTableView reloadData];
                        }];
                    });
                }
            }];
        }
    }];
}

-(void)enumerateOnRestaurantToEstimateDistance: (NSMutableArray *)restaurants onCompletion: (void (^)(NSMutableArray *array))completion{
    dispatch_group_t enumerateGroup = dispatch_group_create();

    dispatch_group_enter(enumerateGroup);
    for(Restaurant *restaurant in restaurants){
        double longitude = restaurant.geoPoint.longitude;
        double latitude = restaurant.geoPoint.latitude;
        
        if([self.permission statusLocationAlways] == 0){
            [self launchLocationManager];
            
            [self.locationManager calculateDistanceFromCurrentPlaceToLatitude:latitude longitude:longitude completion:^(CLLocationDistance distance){
                [restaurant setDistance:distance];
                
                if([restaurants indexOfObject:restaurant] == restaurants.count - 1){
                    dispatch_group_leave(enumerateGroup);
                }
            }];
        }
    }
    
    dispatch_group_notify(enumerateGroup, dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0), ^{
        [restaurants sortUsingDescriptors:@[[NSSortDescriptor sortDescriptorWithKey:@"distance" ascending:YES]]];
        
        if(completion){
            _shouldReload = YES;
            completion(restaurants);
        }
    });
}


#pragma mark - Core Location and Delegate's methods
-(void)preparePermissionOnCompletion: (void (^)(void))completionHandler{
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1.0f * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        BOOL requestPermission = NO;
        
        if([self.permission statusLocationAlways] == 0){
            [self launchLocationManager];
            [self launchBeaconManager];
            if(completionHandler){
                completionHandler();
            }
        }else{
            [self.permission addPermission:[[LocationAlwaysPermission alloc] init] message:NSLocalizedString(@"com.eatus.permission.location.message", "locationMsg")];
            requestPermission = YES;
        }
        
        if([self.permission statusNotifications] != 0){
            [self.permission addPermission:[[NotificationsPermission alloc] initWithNotificationCategories:nil] message:NSLocalizedString(@"com.eatus.permission.notification.message", "notiMsg")];
            requestPermission = YES;
        }
        
        requestPermission ? [self.permission show:^(BOOL completion, NSArray *result){
            if(completion){
                if([Util didAuthorizeForResult:[result firstObject]]){
                    [self launchLocationManager];
                    [self launchBeaconManager];
                    
                    if(completionHandler){
                        completionHandler();
                    }
                }
            }
        }cancelled:^(NSArray *permissions){
            
        }] : nil;
    });
}


#pragma mark - Table View Delegate's methods
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return _allRestaurant.count;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    NSArray *nibArray = [[NSBundle mainBundle] loadNibNamed:@"MainOrderViewCell" owner:self options:nil];
    MainOrderTableViewCell *cell = [nibArray firstObject];
    
    Restaurant *restaurant = [_allRestaurant objectAtIndex:indexPath.row];
    
    cell.storeNameLabel.text = restaurant.name;
    
    [FeedbackManager ratingEmoticonForRestaurant:restaurant onCompletion:^(UIImage *emoticon){
        if(emoticon){
            [UIView transitionWithView:cell.storeRating duration:0.5f options:UIViewAnimationOptionTransitionFlipFromRight animations:^{
                cell.storeRating.image = emoticon;
            }completion:^(BOOL finished){
                
            }];
        }
    }];
    
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_LOW, 0), ^{
        UIImage *backgroundImage = [Util getImageSyncFromURL:restaurant.backgroundImage.url];
        UIImage *logoImage = [Util getImageSyncFromURL:restaurant.logoImage.url];
        
        dispatch_async(dispatch_get_main_queue(), ^{
            if(backgroundImage){
                if(indexPath.row == 0){
                    [cell.storeImageView setImage:backgroundImage];
                }else{
                    [cell.storeImageView setImage:backgroundImage];
                }
            }
            
            if(logoImage){
                cell.smallImageView.image = logoImage;
            }
        });
    });
    
    
    if(![self determineStoreOpen:restaurant.openTime closed:restaurant.closeTime]){
        [cell.storeOpening setTextColor:[Util colorWithHexString:CLOSED_TIME_COLOR]];
        cell.storeOpening.text = [NSString stringWithFormat:@"\U0001F534 %@", NSLocalizedString(@"com.eatus.timing.closed", "closeTime")];
        cell.didClosed = YES;
    }else{
        [cell.storeOpening setTextColor:[Util colorWithHexString:OPEN_TIME_COLOR]];
        cell.storeOpening.text = [NSString stringWithFormat:@"\U0001F535 %@", NSLocalizedString(@"com.eatus.timing.open", "openTime")];
        cell.didClosed = NO;
    }
    
    NSInteger rowsAmount = [tableView numberOfRowsInSection:[indexPath section]];
    if ([indexPath row] == rowsAmount - 1) {
        cell.horizontalSeparator.hidden = YES;
    }
    
    if([self isDistanceDecimalGreaterOrEqualOneKilometer:restaurant.distance]){
        cell.storeAddress.text = [NSString stringWithFormat:@"%@  \U0001F4CD  %.3f km", restaurant.address, restaurant.distance];
    }else{
        cell.storeAddress.text = [NSString stringWithFormat:@"%@  \U0001F4CD  %.3f m", restaurant.address, restaurant.distance * 1000];
    }
    
    
    
    if(indexPath.row == 0){
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(2.0f * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
            [cell firstTimeToolTipOnRating];
        });
    }
    
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    //get restaurant information
    Restaurant *restaurant = [_allRestaurant objectAtIndex:indexPath.row];
    if(!restaurant){
        NSLog(@"Restaurant detail is nil!");
    }
    
    
    MainOrderTableViewCell *cell = [tableView cellForRowAtIndexPath:indexPath];
    
    if(cell.didClosed){
        self.storeSwipeViewController = [[StoreSwipeViewController alloc] initWithNibName:@"StoreSwipeView" bundle:nil storeDidOpenOrClosed:kStoreDidClosed];
    }else{
        self.storeSwipeViewController = [[StoreSwipeViewController alloc] initWithNibName:@"StoreSwipeView" bundle:nil storeDidOpenOrClosed:kStoreDidOpened];
    }
    
    self.storeSwipeViewController.storeMajorId = restaurant.beaconMajorId;
    self.storeSwipeViewController.storeName = restaurant.name;
    self.storeSwipeViewController.restaurant = restaurant;
    
    [self.navigationController pushViewController:self.storeSwipeViewController animated:YES];
}

-(BOOL)determineStoreOpen: (int)openTiming closed: (int)closedTiming{
    NSDate *openTime = [self timeForTotalSecond:openTiming];
    NSDate *closedTime = [self timeForTotalSecond:closedTiming];
    
    NSDate *now = [NSDate date];
    NSTimeZone* sourceTimeZone = [NSTimeZone timeZoneWithAbbreviation:@"GMT"];
    NSTimeZone* destinationTimeZone = [NSTimeZone systemTimeZone];
    
    NSInteger sourceGMTOffset = [sourceTimeZone secondsFromGMTForDate:now];
    NSInteger destinationGMTOffset = [destinationTimeZone secondsFromGMTForDate:now];
    NSTimeInterval interval = destinationGMTOffset - sourceGMTOffset;
    
    now = [[NSDate alloc] initWithTimeInterval:interval sinceDate:now];
    openTime = [[NSDate alloc] initWithTimeInterval:interval sinceDate:openTime];
    closedTime = [[NSDate alloc] initWithTimeInterval:interval sinceDate:closedTime];
    
    if(([now compare:openTime] == NSOrderedDescending) && ([now compare:closedTime] == NSOrderedAscending)){
        return YES;
    }
    return NO;
}

-(NSDate *)timeForTotalSecond: (int)totalSecond{
    int minutes = (totalSecond / 60) % 60;
    int hours = totalSecond / 3600;
    
    NSString *dateStr = [NSString stringWithFormat:@"%02d:%02d", hours, minutes];
    return [self todaysDateFromString:dateStr];
}

- (NSDate *)todaysDateFromString:(NSString *)time
{
    // Split hour/minute into separate strings:
    NSArray *array = [time componentsSeparatedByString:@":"];
    
    // Get year/month/day from today:
    NSCalendar *cal = [NSCalendar currentCalendar];
    [cal setLocale:[NSLocale currentLocale]];
    
    NSDateComponents *comp = [cal components:NSCalendarUnitYear|NSCalendarUnitMonth|NSCalendarUnitDay fromDate:[NSDate date]];
    
    // Set hour/minute from the given input:
    [comp setHour:[array[0] integerValue]];
    [comp setMinute:[array[1] integerValue]];
    
    return [cal dateFromComponents:comp];
}


#pragma mark - Animation
-(void)showActivityIndicatorForLoading{
    _activityIndicator = [_Shared showIndependentActivityIndicatorViewWithTintColor:[Util colorWithHexString:MAIN_THEME_COLOR_DARK] frame:self.activityOverlayView.bounds];
    [_activityLoadingTitle setText:NSLocalizedString(@"com.eatus.hud.load", @"Loading")];
    
    [self.activityOverlayView addSubview:_activityIndicator];
}

-(void)dismissActivityIndicatorForLoadingCompletion: (void (^)(void))completion{
    if(_activityIndicatorView){
        [UIView animateWithDuration:0.6 delay:0.1f options:UIViewAnimationOptionAllowAnimatedContent animations:^{
                [_activityIndicator stopAnimating];
            }completion:^(BOOL finishes){
                [_activityIndicatorView setHidden:YES];
                completion();
            }];
    }
}


#pragma mark - Utilities
-(BOOL)isDistanceDecimalGreaterOrEqualOneKilometer: (double)distance{
    if(distance > 1){
        return YES;
    }else{
        return NO;
    }
}

@end
