//
//  MainOrderTableViewCell.h
//  Light Order
//
//  Created by Phong Nguyen on 10/3/15.
//  Copyright © 2015 Triad Fox. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "JDFTooltips.h"

@interface MainOrderTableViewCell : UITableViewCell{
    JDFTooltipView *toolTipView;
}

@property(nonatomic, strong) IBOutlet UIView *storeBackgroundView;
@property(nonatomic, strong) IBOutlet UIImageView *storeImageView;
@property(nonatomic, strong) IBOutlet UILabel *storeNameLabel;
@property(nonatomic, strong) IBOutlet UILabel *storeAddress;
@property(nonatomic, strong) IBOutlet UILabel *storeOpening;
@property(nonatomic, strong) IBOutlet UIImageView *storeRating;
@property(nonatomic, strong) IBOutlet UILabel *storeRatingUnicode;
@property(nonatomic, strong) IBOutlet UIView *horizontalSeparator;

@property(nonatomic, assign) BOOL didClosed;

@property(nonatomic, weak) IBOutlet UIView *smallBackgroundView;
@property(nonatomic, weak) IBOutlet UIImageView *smallImageView;

-(void)firstTimeToolTipOnRating;
@end
