//
//  StoreViewController.m
//  Light Order
//
//  Created by Phong Nguyen on 10/3/15.
//  Copyright © 2015 Triad Fox. All rights reserved.
//

#import "StoreViewController.h"
#import "StoreDetailTableViewCell.h"
#import "FeedbackPopoverViewController.h"
#import "MenuEngineManager.h"
#import "MenuEngineManager.h"
#import "DishEntity.h"
#import "OptionsDetailViewController.h"
#import "OrderPlacingViewController.h"
#import "MenuJsonSerializer.h"
#import "HeaderSectionViewController.h"

#import "WYPopoverController.h"
#import "HTHorizontalSelectionList.h"
#import "SwipeView.h"

#define HEADER_VIEW_HEIGHTT                 50
#define FOOTER_VIEW_HEIGHT                  80
#define VIEW_ORDER_BUTTON_HEIGHT            50


@interface StoreViewController(){
    NSArray *_allDishes;
    NSArray *_allCategories;
    
    BOOL _didPresentBottomButton;
    BOOL _allowOrder;
    BOOL _didLoadData;
    BOOL _didClosed;
    
    StoreDetailTableViewCell *_reservedCell;
    
    CGRect _frame;
}

@property(nonatomic, retain) UINavigationController *modalNavController;
@property(nonatomic, strong) MZFormSheetPresentationController *mzPresentationController;
@property(nonatomic, retain) OrderViewController *orderViewController;
@property(nonatomic, retain) OptionsDetailViewController *optionsDetailViewController;

@property(nonatomic, strong) MenuJsonSerializer *serializer;
@property(nonatomic, strong) MenuEngineManager *menuManager;

@property(nonatomic, retain) NSArray *filterArray;

@property(nonatomic, strong) UIButton *bottomButton;
@property(nonatomic, weak) UIButton *centerNavButton;
@end

@implementation StoreViewController
@synthesize modalNavController;
@synthesize serializer;

-(id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil storeDidOpenOrClosed:(StoreOpeningTiming)opening{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if(self){
        if(opening == kStoreDidClosed){
            _didClosed = YES;
        }else{
            _didClosed = NO;
        }
    }
    return self;
}

-(void)viewDidLoad{
    [super viewDidLoad];
    [self.view setFrame:_frame];
    
    //instantiate serializer
    serializer = [MenuJsonSerializer shareSerial];
}

-(void)didReceiveMemoryWarning{
    [super didReceiveMemoryWarning];
}

-(void)frameBySuperSwipe:(CGRect)superFrame{
    _frame = superFrame;
}

-(void)prepareRespectiveDishes:(NSArray *)dishes{
    _allDishes = dishes;
}


#pragma mark - Table View Delegate's methods
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return [_allDishes count];
}

-(CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section{
    return HEADER_VIEW_HEIGHTT;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    NSArray *nibArray = [[NSBundle mainBundle] loadNibNamed:@"StoreDetailViewCell" owner:self options:nil];
    StoreDetailTableViewCell *cell = [nibArray firstObject];
    cell.delegate = self;
    
    DishEntity *dish = [_allDishes objectAtIndex:indexPath.row];
    
    cell.dish = dish;
    cell.detailTitle.text = dish.dishName;
    
    if(dish.dishImageURL){
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0), ^{
            UIImage *image = [_Shared imageFromCloudinaryWithURL:dish.dishImageURL];
            
            dispatch_async(dispatch_get_main_queue(), ^{
                if(image){
                    [cell.detailImageView setImage:image];
                    [cell.detailImageView sizeToFit];
                }
            });
        });
    }
    
    cell.detailDescription.text = dish.dishDescription;
    cell.detailPrice.text = [NSString stringWithFormat:@"%@", priceByCurrency(dish.dishPrice)];
    
    if(_didClosed){
        [cell.detailAdd setBackgroundColor:COLOR_MAIN_THEME_SILVER_DARK];
        cell.didClosed = YES;
    }else{
        [cell.detailAdd setBackgroundColor:COLOR_MAIN_THEME];
        cell.didClosed = NO;
    }
    
    //remove horizontal separator between cell of the last cell
    NSInteger rowsAmount = [tableView numberOfRowsInSection:[indexPath section]];
    if ([indexPath row] == rowsAmount - 1) {
        cell.horizontalSeparatorView.hidden = YES;
    }
    return cell;
}

-(void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath{
    StoreDetailTableViewCell *storeCell = (StoreDetailTableViewCell *)cell;
    [storeCell presentBageQuantityGreaterThanZero];
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    StoreDetailTableViewCell *cell = [tableView cellForRowAtIndexPath:indexPath];
    [cell animateShakeAddButton];
}


#pragma mark - Store Detail Cell, Order and Order Details
#pragma mark - Store Detail Cell Delegate methods
-(void)didTapOnAddButtonWithDishEntity:(DishEntity *)dishEntity onCell:(StoreDetailTableViewCell *)cell{
    if(![self determineDishHasOption:dishEntity]){
        [self.delegate shouldPresentBottomButtonNavigateToOrderPlacing:YES];
        [serializer smartAppendDishToOrders:dishEntity];
    }else{
        _reservedCell = cell;
        self.orderViewController = [[OrderViewController alloc] initWithNibName:@"OrderView" bundle:nil mode:kOrderViewModeAdding];
        self.orderViewController.dishEntity = [dishEntity copy];
        self.orderViewController.delegate = self;
        
        modalNavController = [[UINavigationController alloc] initWithRootViewController:self.orderViewController];
        modalNavController.navigationBarHidden = YES;
        
        [self initMZPresentationControllerWith:modalNavController];
    }
}

-(void)didOnTapReachGlobalQuantityLimited{
    AMSmoothAlertView *alertView = [AlertViewManager smoothDropAlertViewWithTitle:NSLocalizedString(@"com.eatus.store.order.limit.title", @"Limited") content:NSLocalizedString(@"com.eauts.store.order.limit.content", @"LimitedContent") cancelButton:NO color:COLOR_MAIN_THEME alertType:AlertInfo onCompletion:^(AMSmoothAlertView *alert, UIButton *button){
        
    }];
    
    [alertView.logoView setImage:[UIImage imageNamed:@"QtyLimited"]];
    [alertView show];
}

-(void)didTapOnAddButtonButStoreClose{
    [self.delegate shouldNotifyStoreClose:YES];
}


#pragma mark - Order Delegate methods
-(void)didSelectOnCellWithDishOption:(DishOptionsEntity *)dishOptions firstTime:(BOOL)flag exclusiveSelection:(BOOL)exclusive{
    self.optionsDetailViewController = [[OptionsDetailViewController alloc] initWithNibName:@"OptionsDetailTableView" bundle:nil];
    [self.optionsDetailViewController reloadTableViewWithChargeOptions:dishOptions];
    self.optionsDetailViewController.firstTime = flag;
    self.optionsDetailViewController.exclusiveSelection = exclusive;
    [self.orderViewController transitionToOptionDetail:self.optionsDetailViewController];
}

-(void)didTapOnDismissOrderViewControllerInMZPresentation{
    [self.mzPresentationController dismissViewControllerAnimated:YES completion:nil];
}

-(void)didTapOnAddToOrderAndDismissMZPresentation:(DishEntity *)selectedDish{
    [serializer smartAppendDishToOrders:selectedDish];
    
    [self.mzPresentationController dismissViewControllerAnimated:YES completion:^{
        if(!self.bottomButton){
            [self.delegate shouldPresentBottomButtonNavigateToOrderPlacing:YES];
        }
        [_reservedCell addToOrderToShowBadgeWithQuantity:selectedDish.dishQuantity];
    }];
}


#pragma mark - Modal View And View Transition
-(void)initMZPresentationControllerWith: (UIViewController *)viewController{
    self.mzPresentationController = [Util presentMZModalWithViewController:viewController contentSize:MODAL_VIEW_SIZE_FROM_CHILDVC];
    [self presentViewController:self.mzPresentationController animated:YES completion:nil];
}


#pragma mark - Dish Determinator
-(BOOL)determineDishHasOption: (DishEntity *)dishEntity{
    if([dishEntity.dishOptions count] > 0){
        return YES;
    }
    return NO;
}

@end
