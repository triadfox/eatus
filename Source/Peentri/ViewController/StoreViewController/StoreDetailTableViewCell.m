//
//  StoreDetailTableViewCell.m
//  Light Order
//
//  Created by Phong Nguyen on 10/3/15.
//  Copyright © 2015 Triad Fox. All rights reserved.
//

#import "StoreDetailTableViewCell.h"
#import "CustomBadge.h"
#import "UIButton+Badge.h"
#import "UIButton+EdgeInset.h"
#import "UIView+DCAnimationKit.h"
#import "MenuEngineManager.h"

#define ANIMATION_KEY_SHAKE_ADD_BUTTON                      @"shakeAddButton"

@interface StoreDetailTableViewCell(){
    BOOL allowOrder;
    
    NSInteger quantityLimited;
}
@end

@implementation StoreDetailTableViewCell
@synthesize dish;
@synthesize quantity;

- (void)awakeFromNib {
    self.selectionStyle = UITableViewCellSelectionStyleNone;
    
    [self.detailAdd spacingBetweenImageAndTitleWith:3.0f];
    
    [self.horizontalSeparatorView setBackgroundColor:[Util colorWithHexString:MAIN_HORIZONTAL_SEPARATOR_SILVER]];
    [self.horizontalSeparatorView.layer setCornerRadius:3.0f];
    [self.horizontalSeparatorView.layer setMasksToBounds:YES];
    
    [self.detailAdd setTitle:NSLocalizedString(@"com.eatus.store.add.button.title", "addMsg") forState:UIControlStateNormal];
    [self.detailAdd setBackgroundColor:[Util colorWithHexString:MAIN_THEME_COLOR]];
    
    quantityLimited = [[MenuEngineManager shareManager] getQuantityLimited];
    quantity = 0.0f;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
}

-(void)setDishForCell: (DishEntity *)dishEntity{
    dish = dishEntity;
}

-(void)addToOrder:(id)sender{
    if(_didClosed){
        [self.delegate didTapOnAddButtonButStoreClose];
    }else{
        if(quantity == quantityLimited){
            [self.delegate didOnTapReachGlobalQuantityLimited];
            return;
        }else{
            if([dish.dishOptions count] == 0){
                quantity += 1;
                self.detailAdd.badgeValue = [NSString stringWithFormat:@"%ld", (long)quantity];
            }
            
            [self.delegate didTapOnAddButtonWithDishEntity:dish onCell:self];
        }
    }
}

-(void)presentBageQuantityGreaterThanZero{
    if(quantity > 0){
        self.detailAdd.badgeValue = [NSString stringWithFormat:@"%ld", (long)quantity];
    }
    return;
}

-(void)addToOrderToShowBadgeWithQuantity:(NSUInteger)addedQuantity{
    quantity += addedQuantity;
    self.detailAdd.badgeValue = [NSString stringWithFormat:@"%ld", (long)quantity];
}

-(void)animateShakeAddButton{
    if(!self.didClosed){
        [self.detailAdd shake:nil];
    }
}

@end
