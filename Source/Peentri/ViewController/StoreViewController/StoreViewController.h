//
//  StoreViewController.h
//  Light Order
//
//  Created by Phong Nguyen on 10/3/15.
//  Copyright © 2015 Triad Fox. All rights reserved.
//

#import "BaseViewController.h"
#import "OrderViewController.h"
#import "StoreDetailTableViewCell.h"

@class SSRollingButtonScrollView;
@class StoreDetailTableViewCell;

@protocol StoreViewProtocols <NSObject>

@required
-(void)shouldPresentBottomButtonNavigateToOrderPlacing: (BOOL)determine;
-(void)shouldNotifyStoreClose: (BOOL)determine;

@end

@interface StoreViewController : BaseViewController<UITableViewDataSource, UITableViewDelegate, OrderProtocols, StoreDetailProtocols>

@property(nonatomic, weak) IBOutlet UITableView *storeTableView;
@property(nonatomic, assign) NSInteger storeMajorId;
@property(nonatomic, strong) NSString *storeName;

@property(nonatomic, strong) id<StoreViewProtocols> delegate;

-(id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil storeDidOpenOrClosed:(StoreOpeningTiming)opening;

-(void)prepareRespectiveDishes: (NSArray *)dishes;
-(void)frameBySuperSwipe: (CGRect)superFrame;
@end
