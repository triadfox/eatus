//
//  StoreSwipeViewController.m
//  Eatus
//
//  Created by Phong Nguyen on 10/31/15.
//  Copyright © 2015 Triad Fox. All rights reserved.
//

#import "StoreSwipeViewController.h"
#import "StoreViewController.h"
#import "MenuEngineManager.h"
#import "FeedbackPopoverViewController.h"
#import "OrderPlacingViewController.h"

#import "HTHorizontalSelectionList.h"
#import "WYPopoverController.h"
#import "UIView+DCAnimationKit.h"
#import "UIButton+EdgeInset.h"

#define VIEW_ORDER_BUTTON_HEIGHT            50

@interface StoreSwipeViewController ()<HTHorizontalSelectionListDataSource, HTHorizontalSelectionListDelegate, StoreViewProtocols, OrderPlacingProtocols, FeedbackPopoverProtocols>{
    
    BOOL _didPresentBottomButton;
    BOOL _allowOrder;
    BOOL _didLoadData;
    BOOL _didClosed;
    
    NSArray *_allCategories;
    NSArray *_allDishes;
    NSMutableDictionary *_viewForCategoryIndex;
    
    CGRect centerNavButtonFrame;
}

@property(nonatomic, copy) StoreViewController *storeViewController;
@property(nonatomic, strong) OrderPlacingViewController *orderPlacingViewController;
@property(nonatomic, weak) UIButton *centerNavButton;
@property(nonatomic, strong) UIButton *bottomButton;

@property(nonatomic, strong) HTHorizontalSelectionList *categorySelectionList;
@property(nonatomic, strong) WYPopoverController *popoverViewController;

@property(nonatomic, strong) MenuEngineManager *menuManager;

@end

@implementation StoreSwipeViewController
@synthesize storeCategoryView;
@synthesize storeSwipeView, categorySelectionList;
@synthesize storeViewController, orderPlacingViewController;
@synthesize menuManager;
@synthesize bottomButton, centerNavButton;

-(id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil storeDidOpenOrClosed:(StoreOpeningTiming)opening{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if(self){
        if(opening == kStoreDidClosed){
            _didClosed = YES;
        }else{
            _didClosed = NO;
        }
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    [self prepareNavBar];
    
    [MenuJsonSerializer cleanUpAndSweepAll];
    [self launchBluetoothMonitoring];
}

-(void)viewDidAppear:(BOOL)animated{
    if(!_didLoadData){
        [self prepareTopCateogory];
        _didLoadData = YES;
        
        if(_didClosed){
            dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.8 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                [self presentStoreClosedButtonAtTheBottomOfScreenWithStoreName];
            });
        }
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}


#pragma mark - View Preparation
#pragma mark - Feedback Popover On Center Nav
-(void)prepareNavBar{
    UIBarButtonItem *menuButtonItem = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"Back"] style:UIBarButtonItemStylePlain target:self action:@selector(popCurrentViewController)];
    self.navigationItem.leftBarButtonItem = menuButtonItem;
    
    [self setNavCenterButtonTitle:self.storeName?self.storeName:NSLocalizedString(@"com.eatus.store.nav.unknown.title", "unknowMsg") onCompletion:^(UIButton *centerButton){
        if(centerButton){
            centerNavButton = centerButton;
            centerNavButtonFrame = centerButton.frame;
        }
    }];
    
    [centerNavButton addTarget:self action:@selector(prepareFeedbackPopoverViewControllerFromFrame) forControlEvents:UIControlEventTouchUpInside];
}

-(void)prepareFeedbackPopoverViewControllerFromFrame{
    FeedbackPopoverViewController *feedbackPopoverViewController = [[FeedbackPopoverViewController alloc] initWithNibName:@"FeedbackPopoverView" bundle:nil];
    feedbackPopoverViewController.delegate = self;
    feedbackPopoverViewController.storeMajorId = [NSNumber numberWithInteger:_storeMajorId];
    
    self.popoverViewController = [[WYPopoverController alloc] initWithContentViewController:feedbackPopoverViewController];
    [self.popoverViewController setPopoverContentSize:CGSizeMake(290, 120)];
    self.popoverViewController.theme.borderWidth = 2.5f;
    self.popoverViewController.theme.fillBottomColor = [Util colorWithHexString:MAIN_HORIZONTAL_SEPARATOR_SILVER];
    [self.popoverViewController presentPopoverFromRect:centerNavButtonFrame inView:self.navigationItem.titleView permittedArrowDirections:WYPopoverArrowDirectionAny animated:YES options:WYPopoverAnimationOptionFadeWithScale];
}


#pragma mark - Feedback Popover Delegate methods
-(void)didFeedbackWithResult:(BOOL)result{
    [self.popoverViewController dismissPopoverAnimated:YES];
    
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.5f * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        if(result){
            [_Shared showSucceedCheckMarkProcessViewWithTitle:@"" tintColor:COLOR_MAIN_THEME onView:self.view];
        }else{
            [_Shared showFailureCrossProcessViewWithTitle:@"" tintColor:COLOR_MAIN_THEME onView:self.view];
        }
    });
}


-(void)prepareSwipeCenterView{
    storeSwipeView.alignment = SwipeViewAlignmentCenter;
    storeSwipeView.bounces = YES;
    storeSwipeView.pagingEnabled = YES;
    storeSwipeView.itemsPerPage = 1;
    storeSwipeView.truncateFinalPage = YES;
}

-(void)prepareTopCateogory{
    [self launchNetworkMonitoring];
    
    [_Shared showOverlayIndetermineSmalProgressViewWithTitle:NSLocalizedString(@"com.eatus.hud.load", @"Loading") style:kOverlayElegantStyle tintColor:[Util colorWithHexString:MAIN_THEME_COLOR] onView:self.view];
    
    [ParseDataManager getRestaurantByMajorId:[NSNumber numberWithInteger:_storeMajorId] onCompletion:^(Restaurant *restaurant, NSError *err) {
        [ParseDataManager downloadOrUpdateMenuAtRestaurant:restaurant onCompletion:^(NSString *path, NSError *err) {
            [self prepareMenuEngine:path];
            
            _allCategories =  [[menuManager getAllCategory] firstObject];
            _allDishes = [menuManager getAllDishes];
            
            [self prepareCategoryScrollingViewWithCategories];
            
            [self prepareSwipeCenterView];
            
            storeSwipeView.delegate = self;
            storeSwipeView.dataSource = self;
            _viewForCategoryIndex = [[NSMutableDictionary alloc] init];
            
            [_Shared hideOverlayIndetermineProgressViewAfter:0.8f];
        }];
    }];
}

-(void)prepareMenuEngine: (NSString *)pathToMenuFile{
    menuManager = [MenuEngineManager shareManager];
    [menuManager initWithMenuFile:pathToMenuFile];
}

-(void)prepareCategoryScrollingViewWithCategories{
    categorySelectionList = [[HTHorizontalSelectionList alloc] initWithFrame:self.storeCategoryView.bounds];
    categorySelectionList.delegate = self;
    categorySelectionList.dataSource = self;
    
    categorySelectionList.showsEdgeFadeEffect = YES;
    
    categorySelectionList.backgroundColor = [UIColor groupTableViewBackgroundColor];
    categorySelectionList.selectionIndicatorColor = [Util colorWithHexString:MAIN_THEME_COLOR_DARK];
    categorySelectionList.selectionIndicatorStyle = HTHorizontalSelectionIndicatorStyleBottomBar;
    categorySelectionList.selectionIndicatorAnimationMode = HTHorizontalSelectionIndicatorAnimationModeLightBounce;
    
    [categorySelectionList setBackgroundColor:[UIColor groupTableViewBackgroundColor]];
    
    [categorySelectionList setTitleColor:[UIColor grayColor] forState:UIControlStateHighlighted];
    [categorySelectionList setTitleFont:[UIFont systemFontOfSize:15] forState:UIControlStateNormal];
    
    [categorySelectionList setTitleFont:[UIFont boldSystemFontOfSize:15] forState:UIControlStateSelected];
    [categorySelectionList setTitleFont:[UIFont boldSystemFontOfSize:15] forState:UIControlStateHighlighted];
    
    categorySelectionList.snapToCenter = YES;
    [self.storeCategoryView addSubview:categorySelectionList];
}


#pragma mark - HTHorizontalSelection Delegate & Datasource methods
-(NSInteger)numberOfItemsInSelectionList:(HTHorizontalSelectionList *)selectionList{
    return [_allCategories count];
}

-(NSString *)selectionList:(HTHorizontalSelectionList *)selectionList titleForItemWithIndex:(NSInteger)index{
    return [_allCategories objectAtIndex:index];
}

-(void)selectionList:(HTHorizontalSelectionList *)selectionList didSelectButtonWithIndex:(NSInteger)index{
    [self.storeSwipeView scrollToItemAtIndex:index duration:0.005f];
}


#pragma mark - Swipe View Delegate & Datasource methods
-(NSInteger)numberOfItemsInSwipeView:(SwipeView *)swipeView{
    return [_allCategories count];
}

-(UIView *)swipeView:(SwipeView *)swipeView viewForItemAtIndex:(NSInteger)index reusingView:(UIView *)view{
    StoreViewController *storeVC = [_viewForCategoryIndex objectForKey:[NSNumber numberWithInteger:index]];
    if(storeVC){
        return storeVC.view;
    }else{
        if(_didClosed){
            storeViewController = [[StoreViewController alloc] initWithNibName:@"StoreView" bundle:nil storeDidOpenOrClosed:kStoreDidClosed];
        }else{
            storeViewController = [[StoreViewController alloc] initWithNibName:@"StoreView" bundle:nil storeDidOpenOrClosed:kStoreDidOpened];
        }
        
        NSString *categoryByIndex = [_allCategories objectAtIndex:index];
        NSArray *dishesByCategory = [self filterDataWithKey:categoryByIndex];
        
        [storeViewController prepareRespectiveDishes:dishesByCategory];
        [storeViewController frameBySuperSwipe:self.storeSwipeView.frame];
        storeViewController.delegate = self;
        
        [_viewForCategoryIndex setObject:storeViewController forKey:[NSNumber numberWithInteger:index]];
        
        return storeViewController.view;

    }
}

-(void)swipeViewDidScroll:(SwipeView *)swipeView{
    [self.categorySelectionList setSelectedButtonIndex:swipeView.currentItemIndex animated:YES];
}


#pragma mark - Store Delegate + Bottom Button
-(void)shouldPresentBottomButtonNavigateToOrderPlacing:(BOOL)determine{
    if(determine && !_didPresentBottomButton){
        [self presentPlaceOrderButtonAtTheBottomOfScreen];
        _didPresentBottomButton = YES;
    }
}

-(void)shouldNotifyStoreClose:(BOOL)determine{
    if(determine){
        [self.bottomButton.titleLabel shake:nil];
        [self.bottomButton.imageView shake:nil];
    }
}

-(void)presentStoreClosedButtonAtTheBottomOfScreenWithStoreName{
    CGRect frame = CGRectMake(0, self.view.frame.size.height-VIEW_ORDER_BUTTON_HEIGHT, self.view.bounds.size.width, VIEW_ORDER_BUTTON_HEIGHT);
    self.bottomButton = [[UIButton alloc] initWithFrame:frame];
    
    [self.bottomButton setBackgroundColor:COLOR_MAIN_THEME_SILVER_DARK];
    [self.bottomButton setTitle:NSLocalizedString(@"com.eatus.timing.closed", @"ClosedStore") forState:UIControlStateNormal];
    [self.bottomButton setImage:[UIImage imageNamed:@"OutletClosed"] forState:UIControlStateNormal];
    self.bottomButton.titleLabel.font = [UIFont boldSystemFontOfSize:17.0f];
    self.bottomButton.contentHorizontalAlignment = UIControlContentHorizontalAlignmentCenter;
    self.bottomButton.contentVerticalAlignment = UIControlContentVerticalAlignmentCenter;
    [self.bottomButton spacingBetweenImageAndTitleWith:5.0f sizeToFit:NO];
    [self.bottomButton setEnabled:NO];
    
    [self.view addSubview:self.bottomButton];
    
    [UIView animateWithDuration:0.9f delay:0.1f options:UIViewAnimationOptionAllowAnimatedContent animations:^{
        CATransition *transition = [CATransition animation];
        transition.type = kCATransitionMoveIn;
        transition.subtype = kCATransitionFromTop;
        transition.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseOut];
        transition.speed = 1.0f;
        [self.bottomButton.layer addAnimation:transition forKey:ANIMATION_KEY_VIEW_ORDER_MOVE_IN];
    }completion:^(BOOL finished){
        [self.view addSubview:self.bottomButton];
    }];

}

-(void)presentPlaceOrderButtonAtTheBottomOfScreen{
    CGRect frame = CGRectMake(0, self.view.frame.size.height-VIEW_ORDER_BUTTON_HEIGHT, self.view.bounds.size.width, VIEW_ORDER_BUTTON_HEIGHT);
    self.bottomButton = [[UIButton alloc] initWithFrame:frame];
    
    [self.bottomButton setBackgroundColor:COLOR_MAIN_THEME];
    [self.bottomButton setTitle:NSLocalizedString(@"com.eatus.store.viewOrder.button.title", "viewOrderMsg") forState:UIControlStateNormal];
    [self.bottomButton setImage:[UIImage imageNamed:@"Wrap"] forState:UIControlStateNormal];
    self.bottomButton.titleLabel.font = [UIFont boldSystemFontOfSize:17.0f];
    self.bottomButton.contentHorizontalAlignment = UIControlContentHorizontalAlignmentCenter;
    self.bottomButton.contentVerticalAlignment = UIControlContentVerticalAlignmentCenter;
    
    [self.view addSubview:self.bottomButton];
    
    [UIView animateWithDuration:0.9f delay:0.1f options:UIViewAnimationOptionAllowAnimatedContent animations:^{
        CATransition *transition = [CATransition animation];
        transition.type = kCATransitionMoveIn;
        transition.subtype = kCATransitionFromTop;
        transition.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseOut];
        transition.speed = 1.0f;
        [self.bottomButton.layer addAnimation:transition forKey:ANIMATION_KEY_VIEW_ORDER_MOVE_IN];
    }completion:^(BOOL finished){
        [self.view addSubview:self.bottomButton];
        
        _didPresentBottomButton = YES;
        
        [self.bottomButton addTarget:self action:@selector(presentOrderPlacingViewController) forControlEvents:UIControlEventTouchUpInside];
    }];
}

-(void)animateHidePlaceOrderButtonAtTheBottomOfScreenOnCompletion: (void (^)(void))completion{
    if(self.bottomButton){
        [UIView animateWithDuration:0.3f delay:0.0f options:UIViewAnimationOptionAllowAnimatedContent animations:^{
            self.bottomButton.alpha = 0.0f;
        }completion:^(BOOL finished){
            if(completion){
                completion();
            }
        }];
    }
}

-(void)animatePresentPlaceOrderButtonAtTheBottomOfScreenBackOnCompletion: (void (^)(void))completion{
    if(self.bottomButton){
        [UIView animateWithDuration:0.3f delay:0.0f options:UIViewAnimationOptionAllowAnimatedContent animations:^{
            self.bottomButton.alpha = 1.0f;
        }completion:^(BOOL finished){
            if(completion){
                completion();
            }
        }];
    }
}


#pragma mark - Order Placing + Delegate
-(void)presentOrderPlacingViewController{
    NSArray *dishesOrdered = [[MenuJsonSerializer shareSerial] deserializeToDishEntity];
    orderPlacingViewController = [[OrderPlacingViewController alloc] initWithNibName:@"OrderPlacingView" bundle:nil mode:kOrderPlacingProceedCheckout];
    orderPlacingViewController.delegate = self;
    orderPlacingViewController.orders = [dishesOrdered mutableCopy];
    
    [ParseDataManager getAvailableTodayPromotionByRestaurant:self.restaurant onCompletion:^(NSArray *promotions, NSError *error){
        if(!error){
            Promotion *todayPromotion = [promotions firstObject];
            [orderPlacingViewController prepareAllCalculationByDiscountPercentage:[todayPromotion.discount floatValue]];
        }
    }];
    
    NavBarController *navControllerOrderPlacing = [[NavBarController alloc] initWithRootViewController:orderPlacingViewController];
    [self animateHidePlaceOrderButtonAtTheBottomOfScreenOnCompletion:^{
        [self.parentViewController presentViewController:navControllerOrderPlacing animated:YES completion:nil];
    }];
}

-(void)orderPlacingWillDisappear{
    
}

-(void)orderPlacingDidDisappear{
    NSArray *dishesOrdered = [[MenuJsonSerializer shareSerial] deserializeToDishEntity];
    if([dishesOrdered count] == 0){
        _didPresentBottomButton = NO;
        [self animateHidePlaceOrderButtonAtTheBottomOfScreenOnCompletion:nil];
    }else{
        [self animatePresentPlaceOrderButtonAtTheBottomOfScreenBackOnCompletion:nil];
    }
}


#pragma mark - Filtering & Determine on Dishes
-(NSArray *)filterDataWithKey: (NSString *)key{
    return predicateArray(_allDishes, @"SELF.dishCategory == %@", key);
}

@end
