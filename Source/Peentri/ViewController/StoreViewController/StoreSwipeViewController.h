//
//  StoreSwipeViewController.h
//  Eatus
//
//  Created by Phong Nguyen on 10/31/15.
//  Copyright © 2015 Triad Fox. All rights reserved.
//

#import "BaseViewController.h"
#import "SwipeView.h"

@interface StoreSwipeViewController : BaseViewController<SwipeViewDataSource, SwipeViewDelegate>

@property(nonatomic, weak) IBOutlet UIView *storeCategoryView;
@property(nonatomic, weak) IBOutlet SwipeView *storeSwipeView;
@property(nonatomic, assign) NSInteger storeMajorId;
@property(nonatomic, strong) Restaurant *restaurant;
@property(nonatomic, strong) NSString *storeName;

-(id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil storeDidOpenOrClosed: (StoreOpeningTiming)opening;

@end
