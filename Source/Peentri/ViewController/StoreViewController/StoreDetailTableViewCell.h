//
//  StoreDetailTableViewCell.h
//  Light Order
//
//  Created by Phong Nguyen on 10/3/15.
//  Copyright © 2015 Triad Fox. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "DishEntity.h"

@class StoreDetailTableViewCell;

@protocol StoreDetailProtocols <NSObject>

@required
-(void)didTapOnAddButtonWithDishEntity: (DishEntity *)dishEntity onCell: (StoreDetailTableViewCell *)cell;
-(void)didOnTapReachGlobalQuantityLimited;
-(void)didTapOnAddButtonButStoreClose;

@end

@interface StoreDetailTableViewCell : UITableViewCell

@property(nonatomic, strong) IBOutlet UIImageView *detailImageView;
@property(nonatomic, weak) IBOutlet UILabel *detailTitle;
@property(nonatomic, weak) IBOutlet UILabel *detailDescription;
@property(nonatomic, weak) IBOutlet UILabel *detailPrice;
@property(nonatomic, weak) IBOutlet UIButton *detailAdd;
@property(nonatomic, weak) IBOutlet UIView *horizontalSeparatorView;

@property(nonatomic, assign) BOOL didClosed;
@property(nonatomic, weak) DishEntity *dish;
@property(nonatomic, strong) id<StoreDetailProtocols> delegate;
@property(nonatomic, assign) NSUInteger quantity;

-(IBAction)addToOrder:(id)sender;

-(void)presentBageQuantityGreaterThanZero;
-(void)addToOrderToShowBadgeWithQuantity: (NSUInteger)addedQuantity;
-(void)animateShakeAddButton;
@end
