//
//  PaymentBaseTableViewCell.h
//  Eatus
//
//  Created by Phong Nguyen on 10/25/15.
//  Copyright © 2015 Triad Fox. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef NS_ENUM(NSInteger, PaymentCellType) {
    kPaymentCellCash,
    kPaymentCellCard
};

@interface PaymentBaseTableViewCell : UITableViewCell

@property(nonatomic, assign) PaymentCellType paymenType;

-(void)checkSelectedOnCompletion: (void (^)(void))completion;
-(void)checkUnselectedOnCompletion: (void (^)(void))completion;

@end
