//
//  PaymentAddCardTableViewCell.m
//  Eatus
//
//  Created by Phong Nguyen on 10/24/15.
//  Copyright © 2015 Triad Fox. All rights reserved.
//

#import "PaymentAddCardTableViewCell.h"

@implementation PaymentAddCardTableViewCell

- (void)awakeFromNib {
    [self.addCardButton setBackgroundColor:[Util colorWithHexString:MAIN_THEME_COLOR]];
    [self.addCardButton setTintColor:[UIColor whiteColor]];
    [self.addCardButton setTitle:@"Add new card" forState:UIControlStateNormal];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
}

@end
