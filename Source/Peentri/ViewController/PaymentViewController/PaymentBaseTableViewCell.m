//
//  PaymentBaseTableViewCell.m
//  Eatus
//
//  Created by Phong Nguyen on 10/25/15.
//  Copyright © 2015 Triad Fox. All rights reserved.
//

#import "PaymentBaseTableViewCell.h"

@implementation PaymentBaseTableViewCell

- (void)awakeFromNib {
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
}

-(void)checkSelectedOnCompletion:(void (^)(void))completion{
    [UIView transitionWithView:self duration:0.5f options:UIViewAnimationOptionTransitionCrossDissolve animations:^{
        self.accessoryType = UITableViewCellAccessoryCheckmark;
    }completion:^(BOOL finished){
        if(finished){
            if(completion){
                completion();
            }
        }
    }];
}

-(void)checkUnselectedOnCompletion:(void (^)(void))completion{
    [UIView transitionWithView:self duration:0.5f options:UIViewAnimationOptionTransitionCrossDissolve animations:^{
        self.accessoryType = UITableViewCellAccessoryNone;
    }completion:^(BOOL finished){
        if(finished){
            if(completion){
                completion();
            }
        }
    }];
}

@end
