//
//  PaymentAddViewController.h
//  Eatus
//
//  Created by Phong Nguyen on 10/24/15.
//  Copyright © 2015 Triad Fox. All rights reserved.
//

#import "BaseViewController.h"

@interface PaymentAddViewController : BaseViewController{
    __weak IBOutlet UIView *stripeCardView;
    __weak IBOutlet UIButton *scanCard;
}

-(IBAction)saveCardInfomartion:(id)sender;
-(IBAction)scanCard:(id)sender;

@end
