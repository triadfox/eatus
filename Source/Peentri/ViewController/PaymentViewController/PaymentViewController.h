//
//  PaymentViewController.h
//  Light Order
//
//  Created by Phong Nguyen on 10/22/15.
//  Copyright © 2015 Triad Fox. All rights reserved.
//

#import "BaseViewController.h"

typedef NS_ENUM(NSUInteger, PaymentModeView) {
    kPaymentModeNormal,
    kPaymentModeFromOrderPlacing
};

@interface PaymentViewController : BaseViewController<UITableViewDataSource, UITableViewDelegate>

@property(nonatomic, weak) IBOutlet UITableView *paymentCardTableView;

-(id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil mode:(PaymentModeView)modeView;

@end
