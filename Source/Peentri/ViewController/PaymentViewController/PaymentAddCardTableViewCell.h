//
//  PaymentAddCardTableViewCell.h
//  Eatus
//
//  Created by Phong Nguyen on 10/24/15.
//  Copyright © 2015 Triad Fox. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PaymentBaseTableViewCell.h"

@interface PaymentAddCardTableViewCell : PaymentBaseTableViewCell

@property(nonatomic, weak) IBOutlet UIButton *addCardButton;

@end
