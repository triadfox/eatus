//
//  PaymentViewController.m
//  Light Order
//
//  Created by Phong Nguyen on 10/22/15.
//  Copyright © 2015 Triad Fox. All rights reserved.
//

#import "PaymentViewController.h"
#import "PaymentManager.h"
#import "PaymentCardTableViewCell.h"
#import "PaymentCashTableViewCell.h"
#import "PaymentAddCardTableViewCell.h"
#import "PaymentAddViewController.h"
#import "HeaderSectionViewController.h"

#define HEIGHT_SECTION_DEFAULT                          16
#define HEIGHT_SECTION_ADD_CARD                         80
#define HEIGH_SECTION_ALL_CARD                          44

@interface PaymentViewController (){
    NSIndexPath *previousCellPath;
    
    BOOL launchFromOrderPlacing;
    BOOL didLoadData;
}

@property(nonatomic, weak) PaymentCardTableViewCell *cardCell;
@property(nonatomic, weak) PaymentCashTableViewCell *cashCell;
@property(nonatomic, weak) PaymentAddCardTableViewCell *addCardCell;
@property(nonatomic, strong) NSArray *cards;

@end

@implementation PaymentViewController
@synthesize cardCell, cashCell, addCardCell;
@synthesize cards;

-(id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil mode:(PaymentModeView)modeView{
    self = [super init];
    if(self){
        if(modeView == kPaymentModeNormal){
            launchFromOrderPlacing = NO;
        }else if(modeView == kPaymentModeFromOrderPlacing){
            launchFromOrderPlacing = YES;
        }
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    [self settingUpInterface];
    launchFromOrderPlacing?[self setDismissLeftBarButton]:[self prepareSideBarMenu];
    [_NotificationCenter addObserver:self selector:@selector(observerReloadDataByAddCard:) name:kNotificationAddNewCard object:nil];
}

-(void)viewDidAppear:(BOOL)animated{
    if(!didLoadData){
        [self preparePaymentDataWithParse];
        didLoadData = YES;
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

-(void)observerReloadDataByAddCard: (id)sender{
    [self preparePaymentDataWithParse];
    [_NotificationCenter removeObserver:self name:kNotificationAddNewCard object:nil];
}

-(void)preparePaymentDataWithParse{
    [_Shared showOverlayIndetermineSmalProgressViewWithTitle:NSLocalizedString(@"com.eatus.hud.load", @"Loading") style:kOverlayElegantStyle tintColor:[Util colorWithHexString:MAIN_THEME_COLOR] onView:self.view];
    [ParseDataManager getAllMyCardLast4IdOnCompletion:^(NSArray *cardCollection, NSError *error){
        if([cardCollection count] != 0){
            cards = cardCollection;
        }
        
        self.paymentCardTableView.delegate = self;
        self.paymentCardTableView.dataSource = self;
        [self.paymentCardTableView reloadData];
        
        [_Shared hideOverlayIndetermineProgressViewAfter:0.8f];
    }];
}

-(void)settingUpInterface{
    [self setNavCenterTitle:NSLocalizedString(@"com.eatus.nav.center.title.payment", @"PAYMENT")];
}


#pragma mark - Table View Delegate methods
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return cards.count + 2;
}

-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    NSInteger rowsAmount = [tableView numberOfSections];
    if(section == 0){
        return HEIGH_SECTION_ALL_CARD;
    }else if (section == rowsAmount - 1){
        return HEIGHT_SECTION_ADD_CARD;
    }else{
        return HEIGHT_SECTION_DEFAULT;
    }
}

-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    if(section == 0){
        HeaderSectionViewController *headerViewController = [[HeaderSectionViewController alloc] initWithNibName:@"HeaderSectionView" bundle:nil];
        [headerViewController presentWithHeaderTitle:NSLocalizedString(@"com.eatus.payment.header.section.label", @"PaymentOption")];
        return headerViewController.view;
    }else{
        UIView *headerView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, tableView.bounds.size.width, HEIGHT_SECTION_DEFAULT)];
        [headerView setBackgroundColor:[UIColor clearColor]];
        return headerView;
    }
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return 1;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    NSInteger rowsAmount = [tableView numberOfSections];
    
    NSString *paymentType = [_UserDefault objectForKey:kCurrenPaymentType];
    
    if ([indexPath section] == rowsAmount - 1) {
        addCardCell = [[[NSBundle mainBundle] loadNibNamed:@"PaymentAddCardViewCell" owner:self options:nil] firstObject];
        [addCardCell.addCardButton addTarget:self action:@selector(addNewCard:) forControlEvents:UIControlEventTouchUpInside];
        return addCardCell;
    }
    
    if(indexPath.section == 0){
        cashCell = [[[NSBundle mainBundle] loadNibNamed:@"PaymentCashViewCell" owner:self options:nil] firstObject];
        cashCell.paymentCash.text = NSLocalizedString(@"com.eatus.payment.pay.cash.label", @"CashPayment");
        
        if(isNilOrEmpty(paymentType) || [paymentType isEqualToString:PAYMENT_CASH]){
            [_UserDefault setObject:PAYMENT_CASH forKey:kCurrenPaymentType];
            cashCell.accessoryType = UITableViewCellAccessoryCheckmark;
            previousCellPath = indexPath;
        }
        
        return cashCell;
    }else{
        cardCell = [[[NSBundle mainBundle] loadNibNamed:@"PaymentCardViewCell" owner:self options:nil] firstObject];
        
        CardPayment *cardPayment = [cards objectAtIndex:indexPath.section - 1];
        NSString *last4Id = cardPayment.last4Id;
        
        NSString *currentLast4Id = [_UserDefault objectForKey:kCurrenCardLast4];
        if([currentLast4Id isEqualToString:last4Id] && [paymentType isEqualToString:PAYMENT_CARD]){
            cardCell.accessoryType = UITableViewCellAccessoryCheckmark;
            previousCellPath = indexPath;
        }
        
        [cardCell determineCardBrandWithLast4Id:last4Id?last4Id:@""];
        return cardCell;
    }
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    //check whether previous
    PaymentBaseTableViewCell *baseCell;
    
    if(previousCellPath){
        //get previous cell, remove check mark
        baseCell = [tableView cellForRowAtIndexPath:previousCellPath];
        [baseCell checkUnselectedOnCompletion:nil];
        
    }
        //previous cell is not existed yet
        previousCellPath = indexPath;
        baseCell = [tableView cellForRowAtIndexPath:indexPath];
        [baseCell checkSelectedOnCompletion:^{
            [self determinePaymentType:baseCell];
        }];
}

-(void)determinePaymentType: (PaymentBaseTableViewCell *)baseCell{
    NSString *paymentType;
    if(baseCell.paymenType == kPaymentCellCash){
        paymentType = PAYMENT_CASH;
    }else{
        paymentType = PAYMENT_CARD;
        PaymentCardTableViewCell *cardViewCell = (PaymentCardTableViewCell *)baseCell;
        NSString *last4Id = [cardViewCell.cardNumberField.text substringFromIndex:cardViewCell.cardNumberField.text.length-4];
        [_UserDefault setObject:last4Id forKey:kCurrenCardLast4];
    }
    [_UserDefault setObject:paymentType forKey:kCurrenPaymentType];
}


#pragma mark - IBAction methods
-(void)addNewCard:(id)sender{
    PaymentAddViewController *addNewCard = [[PaymentAddViewController alloc] initWithNibName:@"PaymentAddView" bundle:nil];
    NavBarController *navController = [[NavBarController alloc] initWithRootViewController:addNewCard];
    [self presentViewController:navController];
}

@end
