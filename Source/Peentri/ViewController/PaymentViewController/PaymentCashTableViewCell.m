//
//  PaymentCashTableViewCell.m
//  Eatus
//
//  Created by Phong Nguyen on 10/23/15.
//  Copyright © 2015 Triad Fox. All rights reserved.
//

#import "PaymentCashTableViewCell.h"

@implementation PaymentCashTableViewCell

- (void)awakeFromNib {
    self.paymenType = kPaymentCellCash;
    self.selectionStyle = UITableViewCellSelectionStyleNone;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

}

@end
