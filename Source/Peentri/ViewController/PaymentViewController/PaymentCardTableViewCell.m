//
//  PaymentTableViewCell.m
//  Eatus
//
//  Created by Phong Nguyen on 10/23/15.
//  Copyright © 2015 Triad Fox. All rights reserved.
//

#import "PaymentCardTableViewCell.h"
#import "PaymentManager.h"

@implementation PaymentCardTableViewCell

- (void)awakeFromNib {
    self.paymenType = kPaymentCellCard;
    self.selectionStyle = UITableViewCellSelectionStyleNone;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

}

-(void)determineCardBrandWithLast4Id:(NSString *)last4Id {
    STPCardBrand cardBrand = [PaymentManager mycardBrandNameKeychainForLast4Id:last4Id];
    
    self.cardBranchImageView.image = [PaymentManager imageForSTPCardBrand:cardBrand];
    
    self.cardNumberField.text = CARD_NUMBER_ENCRYPTED(last4Id);
}

@end
