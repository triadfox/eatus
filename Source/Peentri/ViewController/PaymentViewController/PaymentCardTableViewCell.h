//
//  PaymentTableViewCell.h
//  Eatus
//
//  Created by Phong Nguyen on 10/23/15.
//  Copyright © 2015 Triad Fox. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PaymentBaseTableViewCell.h"

@interface PaymentCardTableViewCell : PaymentBaseTableViewCell

@property(nonatomic, weak) IBOutlet UITextField *cardNumberField;
@property(nonatomic, strong) IBOutlet UIImageView *cardBranchImageView;

-(void)determineCardBrandWithLast4Id: (NSString *)last4Id;
@end
