//
//  PaymentAddViewController.m
//  Eatus
//
//  Created by Phong Nguyen on 10/24/15.
//  Copyright © 2015 Triad Fox. All rights reserved.
//

#import "PaymentAddViewController.h"
#import "Stripe.h"
#import "PaymentManager.h"
#import "CardIO.h"

@interface PaymentAddViewController ()<STPPaymentCardTextFieldDelegate, CardIOPaymentViewControllerDelegate>

@property(nonatomic, strong) STPPaymentCardTextField *stripeTextField;
@property(nonatomic, strong) CardIOPaymentViewController *scanViewController;

@end

@implementation PaymentAddViewController
@synthesize stripeTextField;
@synthesize scanViewController;

- (void)viewDidLoad {
    [super viewDidLoad];
}

-(void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    
    [self preparePaymentStripeTextField];
    [self prepareNavItem];
    [self prepareInterface];
    [self setDismissLeftBarButton];
    [CardIOUtilities preload];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

-(void)preparePaymentStripeTextField{
    stripeTextField = [[STPPaymentCardTextField alloc] initWithFrame:stripeCardView.bounds];
    stripeTextField.delegate = self;
    
    [stripeTextField setBackgroundColor:[UIColor whiteColor]];
    
    [UIView transitionWithView:stripeTextField duration:0.5f options:UIViewAnimationOptionTransitionCrossDissolve animations:^{
        [stripeCardView addSubview:stripeTextField];
    }completion:^(BOOL finishes){
        [stripeTextField becomeFirstResponder];
    }];
}

-(void)prepareInterface{
    [scanCard setTintColor:[Util colorWithHexString:MAIN_THEME_COLOR_LIGHT_LV_1]];
}

-(void)prepareNavItem{
    UIBarButtonItem *cancelButton = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"Delete"] style:UIBarButtonItemStylePlain target:self action:@selector(dismissAddCardViewController:)];
    self.navigationItem.leftBarButtonItem = cancelButton;
    
    UIBarButtonItem *saveButton = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"Card"] style:UIBarButtonItemStyleDone target:self action:@selector(saveCardInfomartion:)];
    self.navigationItem.rightBarButtonItem = saveButton;
    self.navigationItem.rightBarButtonItem.enabled = NO;
    
    [self setNavCenterTitle:NSLocalizedString(@"com.eatus.payment.pay.card.add.button.title", @"NewCard")];
}

-(void)dismissAddCardViewController: (id)sender{
    [self dismissViewControllerAnimated:YES completion:^{
        
    }];
}

-(void)saveCardInfomartion: (id)sender{
    STPCardParams *card = self.stripeTextField.card;
    [self saveCardInformationBy:card];
}

-(void)saveCardInformationBy: (STPCardParams *)card{
    [_Shared showOverlayIndetermineSmalProgressViewWithTitle:NSLocalizedString(@"com.eatus.hud.process", @"Processing") style:kOverlayElegantStyle tintColor:[Util colorWithHexString:MAIN_THEME_COLOR_LIGHT_LV_1] onView:self.view];
    
    [PaymentManager mycardKeychainWithBrandName:card.last4 cardBrand:[PaymentManager cardBrandNameForNumber:card.number]];
    
    [[PaymentManager defaultPayment] registerCardWithHolderName:card.name cardNumber:card.number cvcNumber:card.cvc expMonth:card.expMonth expYear:card.expYear onCompletion:^(STPToken *token, NSError *error){
        
        [ParseDataManager registerMyCardWithTokenId:token.tokenId last4Id:card.last4 onCompletion:^(BOOL succeeded, NSError *error){
            if(succeeded){
                [self dismissViewControllerAnimated:YES completion:nil];
                [_Shared hideOverlayIndetermineProgressViewAfter:0.3f];
                
                [_NotificationCenter postNotificationName:kNotificationAddNewCard object:nil];
            }
        }];
    }];
}

-(void)scanCard:(id)sender{
    scanViewController = [[CardIOPaymentViewController alloc] initWithPaymentDelegate:self];
    scanViewController.hideCardIOLogo = YES;
    scanViewController.disableManualEntryButtons = YES;
    scanViewController.guideColor = [Util colorWithHexString:MAIN_THEME_COLOR_LIGHT_LV_1];
    [scanViewController dismissViewControllerAnimated:YES completion:nil];
    [self presentViewController:scanViewController];
}


#pragma mark - Delegate methods
#pragma mark - CardIO Payment Scanner
-(void)userDidCancelPaymentViewController:(CardIOPaymentViewController *)paymentViewController{
    [paymentViewController dismissViewControllerAnimated:YES completion:nil];
}

-(void)userDidProvideCreditCardInfo:(CardIOCreditCardInfo *)cardInfo inPaymentViewController:(CardIOPaymentViewController *)paymentViewController{
    STPCardParams *params = [PaymentManager generateCardParamWithNameCardHolder:@"" cardNumber:cardInfo.cardNumber cvcNumber:cardInfo.cvv expMonth:cardInfo.expiryMonth expYear:cardInfo.expiryYear];
    [self saveCardInformationBy:params];
}


#pragma mark - SPTPaymentCardTextField Delegate methods
-(void)paymentCardTextFieldDidChange:(STPPaymentCardTextField *)textField{
    self.navigationItem.rightBarButtonItem.enabled = textField.isValid;
}


-(NSString *)last4CardId: (NSString *)cardNumber{
    return [cardNumber substringFromIndex:cardNumber.length-4];
}

@end
