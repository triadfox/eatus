//
//  PaymentCashTableViewCell.h
//  Eatus
//
//  Created by Phong Nguyen on 10/23/15.
//  Copyright © 2015 Triad Fox. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PaymentBaseTableViewCell.h"

@interface PaymentCashTableViewCell : PaymentBaseTableViewCell

@property(nonatomic, weak) IBOutlet UIImageView *paymentCashImageView;
@property(nonatomic, weak) IBOutlet UILabel *paymentCash;

@end
