//
//  BaseViewController.h
//  MiPoBeacon
//
//  Created by Phong Nguyen on 9/4/15.
//  Copyright (c) 2015 Triad Fox. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "NavBarController.h"
#import "Util.h"
#import "ParseDataManager.h"
#import "ConnectionManager.h"
#import "EstimoteBeaconServiceManager.h"
#import "KontaktBeaconServiceManager.h"
#import "LocationManager.h"
#import "MenuEngineManager.h"
#import "MenuJsonSerializer.h"
#import "WatchConnectivityManager.h"

#import "YALSunnyRefreshControl.h"
#import "MZFormSheetPresentationController.h"
#import "PermissionScope+Shared.h"
#import "JDFTooltips.h"
#import "UICKeyChainStore.h"
#import "AlertViewManager.h"
#import "EHFAuthenticator.h"

#import "UIImage+DeviceSpecificMedia.h"
#import "UIView+DCAnimationKit.h"

@interface BaseViewController : UIViewController

@property(nonatomic, strong) UIViewController *parent;
@property(nonatomic, strong) PermissionScope *permission;
@property(nonatomic, strong) LocationManager *locationManager;
@property(nonatomic, strong) WatchConnectivityManager *connectivityManager;

-(id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil;

-(void)prepareSideBarMenu;

-(void)launchBeaconManager;
-(void)launchBeaconSurveillance;
-(void)launchNetworkMonitoring;
-(void)launchBluetoothMonitoring;
-(void)launchLocationManager;

-(void)pushFrontViewController: (UINavigationController *)navController;
-(void)popCurrentViewController;
-(void)setNewFrontViewController: (UIViewController *)viewController;

-(void)setNavCenterTitle: (NSString *)title;
-(void)setNavCenterButtonTitle: (NSString *)title onCompletion: (void (^)(UIButton *centerButton))completion;

-(void)setDismissLeftBarButton;

-(void)hideNavBar;
-(void)presentNavBar;

-(void)presentViewController: (UIViewController *)viewControllerToPresent;
-(void)presentViewControllerFromRootViewController: (UIViewController *)viewControllerToPresent;

-(void)presentNavigateBarController: (UIViewController *)viewControllerToPresent;
-(void)presentSubView: (UIView *)viewToPresent;
-(void)dismissViewController: (UIViewController *)viewControllerToDismiss;
-(void)navigatebarhidden: (BOOL)hidden;

@end
