//
//  BaseViewController.m
//  MiPoBeacon
//
//  Created by Phong Nguyen on 9/4/15.
//  Copyright (c) 2015 Triad Fox. All rights reserved.
//

#import "BaseViewController.h"
#import "SWRevealViewController.h"
#import "UIButton+EdgeInset.h"

@interface BaseViewController()

@property(nonatomic, strong) EstimoteBeaconServiceManager *beaconManager;
@end

@implementation BaseViewController
@synthesize beaconManager, locationManager;

-(id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if(self){
        
    }
    return self;
}

-(void)viewDidLoad{
    [self settingUpNavbar];
    self.permission = [PermissionScope shareScope];
    self.connectivityManager = [WatchConnectivityManager standardConnectivity];
}

-(void)launchBeaconManager{
    [[KontaktBeaconServiceManager shareBeacon] launchKontaktBeaconMonitoring];
}

-(void)launchBeaconSurveillance{
    [[KontaktBeaconServiceManager shareBeacon] launchKontatkBeaconSurveillance];
}

-(void)launchNetworkMonitoring{
    [ConnectionManager currentConnectionStatus];
}

-(void)launchBluetoothMonitoring{
    [ConnectionManager currentBluetoothStatus];
}

-(void)prepareBeaconEngineManager{
    beaconManager = [EstimoteBeaconServiceManager shareBeacon];
    beaconManager.delegate = self;
}

-(void)launchLocationManager{
    locationManager =  [LocationManager defaultLocation];
}

-(void)prepareSideBarMenu{
    SWRevealViewController *revealController = [self revealViewController];
    [revealController panGestureRecognizer];
    [revealController tapGestureRecognizer];
    
    UIBarButtonItem *menuButtonItem = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"Menu"] style:UIBarButtonItemStylePlain target:revealController action:@selector(revealToggle:)];
    self.navigationItem.leftBarButtonItem = menuButtonItem;
}

-(void)pushFrontViewController:(UINavigationController *)navController{
    SWRevealViewController *revealController = [self revealViewController];
    [revealController pushFrontViewController:navController animated:YES];
}

-(void)popCurrentViewController{
    [self.navigationController popViewControllerAnimated:YES];
}

-(void)setNewFrontViewController:(UIViewController *)viewController{
    SWRevealViewController *revealController = [self revealViewController];
    [revealController setFrontViewController:viewController animated:YES];
}

-(void)hideNavBar{
    [self navigatebarhidden:YES];
}

-(void)presentNavBar{
    [self navigatebarhidden:NO];
}

-(void)setNavCenterTitle:(NSString *)title{
    self.navigationItem.title = title;
}

-(void)setNavCenterButtonTitle:(NSString *)title onCompletion:(void (^)(UIButton *))completion{
    UIButton *centerButton = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 200, 35)];
    [centerButton setContentHorizontalAlignment:UIControlContentHorizontalAlignmentCenter];
    
    [centerButton setTitle:title forState:UIControlStateNormal];
    [centerButton setImage:[UIImage imageNamed:@"ExpandR"] forState:UIControlStateNormal];
    [centerButton alignRightTheImageOfButtonWithSpace:3.0f];
    self.navigationItem.titleView = centerButton;
    
    if(completion){
        completion(centerButton);
    }
}

-(void)presentViewController:(UIViewController *)viewControllerToPresent{
    [self presentViewController:viewControllerToPresent animated:YES completion:nil];
}

-(void)presentViewControllerFromRootViewController:(UIViewController *)viewControllerToPresent{
    [self.view.window.rootViewController presentViewController:viewControllerToPresent animated:YES completion:nil];
}

-(void)presentNavigateBarController:(UIViewController *)viewControllerToPresent{
    UIViewController *navBarController = [[NavBarController alloc] initWithRootViewController:viewControllerToPresent];
    [self presentViewController:navBarController animated:YES completion:nil];
}

-(void)presentSubView:(UIView *)viewToPresent{
    [self.view addSubview:viewToPresent];
}

-(void)dismissViewController:(UIViewController *)viewControllerToDismiss{
    [viewControllerToDismiss dismissViewControllerAnimated:YES completion:nil];
}

-(void)navigatebarhidden:(BOOL)hidden{
    self.navigationController.navigationBarHidden = hidden;
}

-(void)settingUpNavbar{
    [UINavigationBar appearance].tintColor = [UIColor whiteColor];
    [[UINavigationBar appearance] setBackgroundColor:COLOR_MAIN_THEME];
    [[UINavigationBar appearance] setBarTintColor:COLOR_MAIN_THEME];
    
    [self.navigationController.navigationBar
     setTitleTextAttributes:@{NSForegroundColorAttributeName : [UIColor whiteColor],
                              NSFontAttributeName: [UIFont fontWithName:@"TrebuchetMS-Bold" size:17.0f]}];
}

-(void)setDismissLeftBarButton{
    UIBarButtonItem *menuButtonItem = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"Delete"] style:UIBarButtonItemStylePlain target:self action:@selector(dismissCurrentViewController)];
    self.navigationItem.leftBarButtonItem = menuButtonItem;
}

-(void)dismissCurrentViewController{
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (CALayer *)gradientBGLayerForBounds:(CGRect)bounds
{
    CAGradientLayer * gradientBG = [CAGradientLayer layer];
    gradientBG.frame = bounds;
    gradientBG.colors = @[ (id)COLOR_MAIN_THEME.CGColor, (id)[UIColor whiteColor].CGColor ];
    return gradientBG;
}

-(UIImage *)imageGradientWithFrame: (CGRect)frame{
    CALayer * bgGradientLayer = [self gradientBGLayerForBounds:frame];
    UIGraphicsBeginImageContext(bgGradientLayer.bounds.size);
    [bgGradientLayer renderInContext:UIGraphicsGetCurrentContext()];
    UIImage * bgAsImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return bgAsImage;
}

@end
