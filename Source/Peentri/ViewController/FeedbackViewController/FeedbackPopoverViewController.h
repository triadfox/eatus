//
//  FeedbackViewController.h
//  Eatus
//
//  Created by Phong Nguyen on 10/29/15.
//  Copyright © 2015 Triad Fox. All rights reserved.
//

#import "BaseViewController.h"

@protocol FeedbackPopoverProtocols <NSObject>

@required
-(void)didFeedbackWithResult: (BOOL)result;

@end

@interface FeedbackPopoverViewController : BaseViewController{
    __weak IBOutlet UIButton *likeButton;
    __weak IBOutlet UIButton *dislikeButton;
}

@property(nonatomic, strong) id<FeedbackPopoverProtocols> delegate;
@property(nonatomic, strong) NSNumber *storeMajorId;

-(IBAction)feedbackWithLike:(id)sender;
-(IBAction)feedbackWithDislike:(id)sender;

@end
