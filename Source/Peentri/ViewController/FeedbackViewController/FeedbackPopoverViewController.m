//
//  FeedbackViewController.m
//  Eatus
//
//  Created by Phong Nguyen on 10/29/15.
//  Copyright © 2015 Triad Fox. All rights reserved.
//

#import "FeedbackPopoverViewController.h"
#import "UIButton+EdgeInset.h"

@interface FeedbackPopoverViewController ()

@end

@implementation FeedbackPopoverViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [likeButton setBackgroundColor:[Util colorWithHexString:MAIN_THEME_COLOR_LIGHT_LV_3]];
    [dislikeButton setBackgroundColor:[Util colorWithHexString:MAIN_THEME_COLOR_LIGHT_LV_3]];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

-(void)feedbackWithLike:(id)sender{
    UIViewController *delegate = (UIViewController *)self.delegate;
    [_Shared showOverlayIndetermineProgressViewWithTitle:@"" tintColor:COLOR_MAIN_THEME onView:delegate.view];
    
    [ParseDataManager getRestaurantByMajorId:self.storeMajorId onCompletion:^(Restaurant *restaurant, NSError *error){
        if(!error){
            [ServiceAPIManager createFeedbackAtRestaurant:restaurant isHappy:YES comment:@"" onCompletion:^(BOOL succeeded, NSError *error){
                [_Shared hideOverlayIndetermineProgressViewAfter:0.0f];
                [self.delegate didFeedbackWithResult:succeeded];
            }];
        }
    }];
}

-(void)feedbackWithDislike:(id)sender{
    UIViewController *delegate = (UIViewController *)self.delegate;
    [_Shared showOverlayIndetermineProgressViewWithTitle:@"" tintColor:COLOR_MAIN_THEME onView:delegate.view];
    
    [ParseDataManager getRestaurantByMajorId:self.storeMajorId onCompletion:^(Restaurant *restaurant, NSError *error){
        if(!error){
            [ServiceAPIManager createFeedbackAtRestaurant:restaurant isHappy:NO comment:@"" onCompletion:^(BOOL succeeded, NSError *error){
                [_Shared hideOverlayIndetermineProgressViewAfter:0.0f];
                [self.delegate didFeedbackWithResult:succeeded];
            }];
        }
    }];
}

@end
