//
//  ProfileAvatarTableViewCell.m
//  Eatus
//
//  Created by Phong Nguyen on 11/7/15.
//  Copyright © 2015 Triad Fox. All rights reserved.
//

#import "ProfileAvatarTableViewCell.h"

#import "CDFInitialsAvatar.h"

@implementation ProfileAvatarTableViewCell
@synthesize profileAvatarImageView;

- (void)awakeFromNib {
    [Util roundViewAsCircle:profileAvatarImageView borderColor:COLOR_MAIN_THEME borderWith:5.0f];
    
    [profileAvatarImageView addGestureRecognizer:[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(didTapOnAvatarOnCell:)]];
    
    [self settingUpInitialAvatarWithName:@"Eat Tus"];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
    
}

-(void)presentLoginAnymousTooltipInView:(UIView *)view{
    CGPoint centerImageViewBox = CGPointMake(profileAvatarImageView.center.x, profileAvatarImageView.frame.size.width/2);
    CGSize screensSize = [UIScreen mainScreen].bounds.size;
    
    JDFTooltipView *toolTip = [[JDFTooltipView alloc] initWithTargetPoint:centerImageViewBox hostView:view?view:self tooltipText:NSLocalizedString(@"com.eatus.profile.toolTip.anonymous.login", @"AnonymousAuth") arrowDirection:JDFTooltipViewArrowDirectionUp width:screensSize.width - 32];
    [toolTip show];
}


#pragma mark - Utilities
-(void)prepareAvartarWithUser: (User *)user{
    [ParseDataManager getAvatarFromUser:user onCompletion:^(UIImage *avatar, NSError *error){
        if(avatar){
            [self.profileAvatarImageView setImage:avatar];
        }else{
            [self settingUpInitialAvatarWithName:user.fullname];
        }
    }];
}

-(void)settingUpInitialAvatarWithName:(NSString *)name{
    [Util roundViewAsCircle:profileAvatarImageView borderColor:[Util colorWithHexString:MAIN_THEME_COLOR_LIGHT_LV_2] borderWith:0.3f];
    CDFInitialsAvatar *avatar = [[CDFInitialsAvatar alloc] initWithRect:self.profileAvatarImageView.bounds fullName:name];
    [avatar setBackgroundColor:[Util colorWithHexString: MAIN_SCROLLING_COLOR_SILVER]];
    self.profileAvatarImageView.image = avatar.imageRepresentation;
}


#pragma mark - Delegate callback
-(void)didTapOnAvatarOnCell: (id)sender{
    UIImageView *avatar = (UIImageView *)sender;
    [self.delegate didTapOnAvatarImageEvent:avatar];
}

@end
