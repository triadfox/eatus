//
//  ProfileInformationTableViewCell.h
//  Light Order
//
//  Created by Phong Nguyen on 10/21/15.
//  Copyright © 2015 Triad Fox. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ProfileInformationTableViewCell : UITableViewCell

@property(nonatomic, weak) IBOutlet UIView *textFieldPlaceHolder;
@property(nonatomic, weak) IBOutlet UIView *horizontalSeparator;

-(void)prepareFloatLabelTextFieldWithPlaceHolder: (NSString *)placeHolder content: (NSString *)content enabled: (BOOL)enable;

@end
