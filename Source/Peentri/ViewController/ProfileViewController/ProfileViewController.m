//
//  ProfileViewController.m
//  Light House
//
//  Created by Phong Nguyen on 9/19/15.
//  Copyright © 2015 Triad Fox. All rights reserved.
//

#import <MobileCoreServices/MobileCoreServices.h>
#import <AssetsLibrary/AssetsLibrary.h>
#import <ParseFacebookUtilsV4/ParseFacebookUtilsV4.h>
#import <QuartzCore/QuartzCore.h>
#import <FBSDKCoreKit/FBSDKCoreKit.h>
#import <Photos/PHPhotoLibrary.h>
#import <AVFoundation/AVFoundation.h>

#import "RootViewController.h"
#import "AppDelegate.h"

#import "ProfileViewController.h"
#import "ProfileInformationTableViewCell.h"
#import "ProfileFacebookTableViewCell.h"
#import "ProfileAvatarTableViewCell.h"
#import "ProfileLogOutTableViewCell.h"

#import "MainOrderViewController.h"

#import "CDFInitialsAvatar.h"

#define HEADER_SECTION_HEIGH_DEFAULT                16.0f
#define HEADER_SECTION_FACEBOOK_CONNECT             60.0f
#define HEADER_SECTION_LOGOUT                       50.0f

#define ROW_HEIGHT_AUTHENTICATE_CELL                200.0f
#define ROW_HEIGH_AVATAR_CELL                       150.0f
#define ROW_HEIGH_LOGOUT_CELL                       90.0f
#define ROW_HEIGHT_DEFAULT_CELL                     44.0

#define INFORMATION_FULL_NAME                       @"Full Name"
#define INFORMATION_DATE_OF_BIRTH                   @"Year Of Birth"
#define INFORMATION_FACEBOOK_ID                     @"Facebook ID"

@interface ProfileViewController ()<ProfileFacebookCellProtocols, ProfileAvatarCellProtocols, ProfileLogoutProtocols>{
    User *_user;
}

@property(nonatomic, weak) ProfileInformationTableViewCell *informationCell;
@property(nonatomic, weak) ProfileFacebookTableViewCell *facebookConnectCell;
@property(nonatomic, strong) ProfileAvatarTableViewCell *avatarCell;
@property(nonatomic, weak) ProfileLogOutTableViewCell *logoutCell;

@end

@implementation ProfileViewController
@synthesize informationCell, facebookConnectCell, avatarCell, logoutCell;

- (void)viewDidLoad {
    [super viewDidLoad];
    
    _user = [User currentUser];
    
    [self settingUpInterface];
}

-(void)prepareNavBar{
    [self prepareSideBarMenu];
    [self setNavCenterTitle:NSLocalizedString(@"com.eatus.nav.center.title.profile", @"Profile")];
}

-(void)settingUpInterface{
    [self prepareNavBar];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}


#pragma mark - Table View Delegate methods
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 6;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return 1;
}

-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    if(section == 5){
        return HEADER_SECTION_FACEBOOK_CONNECT;
    }else if(section == 6){
        return HEADER_SECTION_LOGOUT;
    }
    return HEADER_SECTION_HEIGH_DEFAULT;
}

-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    UIView *headerView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, tableView.bounds.size.width, HEADER_SECTION_HEIGH_DEFAULT)];
    [headerView setBackgroundColor:[UIColor clearColor]];
    return headerView;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    if(indexPath.section == 0 && indexPath.row == 0){
        return ROW_HEIGH_AVATAR_CELL;
    }else if(indexPath.section == 5 && indexPath.row == 0){
        return ROW_HEIGH_LOGOUT_CELL;
    }
    return ROW_HEIGHT_DEFAULT_CELL;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    UITableViewCell *cell;
    static NSString *identifier = @"ProfileInformationCellIdentifier";
    informationCell = [tableView dequeueReusableCellWithIdentifier:identifier];
    
    if(!informationCell){
        NSArray *nibArray = [[NSBundle mainBundle] loadNibNamed:@"ProfileInformationViewCell" owner:self options:nil];
        informationCell = [nibArray firstObject];
    }
    
    if(indexPath.section == 0){
        avatarCell = [[[NSBundle mainBundle] loadNibNamed:@"ProfileAvatarViewCell" owner:self options:nil] firstObject];
        avatarCell.delegate = self;
        [avatarCell prepareAvartarWithUser:_user];
        
        if(isNilOrEmpty(_user.facebookId)){
            dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(3.0 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                [avatarCell presentLoginAnymousTooltipInView:nil];
            });
        }
        cell = avatarCell;
    }else if(indexPath.section == 1){
        [informationCell prepareFloatLabelTextFieldWithPlaceHolder:INFORMATION_FULL_NAME content:_user.fullname enabled:NO];
        cell = informationCell;
    }else if(indexPath.section == 2){
        [informationCell prepareFloatLabelTextFieldWithPlaceHolder:INFORMATION_DATE_OF_BIRTH content:[_user.yearOfBirth stringValue] enabled:NO];
        cell = informationCell;
    }else if(indexPath.section == 3){
        [informationCell prepareFloatLabelTextFieldWithPlaceHolder:INFORMATION_FACEBOOK_ID content:_user.facebookId enabled:NO];
        cell = informationCell;
    }else if(indexPath.section == 4) {
        facebookConnectCell = [[[NSBundle mainBundle] loadNibNamed:@"ProfileFacebookViewCell" owner:self options:nil] firstObject];
        if(!isNilOrEmpty(_user.facebookId)){
            [facebookConnectCell.profileConnectFacebook setTitle:NSLocalizedString(@"com.eatus.profile.facebook.connected", @"FBConnected") forState:UIControlStateNormal];
            facebookConnectCell.profileConnectFacebook.enabled = NO;
        }
        cell = facebookConnectCell;
        facebookConnectCell.delegate = self;
    }else if(indexPath.section == 5){
        //last cell
        logoutCell = [[[NSBundle mainBundle] loadNibNamed:@"ProfileLogOutViewCell" owner:self options:nil] firstObject];
        logoutCell.delegate = self;
        cell = logoutCell;
    }
    
    return cell;
}


#pragma mark - Profile Log Out Cell Delegate methods
-(void)didTapOnLogOut{
    UIAlertController *actionSheetController = [UIAlertController alertControllerWithTitle:NSLocalizedString(@"com.eatus.alert.profile.signOut.title", @"SignOutTitle") message:NSLocalizedString(@"com.eatus.alert.profile.signOut.content", @"SignOutContent") preferredStyle:UIAlertControllerStyleActionSheet];
    [actionSheetController addAction:[UIAlertAction actionWithTitle:NSLocalizedString(@"com.eatus.alert.profile.signOut.title", @"SignOut") style:UIAlertActionStyleDestructive handler:^(UIAlertAction *action){
        
        [_Shared showOverlayIndetermineSmalProgressViewWithTitle:NSLocalizedString(@"com.eatus.notification.profile.signingOut.title", @"Signing Out") style:kOverlayElegantStyle tintColor:COLOR_MAIN_THEME onView:self.view];
        
        [ParseDataManager logoutWithOnCompletion:^(BOOL succeeded, NSError *error){
            [_NotificationCenter postNotificationName:kNotificationSignOut object:nil];
            [_Shared hideOverlayIndetermineProgressViewAfter:0.5f];
            
            [self dismissViewControllerAnimated:YES completion:^{
            }];
            
            [_AppDelegate handleLogoutFromApp];
        }];
    }]];
    
    [actionSheetController addAction:[UIAlertAction actionWithTitle:NSLocalizedString(@"com.eatus.alert.profile.cancel.title", @"Cancel") style:UIAlertActionStyleCancel handler:^(UIAlertAction *action){
        [actionSheetController dismissViewControllerAnimated:YES completion:nil];
    }]];
    
    [self presentViewController:actionSheetController];
}


#pragma mark - Profile Avatar Cell Delegate methods
-(void)didTapOnAvatarImageEvent:(UIImageView *)currentAvatar{
    [self presentActionSheetOnProfileImageViewTap];
}


#pragma mark - Profile Facebook Cell Delegate methods
-(void)didTapOnConnectFacebook{
    [_Shared showOverlayIndetermineSmalProgressViewWithTitle:NSLocalizedString(@"com.eatus.hud.process", @"Processing") style:kOverlayElegantStyle tintColor:COLOR_MAIN_THEME onView:self.view];
    
    __block AMSmoothAlertView *alert;
    
    [PFFacebookUtils linkUserInBackground:[User currentUser] withReadPermissions:kFacebookPermissions block:^(BOOL succeeded, NSError *error){
        if(succeeded){
            [_Shared getFacebookProfile:^(BOOL succeeded, NSError *error){
                if(succeeded){
                    [_Shared getFacebookAvatar:^(BOOL succeed, NSError *error){
                        [[User currentUser] saveInBackgroundWithBlock:^(BOOL succeeded, NSError *error){
                            if(succeeded){
                                [_Shared hideOverlayIndetermineProgressViewAfter:0.3f];
                                [self.profileOptionTableView reloadData];
                                
                                [[NSNotificationCenter defaultCenter] postNotificationName:kNotificationChangeProfile object:nil];
                            }else{
                                 alert = [AlertViewManager smoothDropAlertViewWithTitle:NSLocalizedString(@"com.eatus.alert.profile.social.title", @"SocialConnect") content:[NSString stringWithFormat:@"%@", error.localizedDescription] cancelButton:NO color:COLOR_MAIN_THEME alertType:AlertFailure onCompletion:^(AMSmoothAlertView *alert, UIButton *button){
                                    
                                }];
                                [alert.logoView setImage:[UIImage imageNamed:@"facebook Color"]];
                                
                                [alert show];
                            }
                        }];
                    }];
                }else{
                    alert = [AlertViewManager smoothDropAlertViewWithTitle:NSLocalizedString(@"com.eatus.alert.profile.social.title", @"SocialConnect") content:[NSString stringWithFormat:@"%@", error.localizedDescription] cancelButton:NO color:COLOR_MAIN_THEME alertType:AlertFailure onCompletion:^(AMSmoothAlertView *alert, UIButton *button){
                        
                    }];
                    [alert.logoView setImage:[UIImage imageNamed:@"facebook Color"]];
                    
                    [alert show];
                }
            }];
        }else{
            [_Shared hideOverlayIndetermineProgressViewAfter:0.3];
            if(error){
                alert = [AlertViewManager smoothDropAlertViewWithTitle:NSLocalizedString(@"com.eatus.alert.profile.social.title", @"SocialConnect") content:[NSString stringWithFormat:@"%@", error.localizedDescription] cancelButton:NO color:COLOR_MAIN_THEME alertType:AlertInfo onCompletion:^(AMSmoothAlertView *alertView, UIButton *button){
                    
                }];
                [alert.logoView setImage:[UIImage imageNamed:@"facebook Color"]];
                
                [alert show];
            }
        }
    }];
}


#pragma mark - Profile Image View Handler
-(void)presentActionSheetOnProfileImageViewTap{
    UIAlertController *actionSheetAlert = [UIAlertController alertControllerWithTitle:NSLocalizedString(@"com.eatus.alert.profile.avatar.title", @"Avatar") message:@"" preferredStyle:UIAlertControllerStyleActionSheet];
    
    [actionSheetAlert addAction:[UIAlertAction actionWithTitle:NSLocalizedString(@"com.eatus.alert.profile.avatar.takePhoto", @"TakePhoto") style:UIAlertActionStyleDefault handler:^(UIAlertAction *action){
        [self showCamera];
    }]];
    
    [actionSheetAlert addAction:[UIAlertAction actionWithTitle:NSLocalizedString(@"com.eatus.alert.profile.avatar.photoLib", @"ChooseFromPhotoLib") style:UIAlertActionStyleDefault handler:^(UIAlertAction *action){
        [self showImageLibraryPicker];
    }]];
    
    [actionSheetAlert addAction:[UIAlertAction actionWithTitle:NSLocalizedString(@"com.eatus.alert.profile.cancel.title", @"Cancel") style:UIAlertActionStyleCancel handler:^(UIAlertAction *action){
        
    }]];
    
    [self presentViewController:actionSheetAlert];
}

//image picker controller
-(void)showImageLibraryPicker{
    UIImagePickerController *imagePickerController = [[UIImagePickerController alloc] init];
    
    if([self determinePermissionToAccessPhotoLibrary]){
        imagePickerController.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
        imagePickerController.mediaTypes = @[ (NSString *)kUTTypeImage ];
        imagePickerController.delegate = self;
        imagePickerController.allowsEditing = YES;
        [self presentViewController:imagePickerController];
    }
}

-(void)showCamera{
    UIImagePickerController *cameraController = [[UIImagePickerController alloc] init];
    if ([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera]){
        if([self determinePermissionToAccessCameraRoll]){
            cameraController.modalPresentationStyle = UIModalPresentationFullScreen;
            cameraController.sourceType = UIImagePickerControllerSourceTypeCamera;
            cameraController.cameraDevice = UIImagePickerControllerCameraDeviceFront;
            cameraController.delegate = self;
            
            dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.1f * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                [self presentViewController:cameraController animated:YES completion:^{
                    
                }];
            });
        }
    }
}


#pragma mark - Image Picker Controller Delegate method
-(void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary<NSString *,id> *)info{
    [self saveAvatarInBackgroundWithImage:[info objectForKey:UIImagePickerControllerOriginalImage]];
    [picker dismissViewControllerAnimated:YES completion:nil];
}

-(void)imagePickerControllerDidCancel:(UIImagePickerController *)picker{
    [picker dismissViewControllerAnimated:YES completion:nil];
}


#pragma mark - Utilities
-(void)saveAvatarInBackgroundWithImage: (UIImage *)image{
    [_Shared showOverlayIndetermineSmalProgressViewWithTitle:NSLocalizedString(@"com.eatus.notification.profile.saving.title", @"Saving") style:kOverlayElegantStyle tintColor:COLOR_MAIN_THEME onView:self.view];
    
    [ParseDataManager updateAvatar:image onCompletion:^(BOOL succeeded, NSError *error){
        if(error){
            AMSmoothAlertView *alertView = [AlertViewManager smoothDropAlertViewWithTitle:@"" content:NSLocalizedString(@"com.eatus.alert.profile.saving.error.content", @"ErrorChangePhoto") cancelButton:NO color:COLOR_MAIN_THEME alertType:AlertInfo onCompletion:^(AMSmoothAlertView *alertView, UIButton *button){
                
            }];
            [alertView show];
        }else{
            [self reloadOnSectionAvatar];
        }
    }];
}

-(void)reloadOnSectionAvatar{
    NSRange range = NSMakeRange(0, 1);
    NSIndexSet *section = [NSIndexSet indexSetWithIndexesInRange:range];
    [self.profileOptionTableView reloadSections:section withRowAnimation:UITableViewRowAnimationFade];
}


#pragma mark - Permission For Peripherals: Camera, Photo Library
- (BOOL)determinePermissionToAccessPhotoLibrary {
    PHAuthorizationStatus status = [PHPhotoLibrary authorizationStatus];
    switch (status) {
        case PHAuthorizationStatusAuthorized:
            return YES;
            break;
            
        case PHAuthorizationStatusDenied:{
            AMSmoothAlertView *alertView = [AlertViewManager smoothDropAlertViewWithTitle:NSLocalizedString(@"com.eatus.alert.profile.photoLib.title", @"PhotoLib") content:NSLocalizedString(@"com.eatus.alert.profile.photoLib.content", @"PhotoLibPermission") cancelButton:YES color:COLOR_MAIN_THEME alertType:AlertInfo onCompletion:^(AMSmoothAlertView *alert, UIButton *button){
                if([button isEqual:alert.defaultButton]){
                    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:PREFS_APP_PERMISSION]];
                }
            }];
            [alertView.logoView setImage:[UIImage imageNamed:@"PhotoLib"]];
            [alertView.defaultButton setTitle:NSLocalizedString(@"com.eatus.alert.positive.button.title", @"") forState:UIControlStateNormal];
            
            [alertView show];
            return NO;
        }
            break;
            
        case PHAuthorizationStatusNotDetermined:{
            return NO;
        }
            break;
            
        case PHAuthorizationStatusRestricted:{
            return NO;
        }
            break;
            
        default:
            break;
    }
}

-(BOOL)determinePermissionToAccessCameraRoll{
    AVAuthorizationStatus status = [AVCaptureDevice authorizationStatusForMediaType:AVMediaTypeVideo];
    switch (status) {
        case AVAuthorizationStatusAuthorized:
            return YES;
            break;
            
        case AVAuthorizationStatusDenied:{
            AMSmoothAlertView *alertView = [AlertViewManager smoothDropAlertViewWithTitle:NSLocalizedString(@"com.eatus.alert.profile.camera.error.title", @"Camera") content:NSLocalizedString(@"com.eatus.alert.profile.camera.permission.error.content", @"CameraPermission") cancelButton:YES color:COLOR_MAIN_THEME alertType:AlertInfo onCompletion:^(AMSmoothAlertView *alertView, UIButton *button){
                if([button isEqual:alertView.defaultButton]){
                    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:PREFS_APP_PERMISSION]];
                }
            }];
            [alertView.defaultButton setTitle:NSLocalizedString(@"com.eatus.alert.positive.button.title", @"Settings") forState:UIControlStateNormal];
            [alertView.logoView setImage:[UIImage imageNamed:@"Camera"]];
            
            [alertView show];
            return NO;
        }
            break;
            
        case AVAuthorizationStatusNotDetermined:
            return NO;
            break;
            
        case AVAuthorizationStatusRestricted:
            return NO;
            break;
            
        default:
            break;
    }
}


@end
