//
//  ProfileLogOutTableViewCell.h
//  Eatus
//
//  Created by Phong Nguyen on 11/7/15.
//  Copyright © 2015 Triad Fox. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol ProfileLogoutProtocols <NSObject>

@required
-(void)didTapOnLogOut;

@end

@interface ProfileLogOutTableViewCell : UITableViewCell

@property(nonatomic, strong) IBOutlet UIButton *profileLogoutButton;
@property(nonatomic, strong) IBOutlet UILabel *profileLegalInformationLabel;

@property(nonatomic, strong) id<ProfileLogoutProtocols> delegate;

-(IBAction)didTapOnLogOutButton:(id)sender;

@end
