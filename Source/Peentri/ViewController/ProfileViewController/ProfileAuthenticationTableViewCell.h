//
//  ProfileAuthenticationTableViewCell.h
//  Eatus
//
//  Created by Phong Nguyen on 11/7/15.
//  Copyright © 2015 Triad Fox. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol ProfileAuthenticatorCellProtocols <NSObject>

@required
-(void)didTapOnSetPassCode;
-(void)didTapOnSetPassCodeChange;

@end

@interface ProfileAuthenticationTableViewCell : UITableViewCell

@property(nonatomic, strong) IBOutlet UILabel *profileAuthenticateGuide;
@property(nonatomic, strong) IBOutlet UIButton *profileSecurityTouchID;
@property(nonatomic, strong) UIViewController *parent;

@end
