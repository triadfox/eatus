//
//  ProfileAuthenticationTableViewCell.m
//  Eatus
//
//  Created by Phong Nguyen on 11/7/15.
//  Copyright © 2015 Triad Fox. All rights reserved.
//

#import "ProfileAuthenticationTableViewCell.h"
#import "UIButton+EdgeInset.h"
#import "EHFAuthenticator.h"

@interface ProfileAuthenticationTableViewCell()

@end

@implementation ProfileAuthenticationTableViewCell
@synthesize profileSecurityTouchID;

- (void)awakeFromNib {
    [profileSecurityTouchID addTarget:self action:@selector(authorizeTouchID) forControlEvents:UIControlEventTouchUpInside];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
}

- (void)authorizeTouchID{
    NSError *error = nil;
    BOOL touchIdAvailable =  [EHFAuthenticator canAuthenticateWithError:&error];
    
    if(touchIdAvailable){
        [[EHFAuthenticator sharedInstance] setReason:NSLocalizedString(@"com.eatus.alert.auth.touchId.reason.content", @"TouchIdAuth")];
        [[EHFAuthenticator sharedInstance] authenticateWithSuccess:^{
            [_Shared showSucceedCheckMarkProcessViewWithTitle:@"" tintColor:COLOR_MAIN_THEME_SUCCESS_GREEN onView:self.parent.view];
        }andFailure:^(LAError error){
            [_Shared showFailureCrossProcessViewWithTitle:@"" tintColor:COLOR_MAIN_THEME_FAILURE_RED onView:self.parent.view];
        }];
    }else{
        [_Shared showFailureCrossProcessViewWithTitle:@"" tintColor:COLOR_MAIN_THEME onView:self.parent.view];
    }
}
@end
