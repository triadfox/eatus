//
//  ProfileAvatarTableViewCell.h
//  Eatus
//
//  Created by Phong Nguyen on 11/7/15.
//  Copyright © 2015 Triad Fox. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "JDFTooltips.h"

@protocol ProfileAvatarCellProtocols <NSObject>

@required
-(void)didTapOnAvatarImageEvent: (UIImageView *)currentAvatar;

@end

@interface ProfileAvatarTableViewCell : UITableViewCell

@property(nonatomic, strong) IBOutlet UIImageView *profileAvatarImageView;
@property(nonatomic, strong) id<ProfileAvatarCellProtocols> delegate;

-(void)prepareAvartarWithUser: (User *)user;
-(void)presentLoginAnymousTooltipInView: (UIView *)view;

@end
