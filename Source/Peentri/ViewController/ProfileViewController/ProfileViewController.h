//
//  ProfileViewController.h
//  Light House
//
//  Created by Phong Nguyen on 9/19/15.
//  Copyright © 2015 Triad Fox. All rights reserved.
//

#import "BaseViewController.h"
#import "User.h"

@interface ProfileViewController : BaseViewController<UINavigationControllerDelegate,UIImagePickerControllerDelegate, UITableViewDataSource, UITableViewDelegate>

@property(nonatomic, strong) IBOutlet UITableView *profileOptionTableView;

@end
