//
//  ProfileLogOutTableViewCell.m
//  Eatus
//
//  Created by Phong Nguyen on 11/7/15.
//  Copyright © 2015 Triad Fox. All rights reserved.
//

#import "ProfileLogOutTableViewCell.h"
#import "UIButton+EdgeInset.h"

@implementation ProfileLogOutTableViewCell
@synthesize profileLegalInformationLabel;
@synthesize profileLogoutButton;

- (void)awakeFromNib {
    [profileLogoutButton setBackgroundColor:COLOR_MAIN_THEME];
    [profileLogoutButton setTitle:NSLocalizedString(@"com.eatus.alert.profile.signOut.title", @"Signout") forState:UIControlStateNormal];
    [profileLogoutButton spacingBetweenImageAndTitleWith:5.0f];
    
    [profileLegalInformationLabel setTextColor:COLOR_MAIN_THEME];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
    
}

- (void)didTapOnLogOutButton:(id)sender{
    [self.delegate didTapOnLogOut];
}

@end
