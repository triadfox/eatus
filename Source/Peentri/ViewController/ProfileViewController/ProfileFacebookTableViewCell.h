//
//  ProfileFacebookLoginTableViewCell.h
//  Eatus
//
//  Created by Phong Nguyen on 11/5/15.
//  Copyright © 2015 Triad Fox. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol ProfileFacebookCellProtocols <NSObject>

@required
-(void)didTapOnConnectFacebook;
@end

@interface ProfileFacebookTableViewCell : UITableViewCell

@property(nonatomic, strong) IBOutlet UIButton *profileConnectFacebook;

@property(nonatomic, strong) id<ProfileFacebookCellProtocols> delegate;

-(IBAction)didTapConnectFacebookButton:(id)sender;

@end
