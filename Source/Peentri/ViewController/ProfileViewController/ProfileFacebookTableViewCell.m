//
//  ProfileFacebookLoginTableViewCell.m
//  Eatus
//
//  Created by Phong Nguyen on 11/5/15.
//  Copyright © 2015 Triad Fox. All rights reserved.
//

#import "ProfileFacebookTableViewCell.h"

@implementation ProfileFacebookTableViewCell
@synthesize profileConnectFacebook;

- (void)awakeFromNib {
    [profileConnectFacebook setTitleColor:[Util colorWithHexString:MAIN_FACEBOOK_PALETTE] forState:UIControlStateNormal];
    profileConnectFacebook.layer.borderColor = [Util colorWithHexString:MAIN_FACEBOOK_PALETTE].CGColor;
    profileConnectFacebook.layer.borderWidth = 1.0f;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
}

-(void)didTapConnectFacebookButton:(id)sender{
    [self.delegate didTapOnConnectFacebook];
}
@end
