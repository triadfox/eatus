//
//  ProfileInformationTableViewCell.m
//  Light Order
//
//  Created by Phong Nguyen on 10/21/15.
//  Copyright © 2015 Triad Fox. All rights reserved.
//

#import "ProfileInformationTableViewCell.h"
#import "JVFloatLabeledTextField.h"
#import "JVFloatLabeledTextView.h"

const static CGFloat kJVFieldFontSize = 16.0f;

const static CGFloat kJVFieldFloatingLabelFontSize = 11.0f;

@interface ProfileInformationTableViewCell()

@property(nonatomic, strong) JVFloatLabeledTextField *profileTextField;

@end

@implementation ProfileInformationTableViewCell
@synthesize profileTextField;

- (void)awakeFromNib {
    
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

}

-(void)prepareFloatLabelTextFieldWithPlaceHolder: (NSString *)placeHolder content: (NSString *)content enabled:(BOOL)enable{
    UIColor *floatingLabelColor = [Util colorWithHexString: MAIN_THEME_COLOR];
    
    profileTextField = [[JVFloatLabeledTextField alloc] initWithFrame:self.textFieldPlaceHolder.bounds];
    profileTextField.font = [UIFont systemFontOfSize:kJVFieldFontSize];
    
    profileTextField.text = content;
    profileTextField.attributedPlaceholder = [[NSAttributedString alloc] initWithString:placeHolder
                                    attributes:@{NSForegroundColorAttributeName: [UIColor darkGrayColor]}];
    profileTextField.floatingLabelFont = [UIFont boldSystemFontOfSize:kJVFieldFloatingLabelFontSize];
    profileTextField.floatingLabelTextColor = floatingLabelColor;
    [profileTextField setFloatingLabelActiveTextColor:[Util colorWithHexString:MAIN_THEME_COLOR]];
    profileTextField.clearButtonMode = UITextFieldViewModeWhileEditing;
    profileTextField.translatesAutoresizingMaskIntoConstraints = YES;
    profileTextField.keepBaseline = YES;
    
    [self.textFieldPlaceHolder addSubview:profileTextField];
    
    if(enable){
        profileTextField.enabled = YES;
    }else{
        profileTextField.userInteractionEnabled = NO;
    }
}

@end
