//
//  ReceiptViewController.m
//  Light Order
//
//  Created by Phong Nguyen on 10/18/15.
//  Copyright © 2015 Triad Fox. All rights reserved.
//

#import "ReceiptViewController.h"
#import "ReceiptTableViewCell.h"
#import "NoDataTableViewCell.h"
#import "HeaderSectionViewController.h"
#import "OrderPlacingViewController.h"

#import "YALSunnyRefreshControl.h"
#import "MenuJsonSerializer.h"
#import "MenuJsonSerializerConstant.h"

#define SECTION_HEIGH_DEFAULT                   8
#define SECTION_HEIGHT_RECEIPT                  16

@interface ReceiptViewController (){
    NSArray *allReceipts;
    
    BOOL _isRefreshAnimating;
    BOOL didLoadReceipts;
    BOOL notFirstTime;
}

@property(nonatomic, strong) NSMutableArray *tableViewSections;
@property(nonatomic, strong) NSMutableDictionary *tableViewCells;
@property (nonatomic, strong) YALSunnyRefreshControl *refreshController;

@end

@implementation ReceiptViewController
@synthesize tableViewCells;
@synthesize tableViewSections;

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self setNavCenterTitle:NSLocalizedString(@"com.eatus.nav.center.title.receipt", @"RECEITPS")];
    
    [self prepareSideBarMenu];
    
    [self prepareRefreshController];
}

-(void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    
    if(!didLoadReceipts){
        [self prepareReceiptDataFromParse];
        didLoadReceipts = YES;
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

-(void)prepareRefreshController{
    self.refreshController = [[YALSunnyRefreshControl alloc] init];
    [self.refreshController addTarget:self action:@selector(startRefreshing) forControlEvents:UIControlEventValueChanged];
    [self.refreshController attachToScrollView:self.receiptTableView];
}

-(void)startRefreshing{
    //start to refresh data
    _isRefreshAnimating = YES;
    [self prepareReceiptDataFromParse];
}

-(void)stopRefreshing{
    [self.refreshController endRefreshing];
}

-(void)sortOrderGroupedByDate: (NSArray *)orders{
    tableViewCells = [[NSMutableDictionary alloc] initWithCapacity:0];
    tableViewSections = [[NSMutableArray alloc] initWithCapacity:0];
    
    NSCalendar *calendar = [NSCalendar currentCalendar];
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    dateFormatter.locale = [NSLocale currentLocale];
    dateFormatter.timeZone = calendar.timeZone;
    [dateFormatter setDateStyle:NSDateFormatterFullStyle];
    [dateFormatter setDateFormat:[NSDateFormatter dateFormatFromTemplate:@"EEEE MMMM dd yyyy" options:0 locale:[NSLocale currentLocale]]];
    
    NSUInteger dateComponents = NSCalendarUnitYear | NSCalendarUnitMonth | NSCalendarUnitDay;
    NSInteger previousYear = -1;
    NSInteger previousMonth = -1;
    NSInteger previousDay = -1;
    
    NSMutableArray *tableViewCellForSection;
    
    for(Order *order in orders) {
        NSDate *orderDate = order.createdAt;
        NSDateComponents *components = [calendar components:dateComponents fromDate:orderDate];
        NSInteger year = [components year];
        NSInteger month = [components month];
        NSInteger day = [components day];
        
        if(year != previousYear || month != previousMonth || day != previousDay){
            NSString *sectionHeading = [dateFormatter stringFromDate:orderDate];
            [tableViewSections addObject:sectionHeading];
            tableViewCellForSection = [NSMutableArray arrayWithCapacity:0];
            [tableViewCells setObject:tableViewCellForSection forKey:sectionHeading];
            previousYear = year;
            previousMonth = month;
            previousDay = day;
        }
        [tableViewCellForSection addObject:order];
    }
}

-(NSArray *)sortAllReceiptByDate: (NSArray *)receipts{
    NSSortDescriptor *dateDescriptor = [NSSortDescriptor
                                        sortDescriptorWithKey:@"createdAt"
                                        ascending:NO];
    return [receipts sortedArrayUsingDescriptors:@[dateDescriptor]];
}

-(void)prepareReceiptDataFromParse{
    if(!notFirstTime){
        [_Shared showOverlayIndetermineSmalProgressViewWithTitle:NSLocalizedString(@"com.eatus.hud.load", @"Loading") style:kOverlayElegantStyle tintColor:COLOR_MAIN_THEME onView:self.view];
    }
    
    [ParseDataManager getAllOrderOfCurrentUserOnCompletion:^(NSArray *receipts, NSError *error){
        if(!error){
            allReceipts = [self sortAllReceiptByDate:receipts];
            
            [self sortOrderGroupedByDate:allReceipts];
            
            self.receiptTableView.delegate = self;
            self.receiptTableView.dataSource = self;
            
            if(!notFirstTime){
                [_Shared hideOverlayIndetermineProgressViewAfter:0.5f];
                notFirstTime = YES;
            }else{
                [self stopRefreshing];
            }
            
            [self.receiptTableView reloadData];
        }
    }];
}

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    if([tableViewSections count] != 0){
        return [tableViewSections count];
    }else{
        return 1.0f;
    }
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    if([tableViewSections count] != 0){
        NSArray *orderForSection = [tableViewCells objectForKey:[tableViewSections objectAtIndex:section]];
        return [orderForSection count];
    }else{
        return 1.0f;
    }
}

-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    if(section == 0){
        return SECTION_HEIGH_DEFAULT;
    }else{
        return SECTION_HEIGHT_RECEIPT;
    }
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    if([tableViewSections count] == 0){
        return 80.0f;
    }
    return 64.0f;
}

-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    HeaderSectionViewController *headerViewController = [[HeaderSectionViewController alloc] initWithNibName:@"HeaderSectionView" bundle:nil];
    
    if([tableViewSections count] != 0){
        [headerViewController presentWithHeaderTitle:[tableViewSections objectAtIndex:section]];
    }
    return headerViewController.view;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    static NSString *identifier = @"ReceiptCellIdentifier";
    ReceiptTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:identifier];
    if(!cell){
        NSArray *nibArray = [[NSBundle mainBundle] loadNibNamed:@"ReceiptViewCell" owner:self options:nil];
        cell = [nibArray firstObject];
    }
    
    if([tableViewSections count] != 0){
        Order *order = [[tableViewCells objectForKey:[tableViewSections objectAtIndex:indexPath.section]] objectAtIndex:indexPath.row];
        
        NSDictionary *neededData = [MenuJsonSerializer allNeededOrderDetailGotJsonFromBackend:order.orderDetail];
        
        NSLocale *localeByCurrencyGotJson = [neededData objectForKey:KEY_RECEIPT_LOCALE];
        
        cell.receiptHour.text = [NSString stringWithFormat:@"%@", [Util dateToStringByLocale:[NSLocale currentLocale] date:order.createdAt format:@"HH:mm"]];
        cell.localeAlongCurrency = localeByCurrencyGotJson;
        
        cell.currentOrder = order;
        
        [order fetchIfNeededInBackgroundWithBlock:^(PFObject *obj, NSError *error){
            RestaurantTable *table = obj[@"restaurantTable"];
            
            [table fetchIfNeededInBackgroundWithBlock:^(PFObject *object, NSError *error){
                cell.receiptTableName = object[@"name"];
            }];
        }];
        
        NSUInteger orderStatus = order.status;
        NSString *statusBuilder;
        UIColor *statusColor;
        
        switch (orderStatus) {
            case kReceiptDelivered:{
                statusBuilder = NSLocalizedString(@"com.eatus.receipt.status.delivered", @"Delivered");
                statusColor = COLOR_MAIN_THEME_SUCCESS_GREEN;
            }
                break;
                
            case kReceiptConfirmed:{
                statusBuilder = NSLocalizedString(@"com.eatus.receipt.status.confirmed", @"Confirmed");
                statusColor = COLOR_MAIN_THEME_PENDING_BLUE;
            }
                break;
                
            case kReceiptPending:{
                statusBuilder = NSLocalizedString(@"com.eatus.receipt.status.pending", @"Pending");
                statusColor = COLOR_MAIN_THEME_DARK;
            }
                break;
                
            case kReceiptPrecanceled | kReceiptCancel:{
                statusBuilder = NSLocalizedString(@"com.eatus.receipt.status.cancelled", @"Cancelled");
                statusColor = COLOR_MAIN_THEME_FAILURE_RED;
            }
            default:
                break;
        }
        
        if(isNilOrEmpty(statusBuilder)){
            statusBuilder = NSLocalizedString(@"com.eatus.receipt.status.waiting", @"Waiting");
        }
        
        [cell.receiptStatus setTextColor:statusColor];
        cell.receiptStatus.text = [NSString stringWithFormat:@"%@", statusBuilder];
        cell.receiptSpentMoney.text = [Util stringForNumberWithLocale:localeByCurrencyGotJson number:[neededData objectForKey:KEY_TOTAL]];
        
        [order fetchIfNeededInBackgroundWithBlock:^(PFObject *orderFetch, NSError *error){
            RestaurantTable *table = orderFetch[kRestaurantTable];
            
            [table fetchIfNeededInBackgroundWithBlock:^(PFObject *table, NSError *error){
                Restaurant *restaurant = table[kRestaurant];
                
                [restaurant fetchIfNeededInBackgroundWithBlock:^(PFObject *restaurantFetch, NSError *error){
                    cell.receiptRestaurantName.text = restaurantFetch[kName];
                    
                    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_LOW, 0), ^{
                        PFFile *logoFile = restaurantFetch[kLogoImage];
                        UIImage *logoImage = [Util getImageSyncFromURL:logoFile.url];
                        
                        dispatch_async(dispatch_get_main_queue(), ^{
                            if(logoImage){
                                cell.receiptRestaurantImage.image = logoImage;
                            }
                        });
                    });
                }];
            }];
        }];
        
        return cell;
    }else{
        NoDataTableViewCell *cell = [[[NSBundle mainBundle] loadNibNamed:@"NoDataTableViewCell" owner:self options:nil] firstObject];
        cell.nodataTitle.text = NSLocalizedString(@"com.eatus.receipt.noReceipt", @"NoReceipt");
        return cell;
    }
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    ReceiptTableViewCell *cell = [tableView cellForRowAtIndexPath:indexPath];
    [cell setSelected:NO animated:YES];
    
    OrderPlacingViewController *orderPlacingViewController = [[OrderPlacingViewController alloc] initWithNibName:@"OrderPlacingView" bundle:nil mode:kOrderPlacingReceiptReview];
    [orderPlacingViewController prepareAllCalculationByReceiptDataAtTableWithOrder:cell.currentOrder locale:cell.localeAlongCurrency tableName:cell.receiptTableName];
    [self presentViewController:orderPlacingViewController];
}


#pragma mark - Utilities
-(BOOL)isLastCellOfSectionWithNumberOfCell: (NSInteger)numberOfCellInThisSection cellIndexpath: (NSIndexPath *)indexpath{
    if(indexpath.row == numberOfCellInThisSection - 1){
        return YES;
    }else{
        return NO;
    }
}
@end
