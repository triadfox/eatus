//
//  ReceiptTableViewCell.m
//  Light Order
//
//  Created by Phong Nguyen on 10/22/15.
//  Copyright © 2015 Triad Fox. All rights reserved.
//

#import "ReceiptTableViewCell.h"

@implementation ReceiptTableViewCell

- (void)awakeFromNib {
    [Util roundView:self.receiptRestaurantImage background:nil borderColor:COLOR_MAIN_THEME borderWidth:1.0f];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
}

-(void)determineReceiptStatusAndPaymentType:(OrderStatus)status{
    
}

@end
