//
//  ReceiptTableViewCell.h
//  Light Order
//
//  Created by Phong Nguyen on 10/22/15.
//  Copyright © 2015 Triad Fox. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "DishEntity.h"

@interface ReceiptTableViewCell : UITableViewCell

@property(nonatomic, weak) IBOutlet UILabel *receiptStatus;
@property(nonatomic, weak) IBOutlet UIImageView *receiptRestaurantImage;
@property(nonatomic, weak) IBOutlet UILabel *receiptRestaurantName;
@property(nonatomic, weak) IBOutlet UILabel *receiptHour;
@property(nonatomic, weak) IBOutlet UILabel *receiptSpentMoney;

@property(nonatomic, strong) NSString *receiptTableName;

@property(nonatomic, strong) Order *currentOrder;

@property(nonatomic, strong) NSLocale *localeAlongCurrency;

-(void)determineReceiptStatusAndPaymentType: (OrderStatus)status;

@end
