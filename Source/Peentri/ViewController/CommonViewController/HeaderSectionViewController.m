//
//  OrderPlacingHeaderView.m
//  Light Order
//
//  Created by Phong Nguyen on 10/12/15.
//  Copyright © 2015 Triad Fox. All rights reserved.
//

#import "HeaderSectionViewController.h"

@interface HeaderSectionViewController(){
    NSString *titleContent;
}

@end

@implementation HeaderSectionViewController

-(void)viewDidLoad{
    if(!isNilOrEmpty(titleContent)){
        [self.headerTitle setText:titleContent];
    }
}

-(void)presentWithHeaderTitle:(NSString *)title{
    titleContent = [[NSString alloc] init];
    titleContent = title;
}

@end
