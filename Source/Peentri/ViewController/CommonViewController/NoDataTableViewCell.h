//
//  NoDataTableViewCell.h
//  Eatus
//
//  Created by Phong Nguyen on 11/20/15.
//  Copyright © 2015 Triad Fox. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface NoDataTableViewCell : UITableViewCell

@property(nonatomic, weak) IBOutlet UIImageView *nodataImageView;
@property(nonatomic, weak) IBOutlet UILabel *nodataTitle;

@end
