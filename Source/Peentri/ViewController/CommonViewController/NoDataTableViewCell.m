//
//  NoDataTableViewCell.m
//  Eatus
//
//  Created by Phong Nguyen on 11/20/15.
//  Copyright © 2015 Triad Fox. All rights reserved.
//

#import "NoDataTableViewCell.h"

@implementation NoDataTableViewCell

- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
