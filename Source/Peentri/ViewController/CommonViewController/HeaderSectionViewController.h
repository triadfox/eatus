//
//  OrderPlacingHeaderView.h
//  Light Order
//
//  Created by Phong Nguyen on 10/12/15.
//  Copyright © 2015 Triad Fox. All rights reserved.
//

#import "BaseViewController.h"

@interface HeaderSectionViewController : UIViewController

@property(nonatomic, strong) IBOutlet UILabel *headerTitle;

-(void)presentWithHeaderTitle: (NSString *)title;
@end
