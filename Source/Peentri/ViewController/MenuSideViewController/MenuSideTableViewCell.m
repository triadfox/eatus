//
//  MenuSideTableViewCell.m
//  Light Order
//
//  Created by Phong Nguyen on 10/7/15.
//  Copyright © 2015 Triad Fox. All rights reserved.
//

#import "MenuSideTableViewCell.h"

@implementation MenuSideTableViewCell

- (void)awakeFromNib {
    self.selectionStyle = UITableViewCellSelectionStyleNone;
    [self setBackgroundColor:COLOR_MAIN_MENU_ITEM_BACKGROUND];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
}

@end
