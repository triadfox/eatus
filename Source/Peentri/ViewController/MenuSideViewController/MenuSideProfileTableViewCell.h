//
//  MenuSideProfileTableViewCell.h
//  Light Order
//
//  Created by Phong Nguyen on 10/19/15.
//  Copyright © 2015 Triad Fox. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MenuSideProfileTableViewCell : UITableViewCell

@property(nonatomic, weak) IBOutlet UIImageView *profileImageView;
@property(nonatomic, weak) IBOutlet UILabel *profileName;

-(void)settingUpInitialAvatarWithName: (NSString *)name;
-(void)settingUpAvatarWithImage: (UIImage *)image;

@end
