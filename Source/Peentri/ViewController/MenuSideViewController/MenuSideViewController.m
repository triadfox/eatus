//
//  MenuSideViewController.m
//  Light Order
//
//  Created by Phong Nguyen on 10/7/15.
//  Copyright © 2015 Triad Fox. All rights reserved.
//

#import "MenuSideViewController.h"
#import "MenuSideTableViewCell.h"
#import "MenuSideProfileTableViewCell.h"
#import "MainOrderViewController.h"
#import "PromotionViewController.h"
#import "ReceiptViewController.h"
#import "ProfileViewController.h"
#import "PaymentViewController.h"
#import "TouchIDManipulatorViewController.h"
#import "BeaconSurveillanceViewController.h"

#import "NavBarController.h"
#import "HeaderSectionViewController.h"
#import "User.h"

#define ROW_DEFAULT_HEIGHT                  50.0f
#define ROW_PROFILE_HEIGHT                  168.0f

#define SECTION_AVATAR_HEIGHT               32.0f
#define SECTION_DEFAULT_HEIGHT              25.0f
#define SECTION_PROFILE_HEIGHT              32.0f

@interface MenuSideViewController (){
    NSArray *arrayOfMenu;
    NSArray *arrayOfSection;
    NSArray *arrayOfSecurityItem;
    NSArray *arrayOfPrivacyItem;
    
    int indexMenu;
    
    NavBarController *mainNavController;
}

@property(nonatomic, strong) PromotionViewController *promotionViewController;
@property(nonatomic, strong) MainOrderViewController *mainOrderViewController;
@property(nonatomic, strong) ReceiptViewController *receiptViewController;
@property(nonatomic, strong) ProfileViewController *profileViewController;
@property(nonatomic, strong) PaymentViewController *paymentViewController;
@property(nonatomic, strong) TouchIDManipulatorViewController *touchIdViewController;
@property(nonatomic, strong) BeaconSurveillanceViewController *surveillanceViewController;

@property(nonatomic, strong) MenuSideProfileTableViewCell *profileAvatarCell;

@end

@implementation MenuSideViewController
@synthesize menuSideTableView;
@synthesize profileAvatarCell;

@synthesize promotionViewController, mainOrderViewController, receiptViewController, profileViewController, paymentViewController, touchIdViewController, surveillanceViewController;

- (void)viewDidLoad {
    [super viewDidLoad];
    [self prepareMenuItems];
    [self.menuSideTableView setBackgroundColor:COLOR_MAIN_MENU_SIDE_BACKGROUND];
    
    [_NotificationCenter addObserver:self selector:@selector(refreshLoggedStatus:) name:kNotificationSignOut object:nil];
    [_NotificationCenter addObserver:self selector:@selector(refreshAvatarChange:) name:kNotificationChangeProfile object:nil];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

-(void)refreshLoggedStatus: (id)sender{
    [self fetchAvatarForCurrentUser];
}

-(void)refreshAvatarChange: (id)sender{
    [self fetchAvatarForCurrentUser];
}

-(void)fetchAvatarForCurrentUser{
    User *currentUser = [User currentUser];
    if(currentUser){
        [ParseDataManager getAvatarFromUser:currentUser onCompletion:^(UIImage *image, NSError *error){
            if(image){
                [profileAvatarCell settingUpAvatarWithImage:image];
            }else{
                [profileAvatarCell settingUpInitialAvatarWithName:currentUser.fullname];
            }
        }];
    }else{
        [profileAvatarCell settingUpInitialAvatarWithName:@"Eat Tus"];
    }
}

-(void)prepareMenuItems{
    NSString *nearbyItem = NSLocalizedString(@"com.eatus.nav.center.title.nearby", @"NEARBY");
    NSString *promotsionItem = NSLocalizedString(@"com.eatus.nav.center.title.promotion", @"PROMOTIONS");
    NSString *receiptsItem = NSLocalizedString(@"com.eatus.nav.center.title.receipt", @"RECEIPTS");
    NSString *paymentItem = NSLocalizedString(@"com.eatus.nav.center.title.payment", @"PAYMENT");
    
    NSString *touchIdItem = NSLocalizedString(@"com.eatus.nav.center.title.touchIdAuth", @"TOUCHID");
    NSString *surveillanceItem = NSLocalizedString(@"com.eatus.nav.center.title.surveillance", @"SURVEILLANCE");
    
    arrayOfMenu = @[nearbyItem, promotsionItem, receiptsItem, paymentItem];
    arrayOfSection = @[@"Profile", @"Eat me", @"Security", @"Privacy"];
    arrayOfSecurityItem = @[touchIdItem];
    arrayOfPrivacyItem = @[surveillanceItem];
}

-(void)settingUpBackgroundColor: (UIColor *)color{
    [self.view setBackgroundColor:color];
}

-(void)preserveMainNavController:(UINavigationController *)navController{
    navController?mainNavController = (NavBarController*)navController:nil;
}

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return  [arrayOfSection count];
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    NSUInteger numberOfRowPerSection;
    
    if(section == 0){
        //profile section
        numberOfRowPerSection = 1;
    }else if(section == 1){
        //eat me section
        numberOfRowPerSection = [arrayOfMenu count];
    }else if(section == 2){
        //security section
        numberOfRowPerSection =  [arrayOfSecurityItem count];
    }else {
        //privacy section
        numberOfRowPerSection = [arrayOfPrivacyItem count];
    }
    return numberOfRowPerSection;
}

-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    if(section == 0){
        return SECTION_AVATAR_HEIGHT;
    }
    return SECTION_DEFAULT_HEIGHT;
}


-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    if(indexPath.section == kItemProfilePix){
        return ROW_PROFILE_HEIGHT;
    }else{
        return ROW_DEFAULT_HEIGHT;
    }
}

-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    HeaderSectionViewController *headerViewController = [[HeaderSectionViewController alloc] initWithNibName:@"HeaderSectionView" bundle:nil];
    
    if(section != 0){
        [headerViewController.view setBackgroundColor:[UIColor groupTableViewBackgroundColor]];
    }else{
        [headerViewController.view setBackgroundColor:[UIColor clearColor]];
    }
    
    [headerViewController.view setBackgroundColor:COLOR_MAIN_MENU_SIDE_BACKGROUND];
    
    return headerViewController.view;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    if(indexPath.section == 0){
        profileAvatarCell = [[[NSBundle mainBundle] loadNibNamed:@"MenuSideProfileTableViewCell" owner:self options:nil] firstObject];
        [self fetchAvatarForCurrentUser];
        return profileAvatarCell;
    }else{
        static NSString *identifier = @"MenuSideCellIdentifier";
        MenuSideTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:identifier];
        
        if(!cell){
            NSArray *nibArray = [[NSBundle mainBundle] loadNibNamed:@"MenuSideTableViewCell" owner:self options:nil];
            cell = [nibArray firstObject];
        }
        
        NSString *menuItem;
        UIImage *itemImage;
        
        if(indexPath.section == 1){
            menuItem = [arrayOfMenu objectAtIndex:indexPath.row];
            
            if(indexPath.row == 0){
                itemImage = [UIImage imageNamed:@"NearbyColor"];
            }else if(indexPath.row == 1){
                itemImage = [UIImage imageNamed:@"PromotionColor"];
            }else if(indexPath.row == 2){
                itemImage = [UIImage imageNamed:@"ReceiptColor"];
            }else if(indexPath.row == 3){
                itemImage = [UIImage imageNamed:@"PaymentColor"];
            }
            
            if([self isLastCellOfSectionWithNumberOfCell:arrayOfMenu.count cellIndexpath:indexPath]){
                cell.menuHorizontalSeparatorCell.hidden = YES;
            }
            
        }else if(indexPath.section == 2){
            menuItem = [arrayOfSecurityItem objectAtIndex:indexPath.row];
            itemImage = [UIImage imageNamed:@"TouchIDSmallColor"];
        }else if(indexPath.section == 3){
            menuItem = [arrayOfPrivacyItem objectAtIndex:indexPath.row];
            itemImage = [UIImage imageNamed:@"SurveillanceSmallColor"];
        }
        
        cell.menuImageView.image = itemImage;
        cell.menuTitle.text = menuItem;
        [cell.menuHorizontalSeparatorCell setBackgroundColor:COLOR_MAIN_MENU_SIDE_BACKGROUND];
        return cell;
    }
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    NavBarController *navPushController;
    
    switch (indexPath.section) {
        case 0:{
            profileViewController = [[ProfileViewController alloc] initWithNibName:@"ProfileView" bundle:nil];
            navPushController = [[NavBarController alloc] initWithRootViewController:profileViewController];
        }
            break;
            
        case 1:{
            if(indexPath.row == 0){
                mainOrderViewController = [[MainOrderViewController alloc] initWithNibName:@"MainOrderView" bundle:nil];
                navPushController = [[NavBarController alloc] initWithRootViewController:mainOrderViewController];
            }else if(indexPath.row == 1){
                promotionViewController = [[PromotionViewController alloc] initWithNibName:@"PromotionView" bundle:nil];
                navPushController = [[NavBarController alloc] initWithRootViewController:promotionViewController];
            }else if(indexPath.row == 2){
                receiptViewController = [[ReceiptViewController alloc] initWithNibName:@"ReceiptView" bundle:nil];
                navPushController = [[NavBarController alloc] initWithRootViewController:receiptViewController];
            }else if(indexPath.row == 3){
                paymentViewController = [[PaymentViewController alloc] initWithNibName:@"PaymentView" bundle:nil mode:kPaymentModeNormal];
                navPushController = [[NavBarController alloc] initWithRootViewController:paymentViewController];
            }
            
        }
            break;
            
        case 2:{
            if(indexPath.row == 0){
                touchIdViewController = [[TouchIDManipulatorViewController alloc] initWithNibName:@"TouchIDManipulationView" bundle:nil];
                navPushController = [[NavBarController alloc] initWithRootViewController:touchIdViewController];
            }
        }
            break;
            
        case 3:{
            if(indexPath.row == 0){
                surveillanceViewController = [[BeaconSurveillanceViewController alloc] initWithNibName:@"BeaconSurveillanceView" bundle:nil];
                navPushController = [[NavBarController alloc] initWithRootViewController:surveillanceViewController];
            }
        }
            break;
            
        default:
            break;
    }
    
    [self pushFrontViewController:navPushController];
}


#pragma mark - Utilities
-(BOOL)isLastCellOfSectionWithNumberOfCell: (NSInteger)numberOfCellInThisSection cellIndexpath: (NSIndexPath *)indexpath{
    if(indexpath.row == numberOfCellInThisSection - 1){
        return YES;
    }else{
        return NO;
    }
}

#pragma mark - Parse For Handle Current User
-(User *)determineLoggedUser{
    User *currentUser = [User currentUser];
    if(currentUser){
        //user logged
        return currentUser;
    }else{
        
    }
    return nil;
}

@end
