//
//  MenuSideTableViewCell.h
//  Light Order
//
//  Created by Phong Nguyen on 10/7/15.
//  Copyright © 2015 Triad Fox. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MenuSideTableViewCell : UITableViewCell

@property(nonatomic, weak) IBOutlet UIImageView *menuImageView;
@property(nonatomic, weak) IBOutlet UILabel *menuTitle;
@property(nonatomic, weak) IBOutlet UIView *menuHorizontalSeparatorCell;

@end
