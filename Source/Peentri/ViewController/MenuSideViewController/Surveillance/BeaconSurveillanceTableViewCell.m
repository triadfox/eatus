//
//  BeaconSurveillanceTableViewCell.m
//  Eatus
//
//  Created by Phong Nguyen on 11/10/15.
//  Copyright © 2015 Triad Fox. All rights reserved.
//

#import "BeaconSurveillanceTableViewCell.h"

@implementation BeaconSurveillanceTableViewCell
@synthesize surveillanceSwitcher, surveillanceTitle;

- (void)awakeFromNib {
    self.selectionStyle = UITableViewCellSelectionStyleNone;
    
    [self.surveillanceSwitcher addTarget:self action:@selector(switcherChanged:) forControlEvents:UIControlEventValueChanged];
    if([[_UserDefault objectForKey:kShouldSurveillanceBeacons] boolValue]){
        [surveillanceSwitcher setOn:YES animated:YES];
    }else{
        [surveillanceSwitcher setOn:NO animated:YES];
    }
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
}

#pragma mark - IBAction methods
-(void)switcherChanged:(id)sender{
    [_UserDefault setObject:[NSNumber numberWithBool:surveillanceSwitcher.isOn] forKey:kShouldSurveillanceBeacons];
    [_NotificationCenter postNotificationName:kNotificationEnableBeaconSurveillanceDidChange object:nil];
}

@end
