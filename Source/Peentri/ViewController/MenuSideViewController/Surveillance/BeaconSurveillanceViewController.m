//
//  BeaconSurveillanceViewController.m
//  Eatus
//
//  Created by Phong Nguyen on 11/10/15.
//  Copyright © 2015 Triad Fox. All rights reserved.
//

#import "BeaconSurveillanceViewController.h"
#import "BeaconSurveillanceTableViewCell.h"
#import "TouchID+SurveillanceHeaderViewController.h"

@interface BeaconSurveillanceViewController ()

@end

@implementation BeaconSurveillanceViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self setNavCenterTitle:NSLocalizedString(@"com.eatus.nav.center.title.surveillance", @"TitleSurveillance")];
    [self prepareSideBarMenu];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

#pragma mark - UITableView Delegate methods
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return 1;
}

-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    return 150.0f;
}

-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    TouchID_SurveillanceHeaderViewController *headerViewController = [[TouchID_SurveillanceHeaderViewController alloc] initWithNibName:@"TouchIDAndSurveillanceHeaderSection" bundle:nil];
    [headerViewController presentWithHeaderTitle:NSLocalizedString(@"com.eatus.surveillance.header.content", @"EnableSurveillance") image:[UIImage imageNamed:@"SurveillanceColor"]];
    return headerViewController.view;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    static NSString *identifier = @"SurveillanceCellIdentifier";
    BeaconSurveillanceTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:identifier];
    
    if(!cell){
        NSArray *nibArray = [[NSBundle mainBundle] loadNibNamed:@"BeaconSurveillanceTableViewCell" owner:self options:nil];
        cell = [nibArray firstObject];
    }
    
    cell.surveillanceTitle.text = NSLocalizedString(@"com.eatus.surveillance.enable.title", @"Surveillance");
    return cell;
}

@end
