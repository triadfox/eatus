//
//  BeaconSurveillanceTableViewCell.h
//  Eatus
//
//  Created by Phong Nguyen on 11/10/15.
//  Copyright © 2015 Triad Fox. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface BeaconSurveillanceTableViewCell : UITableViewCell

@property(nonatomic, strong) IBOutlet UILabel *surveillanceTitle;
@property(nonatomic, strong) IBOutlet UISwitch *surveillanceSwitcher;

@end
