//
//  MenuSideViewController.h
//  Light Order
//
//  Created by Phong Nguyen on 10/7/15.
//  Copyright © 2015 Triad Fox. All rights reserved.
//

#import "BaseViewController.h"

@interface MenuSideViewController : BaseViewController<UITableViewDataSource, UITableViewDelegate>

@property(nonatomic, strong) IBOutlet UITableView *menuSideTableView;

-(void)preserveMainNavController: (UINavigationController *)navController;
@end
