//
//  MenuSideProfileTableViewCell.m
//  Light Order
//
//  Created by Phong Nguyen on 10/19/15.
//  Copyright © 2015 Triad Fox. All rights reserved.
//

#import "MenuSideProfileTableViewCell.h"
#import "CDFInitialsAvatar.h"

@implementation MenuSideProfileTableViewCell

- (void)awakeFromNib {
    self.selectionStyle = UITableViewCellSelectionStyleNone;
    [Util roundViewAsCircle:_profileImageView borderColor:[Util colorWithHexString:MAIN_THEME_COLOR_LIGHT_LV_2] borderWith:0.3f];
    [self settingUpInitialAvatarWithName:@"Eat Tus"];
    [self setBackgroundColor:COLOR_MAIN_MENU_SIDE_BACKGROUND];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
}

-(void)settingUpInitialAvatarWithName:(NSString *)name{
    CDFInitialsAvatar *avatar = [[CDFInitialsAvatar alloc] initWithRect:self.profileImageView.bounds fullName:name];
    [avatar setBackgroundColor:[Util colorWithHexString: MAIN_SCROLLING_COLOR_SILVER]];
    self.profileImageView.image = avatar.imageRepresentation;
    
    [self settingUpProfileName];
}

-(void)settingUpAvatarWithImage:(UIImage *)image{
    self.profileImageView.image = image;
    [self settingUpProfileName];
}

-(void)settingUpProfileName{
    User *currentUser = [User currentUser];
    if(!isNilOrEmpty(currentUser.fullname)){
        self.profileName.text = currentUser.fullname;
    }else{
        self.profileName.text = NSLocalizedString(@"com.eatus.menu.profile.unknown", @"UnknowProfile");
    }
}

@end
