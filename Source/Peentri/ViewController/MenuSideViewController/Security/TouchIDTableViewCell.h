//
//  TouchIDTableViewCell.h
//  Eatus
//
//  Created by Phong Nguyen on 11/10/15.
//  Copyright © 2015 Triad Fox. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TouchIDTableViewCell : UITableViewCell

@property(nonatomic, strong) IBOutlet UILabel *touchIdTitle;
@property(nonatomic, strong) IBOutlet UISwitch *touchIdSwitcher;

@end
