//
//  TouchIDManipulatorViewController.h
//  Eatus
//
//  Created by Phong Nguyen on 11/10/15.
//  Copyright © 2015 Triad Fox. All rights reserved.
//

#import "BaseViewController.h"

@interface TouchIDManipulatorViewController : BaseViewController<UITableViewDataSource, UITableViewDelegate>{
    __weak IBOutlet UITableView *touchIdTableView;
}

@end
