//
//  TouchIDManipulatorViewController.m
//  Eatus
//
//  Created by Phong Nguyen on 11/10/15.
//  Copyright © 2015 Triad Fox. All rights reserved.
//

#import "TouchIDManipulatorViewController.h"
#import "TouchIDTableViewCell.h"
#import "TouchID+SurveillanceHeaderViewController.h"

@interface TouchIDManipulatorViewController ()

@end

@implementation TouchIDManipulatorViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self setNavCenterTitle:NSLocalizedString(@"com.eatus.nav.center.title.touchIdAuth", @"TouchIdAuth")];
    [self prepareSideBarMenu];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    
}


#pragma mark - UITableView Delegate methods
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}

-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    return 150.0f;
}

-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    TouchID_SurveillanceHeaderViewController *headerViewController = [[TouchID_SurveillanceHeaderViewController alloc] initWithNibName:@"TouchIDAndSurveillanceHeaderSection" bundle:nil];
    [headerViewController presentWithHeaderTitle:NSLocalizedString(@"com.eatus.touchId.header.content", @"TouchId") image:[UIImage imageNamed:@"TouchID"]];
    return headerViewController.view;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return 1;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    static NSString *identifier = @"TouchIDCellIdentifier";
    TouchIDTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:identifier];
    
    if(!cell){
        NSArray *nibArray = [[NSBundle mainBundle] loadNibNamed:@"TouchIDTableViewCell" owner:self options:nil];
        cell = [nibArray firstObject];
    }
    
    cell.touchIdTitle.text = NSLocalizedString(@"com.eatus.touchId.enable.title", @"EnableTouchId");
    return cell;
}

@end
