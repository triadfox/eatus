//
//  TouchID+SurveillanceHeaderViewController.m
//  Eatus
//
//  Created by Phong Nguyen on 11/10/15.
//  Copyright © 2015 Triad Fox. All rights reserved.
//

#import "TouchID+SurveillanceHeaderViewController.h"

@interface TouchID_SurveillanceHeaderViewController (){
    NSString *titleContent;
    UIImage *imageContent;
}

@end

@implementation TouchID_SurveillanceHeaderViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    if(!isNilOrEmpty(titleContent)){
        self.iconTitle.text = titleContent;
    }
    
    if(imageContent){
        self.iconImageView.image = imageContent;
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

-(void)presentWithHeaderTitle:(NSString *)title image:(UIImage *)image{
    titleContent = [[NSString alloc] init];
    titleContent = title;
    imageContent = image;
}

@end
