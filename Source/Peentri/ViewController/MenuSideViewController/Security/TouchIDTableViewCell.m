//
//  TouchIDTableViewCell.m
//  Eatus
//
//  Created by Phong Nguyen on 11/10/15.
//  Copyright © 2015 Triad Fox. All rights reserved.
//

#import "TouchIDTableViewCell.h"
#import "EHFAuthenticator.h"

@implementation TouchIDTableViewCell
@synthesize touchIdSwitcher, touchIdTitle;

- (void)awakeFromNib {
    self.selectionStyle = UITableViewCellSelectionStyleNone;
    
    [touchIdSwitcher addTarget:self action:@selector(switcherChanged:) forControlEvents:UIControlEventValueChanged];
    
    if([[_UserDefault objectForKey:kShouldAuthTouchIdForPlaceOrder] boolValue]){
        [touchIdSwitcher setOn:YES animated:YES];
    }else{
        [touchIdSwitcher setOn:NO animated:YES];
    }
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

}


#pragma mark - IBAction methods
-(void)switcherChanged:(id)sender{
    [[EHFAuthenticator sharedInstance] setReason:NSLocalizedString(@"com.eatus.touchId.setup.title", @"TouchIdSetup")];
    [[EHFAuthenticator sharedInstance] hideFallbackButton];
    [[EHFAuthenticator sharedInstance] authenticateWithSuccess:^{
        [_UserDefault setObject:[NSNumber numberWithBool:touchIdSwitcher.isOn] forKey:kShouldAuthTouchIdForPlaceOrder];
        [_NotificationCenter postNotificationName:kNotificationEnableTouchIdForPlaceOrderDidChange object:nil];
    }andFailure:^(LAError error){
        [touchIdSwitcher setOn:!touchIdSwitcher.isOn animated:YES];
    }];
}

@end
