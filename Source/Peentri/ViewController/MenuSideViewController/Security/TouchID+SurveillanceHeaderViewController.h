//
//  TouchID+SurveillanceHeaderViewController.h
//  Eatus
//
//  Created by Phong Nguyen on 11/10/15.
//  Copyright © 2015 Triad Fox. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TouchID_SurveillanceHeaderViewController : UIViewController

@property(nonatomic, strong) IBOutlet UIImageView *iconImageView;
@property(nonatomic, strong) IBOutlet UILabel *iconTitle;

-(void)presentWithHeaderTitle: (NSString *)title image: (UIImage *)image;
@end
