//
//  OptionsDetailViewController.h
//  Light Order
//
//  Created by Phong Nguyen on 10/8/15.
//  Copyright © 2015 Triad Fox. All rights reserved.
//

#import "BaseViewController.h"
#import "DishEntity.h"
#import "DishOptionsEntity.h"
#import "DishOptionEntity.h"
#import "OrderViewController.h"

@protocol OptionsDetailProtocols <NSObject>

@required
-(void)didSelectOptions: (DishOptionsEntity *)options;

@end

@interface OptionsDetailViewController : BaseViewController<UITableViewDataSource, UITableViewDelegate, OrderOptionsProtocols>

@property(nonatomic, strong) IBOutlet UITableView *optionsDetailTableView;

@property(nonatomic, strong) DishOptionsEntity *options;
@property(nonatomic, assign) BOOL firstTime;
@property(nonatomic, assign) BOOL exclusiveSelection;
@property(nonatomic, strong) id delegate;

-(void)reloadTableViewWithChargeOptions: (DishOptionsEntity *)optionsEntity;
@end
