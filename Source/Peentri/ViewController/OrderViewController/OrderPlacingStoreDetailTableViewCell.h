//
//  OrderPlacingOrderDetailTableViewCell.h
//  Eatus
//
//  Created by Phong Nguyen on 11/18/15.
//  Copyright © 2015 Triad Fox. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface OrderPlacingStoreDetailTableViewCell : UITableViewCell

@property(nonatomic, strong) IBOutlet UIView *leftHorizontalTopLine;
@property(nonatomic, strong) IBOutlet UIView *rightHorizontalTopLife;
@property(nonatomic, strong) IBOutlet UILabel *receiptIssueDate;
@property(nonatomic, strong) IBOutlet UILabel *storeNameTitle;
@property(nonatomic, strong) IBOutlet UIImageView *storeLogoImageView;

@end
