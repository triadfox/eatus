//
//  OrderPlacingStatusTableViewCell.m
//  Eatus
//
//  Created by Phong Nguyen on 11/17/15.
//  Copyright © 2015 Triad Fox. All rights reserved.
//

#import "OrderPlacingStatusAndPaymentTypeTableViewCell.h"
#import "JDFTooltips.h"

@interface OrderPlacingStatusAndPaymentTypeTableViewCell(){
    BOOL shouldPresentToolTip;
    
    NSString *toolTipContent;
    UIView *currrentHostView;
}

@property(nonatomic, retain) JDFTooltipView *toolTipView;

@end

@implementation OrderPlacingStatusAndPaymentTypeTableViewCell
@synthesize toolTipView;

- (void)awakeFromNib {
    self.selectionStyle = UITableViewCellSelectionStyleNone;
    if(shouldPresentToolTip){
        [self presentTooltipOnStatus];
    }
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
}

-(void)shouldPresentToolTipOnTapOnStatusWithContent:(NSString *)content inHostView:(UIView *)hostView{
    toolTipContent = content;
    currrentHostView = hostView;
    
    UITapGestureRecognizer *tapGestureRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(presentTooltipOnStatus)];
    [self.receiptStatus addGestureRecognizer:tapGestureRecognizer];
    
    shouldPresentToolTip = YES;
}

-(void)presentTooltipOnStatus{
    toolTipView = [[JDFTooltipView alloc] initWithTargetPoint:self.center hostView:currrentHostView tooltipText:toolTipContent arrowDirection:JDFTooltipViewArrowDirectionDown width:self.bounds.size.width * 0.5];
    [toolTipView setTooltipBackgroundColour:COLOR_MAIN_THEME_LIGHT_LV_1];
    [toolTipView show];
}

@end
