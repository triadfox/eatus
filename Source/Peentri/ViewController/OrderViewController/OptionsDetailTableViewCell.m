//
//  OrderDetailTableViewCell.m
//  Light Order
//
//  Created by Phong Nguyen on 10/8/15.
//  Copyright © 2015 Triad Fox. All rights reserved.
//

#import "OptionsDetailTableViewCell.h"

@implementation OptionsDetailTableViewCell

- (void)awakeFromNib {
    self.selectionStyle = UITableViewCellSelectionStyleNone;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
}

@end
