//
//  OrderViewController.h
//  Light Order
//
//  Created by Phong Nguyen on 10/3/15.
//  Copyright © 2015 Triad Fox. All rights reserved.
//

#import "BaseViewController.h"
#import "DishEntity.h"
#import "DishOptionEntity.h"
#import "DishOptionsEntity.h"

@class OptionsDetailViewController;

@protocol OrderProtocols <NSObject>

@required
-(void)didTapOnDismissOrderViewControllerInMZPresentation;

@optional
-(void)didTapOnAddToOrderAndDismissMZPresentation: (DishEntity *)selectedDish;
-(void)didSelectOnCellWithDishOption: (DishOptionsEntity *)dishOptions firstTime: (BOOL)flag exclusiveSelection: (BOOL)exclusive;
-(void)didTapOnDoneInEditModeWithDishEntity: (DishEntity *)editedDish;

@optional


@end

@protocol OrderOptionsProtocols <NSObject>

@required
-(void)didTurnBackFromOptionSelectionWithCompletion: (void (^)(DishOptionsEntity *dishOptions))completion;

@end

@interface OrderViewController : BaseViewController<UITableViewDataSource, UITableViewDelegate>

@property(nonatomic, strong) IBOutlet UITableView *orderTableView;
@property(nonatomic, weak) IBOutlet UILabel *orderNameTitle;
@property(nonatomic, strong) IBOutlet UILabel *orderOptionTitle;
@property(nonatomic, strong) IBOutlet UIView *orderHorizontalSeparator;
@property(nonatomic, strong) IBOutlet UIView *orderContentView;
@property(nonatomic, strong) IBOutlet UIButton *orderDismiss;
@property(nonatomic, strong) IBOutlet UIButton *bottomButton;

@property(nonatomic, strong) DishEntity *dishEntity;

@property(nonatomic, strong) id<OrderProtocols> delegate;
@property(nonatomic, strong) id<OrderOptionsProtocols> delegateO;

-(void)changeBottomButtonAsFitOptionDetail;
-(void)changeBottomButtonFitRevert;
-(void)transitionToOptionDetail: (OptionsDetailViewController *)optionsDetailViewController;
-(void)transitionMoveBackToOrder;
-(void)animationChangeOptionsTitleWithContent: (NSString *)content;
-(void)animationRevertOptionTitleContent;
-(void)hideDismissButton;

-(IBAction)dismissOrderViewControllerFromMZPresentation:(id)sender;

-(id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil mode:(OrderViewModes)mode;
@end
