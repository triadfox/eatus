//
//  OrderPlacingDishesTableViewCell.h
//  Light Order
//
//  Created by Phong Nguyen on 10/11/15.
//  Copyright © 2015 Triad Fox. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SWTableViewCell.h"

@interface OrderPlacingDishesTableViewCell : SWTableViewCell

@property(nonatomic, weak) IBOutlet UIView *horizontalSeparatorView;
@property(nonatomic, weak) IBOutlet UILabel *dishName;
@property(nonatomic, weak) IBOutlet UILabel *dishQuantity;
@property(nonatomic, weak) IBOutlet UILabel *dishTotal;
@property(nonatomic, weak) IBOutlet UILabel *dishOptions;

@end
