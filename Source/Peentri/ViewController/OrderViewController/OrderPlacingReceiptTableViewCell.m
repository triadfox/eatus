//
//  OrderPlacingReceiptTableViewCell.m
//  Light Order
//
//  Created by Phong Nguyen on 10/11/15.
//  Copyright © 2015 Triad Fox. All rights reserved.
//

#import "OrderPlacingReceiptTableViewCell.h"

@implementation OrderPlacingReceiptTableViewCell

- (void)awakeFromNib {
    self.selectionStyle = UITableViewCellSelectionStyleNone;
    
    [self.subTotalLabel setText:NSLocalizedString(@"com.eatus.order.placing.subTotal.label", @"SubTotal")];
    [self.saleTaxLabel setText:NSLocalizedString(@"com.eatus.order.placing.tax.label", @"SaleTax")];
    [self.discountLabel setText:NSLocalizedString(@"com.eatus.order.placing.todayDiscount.label", @"Discount")];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

}

@end
