//
//  OptionsDetailViewController.m
//  Light Order
//
//  Created by Phong Nguyen on 10/8/15.
//  Copyright © 2015 Triad Fox. All rights reserved.
//

#import "OptionsDetailViewController.h"
#import "OptionsDetailTableViewCell.h"

@interface OptionsDetailViewController (){
    NSArray *_allOptions;
    NSIndexPath *_selectedOption;
}

@end

@implementation OptionsDetailViewController
@synthesize options;
@synthesize firstTime;

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self markAllDefaultOptionAsSelected];
    
    self.optionsDetailTableView.delegate = self;
    self.optionsDetailTableView.dataSource = self;
    [self.optionsDetailTableView reloadData];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

-(void)reloadTableViewWithChargeOptions:(DishOptionsEntity *)optionsEntity{
    options = optionsEntity;
    _allOptions = options.options;
}

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return [_allOptions count];
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    static NSString *identifier = @"OptionsDetailCellIdentifier";
    OptionsDetailTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:identifier];
    if(!cell){
        NSArray *nibArray = [[NSBundle mainBundle] loadNibNamed:@"OptionsDetailVIewCell" owner:self options:nil];
        cell = [nibArray firstObject];
    }
    
    DishOptionEntity *individual = [options.options objectAtIndex:indexPath.row];
    if(individual){
        if(options.optionAddOn){
            cell.detailsTitle.text = [NSString stringWithFormat:@"%@ (+ %.3f)", individual.dishOptionName, individual.dishOptionAddOnPrice];
        }else{
            cell.detailsTitle.text = individual.dishOptionName;
        }
        
        if([self determineShouldMarkChecked:individual]){
            [self animateCheckmarkOnView:cell];
            if(_exclusiveSelection){
                _selectedOption = indexPath;
            }
        }else{
            [self animateUnCheckmarkOnView:cell];
        }
    }
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    DishOptionEntity *option = [_allOptions objectAtIndex:indexPath.row];
    DishOptionEntity *previousOption;
    
    OptionsDetailTableViewCell *cell = [tableView cellForRowAtIndexPath:indexPath];
    
    if(_exclusiveSelection){
        if(!_selectedOption){
            _selectedOption = indexPath;
            previousOption = [self getDefaultOptionForExclusiveSelection];
        }else{
            OptionsDetailTableViewCell *previousCell = [tableView cellForRowAtIndexPath:_selectedOption];
            previousOption = [_allOptions objectAtIndex:_selectedOption.row];
            [self animateUnCheckmarkOnView:previousCell];
            _selectedOption = indexPath;
        }
        
        [self animateCheckmarkOnView:cell];
        option.selected = YES;
        [self changeValueForExclusiveNewSelection:option previous:previousOption];
    }else{
        if(option.selected){
            option.selected = NO;
            [self animateUnCheckmarkOnView:cell];
        }else{
            option.selected = YES;
            [self animateCheckmarkOnView:cell];
        }
    }
}

-(BOOL)determineShouldMarkChecked: (DishOptionEntity *)option{
    if(firstTime){
        if(option.dishOptionItemType == kOptionItemDefault){
            return YES;
        }else{
            return NO;
        }
    }
    else{
        if(option.selected){
            return YES;
        }else{
            return NO;
        }
    }
}

-(void)markAllDefaultOptionAsSelected{
    if(!firstTime){
        return;
    }
    for(DishOptionEntity *option in _allOptions){
        if(option.dishOptionItemType == kOptionItemDefault){
            option.selected = YES;
        }
    }
}

-(void)changeValueForExclusiveNewSelection: (DishOptionEntity *)new previous: (DishOptionEntity *)previous{
    for(DishOptionEntity *opt in _allOptions){
        NSString *optName = opt.dishOptionName;
        if([optName isEqualToString:new.dishOptionName]){
            opt.selected = YES;
        }
        
        if([optName isEqualToString:previous.dishOptionName]){
            opt.selected = NO;
        }
    }
}

-(DishOptionEntity *)getDefaultOptionForExclusiveSelection{
    for(DishOptionEntity *opt in _allOptions){
        if(opt.selected){
            return opt;
        }
    }
    return nil;
}


#pragma mark - Animation
-(void)animateCheckmarkOnView: (OptionsDetailTableViewCell *)cell{
    [UIView transitionWithView:cell duration:0.2f options:UIViewAnimationOptionTransitionCrossDissolve animations:^{
        cell.detailCheck.hidden = NO;
    }completion:^(BOOL finishes){
        
    }];
}

-(void)animateUnCheckmarkOnView: (OptionsDetailTableViewCell *)cell{
    [UIView transitionWithView:cell duration:0.2f options:UIViewAnimationOptionTransitionCrossDissolve animations:^{
        cell.detailCheck.hidden = YES;
    }completion:^(BOOL finishes){
        
    }];
}

#pragma mark - Order Option Delegate methods
-(void)didTurnBackFromOptionSelectionWithCompletion:(void (^)(DishOptionsEntity *))completion{
    completion(options);
}

@end
