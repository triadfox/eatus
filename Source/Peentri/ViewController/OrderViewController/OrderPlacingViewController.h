//
//  ViewOrderViewController.h
//  Light Order
//
//  Created by Phong Nguyen on 10/11/15.
//  Copyright © 2015 Triad Fox. All rights reserved.
//

#import "BaseViewController.h"

typedef NS_ENUM(NSUInteger, OrderPlacingModeView) {
    kOrderPlacingProceedCheckout,
    kOrderPlacingReceiptReview
};

@protocol OrderPlacingProtocols <NSObject>

@optional
-(void)orderPlacingDidDisappear;
-(void)orderPlacingWillDisappear;

@end

@interface OrderPlacingViewController : BaseViewController<UITableViewDataSource, UITableViewDelegate>

@property(nonatomic, strong) IBOutlet UITableView *orderPlacingTableView;
@property(nonatomic, strong) IBOutlet UIView *seatingBackgroundView;
@property(nonatomic, strong) IBOutlet UILabel *seatingLocationLabel;
@property(nonatomic, strong) IBOutlet UILabel *totalLabel;
@property(nonatomic, weak) IBOutlet UIButton *placeOrderButton;
@property(nonatomic, weak) IBOutlet UIButton *dismissButton;

@property(nonatomic, strong) Restaurant *restaurant;
@property(nonatomic, strong) NSMutableArray *orders;

@property(nonatomic, strong) id<OrderPlacingProtocols> delegate;

-(instancetype)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil mode:(OrderPlacingModeView)mode;

-(void)prepareAllCalculationByDiscountPercentage: (float)percentage;
-(void)prepareAllCalculationByReceiptDataAtTableWithOrder: (Order *)order locale: (NSLocale *)locale tableName: (NSString *)tableName;

-(IBAction)dismissOrderPlacingViewController:(id)sender;
-(IBAction)placeOrder:(id)sender;
@end
