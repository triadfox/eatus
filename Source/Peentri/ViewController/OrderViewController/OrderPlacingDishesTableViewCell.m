//
//  OrderPlacingDishesTableViewCell.m
//  Light Order
//
//  Created by Phong Nguyen on 10/11/15.
//  Copyright © 2015 Triad Fox. All rights reserved.
//

#import "OrderPlacingDishesTableViewCell.h"

@implementation OrderPlacingDishesTableViewCell

- (void)awakeFromNib {
    self.selectionStyle = UITableViewCellSelectionStyleNone;
    [self.dishQuantity setTextColor:[Util colorWithHexString:MAIN_THEME_COLOR]];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
}

@end
