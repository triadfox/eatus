//
//  OrderPlacingPaymentTableViewCell.m
//  Light Order
//
//  Created by Phong Nguyen on 10/11/15.
//  Copyright © 2015 Triad Fox. All rights reserved.
//

#import "OrderPlacingPaymentTableViewCell.h"

@implementation OrderPlacingPaymentTableViewCell

- (void)awakeFromNib {
    self.selectionStyle = UITableViewCellSelectionStyleGray;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
}

@end
