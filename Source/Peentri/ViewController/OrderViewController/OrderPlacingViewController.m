//
//  ViewOrderViewController.m
//  Light Order
//
//  Created by Phong Nguyen on 10/11/15.
//  Copyright © 2015 Triad Fox. All rights reserved.
//

#import "OrderPlacingViewController.h"
#import "OrderPlacingDishesTableViewCell.h"
#import "OrderPlacingPaymentTableViewCell.h"
#import "OrderPlacingReceiptTableViewCell.h"
#import "OrderPlacingAddItemTableViewCell.h"
#import "OrderPlacingStatusAndPaymentTypeTableViewCell.h"
#import "OrderPlacingStoreDetailTableViewCell.h"

#import "PaymentViewController.h"
#import "PaymentAddViewController.h"
#import "OrderViewController.h"
#import "HeaderSectionViewController.h"

#import "DishEntity.h"
#import "DishOptionEntity.h"
#import "DishOptionsEntity.h"

#import "OrderViewController.h"
#import "MenuJsonSerializer.h"
#import "MenuJsonSerializerConstant.h"
#import "ServiceAPIManager.h"
#import "PaymentManager.h"

#import "SWTableViewCell.h"

#define HEADER_SECTION_HEIGHT_DISHES_OVERVIEW            44

#define CELL_ROW_HEIGHT_NORMALLY                         125
#define CELL_ROW_STORE_DETAIL                            80
#define CELL_ROW_HEIGHT_ADD_ITEM                         50
#define CELL_ROW_HEIGHT_STATUS                           74
#define CELL_ROW_HEIGHT_PAYMENT                          50

#define RIGHT_UTILITY_CELL_BUTTON_WIDTH                  80

#define CELL_DISHES_IDENTIFIER                          @"DishesCellIdentifier"
#define CELL_RECEIPT_IDENTIFIER                         @"ReceiptCellIdentifier"
#define CELL_PAYMENT_IDENTIFIER                         @"PaymentCellIdentifier"
#define CELL_ADD_ITEM_IDENTIFIER                        @"AddItemCellIdentifier"

#define CELL_STATUS_NIB_NAME                            @"OrderPlacingStatusPaymentTypeViewCell"
#define CELL_STORE_NIB_NAME                             @"OrderPlacingStoreDetailViewCell"
#define CELL_DISHES_NIB_NAME                            @"OrderPlacingDishesTableViewCell"
#define CELL_ADD_ITEM_NIB_NAME                          @"OrderPlacingAddItemTableViewCell"
#define CELL_RECEIPT_NIB_NAME                           @"OrderPlacingReceiptTableViewCell"
#define CELL_PAYMENT_NIB_NAME                           @"OrderPlacingPaymentTableViewCell"

@interface OrderPlacingViewController ()<SWTableViewCellDelegate, OrderProtocols>{
    double orderTotal;
    double orderSubTotal;
    double orderDiscount;
    float orderDiscountPercentage;
    double orderTax;
    
    NSString *receiptId;
    
    BOOL didLocateTable;
    BOOL needRelocateTable;
    BOOL receiptReviewMode;
    
    NSString *restaurantId;
    NSString *restaurantName;
    NSString *tableReceiptName;
    
    RestaurantTable *restaurantTable;
    Restaurant *currentRestaurant;
    
    MenuJsonSerializer *serializer;
    
    NSLocale *localeAlongCurrency;
}

@property(nonatomic, weak) OrderPlacingDishesTableViewCell *dishCell;
@property(nonatomic, weak) OrderPlacingReceiptTableViewCell *receiptCell;
@property(nonatomic, weak) OrderPlacingAddItemTableViewCell *addCell;
@property(nonatomic, weak) OrderPlacingPaymentTableViewCell *paymentCell;
@property(nonatomic, weak) OrderPlacingStatusAndPaymentTypeTableViewCell *statusCell;
@property(nonatomic, weak) OrderPlacingStoreDetailTableViewCell *storeCell;

@property(nonatomic, strong) MZFormSheetPresentationController *mzPresentationControllerEditDish;
@property(nonatomic, strong) MZFormSheetPresentationController *mzPresentationControllerPayment;

@property(nonatomic, strong) Order *orderFromReceipt;

@end

@implementation OrderPlacingViewController
@synthesize dishCell, receiptCell, addCell, paymentCell, statusCell, storeCell;
@synthesize orderPlacingTableView;
@synthesize mzPresentationControllerEditDish, mzPresentationControllerPayment;
@synthesize orders, orderFromReceipt;

-(instancetype)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil mode:(OrderPlacingModeView)mode{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if(self){
        if(mode == kOrderPlacingProceedCheckout){
            
        }else if(mode == kOrderPlacingReceiptReview){
            receiptReviewMode = YES;
        }
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    serializer = [MenuJsonSerializer shareSerial];
    
    [self hideNavBar];
    
    [self settingUpColor];
    
    if(receiptReviewMode){
        [self.placeOrderButton setTitle:NSLocalizedString(@"com.eatus.order.receipt.total.button.title", @"Total") forState:UIControlStateNormal];
    }else{
        [self.placeOrderButton setTitle:NSLocalizedString(@"com.eatus.order.button.place.order", @"PlaceOrder") forState:UIControlStateNormal];
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

-(void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    
    if(!receiptReviewMode){
        [self locateSittingTableNow];
    }
}

-(void)viewDidDisappear:(BOOL)animated{
    [super viewDidDisappear:animated];
    
    [self.delegate orderPlacingDidDisappear];
}

-(void)viewWillDisappear:(BOOL)animated{
    [self.delegate orderPlacingWillDisappear];
}


#pragma mark - Data Preparation For Case: Load From Store (To Order) and Load From Receipt
-(void)prepareAllCalculationByDiscountPercentage: (float)percentage{
    orderDiscountPercentage = percentage;
    
    orderSubTotal = [serializer getCurrentTotal];
    orderTax = orderSubTotal * [[MenuEngineManager shareManager] taxPercentageForCurrentMenu];
    orderDiscount = orderSubTotal * percentage;
    orderTotal = orderSubTotal + orderTax - orderDiscount;
    self.totalLabel.text = priceByCurrency(orderTotal);
    
    NSRange range = NSMakeRange(0, 3);
    NSIndexSet *section = [[NSIndexSet alloc] initWithIndexesInRange:range];
    [self.orderPlacingTableView reloadSections:section withRowAnimation:UITableViewRowAnimationFade];
}

-(void)prepareAllCalculationByReceiptDataAtTableWithOrder: (Order *)order locale: (NSLocale *)locale tableName: (NSString *)tableName{
    orderFromReceipt = order;
    localeAlongCurrency = locale;
    self.orders = [[MenuJsonSerializer allDishesGotJsonFromBackend:order.orderDetail] mutableCopy];
    
    NSDictionary *neededData = [MenuJsonSerializer allNeededOrderDetailGotJsonFromBackend:order.orderDetail];
    
    receiptId = [neededData objectForKey:KEY_RECEIPT_ID];
    tableReceiptName = tableName;
    
    orderSubTotal = [[neededData objectForKey:KEY_SUB_TOTAL] doubleValue];
    orderTotal = [[neededData objectForKey:KEY_TOTAL] doubleValue];
    orderDiscount = [[neededData objectForKey:KEY_RECEIPT_DISCOUNT_AMOUNT] doubleValue];
    orderDiscountPercentage = [[neededData objectForKey:KEY_RECEIPT_DISCOUNT_PERCENTAGE] floatValue];
    orderTax = [[neededData objectForKey:KEY_RECEIPT_TAX] doubleValue];
}


#pragma mark - Table Indicator Section
-(void)locateSittingTableNow{
    [_Shared showOverlayIndetermineSmalProgressViewWithTitle:NSLocalizedString(@"com.eatus.notification.orderPlacing.locateTable", @"LocatingTable") style:kOverlayElegantStyle tintColor:COLOR_MAIN_THEME onView:self.view];
    if(needRelocateTable){
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(3.0f * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
            [self determineNearestTableWithBeacon];
            needRelocateTable = NO;
        });
    }else{
        [self determineNearestTableWithBeacon];
    }
}

-(void)determineNearestTableWithBeacon{
    [self retrieveCurrentPositionOnCompletion:^(RestaurantTable *table, NSError *error){
        if(!error){
            if(table){
                restaurantTable = table;
                
                [CATransaction begin];
                
                CATransition *transition = [CATransition animation];
                transition.type = kCATransitionPush;
                transition.subtype = kCATransitionFromBottom;
                transition.removedOnCompletion = NO;
                transition.duration = 0.8f;
                transition.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut];
                [self.seatingLocationLabel.layer addAnimation:transition forKey:nil];
                
                [CATransaction commit];
                
                self.seatingLocationLabel.text = [NSString stringWithFormat:NSLocalizedString(@"com.eatus.order.placing.seat.label", @"SittingTable"), table.name];
                [CATransaction setCompletionBlock:^{
                    didLocateTable = YES;
                    [self performSelector:@selector(determineDidLocatedPositionAndAuthenticate)];
                }];
            }
        }
        [_Shared hideOverlayIndetermineProgressViewAfter:1.5f];
    }];

}

-(void)settingUpColor{
    [self.seatingBackgroundView setBackgroundColor:COLOR_MAIN_THEME];
    [self.placeOrderButton setEnabled:NO];
    
    if(receiptReviewMode){
        self.totalLabel.text = [Util stringForNumberPrimitiveWithLocale:localeAlongCurrency number:orderTotal];
        
        NSString *seatingBuilder;
        if(tableReceiptName){
            seatingBuilder = [NSString stringWithFormat:NSLocalizedString(@"com.eatus.order.receipt.table.order.found", @"ReceiptTable"), tableReceiptName];
        }else{
            seatingBuilder = NSLocalizedString(@"com.eatus.order.receipt.table.order.notFound", @"ReceiptTableNotFound");
        }
        
        [self.placeOrderButton setBackgroundColor:COLOR_MAIN_THEME];
        [self.placeOrderButton setTitle:NSLocalizedString(@"com.eatus.order.receipt.total.button.title", "ReceiptTotal") forState:UIControlStateNormal];
        
        self.seatingLocationLabel.text = seatingBuilder;
    }else{
        [self.placeOrderButton setEnabled:NO];
        [self.placeOrderButton setBackgroundColor:COLOR_MAIN_THEME_SILVER];
    }
}


#pragma mark - Retrieve Position by Beacon
-(void)retrieveCurrentPositionOnCompletion: (void (^)(RestaurantTable *, NSError *))completion{
    [[KontaktBeaconServiceManager shareBeacon] nearestBeaconInRange:^(CLBeacon *beacon){
        if(beacon){
            [ParseDataManager getRestaurantByMajorId:beacon.major onCompletion:^(Restaurant *restaurant, NSError *error){
                if(!error){
                    currentRestaurant = restaurant;
                    restaurantId = [restaurant objectId];
                    restaurantName = restaurant[kName];
                    
                    [ParseDataManager gettableByBeaconMinorid:beacon.minor atRestaurant:restaurant onCompletion:^(RestaurantTable *table, NSError *error){
                        if(!error){
                            if(completion){
                                completion(table, nil);
                            }
                        }else{
                            if(completion){
                                completion(nil, error);
                            }
                        }
                    }];
                }
            }];
        }else{
            [_Shared hideOverlayIndetermineProgressViewAfter:2.0f];
            
            dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(2.0f * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                AMSmoothAlertView *alertView = [AlertViewManager smoothDropAlertViewWithTitle:NSLocalizedString(@"com.eatus.alert.alertPlacing.localeTable.title", @"LocateTable") content:NSLocalizedString(@"com.eatus.alert.orderPlacing.locateTable.notFound.content", @"LocateTablContent") cancelButton:YES color:COLOR_MAIN_THEME alertType:AlertInfo onCompletion:^(AMSmoothAlertView *alert, UIButton *button){
                    if([button isEqual:alert.defaultButton]){
                        needRelocateTable = YES;
                        [self locateSittingTableNow];
                    }else if([button isEqual:alert.cancelButton]){
                        [self dismissViewControllerAnimated:YES completion:nil];
                    }
                }];
                [alertView.logoView setImage:[UIImage imageNamed:@"TablePosition"]];
                [alertView.defaultButton setTitle:NSLocalizedString(@"com.eatus.alert.orderPlacing.locateTable.positive.button.title", @"Retry") forState:UIControlStateNormal];
                [alertView.cancelButton setTitle:NSLocalizedString(@"com.eatus.alert.orderPlacing.locateTable.negative.button.title", @"Cancel") forState:UIControlStateNormal];
                
                [alertView show];
            });
        }
    }];
}

-(void)determineDidLocatedPositionAndAuthenticate{
    if(didLocateTable){
        [self.placeOrderButton setBackgroundColor:COLOR_MAIN_THEME];
        [self.placeOrderButton setEnabled:YES];
    }
}


#pragma mark - Table View Delegate methods
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 4;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    if(section == kOrderPlacingDishes && !receiptReviewMode){
        return [self.orders count];
    }else if(receiptReviewMode && section == 1){
        return [self.orders count];
    }
    return 1;
}

-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    HeaderSectionViewController *headerViewController;
    headerViewController = [[HeaderSectionViewController alloc] initWithNibName:@"HeaderSectionView" bundle:nil];
    
    if(section == kOrderPlacingDishes && !receiptReviewMode){
        [headerViewController presentWithHeaderTitle:NSLocalizedString(@"com.eatus.order.placing.dishesOverview.label", @"Overview")];
    }
    
    if(receiptReviewMode){
        if(section == 1){
            [headerViewController presentWithHeaderTitle:NSLocalizedString(@"com.eatus.order.header.overview", @"Overview")];
        }else if(section == 2){
            [headerViewController presentWithHeaderTitle:NSLocalizedString(@"com.eatus.order.receipt.header.payment", @"Payment")];
        }else if(section == 3){
            [headerViewController presentWithHeaderTitle:NSLocalizedString(@"com.eatus.order.receipt.header.pricing", @"Pricing")];
        }
    }
    return headerViewController.view;
}

-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    if(!receiptReviewMode){
        if(section == kOrderPlacingDishes){
            return 50.0f;
        }
    }
    return 0.0f;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    switch (indexPath.section) {
        case kOrderPlacingDishes:{
            if(!receiptReviewMode){
                return CELL_ROW_HEIGHT_NORMALLY;
            }else{
                return CELL_ROW_STORE_DETAIL;
            }
        }
            
            break;
            
        case kOrderPlacingAddItem:{
            if(!receiptReviewMode){
                return CELL_ROW_HEIGHT_ADD_ITEM;
            }else{
                return CELL_ROW_HEIGHT_NORMALLY;
            }
        }
            
        break;
            
        case kOrderPlacingReceipt:
            if(!receiptReviewMode){
                return CELL_ROW_HEIGHT_NORMALLY;
            }else{
                return CELL_ROW_HEIGHT_STATUS;
            }
            break;
            
        case kOrderPlacingPayment:
            if(!receiptReviewMode){
                return CELL_ROW_HEIGHT_PAYMENT;
            }else{
                return CELL_ROW_HEIGHT_NORMALLY;
            }
            
            break;
            
        default:
            break;
    }
    return 0.0f;
}

//table view will populate data as this description by the following sections: Dish(Section: 0) -> Add new Item(Section: 1) -> Receipt(Section: 2) -> Payment Section(Section: 3)
/***************************************
 *------------Dish Section-------------*
 *                                     *
 *                                     *
 *                                     *
 *                                     *
 *------------Add New Item-------------*
 *                                     *
 *                                     *
 *------------Receipt Secion-----------*
 *                                     *
 *                                     *
 *                                     *
 *------------Payment Section----------*
 *                                     *
 *                                     *
 ***************************************/
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    UITableViewCell *cell = [[UITableViewCell alloc] init];
    
    switch (indexPath.section) {
        case kOrderPlacingDishes:{
            if(!receiptReviewMode){
                cell = [self handleDishCellForIndexPath:indexPath forTableView:tableView];
            }else{
                cell = [self handleStoreDetailCellForIndexPath:indexPath forTableView:tableView];
            }
            
        }
            break;
            
        case kOrderPlacingAddItem:{
            if(!receiptReviewMode){
                cell = [self handleAddItemCellForIndexPath:indexPath forTableView:tableView];
            }else{
                cell = [self handleDishCellForIndexPath:indexPath forTableView:tableView];
            }
        }
            break;
            
        case kOrderPlacingReceipt:{
            if(!receiptReviewMode){
                cell = [self handleReceiptCellForIndexPath:indexPath forTableView:tableView];
            }else{
                cell = [self handleAddItemCellForIndexPath:indexPath forTableView:tableView];
            }
            
        }
            break;
            
        case kOrderPlacingPayment:{
            if(!receiptReviewMode){
                cell = [self handlePaymentCellForIndexPath:indexPath forTableView:tableView];
            }else{
                cell = [self handleReceiptCellForIndexPath:indexPath forTableView:tableView];
            }
        }
            break;
            
        default:
            break;
    }
    
    return cell;
}

-(UITableViewCell *)handleStoreDetailCellForIndexPath: (NSIndexPath *)indexPath forTableView: (UITableView *)tableView{
    storeCell = [[[NSBundle mainBundle] loadNibNamed:CELL_STORE_NIB_NAME owner:self options:nil] firstObject];
    [storeCell.leftHorizontalTopLine setBackgroundColor:COLOR_MAIN_THEME];
    [storeCell.rightHorizontalTopLife setBackgroundColor:COLOR_MAIN_THEME];
    
    storeCell.receiptIssueDate.text = [Util dateToStringByCurrentLocale:orderFromReceipt.createdAt format:@"hh:mm MMMM dd yyyy"];
    
    [orderFromReceipt fetchIfNeededInBackgroundWithBlock:^(PFObject *orderFetch, NSError *error){
        RestaurantTable *table = orderFetch[kRestaurantTable];
        
        [table fetchIfNeededInBackgroundWithBlock:^(PFObject *table, NSError *error){
            Restaurant *restaurant = table[kRestaurant];
            
            [restaurant fetchIfNeededInBackgroundWithBlock:^(PFObject *restaurantFetch, NSError *error){
                storeCell.storeNameTitle.text = [NSString stringWithFormat:@"%@ (%@)", restaurantFetch[kName], receiptId];
                
                dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_LOW, 0), ^{
                    PFFile *logoFile = restaurantFetch[kLogoImage];
                    UIImage *logoImage = [Util getImageSyncFromURL:logoFile.url];
                    
                    dispatch_async(dispatch_get_main_queue(), ^{
                        if(logoImage){
                            storeCell.storeLogoImageView.image = logoImage;
                        }
                    });
                });
            }];
        }];
    }];
    
    return storeCell;
}

-(UITableViewCell *)handleDishCellForIndexPath: (NSIndexPath *)indexPath forTableView: (UITableView *)tableView{
    dishCell = [tableView dequeueReusableCellWithIdentifier:CELL_DISHES_IDENTIFIER];
    if(!dishCell){
        NSArray *nibArray = [[NSBundle mainBundle] loadNibNamed:CELL_DISHES_NIB_NAME owner:self options:nil];
        dishCell = [nibArray firstObject];
    }
    
    DishEntity *dish = [self.orders objectAtIndex:indexPath.row];
    
    dishCell.dishName.text = dish.dishName;
    dishCell.dishQuantity.text = [NSString stringWithFormat:@"x %ld", (long)dish.dishQuantity];
    dishCell.dishOptions.text = [dish.dishOptions count] > 0?[self buildOptionsFromOptionCollection:dish.dishOptions]:NSLocalizedString(@"com.eatus.order.modal.default.none", @"None");
    dishCell.dishTotal.text = priceByCurrency(dish.dishPrice * dish.dishQuantity);
    
    //setting up utilities button on swipe
    
    if(!receiptReviewMode){
        [dishCell setRightUtilityButtons:[self prepareRightUtilitiesButton] WithButtonWidth:RIGHT_UTILITY_CELL_BUTTON_WIDTH];
        dishCell.delegate = self;
    }
    return dishCell;
}

-(UITableViewCell *)handleAddItemCellForIndexPath: (NSIndexPath *)indexPath forTableView: (UITableView *)tableView{
    if(!receiptReviewMode){
        NSArray *nibArray = [[NSBundle mainBundle] loadNibNamed:CELL_ADD_ITEM_NIB_NAME owner:self options:nil];
        addCell = [nibArray firstObject];
        [addCell.addItem setBackgroundColor:[Util colorWithHexString:MAIN_THEME_COLOR]];
        
        [addCell.addItem addTarget:self action:@selector(dismissOrderPlacingViewController:) forControlEvents:UIControlEventTouchUpInside];
        
        return addCell;
    }else{
        NSArray *nibArray = [[NSBundle mainBundle] loadNibNamed:CELL_STATUS_NIB_NAME owner:self options:nil];
        statusCell = [nibArray firstObject];
        statusCell.receiptStatus.text = [Util orderStatusBy:[self.orderFromReceipt[@"status"] unsignedIntegerValue]];
        statusCell.receiptPaymentImageView.image = [Util paymentImageForPaymentType:[self.orderFromReceipt[kPaymentType] unsignedIntegerValue]];
        
        [statusCell shouldPresentToolTipOnTapOnStatusWithContent:self.orderFromReceipt[@"cancelledReason"] inHostView:self.view];
        
        return statusCell;
    }
}

-(UITableViewCell *)handleReceiptCellForIndexPath: (NSIndexPath *)indexPath forTableView: (UITableView *)tableView{
    NSArray *nibArray = [[NSBundle mainBundle] loadNibNamed:CELL_RECEIPT_NIB_NAME owner:self options:nil];
    receiptCell = [nibArray firstObject];
    
    if(!receiptReviewMode){
        receiptCell.subTotal.text = priceByCurrency(orderSubTotal);
        receiptCell.saleTax.text = priceByCurrency(orderTax);
    }else{
        receiptCell.subTotal.text = [Util stringForNumberPrimitiveWithLocale:localeAlongCurrency number:orderSubTotal];
        receiptCell.saleTax.text = [Util stringForNumberPrimitiveWithLocale:localeAlongCurrency number:orderTax];
        [receiptCell.discountTitle setText:NSLocalizedString(@"com.eatus.order.discount", @"Discount")];
    }
    
    if(orderDiscountPercentage == 0.0f){
        receiptCell.discountPercentage.text = NSLocalizedString(@"com.eatus.order.modal.default.none", @"None");
        receiptCell.discount.text = [Util stringForNumberWithLocale:localeAlongCurrency number:[NSNumber numberWithFloat:0.0f]];
    }else{
        receiptCell.discountPercentage.text = [NSString stringWithFormat:@"%.0f%@", orderDiscountPercentage * 100, @"%"];
        receiptCell.discount.text = [NSString stringWithFormat:@"- %@", [Util stringForNumberPrimitiveWithLocale:localeAlongCurrency number:orderDiscount]];
    }
    
    return receiptCell;
}

-(UITableViewCell *)handlePaymentCellForIndexPath: (NSIndexPath *)indexPath forTableView: (UITableView *)tableView{
    NSArray *nibArray = [[NSBundle mainBundle] loadNibNamed:CELL_PAYMENT_NIB_NAME owner:self options:nil];
    paymentCell = [nibArray firstObject];
    [self determinePaymentTypeOnCell:paymentCell];
    
    return paymentCell;
}


-(void)determinePaymentTypeOnCell: (OrderPlacingPaymentTableViewCell *)cell{
    BOOL payment = [self determineCurrentPaymentType];
    
    if(payment){
        cell.paymentImageView.image = [UIImage imageNamed:@"Cash Color"];
        cell.paymentType.text = NSLocalizedString(@"com.eatus.order.cash.usage", @"CashUsage");
    }else{
        NSString *last4Id = [_UserDefault objectForKey:kCurrenCardLast4];
        STPCardBrand cardBrand = [PaymentManager mycardBrandNameKeychainForLast4Id:last4Id];
        cell.paymentImageView.image = [PaymentManager imageForSTPCardBrand:cardBrand];
        cell.paymentType.text = CARD_NUMBER_ENCRYPTED(last4Id);
    }
}

-(NSUInteger)determineCurrentPaymentType{
    NSString *paymentType = [_UserDefault objectForKey:kCurrenPaymentType];
    
    if([paymentType isEqualToString:PAYMENT_CASH]){
        return 1;
    }else if([paymentType isEqualToString:PAYMENT_CARD]){
        return 0;
    }
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    if(indexPath.section == kOrderPlacingDishes){
        SWTableViewCell *cell = [tableView cellForRowAtIndexPath:indexPath];
        [cell showRightUtilityButtonsAnimated:YES];
    }
    
    if(indexPath.section == kOrderPlacingPayment){
        [self dismissViewControllerAnimated:YES completion:^{
            PaymentViewController *paymentViewController = [[PaymentViewController alloc] initWithNibName:@"PaymentView" bundle:nil mode:kPaymentModeFromOrderPlacing];
            mzPresentationControllerPayment = [Util presentMZModalWithViewController:paymentViewController contentSize:MODAL_VIEW_SIZE_ORDER_PLACING];
            [self presentViewController:mzPresentationControllerPayment];
        }];
    }
}


#pragma mark - SWTableCell Delegate methods and Utilties
#pragma mark - Delegate methods
-(BOOL)swipeableTableViewCell:(SWTableViewCell *)cell canSwipeToState:(SWCellState)state{
    switch (state) {
        case kCellStateLeft:
            return NO;
            break;
            
        case kCellStateRight:
            return YES;
            break;
            
        default:
            break;
    }
    return YES;
}

-(void)swipeableTableViewCell:(SWTableViewCell *)cell didTriggerRightUtilityButtonWithIndex:(NSInteger)index{
    switch (index) {
        case 0:{
            //tap on edit dish, let open a modal
            NSIndexPath *indexPathOfEditCell = [self.orderPlacingTableView indexPathForCell:cell];
            if(!indexPathOfEditCell){
                return;
            }
            
            if(indexPathOfEditCell.row > self.orders.count){
                return;
            }
            
            DishEntity *dishNeedToEdit = [self.orders objectAtIndex:indexPathOfEditCell.row];
            OrderViewController *orderViewController = [[OrderViewController alloc] initWithNibName:@"OrderView" bundle:nil mode:kOrderViewModeEditing];
            orderViewController.dishEntity = dishNeedToEdit;
            orderViewController.delegate = self;
            
            mzPresentationControllerEditDish = [Util presentMZModalWithViewController:orderViewController contentSize:MODAL_VIEW_SIZE_ORDER_PLACING];
            [self presentViewController:mzPresentationControllerEditDish];
        }
            break;
            
        case 1:{
            //tap on delete dish
            NSIndexPath *indexPathRemove = [orderPlacingTableView indexPathForCell:cell];
            
            NSArray *dishes = [serializer deserializeToDishEntity];
            if([dishes count] == 1){
                AMSmoothAlertView *alertView = [AlertViewManager smoothDropAlertViewWithTitle:NSLocalizedString(@"com.eatus.alert.orderPlacing.last.item.title", @"LastItem") content:NSLocalizedString(@"com.eatus.alert.orderPlacing.last.item.content", @"LastItemContent") cancelButton:YES color:COLOR_MAIN_THEME alertType:AlertInfo onCompletion:^(AMSmoothAlertView *alert, UIButton *button){
                    if([button isEqual:alert.defaultButton]){
                        [self.orders removeObjectAtIndex:indexPathRemove.row];
                        [serializer removeDishAtIndex:indexPathRemove.row];
                        [self reloadAllDishInSectionZero];
                        
                        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.5f * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                            [self dismissViewControllerAnimated:YES completion:^{
                                
                            }];
                        });
                    }else{
                        [alert dismissAlertView];
                    }
                }];
                [alertView.logoView setImage:[UIImage imageNamed:@"Cancel"]];
                [alertView.defaultButton setTitle:NSLocalizedString(@"com.eatus.alert.orderPlacing.removeItem.positive.buton.title", @"RemoveItem")     forState:UIControlStateNormal];
                [alertView.cancelButton setTitle:NSLocalizedString(@"com.eatus.alert.orderPlacing.locateTable.negative.button.title", @"Cancel") forState:UIControlStateNormal];
                
                [alertView show];
            }else{
                [self.orders removeObjectAtIndex:indexPathRemove.row];
                [serializer removeDishAtIndex:indexPathRemove.row];
                [self reloadAllDishInSectionZero];
            }
            
        }
            break;
            
        default:
            break;
    }
}

-(BOOL)swipeableTableViewCellShouldHideUtilityButtonsOnSwipe:(SWTableViewCell *)cell{
    return YES;
}

-(NSArray *)prepareRightUtilitiesButton{
    NSMutableArray *rightUtilitiesButton = [[NSMutableArray alloc] init];
    [rightUtilitiesButton sw_addUtilityButtonWithColor:[Util colorWithHexString:MAIN_THEME_COLOR_LIGHT_LV_2] icon:[UIImage imageNamed:@"Pencil"]];
    [rightUtilitiesButton sw_addUtilityButtonWithColor:[UIColor colorWithRed:1.0f green:0.231f blue:0.188 alpha:1.0f] icon:[UIImage imageNamed:@"Trash"]];    
    return rightUtilitiesButton;
}

-(void)reloadAllDishInSectionZero{
    NSRange rangeDish = NSMakeRange(0, 1);
    NSRange rangeReceipt = NSMakeRange(0, 3);
    
    NSIndexSet *sectionDish = [NSIndexSet indexSetWithIndexesInRange:rangeDish];
    NSIndexSet *sectionReceipt = [NSIndexSet indexSetWithIndexesInRange:rangeReceipt];
    
    [self prepareAllCalculationByDiscountPercentage:orderDiscountPercentage];
    
    [orderPlacingTableView reloadSections:sectionReceipt withRowAnimation:UITableViewRowAnimationFade];
    [orderPlacingTableView reloadSections:sectionDish withRowAnimation:UITableViewRowAnimationFade];
}


#pragma mark - Order Delegate methods
-(void)didTapOnDismissOrderViewControllerInMZPresentation{
    if(mzPresentationControllerEditDish){
        [mzPresentationControllerEditDish dismissViewControllerAnimated:YES completion:^{
            
        }];
    }
}

-(void)didTapOnDoneInEditModeWithDishEntity:(DishEntity *)editedDish{
    [mzPresentationControllerEditDish dismissViewControllerAnimated:YES completion:^{
        [serializer smartAppendDishInEditMode:editedDish];
        
        [self.orders removeAllObjects];
        self.orders = [[serializer deserializeToDishEntity] mutableCopy];
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.1f * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
            [self reloadAllDishInSectionZero];
        });
    }];
}


#pragma mark - IBAction Methods
-(void)dismissOrderPlacingViewController:(id)sender{
    [self dismissViewControllerAnimated:YES completion:nil];
}

-(void)placeOrder:(id)sender{
    NSNumber *shouldUseTouchId = [_UserDefault objectForKey:kShouldAuthTouchIdForPlaceOrder];
    
    if(shouldUseTouchId && [shouldUseTouchId boolValue]){
        [[EHFAuthenticator sharedInstance] setReason:NSLocalizedString(@"com.eatus.alert.orderPlacing.auth.touchId.reason", @"Reason")];
        [[EHFAuthenticator sharedInstance] authenticateWithSuccess:^{
            [self pushOrderToRestaurant];
        }andFailure:^(LAError error){
            
        }];
    }else if((shouldUseTouchId && ![shouldUseTouchId boolValue]) || !shouldUseTouchId){
        AMSmoothAlertView *alertView = [AlertViewManager smoothDropAlertViewWithTitle:NSLocalizedString(@"com.eatus.alert.orderPlacing.sendOrder.title", @"OrderSending")   content:NSLocalizedString(@"com.eatus.alert.orderPlacing.sendOrder.content", @"SendOrderContent") cancelButton:YES color:COLOR_MAIN_THEME alertType:AlertInfo onCompletion:^(AMSmoothAlertView *alert, UIButton *button){
            if([button isEqual:alert.defaultButton]){
                [self pushOrderToRestaurant];
            }else if([button isEqual:alert.cancelButton]){
                [alert dismissAlertView];
            }
        }];
        [alertView.defaultButton setTitle:NSLocalizedString(@"com.eatus.alert.orderPlacing.sendOrder.positive.button.title", @"SendOrder") forState:UIControlStateNormal];
        [alertView.cancelButton setTitle:NSLocalizedString(@"com.eatus.alert.orderPlacing.locateTable.negative.button.title", @"CancelSendOrder") forState:UIControlStateNormal];
        [alertView.logoView setImage:[UIImage imageNamed:@"SendOrder"]];
        
        [alertView show];
        
    }
}

-(void)pushOrderToRestaurant{
    [_Shared showOverlayIndetermineSmalProgressViewWithTitle:NSLocalizedString(@"com.eatus.notification.orderPlacing.sendOrder", @"SendOrderNotification") style:kOverlayElegantStyle tintColor:COLOR_MAIN_THEME onView:self.view];
    
    [ServiceAPIManager createOrderRequest:[serializer packJsonByStringWithPromotionPercentage:orderDiscountPercentage tax:orderTax] withRestaurantId:restaurantId atTableId:restaurantTable.objectId withTableName:restaurantTable.name paymentType:[self determineCurrentPaymentType] onCompletion:^(BOOL succeeded, NSError *error){
        
        [_Shared hideOverlayIndetermineProgressViewAfter:0.0f];
        
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1.0f * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
            if(succeeded){
                [_Shared showSucceedCheckMarkProcessViewWithTitle:@"" tintColor:COLOR_MAIN_THEME onView:self.view];
                
                dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(2.0f * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                    [self dismissViewControllerAnimated:YES completion:nil];
                });
                
                //check in current user
                [ParseDataManager logMeAtRestaurantInBackground:currentRestaurant];
                
                //update application context for queue up to transfer to watch
                [self.connectivityManager sendMessageToWatchWithApplicationContextWithMessage:[serializer packedJsonWithPromotionPercentageForTransferWatch:orderDiscountPercentage tax:orderTax atRestaurant:restaurantName] onCompletion:^(BOOL succeeded, NSError *error){
                }];
            }else{
                [_Shared showFailureCrossProcessViewWithTitle:@"" tintColor:COLOR_MAIN_THEME_FAILURE_RED onView:self.view];
            }
        });
    }];
}


#pragma mark - Utilities
-(NSString *)buildOptionsFromOptionCollection: (NSArray *)options{
    NSString *buildStr;
    NSString *finalStr = [[NSString alloc] init];
    for(DishOptionsEntity *dishOptions in options){
        NSInteger index = [options indexOfObject:dishOptions];
        buildStr = [[NSString alloc] init];
        buildStr = [buildStr stringByAppendingFormat:@"%@: ", dishOptions.optionName];
        
        BOOL notFirstTime = NO;
        BOOL didChecked = NO;
        for(DishOptionEntity *optionEntity in dishOptions.options){
            if(optionEntity.selected){
                if(!notFirstTime){
                    buildStr = optionEntity.dishOptionAddOnPrice==0?[buildStr stringByAppendingFormat:@"%@", optionEntity.dishOptionName]:[buildStr stringByAppendingFormat:@"%@ (+%@)", optionEntity.dishOptionName, priceByCurrency(optionEntity.dishOptionAddOnPrice)];
                    notFirstTime = YES;
                }else{
                    buildStr = optionEntity.dishOptionAddOnPrice==0?[buildStr stringByAppendingFormat:@", %@", optionEntity.dishOptionName]:[buildStr stringByAppendingFormat:@", %@ (+%@)", optionEntity.dishOptionName, priceByCurrency(optionEntity.dishOptionAddOnPrice)];
                }
                didChecked = YES;
            }
        }
        if(!didChecked){
            buildStr = [buildStr stringByAppendingString:@" None"];
        }
        
        finalStr = index == 0?[finalStr stringByAppendingFormat:@"%@", buildStr]:[finalStr stringByAppendingFormat:@"\n%@", buildStr];
    }
    return finalStr;
}

@end
