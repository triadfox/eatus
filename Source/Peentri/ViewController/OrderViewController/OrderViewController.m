//
//  OrderViewController.m
//  Light Order
//
//  Created by Phong Nguyen on 10/3/15.
//  Copyright © 2015 Triad Fox. All rights reserved.
//

#import "OrderViewController.h"
#import "OptionsDetailViewController.h"
#import "OrderOptionsTableViewCell.h"
#import "DishOptionsEntity.h"
#import "DishOptionEntity.h"
#import "AKPickerView.h"
#import "MenuEngineManager.h"

#define QUANTITY_OPTION             @"Quantity"

@interface OrderViewController ()<AKPickerViewDataSource, AKPickerViewDelegate>{
    BOOL _notFirstTime;
    BOOL editMode;
    NSMutableDictionary *optionDictionary;
    NSArray *quantityCollection;
    
    NSInteger selectedQuantity;
    NSInteger reservedQuantity;
    NSInteger quantityLimited;
}

@property(nonatomic, retain) OptionsDetailViewController *optionsDetailViewController;
@property (nonatomic, strong) AKPickerView *pickerView;

@end

@implementation OrderViewController
@synthesize dishEntity;

-(id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil mode:(OrderViewModes)mode{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if(self){
        if(mode == kOrderViewModeEditing){
            editMode = YES;
        }
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    [self prepareHorizontalSeparator];
    self.orderNameTitle.text = dishEntity.dishName;
    [self settingUpThemeColor];
    
    [self determineAllOptionsAtBeginning];
    [self prepareTargetAddDishToOrder];
    [self prepareMenu];
    
    [self changeBottomButtonAsFitEditMode];
    
    self.orderOptionTitle.text = NSLocalizedString(@"com.eatus.order.modal.overview.title", "overviewMsg");
}

-(void)viewDidDisappear:(BOOL)animated{
    if(editMode){
        [self cleanUpAndSweepEditMode];
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

-(void)cleanUpAndSweepEditMode{
    editMode = NO;
}

-(void)settingUpThemeColor{
    [self.bottomButton setBackgroundColor:[Util colorWithHexString:MAIN_THEME_COLOR]];
}

-(void)prepareHorizontalSeparator{
    [self.orderHorizontalSeparator setBackgroundColor:[Util colorWithHexString:MAIN_HORIZONTAL_SEPARATOR_SILVER]];
    [self.orderHorizontalSeparator.layer setCornerRadius:3.0f];
    [self.orderHorizontalSeparator.layer setMasksToBounds:YES];
}

-(void)prepareMenu{
    quantityLimited = [[MenuEngineManager shareManager] getQuantityLimited];
}


#pragma mark - IBAction methods
-(void)addDishToOrder:(id)sender{
    [self calculateQuantityBeforeAddToOrder];
    if(editMode){
        [self.delegate didTapOnDoneInEditModeWithDishEntity:self.dishEntity];
    }else{
        [self.delegate didTapOnAddToOrderAndDismissMZPresentation:self.dishEntity];
    }
}

-(void)calculateQuantityBeforeAddToOrder{
    [self.dishEntity setDishQuantity:selectedQuantity];
}

-(void)dismissOrderViewControllerFromMZPresentation:(id)sender{
    [self.delegate didTapOnDismissOrderViewControllerInMZPresentation];
}


#pragma mark - Table View Delegate methods
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return [dishEntity.dishOptions count] + 1;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    static NSString *identifier = @"OrderOptionsCellIdentifier";
    OrderOptionsTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:identifier];
    if(!cell){
        NSArray *nibArray = [[NSBundle mainBundle] loadNibNamed:@"OrderOptionsTableViewCell" owner:self options:nil];
        cell = [nibArray firstObject];
    }
    
    DishOptionsEntity *optionsEntity;
    if(indexPath.row == 0){
        cell.optionTitle.text = NSLocalizedString(@"com.eatus.store.option.quantity", "Quantity");
        quantityCollection = [self generateCollectionOfQuantityLimited];
        [self initQuantityPickerWithFrame:cell.optionsSelected.frame onView:cell];
        [cell.optionsSelected setHidden:YES];
        [self determineQuantityInEditMode];
    }else{
        
        [self disableCellForEditMode:cell];
        
        optionsEntity = [dishEntity.dishOptions objectAtIndex:indexPath.row-1];
        
        if(editMode){
            cell.optionTitle.text = optionsEntity.optionName;
            cell.optionsSelected.text = [self prepareOpionsInEditMode:optionsEntity];
        }else{
            if(!_notFirstTime){
                cell.optionTitle.text = optionsEntity.optionName;
                cell.optionsSelected.text = [self prepareDefaultOptions:optionsEntity];
                if(indexPath.row == [dishEntity.dishOptions count] + 1){
                    _notFirstTime = YES;
                }
            }else if(_notFirstTime){
                cell.optionTitle.text = optionsEntity.optionName;
                if(![self determineFirstTimeOrNot:optionsEntity.optionName]){
                    cell.optionsSelected.text = [self prepareSelectedOptions:optionsEntity];
                }else{
                    cell.optionsSelected.text = [self prepareDefaultOptions:optionsEntity];
                }
            }
        }
    }
    
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    DishOptionsEntity *optionsEntity;
    
    if(indexPath.row == 0){
        return;
    }else{
        optionsEntity = [dishEntity.dishOptions objectAtIndex:indexPath.row-1];
    }
    
    [self animationChangeOptionsTitleWithContent:optionsEntity.optionName];
    
    [self.delegate didSelectOnCellWithDishOption:optionsEntity firstTime:[self determineFirstTimeOrNot:optionsEntity.optionName] exclusiveSelection:optionsEntity.optionExclusiveSelection];
    
    [self reflectOptionsAtTheSecond:optionsEntity];
}

-(void)disableCellForEditMode: (OrderOptionsTableViewCell *)cell{
    if(editMode){
        if(cell){
            cell.userInteractionEnabled = NO;
            cell.optionsSelected.enabled = NO;
            cell.optionTitle.enabled = NO;
        }
    }
}


#pragma mark - Quantity Picker View
-(void)initQuantityPickerWithFrame: (CGRect)frame onView: (UIView *)view{
    self.pickerView = [[AKPickerView alloc] initWithFrame:frame];
    self.pickerView.delegate = self;
    self.pickerView.dataSource = self;
    self.pickerView.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
    [view addSubview:self.pickerView];

    self.pickerView.font = [UIFont fontWithName:@"HelveticaNeue-Light" size:20];
    self.pickerView.highlightedFont = [UIFont fontWithName:@"HelveticaNeue" size:20];
    self.pickerView.highlightedTextColor = [UIColor orangeColor];
    
    self.pickerView.interitemSpacing = 44.0f;
    self.pickerView.fisheyeFactor = 0.001;
    self.pickerView.pickerViewStyle = AKPickerViewStyle3D;
    self.pickerView.maskDisabled = false;
    
    selectedQuantity = 1;
    [self.pickerView reloadData];
}

-(NSUInteger)numberOfItemsInPickerView:(AKPickerView *)pickerView{
    return [quantityCollection count];
}

-(NSString *)pickerView:(AKPickerView *)pickerView titleForItem:(NSInteger)item{
    return [quantityCollection objectAtIndex:item];
}

-(void)pickerView:(AKPickerView *)pickerView didSelectItem:(NSInteger)item{
    selectedQuantity = [[quantityCollection objectAtIndex:item] integerValue];
}

-(void)dismissQuantityPickerView{
    [self.pickerView removeFromSuperview];
    reservedQuantity = selectedQuantity;
}

-(void)resumeQuantityPickerView{
    if(self.pickerView){
        [self.pickerView selectItem:[quantityCollection indexOfObject:[@(reservedQuantity) stringValue]] animated:YES];
    }
}

-(void)determineQuantityInEditMode{
    if(editMode){
        [self.pickerView selectItem:[quantityCollection indexOfObject:[@(dishEntity.dishQuantity) stringValue]] animated:YES];
    }
}


#pragma mark - Data Fetching, Filtering & Preparing
-(BOOL)determineFirstTimeOrNot: (NSString *)optionName{
    return [[optionDictionary objectForKey:optionName] boolValue];
}

-(void)determineAllOptionsAtBeginning{
    optionDictionary = [[NSMutableDictionary alloc] init];
    for(DishOptionsEntity *options in dishEntity.dishOptions){
        [optionDictionary setObject:[NSNumber numberWithBool:YES] forKey:options.optionName];
    }
    [optionDictionary setObject:[NSNumber numberWithBool:YES] forKey:QUANTITY_OPTION];
}

-(void)reflectOptionsAtTheSecond: (DishOptionsEntity *)dishOptions{
    for(NSString *key in optionDictionary.allKeys){
        if([key isEqualToString:dishOptions.optionName]){
            [optionDictionary setObject:[NSNumber numberWithBool:NO] forKey:dishOptions.optionName];
        }
    }
}

-(NSString *)prepareDefaultOptions: (DishOptionsEntity *)optionsEntity{
    NSString *defaultOptions = [[NSString alloc] init];
    NSArray *allOptions = optionsEntity.options;
    NSString *appendingFormat;
    
    BOOL firstOption = NO;
    
    for(DishOptionEntity *option in allOptions){
        if(option.dishOptionItemType == kOptionItemDefault){
            if(!firstOption){
                appendingFormat = @"%@";
                firstOption = YES;
            }else{
                appendingFormat = @", %@";
            }
            defaultOptions = [defaultOptions stringByAppendingFormat:appendingFormat, option.dishOptionName];
        }
    }
    
    if(defaultOptions.length == 0)
        defaultOptions = NSLocalizedString(@"com.eatus.order.modal.default.none", @"NoneDefault");
    
    return defaultOptions;
}

-(NSString *)prepareSelectedOptions: (DishOptionsEntity *)optionsEntity{
    NSString *selectedOptions = [[NSString alloc] init];
    NSArray *allOptions = optionsEntity.options;
    NSString *appendingFormat;
    BOOL firstOption;
    
    for(DishOptionEntity *option in allOptions){
        if(option.selected){
            if(!firstOption){
                appendingFormat = @"%@";
                firstOption = YES;
            }else{
                appendingFormat = @", %@";
            }
            selectedOptions = [selectedOptions stringByAppendingFormat:appendingFormat, option.dishOptionName];
        }
    }
    
    if(selectedOptions.length == 0)
        selectedOptions = NSLocalizedString(@"com.eatus.order.modal.default.none", @"NoneDefault");
    
    return selectedOptions;
}

-(NSString *)prepareOpionsInEditMode: (DishOptionsEntity *)optionsEntity{
    NSString *selectedOptions = [[NSString alloc] init];
    NSArray *allOptions = optionsEntity.options;
    NSString *appendingFormat;
    BOOL firstOption = NO;
    
    for(DishOptionEntity *option in allOptions){
        if(!firstOption){
            appendingFormat = @"%@";
            firstOption = YES;
        }else{
            appendingFormat = @", %@";
        }
        selectedOptions = [selectedOptions stringByAppendingFormat:appendingFormat, option.dishOptionName];
    }
    
    if(selectedOptions.length == 0)
        selectedOptions = @"None";
    
    return selectedOptions;
}

-(void)fetchDataFromSelectedOptions: (DishOptionsEntity *)options{
    NSMutableArray *copied = [dishEntity.dishOptions mutableCopy];
    for(DishOptionsEntity *opt in copied){
        if([opt.optionName isEqualToString:options.optionName]){
            NSInteger index = [copied indexOfObject:opt];
            [copied replaceObjectAtIndex:index withObject:options];
            break;
        }
        dishEntity.dishOptions = [[NSArray alloc] initWithArray:copied];
    }
    
    [self.orderTableView reloadData];
    _notFirstTime = YES;
}


#pragma mark - Animation Handler
#pragma mark - Animation Hander for Bottom Button
//animation handle bottom button (Add to Order or Back from Options)
-(void)changeBottomButtonAsFitOptionDetail{
    [UIView animateWithDuration:0.9f delay:0.0f options:UIViewAnimationOptionAllowAnimatedContent animations:^{
        CATransition *transition = [CATransition animation];
        transition.type = kCATransitionFade;
        transition.subtype = kCATransitionFromRight;
        transition.duration = 0.9f;
        transition.speed = 1.0f;
        transition.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut];
        [self.bottomButton.layer addAnimation:transition forKey:ANIMATION_KEY_BOTTOM_BUTTON_CHANGE_TO_OPTION];
    }completion:^(BOOL finished){
        [self.bottomButton setTitle:@"" forState:UIControlStateNormal];
        [self.bottomButton setTintColor:[UIColor whiteColor]];
        [self.bottomButton setImage:[UIImage imageNamed:@"Left"] forState:UIControlStateNormal];
        //change event on touch up, instead of dismiss modal view (container), it transition back to order view controller (self)
        [self removeTargetAddDishToOrder];
        [self.bottomButton addTarget:self action:@selector(transitionMoveBackToOrder) forControlEvents:UIControlEventTouchUpInside];
        [self dismissQuantityPickerView];
    }];
}

-(void)changeBottomButtonFitRevert{
    [self showDismissButton];
    [UIView animateWithDuration:0.9f delay:0.0f options:UIViewAnimationOptionAllowAnimatedContent animations:^{
        CATransition *transition = [CATransition animation];
        transition.type = kCATransitionFade;
        transition.subtype = kCATransitionFromLeft;
        transition.duration = 0.9f;
        transition.speed = 1.0f;
        transition.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut];
        [self.bottomButton.layer addAnimation:transition forKey:ANIMATION_KEY_BOTTOM_BUTTON_BACK_TO_ORDER];
    }completion:^(BOOL finished){
        [self.bottomButton setTitle:NSLocalizedString(@"com.eatus.order.modal.bottom.button.add.title", "addToOrderMsg") forState:UIControlStateNormal];
        [self.bottomButton setImage:nil forState:UIControlStateNormal];
        [self prepareTargetAddDishToOrder];
        [self resumeQuantityPickerView];
    }];
}

-(void)changeBottomButtonAsFitEditMode{
    if(editMode){
        [UIView animateWithDuration:0.4f delay:0.0f options:UIViewAnimationOptionTransitionCrossDissolve animations:^{
            [self.bottomButton setTitle:NSLocalizedString(@"com.eatus.order.modal.bottom.button.done.title", "doneMsg") forState:UIControlStateNormal];
        }completion:^(BOOL finished){
            [self prepareTargetAddDishToOrder];
        }];
    }else{
        [self.bottomButton setTitle:NSLocalizedString(@"com.eatus.order.modal.bottom.button.add.title", "addMsg") forState:UIControlStateNormal];
    }
}

-(void)prepareTargetAddDishToOrder{
    [self.bottomButton addTarget:self action:@selector(addDishToOrder:) forControlEvents:UIControlEventTouchUpInside];
}

-(void)removeTargetAddDishToOrder{
    [self.bottomButton removeTarget:self action:@selector(addDishToOrder:) forControlEvents:UIControlEventTouchUpInside];
}


#pragma mark - Animation Handler for Options Title
-(void)animationChangeOptionsTitleWithContent:(NSString *)content{
    [UIView animateWithDuration:0.9f delay:0.0f options:UIViewAnimationOptionAllowAnimatedContent animations:^{
        CATransition *transition = [CATransition animation];
        transition.duration = 0.5f;
        transition.type = kCATransitionPush;
        transition.subtype = kCATransitionFromRight;
        transition.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut];
        
        [self.orderOptionTitle.layer addAnimation:transition forKey:ANIMATION_KEY_OPTION_TITLE_CHANGE_VALUE];
    }completion:^(BOOL finished){
        self.orderOptionTitle.text = content;
    }];
}

-(void)animationRevertOptionTitleContent{
    [UIView animateWithDuration:0.9f delay:0.0f options:UIViewAnimationOptionAllowAnimatedContent animations:^{
        CATransition *transition = [CATransition animation];
        transition.duration = 0.5f;
        transition.type = kCATransitionPush;
        transition.subtype = kCATransitionFromLeft;
        transition.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut];
        
        [self.orderOptionTitle.layer addAnimation:transition forKey:ANIMATION_KEY_OPTION_TITLE_CHANGE_VALUE];
    }completion:^(BOOL finished){
        self.orderOptionTitle.text = NSLocalizedString(@"com.eatus.order.modal.overview.title", "overviewMsg");
    }];
}


#pragma mark - Animation Handler Transition
-(void)transitionMoveBackToOrder{
    __block CGPoint restorePoint;
    [UIView transitionWithView:self.orderTableView duration:0.5f options:UIViewAnimationOptionAllowAnimatedContent animations:^{
        
        CABasicAnimation *animation = [CABasicAnimation animation];
        
        [CATransaction begin];
        
        animation.keyPath = @"transform.translation";
        animation.byValue = [NSValue valueWithCGPoint:CGPointMake(1000.0f, 0.0f)];
        animation.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseOut];
        animation.duration = 0.9f;
        animation.speed = 0.9f;
        animation.fillMode = kCAFillModeForwards;
        animation.removedOnCompletion = NO;
        
        restorePoint = CGPointMake(self.optionsDetailViewController.view.frame.origin.x, self.optionsDetailViewController.view.frame.origin.y);
        [CATransaction commit];
        
        [self.optionsDetailViewController.view.layer addAnimation:animation forKey:ANIMATION_KEY_OPTION_MOVE_OUT];
        
        [CATransaction setCompletionBlock:^{
            [self.optionsDetailViewController willMoveToParentViewController:nil];
            [self.optionsDetailViewController.view removeFromSuperview];
            [self.optionsDetailViewController removeFromParentViewController];
            [self.optionsDetailViewController didMoveToParentViewController:nil];
        }];
        
    }completion:^(BOOL finished){
        
        CABasicAnimation *anim = [CABasicAnimation animation];
        anim.keyPath = @"transform.translation";
        anim.toValue = [NSValue valueWithCGPoint:restorePoint];
        anim.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseIn];
        anim.duration = 0.5f;
        anim.speed = 1.0f;
        anim.fillMode = kCAFillModeForwards;
        anim.removedOnCompletion = NO;
        
        [self.orderTableView.layer addAnimation:anim forKey:ANIMATION_KEY_ORDER_TABLE_MOVE_IN];
        
        [self changeBottomButtonFitRevert];
        
        [self animationRevertOptionTitleContent];
        
        [self.delegateO didTurnBackFromOptionSelectionWithCompletion:^(DishOptionsEntity *dishOptions){
            [self fetchDataFromSelectedOptions:dishOptions];
        }];
    }];
}

-(void)transitionToOptionDetail:(OptionsDetailViewController *)optionsDetailViewController{
    __block CGPoint destinationPoint;
    [UIView transitionWithView:optionsDetailViewController.view duration:0.5f options:UIViewAnimationOptionAllowAnimatedContent animations:^{
        
        [optionsDetailViewController.view setFrame:self.orderContentView.bounds];
        
        [CATransaction begin];
        
        CABasicAnimation *animation = [CABasicAnimation animation];
        animation.delegate = self;
        animation.keyPath = @"transform.translation";
        animation.byValue = [NSValue valueWithCGPoint:CGPointMake(-1000.0f, 0.0f)];
        animation.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseOut];
        animation.duration = 0.9f;
        animation.speed = 0.9f;
        animation.fillMode = kCAFillModeForwards;
        animation.removedOnCompletion = NO;
        
        destinationPoint = self.orderTableView.frame.origin;
        
        [CATransaction commit];
        
        [self.orderTableView.layer addAnimation:animation forKey:ANIMATION_KEY_ORDER_TABLE_MOVE_OUT];
        
    }completion:^(BOOL finished){
        self.delegateO = optionsDetailViewController;
        [optionsDetailViewController willMoveToParentViewController:self];
        [self addChildViewController:optionsDetailViewController];
        
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.2f * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
            CATransition *transition = [CATransition animation];
            transition.type = kCATransitionPush;
            transition.subtype = kCATransitionFromRight;
            transition.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseIn];
            [optionsDetailViewController.view.layer addAnimation:transition forKey:ANIMATION_KEY_OPTION_MOVE_IN];
        
        [self.orderContentView addSubview:optionsDetailViewController.view];
        
        //change bottom button with appeariate and event on touch up
        [self changeBottomButtonAsFitOptionDetail];
        
        //hide dismiss button at the top-right corner
        [self hideDismissButton];
        
        [self.optionsDetailViewController didMoveToParentViewController:self];
        
        self.optionsDetailViewController = optionsDetailViewController;
        });
    }];
}


#pragma mark - Animation Utilities
-(void)hideDismissButton{
    [UIView transitionWithView:self.orderDismiss duration:0.3f options:UIViewAnimationOptionTransitionFlipFromBottom animations:^{
        self.orderDismiss.hidden = YES;
    }completion:^(BOOL finished){
        
    }];
    
}

-(void)showDismissButton{
    [UIView transitionWithView:self.orderDismiss duration:0.3f options:UIViewAnimationOptionTransitionFlipFromTop animations:^{
        self.orderDismiss.hidden = NO;
    }completion:^(BOOL finished){
        
    }];
}

-(void)removeAllAnimationOnLayers{
    [self.orderTableView.layer removeAllAnimations];
    if(self.optionsDetailViewController.view.layer){
        [self.optionsDetailViewController.view.layer removeAllAnimations];
    }
}

-(NSArray *)generateCollectionOfQuantityLimited{
    NSMutableArray *collection = [[NSMutableArray alloc] init];
    for(NSInteger iVal = 1; iVal <= quantityLimited; iVal++){
        [collection addObject:[NSString stringWithFormat:@"%ld", (long)iVal]];
    }
    return collection;
}
@end
