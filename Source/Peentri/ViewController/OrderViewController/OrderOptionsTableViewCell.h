//
//  OrderOptionsTableViewCell.h
//  Light Order
//
//  Created by Phong Nguyen on 10/4/15.
//  Copyright © 2015 Triad Fox. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface OrderOptionsTableViewCell : UITableViewCell

@property(nonatomic, strong) IBOutlet UILabel *optionTitle;
@property(nonatomic, strong) IBOutlet UILabel *optionsSelected;

@end
