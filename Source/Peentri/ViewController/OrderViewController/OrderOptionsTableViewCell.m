//
//  OrderOptionsTableViewCell.m
//  Light Order
//
//  Created by Phong Nguyen on 10/4/15.
//  Copyright © 2015 Triad Fox. All rights reserved.
//

#import "OrderOptionsTableViewCell.h"

@implementation OrderOptionsTableViewCell

- (void)awakeFromNib {
    self.selectionStyle = UITableViewCellSelectionStyleNone;
    [self.optionTitle setTextColor:[Util colorWithHexString:MAIN_THEME_COLOR]];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
}

@end
