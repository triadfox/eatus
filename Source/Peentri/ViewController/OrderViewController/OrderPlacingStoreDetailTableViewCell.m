//
//  OrderPlacingOrderDetailTableViewCell.m
//  Eatus
//
//  Created by Phong Nguyen on 11/18/15.
//  Copyright © 2015 Triad Fox. All rights reserved.
//

#import "OrderPlacingStoreDetailTableViewCell.h"

@implementation OrderPlacingStoreDetailTableViewCell

-(void)awakeFromNib{
    self.selectionStyle = UITableViewCellSelectionStyleNone;
}

-(void)setSelected:(BOOL)selected animated:(BOOL)animated{
    [super setSelected:selected animated:animated];
}

@end
