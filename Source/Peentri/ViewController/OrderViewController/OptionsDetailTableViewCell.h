//
//  OrderDetailTableViewCell.h
//  Light Order
//
//  Created by Phong Nguyen on 10/8/15.
//  Copyright © 2015 Triad Fox. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface OptionsDetailTableViewCell : UITableViewCell

@property(nonatomic, strong) IBOutlet UILabel *detailsTitle;
@property(nonatomic, strong) IBOutlet UIImageView *detailCheck;

@end
