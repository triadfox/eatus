//
//  OrderPlacingReceiptTableViewCell.h
//  Light Order
//
//  Created by Phong Nguyen on 10/11/15.
//  Copyright © 2015 Triad Fox. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface OrderPlacingReceiptTableViewCell : UITableViewCell

@property(nonatomic, strong) IBOutlet UILabel *subTotal;
@property(nonatomic, strong) IBOutlet UILabel *saleTax;
@property(nonatomic, strong) IBOutlet UILabel *discount;
@property(nonatomic, strong) IBOutlet UILabel *discountPercentage;
@property(nonatomic, strong) IBOutlet UILabel *discountTitle;

@property(nonatomic, strong) IBOutlet UILabel *subTotalLabel;
@property(nonatomic, strong) IBOutlet UILabel *saleTaxLabel;
@property(nonatomic, strong) IBOutlet UILabel *discountLabel;

@end
