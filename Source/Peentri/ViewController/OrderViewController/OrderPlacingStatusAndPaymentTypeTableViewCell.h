//
//  OrderPlacingStatusTableViewCell.h
//  Eatus
//
//  Created by Phong Nguyen on 11/17/15.
//  Copyright © 2015 Triad Fox. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface OrderPlacingStatusAndPaymentTypeTableViewCell : UITableViewCell

@property(nonatomic, weak) IBOutlet UILabel *receiptStatus;
@property(nonatomic, weak) IBOutlet UIImageView *receiptPaymentImageView;

-(void)shouldPresentToolTipOnTapOnStatusWithContent: (NSString *)content inHostView: (UIView *)hostView;

@end