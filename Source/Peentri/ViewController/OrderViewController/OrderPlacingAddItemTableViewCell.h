//
//  OrderPlacingAddItemTableViewCell.h
//  Light Order
//
//  Created by Phong Nguyen on 10/11/15.
//  Copyright © 2015 Triad Fox. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface OrderPlacingAddItemTableViewCell : UITableViewCell

@property(nonatomic, strong) IBOutlet UIButton *addItem;

@end
