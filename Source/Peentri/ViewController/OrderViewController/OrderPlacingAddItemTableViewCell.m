//
//  OrderPlacingAddItemTableViewCell.m
//  Light Order
//
//  Created by Phong Nguyen on 10/11/15.
//  Copyright © 2015 Triad Fox. All rights reserved.
//

#import "OrderPlacingAddItemTableViewCell.h"

#import "UIButton+EdgeInset.h"

@implementation OrderPlacingAddItemTableViewCell

- (void)awakeFromNib {
    self.selectionStyle = UITableViewCellSelectionStyleNone;
    
    [self setBackgroundColor:[UIColor groupTableViewBackgroundColor]];
    
    [self.addItem setTitle:NSLocalizedString(@"com.eatus.order.placing.addItem.button.title", @"AddItem") forState:UIControlStateNormal];
    [self.addItem spacingBetweenImageAndTitleWith:5.0f sizeToFit:NO];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
}

@end
