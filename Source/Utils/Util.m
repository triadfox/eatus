//
//  Utils.m
//  MiPoBeacon
//
//  Created by Phong Nguyen on 8/29/15.
//  Copyright (c) 2015 MiPo Consolidate LLC. All rights reserved.
//

#import "Util.h"
#import <CoreFoundation/CoreFoundation.h>
#import <ImageIO/ImageIO.h>
#import <LocalAuthentication/LocalAuthentication.h>
#import <MobileCoreServices/MobileCoreServices.h>
#import <Sys/sysctl.h>
#import <dlfcn.h>

@implementation Util

+(void)dropShadowForView:(UIView *)view{
    UIBezierPath *shadowPath = [UIBezierPath bezierPathWithRect:view.bounds];
    view.layer.masksToBounds = YES;
    view.layer.shadowColor = [UIColor blackColor].CGColor;
    view.layer.shadowOffset = CGSizeMake(0.2f, 0.5f);
    view.layer.shadowOpacity = 0.5f;
    view.layer.shadowPath = shadowPath.CGPath;
}

+(UIColor*)colorWithHexString:(NSString*)hex
{
    NSString *cString = [[hex stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]] uppercaseString];
    
    // String should be 6 or 8 characters
    if ([cString length] < 6) return [UIColor grayColor];
    
    // strip 0X if it appears
    if ([cString hasPrefix:@"0X"]) cString = [cString substringFromIndex:2];
    
    if ([cString length] != 6) return  [UIColor grayColor];
    
    // Separate into r, g, b substrings
    NSRange range;
    range.location = 0;
    range.length = 2;
    NSString *rString = [cString substringWithRange:range];
    
    range.location = 2;
    NSString *gString = [cString substringWithRange:range];
    
    range.location = 4;
    NSString *bString = [cString substringWithRange:range];
    
    // Scan values
    unsigned int r, g, b;
    [[NSScanner scannerWithString:rString] scanHexInt:&r];
    [[NSScanner scannerWithString:gString] scanHexInt:&g];
    [[NSScanner scannerWithString:bString] scanHexInt:&b];
    
    return [UIColor colorWithRed:((float) r / 255.0f)
                           green:((float) g / 255.0f)
                            blue:((float) b / 255.0f)
                           alpha:1.0f];
}

+ (void)roundButton: (UIButton *)button background: (UIColor *)backgroundColor borderColor: (UIColor *)borderColor borderWith:(CGFloat)borderWidth{
    button.clipsToBounds = YES;
    button.layer.cornerRadius = button.frame.size.width/2.0f;
    if(backgroundColor){
        button.layer.backgroundColor = backgroundColor.CGColor;
    }
    
    if(borderColor){
        button.layer.borderColor = borderColor.CGColor;
    }
    button.layer.borderWidth = borderWidth;
}

+ (void)roundView:(UIView *)view background:(UIColor *)backgroundColor borderColor:(UIColor *)borderColor borderWidth:(CGFloat)borderWidth{
    
    view.clipsToBounds = YES;
    view.layer.cornerRadius = 10.0f;
    if(backgroundColor){
        view.layer.backgroundColor = backgroundColor.CGColor;
    }
    
    if(borderColor){
        view.layer.borderColor = borderColor.CGColor;
    }
    view.layer.borderWidth = borderWidth;
}

+ (void)roundViewAsCircle:(UIView *)view borderColor:(UIColor *)borderColor borderWith:(CGFloat)borderWidth{
    CGFloat widthRendering = view.frame.size.width;
    view.clipsToBounds = YES;
    view.layer.cornerRadius = widthRendering/2.0f;
    
    if(borderColor){
        [view.layer setBorderColor:borderColor.CGColor];
    }
    [view.layer setBorderWidth:borderWidth];
}

+ (void)gradientBackgroundColorForView:(UIView*)view colors:(NSArray *)colorSet{
    CAGradientLayer *gradientLayer = [CAGradientLayer layer];
    gradientLayer.frame = view.bounds;
    gradientLayer.colors = colorSet;
    [view.layer insertSublayer:gradientLayer atIndex:0];
}

+ (UIImage*) getImage:(NSString*)imageName{
    
    UIImage *image;
    
    if ([[UIScreen mainScreen] respondsToSelector:@selector(scale)] == YES && [[UIScreen mainScreen] scale] == 2.00) {
        
        // RETINA DISPLAY
        NSString *jpegFile = [imageName stringByAppendingString:@"@2x.jpg"];
        image = [UIImage imageNamed:jpegFile];
    }
    else{
        
        NSString *jpegFile = [imageName stringByAppendingString:@".jpg"];
        image = [UIImage imageNamed:jpegFile];
    }
    
    return image;
}

+ (UIImage*)getImageSyncFromURL:(NSString*)URL{
    //Cache image to improve performance
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:URL]];
    request.cachePolicy = NSURLRequestReturnCacheDataElseLoad; // this will make sure the request always returns the cached image
    request.HTTPShouldHandleCookies = NO;
    request.HTTPShouldUsePipelining = YES;
    [request addValue:@"image/*" forHTTPHeaderField:@"Accept"];
    
    NSData *imageData = [NSURLConnection sendSynchronousRequest:request returningResponse:nil error:nil];
    return [UIImage imageWithData:imageData];
}

+ (void)getImageAsyncFromURL:(NSString *)URL completion:(void (^)(UIImage *, NSError *))completion{
    NSURL *urlImage = [NSURL URLWithString:URL];
    NSURLRequest *requestURL = [[NSURLRequest alloc] initWithURL:urlImage cachePolicy:NSURLRequestReturnCacheDataDontLoad timeoutInterval:5.0f];
    NSURLSession *session = [NSURLSession sharedSession];
    [[session dataTaskWithRequest:requestURL completionHandler:^(NSData *data, NSURLResponse *response, NSError *error){
        if(error){
            NSLog(@"Error download image: %@", error.description);
        }else{
            completion([UIImage imageWithData:data], nil);
        }
    }] resume];
}

+ (UIImage*)scaleImage:(UIImage*)image toWidth:(CGFloat)width andHeight:(CGFloat)height {
    UIGraphicsBeginImageContext( CGSizeMake(width, height));
    [image drawInRect:CGRectMake(0,0,width,height)];
    UIImage* newImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    return newImage;
}

+ (UIImage*)modifyImage:(UIImage*)image toBytes:(int)bytes {
    NSData *data = UIImagePNGRepresentation(image);
    while (data.length >= bytes) {
        image = [Util scaleImage:image toWidth:image.size.width/2 andHeight:image.size.height/2];
        data = UIImagePNGRepresentation(image);
    }
    
    return image;
}

+ (NSString*)dateToString:(NSDate*) date withFormat:(NSString*) format
{
    NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
    [dateFormat setDateFormat:format];
    return [dateFormat stringFromDate:date];
}

+ (NSString*)dateToString:(NSDate*) date withStyle:(NSDateFormatterStyle) style
{
    NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
    [dateFormat setDateStyle:style];
    return [dateFormat stringFromDate:date];
}
+ (NSString*)dateToStringFormatUTC:(NSDate*) date withFormat:(NSString*) format{
    NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
    [dateFormat setDateFormat:format];
    [dateFormat setTimeZone:[NSTimeZone timeZoneWithAbbreviation:@"UTC"]];
    return [dateFormat stringFromDate:date];
    
}
+ (NSDate*)stringToDate:(NSString*) date
{
    NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
    [dateFormat setDateFormat:STANDARD_DATE_FORMAT];
    return [dateFormat dateFromString:date];
}

+ (NSDate*)stringToDateUTC:(NSString*)date format:(NSString*)format
{
    NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
    [dateFormat setTimeZone:[NSTimeZone timeZoneWithAbbreviation:@"UTC"]];
    [dateFormat setDateFormat:format];
    return [dateFormat dateFromString:date];
}

+ (NSDate*)stringToDateTime:(NSString*) date
{
    NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
    [dateFormat setDateFormat:STANDARD_DATE_TIME_FORMAT];
    return [dateFormat dateFromString:date];
}

+ (NSDate*)stringToDateTime:(NSString*) date format:(NSString*)format
{
    NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
    [dateFormat setDateFormat:format];
    return [dateFormat dateFromString:date];
}

+ (NSString*)dateToString:(NSDate*) date
{
    return [self dateToString:date withFormat:VIETNAM_DATE_FORMAT];
}

+ (NSString *)dateToStringByLocale:(NSLocale *)locale date:(NSDate *)date format:(NSString *)format{
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:[NSDateFormatter dateFormatFromTemplate:format options:0 locale:locale]];
    return [formatter stringFromDate:date];
}

+ (NSString *)dateToStringByCurrentLocale:(NSDate *)date format:(NSString *)format{
    return [Util dateToStringByLocale:[NSLocale currentLocale] date:date format:format];
}

+ (NSString*)getTemporaryDir
{
    return NSTemporaryDirectory();
}

+ (NSString *)getMenusDir{
    return [NSString stringWithFormat:@"%@/%@", [Util getCacheDir], MENUS_DIRECTORY];
}

+ (NSString *)getMenusDirWithMajor:(NSInteger)majorId{
    return [NSString stringWithFormat:@"%@/%@", [Util getMenusDir], [@(majorId) stringValue]];
}

+ (NSString*)getDocumentDir
{
    return [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) lastObject];
}

+ (NSString*)getLibraryDir
{
    return [NSSearchPathForDirectoriesInDomains(
                                                NSLibraryDirectory, NSUserDomainMask, YES) objectAtIndex:0];
}

+ (NSString*)getCacheDir
{
    return [NSSearchPathForDirectoriesInDomains(
                                                NSCachesDirectory, NSUserDomainMask, YES) firstObject];
}

+ (NSString*)getApplicationSupportDir
{
    return [NSSearchPathForDirectoriesInDomains(NSApplicationSupportDirectory, NSUserDomainMask, YES) firstObject];
}

+ (NSString*)getAndCreatePath:(NSString*)newPath withExistingPath:(NSString*)existingPath
{
    NSFileManager* fileManager = [NSFileManager defaultManager];
    NSString* currentPath = existingPath;
    
    NSArray* paths = [newPath componentsSeparatedByString:@"/"];
    for (NSString* path in paths)
    {
        currentPath = [currentPath stringByAppendingPathComponent:path];
        BOOL isDir;
        if (![fileManager fileExistsAtPath:currentPath isDirectory:&isDir])
        {
            [fileManager createDirectoryAtPath:currentPath withIntermediateDirectories:YES attributes:nil error:nil];
        }
    }
    
    return currentPath;
}

+ (NSString*)subString:(NSString*)sourceString withStartString:(NSString*)startString withEndString:(NSString*)endString{
    
    NSRange range = [sourceString rangeOfString:startString];
    int startPos = (int)range.location;
    
    range = [sourceString rangeOfString:endString];
    int endPos = (int)range.location;
    
    return [sourceString substringWithRange:NSMakeRange(startPos + 1, endPos - startPos - 1)];
}

+ (NetworkStatus)getNetworkStatus
{
    Reachability *reachability = [Reachability reachabilityForInternetConnection];
    [reachability startNotifier];
    
    return [reachability currentReachabilityStatus];
}

+ (BOOL)contains:(NSString*)sourceString withString:(NSString*)checkString{
    NSRange range = [sourceString rangeOfString:checkString];
    if (range.location != NSNotFound) {
        return TRUE;
    }
    
    return FALSE;
}

+ (NSString*) parseProperty:(NSMutableDictionary*)properties withKey:(NSString*)key{
    
    if (key != nil && [Util contains:key withString:@"["] && [Util contains:key withString:@"]"] ) {
        
        //Replace attribute property
        key = [Util subString:key withStartString:@"[" withEndString:@"]"];
        return properties[key];
    }
    
    return key;
}

+(NSDictionary *)dictionaryWithData:(NSData *)data{
    NSError *error = nil;
    if(!data){
        return nil;
    }
    
    NSDictionary *dict = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableLeaves error:&error];
    if(error){
        return nil;
    }
    return dict;
}

+(NSData *)dataWithDictionary:(NSDictionary *)dictionary{
    NSError *error = nil;
    if(!dictionary){
        return nil;
    }
    
    NSData *data = [NSJSONSerialization dataWithJSONObject:dictionary options:NSJSONWritingPrettyPrinted error:&error];
    if(error){
        return nil;
    }
    return data;
}

+ (bool)isFileExisting:(NSString*)file
{
    NSFileManager* fileManager = [NSFileManager defaultManager];
    return [fileManager fileExistsAtPath:file];
}

+(NSArray *)fileFromDirectory:(NSString *)directionPath{
    NSFileManager *fileManager = [NSFileManager defaultManager];
    BOOL isDir;
    if([fileManager fileExistsAtPath:directionPath isDirectory:&isDir]){
        if(isDir){
            return [fileManager contentsOfDirectoryAtPath:directionPath error:nil];
        }
    }
    return nil;
}

+ (NSInteger)countFilesFromFolder:(NSString*)folderPath
{
    NSFileManager* fileManager = [NSFileManager defaultManager];
    BOOL isDir;
    if ([fileManager fileExistsAtPath:folderPath isDirectory:&isDir])
    {
        if (isDir)
            return  [[fileManager contentsOfDirectoryAtPath:folderPath error:nil] count];
        else
            return -1;
    }
    return -1;
}

+ (NSDictionary*)splitParentFolder:(NSString*)fullPath
{
    
    NSArray* folders = [fullPath componentsSeparatedByString:@"/"];
    NSString* lastFolder = [folders lastObject];
    
    NSString* parentFolder = @"";
    if ([folders count] == 1)
        parentFolder = @"";
    else
    {
        for (int i = 0; i < [folders count] - 1; i++)
        {
            NSString* folder = [folders objectAtIndex:i];
            parentFolder = [parentFolder stringByAppendingPathComponent:folder];
        }
    }
    return [NSDictionary dictionaryWithObjectsAndKeys:
            parentFolder,@"parentFolder", lastFolder, @"name", nil];
    
}

+ (NSError*)errorWithDomain:(NSString*)domain code:(long)errorCode message:(NSString*)message
{
    NSMutableDictionary* errorMessage = [NSMutableDictionary dictionary];
    [errorMessage setValue:message forKey:NSLocalizedDescriptionKey];
    return [NSError errorWithDomain:domain code:errorCode userInfo:errorMessage];
}

+ (bool)copyFileIfNeeded:(NSString*)sourceFile targetFile:(NSString*) targetFile {
    
    // First, test for existence.
    BOOL success = YES;
    
    //    NSFileManager *fileManager = [NSFileManager defaultManager];
    NSError *error = nil;
    
    if (![[NSFileManager defaultManager] fileExistsAtPath:sourceFile])
    {
        NSLog(@"source not found=%@", sourceFile);
        return NO;
    }
    
    if (![[NSFileManager defaultManager] fileExistsAtPath:targetFile])
        success = [[[NSFileManager alloc] init] copyItemAtPath:sourceFile toPath:targetFile error:&error];
    
    if (!success) {
        NSLog(@"%@", error);
        return NO;
    }
    
    return YES;
    
}

//#define kMilisecons 1

/**
 *  getTimeIntervalSinceDate
 *
 *  @param date
 *  @param type
 *
 *  @return
 */


/**
 *  getTimeIntervalSinceDate
 *
 *  @param date
 *  @param type
 *
 *  @return
 */
+ (long long)getTimeIntervalSinceDate:(NSDate*)date ByType:(NSString *)type
{
    if (date == nil)
        return 0;
    
    if (isNilOrEmpty(type))
        type = DURATION_TYPE_SECOND;
    
    NSString *typeLowercase = [type lowercaseString];
    NSDate *newDate = [NSDate date];
    
    if ([typeLowercase isEqualToString:DURATION_TYPE_HOUR]) {
        NSCalendar *calendar = [NSCalendar currentCalendar];
        unsigned unitFlags = NSCalendarUnitYear | NSCalendarUnitMonth |  NSCalendarUnitDay | NSCalendarUnitHour;
        
        NSDateComponents *comps = [calendar components:unitFlags fromDate:date];
        
        date = [calendar dateFromComponents:comps];
    }
    else if ([typeLowercase isEqualToString:DURATION_TYPE_MINUTE]) {
        NSCalendar *calendar = [NSCalendar currentCalendar];
        unsigned unitFlags = NSCalendarUnitYear | NSCalendarUnitMonth |  NSCalendarUnitDay | NSCalendarUnitHour |  NSCalendarUnitMinute;
        
        NSDateComponents *comps = [calendar components:unitFlags fromDate:date];
        
        date = [calendar dateFromComponents:comps];
    }
    else if ([typeLowercase isEqualToString:DURATION_TYPE_SECOND]) {
        NSCalendar *calendar = [NSCalendar currentCalendar];
        unsigned unitFlags = NSCalendarUnitYear | NSCalendarUnitMonth |  NSCalendarUnitDay | NSCalendarUnitHour | NSCalendarUnitMinute | NSCalendarUnitSecond;
        
        NSDateComponents *comps = [calendar components:unitFlags fromDate:date];
        
        date = [calendar dateFromComponents:comps];
    }
    
    return roundl([newDate timeIntervalSinceDate:date]);
}

/**
 *  getTimeIntervalSinceDateInMilisecond
 *
 *  @param date
 *  @param type
 *
 *  @return
 */

+ (long long)getTimeIntervalSinceDateInMilisecond:(NSDate*)date ByType:(NSString *)type
{
    NSDate *newDate = [NSDate date];
    if ([type isEqualToString:DURATION_TYPE_HOUR]) {
        NSCalendar *calendar = [NSCalendar currentCalendar];
        unsigned unitFlags = NSCalendarUnitYear | NSCalendarUnitMonth |  NSCalendarUnitDay | NSCalendarUnitHour;
        
        NSDateComponents *comps = [calendar components:unitFlags fromDate:date];
        
        newDate = [calendar dateFromComponents:comps];
    }
    return roundl([newDate timeIntervalSinceDate:newDate] *1000);
}


/**
 *  getCurrentTimeStamp
 *
 *  @return
 */
+ (long long)getCurrentTimeStamp
{
    NSDate* date = [NSDate date];
    NSTimeInterval time = [date timeIntervalSince1970];
    //    return roundl(time*kMilisecons);
    return roundl(time);
}

+ (long long)getTimeStamp:(NSDate*)date
{
    NSTimeInterval time = [date timeIntervalSince1970];
    //    return roundl(time*kMilisecons);
    return roundl(time);
}

+ (NSDateComponents *)dateTimeComponentsAtLocale:(NSLocale *)locale date:(NSDate *)date{
    NSCalendar *calendar = [NSCalendar currentCalendar];
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    dateFormatter.locale = locale;
    dateFormatter.timeZone = calendar.timeZone;
    [dateFormatter setDateStyle:NSDateFormatterFullStyle];
    [dateFormatter setDateFormat:[NSDateFormatter dateFormatFromTemplate:@"EEEE MMMM dd yyyy" options:0 locale:[NSLocale currentLocale]]];
    NSUInteger dateComponents = NSCalendarUnitYear | NSCalendarUnitMonth | NSCalendarUnitDay | NSCalendarUnitHour | NSCalendarUnitMinute;
    return [calendar components:dateComponents fromDate:date];
}

+ (NSDate*)getDateFromTimeStamp:(long long)timeStamp
{
    //    return [NSDate dateWithTimeIntervalSince1970:(timeStamp/kMilisecons)];
    return [NSDate dateWithTimeIntervalSince1970:(timeStamp)];
    
}

/**
 *  createDirIfNeeded
 *
 *  @param dir
 */
+ (void)createDirIfNeeded:(NSString*)dir
{
    NSFileManager *fileManager = [NSFileManager defaultManager];
    NSError *error;
    bool success = [fileManager fileExistsAtPath:dir];
    if (!success)
        [fileManager createDirectoryAtPath:dir withIntermediateDirectories:YES attributes:nil error:&error];
}

+(MZFormSheetPresentationController *)presentMZModalWithViewController:(UIViewController *)viewController contentSize:(CGSize)contentSize{
    MZFormSheetPresentationController *modal = [[MZFormSheetPresentationController alloc] initWithContentViewController:viewController];
    [modal setBlurEffectStyle:UIBlurEffectStyleDark];
    modal.shouldUseMotionEffect = YES;
    modal.shouldApplyBackgroundBlurEffect = YES;
    modal.contentViewControllerTransitionStyle = MZFormSheetPresentationTransitionStyleDropDown;    [modal setContentViewSize:contentSize];
    return modal;
}

+ (NSString*)getDeviceInfo{ //return such as iPhone 5s , Ipad 4 Cellular
    
    size_t size;
    sysctlbyname("hw.machine", NULL, &size, NULL, 0);
    char *machine = malloc(size);
    sysctlbyname("hw.machine", machine, &size, NULL, 0);
    NSString *platform = [NSString stringWithCString:machine encoding:NSUTF8StringEncoding];
    free(machine);
    
    return platform;
}

+ (NSString*)getDeviceUUID {
    UIDevice *device = [UIDevice currentDevice];
    NSString *currentDeviceId = [[device identifierForVendor]UUIDString];
    return currentDeviceId;
}

+ (NSString*)subString:(NSString *)sourceString withStartIndex:(int)startIndex withEndIndex:(int)endIndex{
    if (startIndex>endIndex) {
        int x;
        x=endIndex;
        endIndex=startIndex;
        startIndex=x;
    }
    NSRange range=NSMakeRange(startIndex, endIndex-startIndex);
    if (endIndex-startIndex > sourceString.length-startIndex) {
        return @"";
    }
    NSString *tempString = [sourceString substringWithRange:range];
    return tempString;
    
}

+(NSIndexSet *)sectionAsIndexSetForSection:(NSInteger)path{
    NSRange range = NSMakeRange(0, path);
    return [[NSIndexSet alloc] initWithIndexesInRange:range];
}

+(float)miliSecond:(NSDate*)fromDate toDate:(NSDate*)toDate{
    
    NSTimeInterval milisecondedDate = ([toDate timeIntervalSince1970])-[fromDate timeIntervalSince1970];
    long long f1=lroundf(milisecondedDate*1000);
    return f1/1000.0;
    
}

//+ (NSString*)JSONString:(NSObject*)source{
//
//        NSError* error=nil;
//        NSData* data = [NSJSONSerialization dataWithJSONObject:source options:NSJSONWritingPrettyPrinted error:&error];
//        NSString* JSON=[[NSString alloc]initWithData:data encoding:NSUTF8StringEncoding];
//        return JSON;
//}
//+ (NSDate*)dateFromString:(NSString*)stringDate
//{
//    NSTimeInterval interval =  ([stringDate doubleValue]/1000);
//    NSDate* date = [NSDate dateWithTimeIntervalSince1970:interval];
//    return date;
//
//}
+ (NSString*)xmlDecode:(NSString*)sourceData
{
    NSMutableString* sourceString =[NSMutableString stringWithString:sourceData];
    [sourceString replaceOccurrencesOfString:@"&amp;"  withString:@"&"  options:NSLiteralSearch range:NSMakeRange(0, [sourceString length])];
    [sourceString replaceOccurrencesOfString:@"&quot;" withString:@"\"" options:NSLiteralSearch range:NSMakeRange(0, [sourceString length])];
    [sourceString replaceOccurrencesOfString:@"&#x27;" withString:@"'"  options:NSLiteralSearch range:NSMakeRange(0, [sourceString length])];
    [sourceString replaceOccurrencesOfString:@"&#x39;" withString:@"'"  options:NSLiteralSearch range:NSMakeRange(0, [sourceString length])];
    [sourceString replaceOccurrencesOfString:@"&#x92;" withString:@"'"  options:NSLiteralSearch range:NSMakeRange(0, [sourceString length])];
    [sourceString replaceOccurrencesOfString:@"&#x96;" withString:@"'"  options:NSLiteralSearch range:NSMakeRange(0, [sourceString length])];
    
    [sourceString replaceOccurrencesOfString:@"&gt;"   withString:@">"  options:NSLiteralSearch range:NSMakeRange(0, [sourceString length])];
    [sourceString replaceOccurrencesOfString:@"&lt;"   withString:@"<"  options:NSLiteralSearch range:NSMakeRange(0, [sourceString length])];
    
    return sourceString;
}

+ (NSString*)xmlEncode:(NSString*)sourceData
{
    NSMutableString* sourceString =[NSMutableString stringWithString:sourceData];
    [sourceString replaceOccurrencesOfString:@"&"   withString: @"&amp;" options:NSLiteralSearch range:NSMakeRange(0, [sourceString length])];
    [sourceString replaceOccurrencesOfString:@"\""  withString: @"&quot;"options:NSLiteralSearch range:NSMakeRange(0, [sourceString length])];
    [sourceString replaceOccurrencesOfString:@"'"  withString:  @"&#x27;"options:NSLiteralSearch range:NSMakeRange(0, [sourceString length])];
    //    [sourceString replaceOccurrencesOfString:@"'"  withString:  @"&#x39;"options:NSLiteralSearch range:NSMakeRange(0, [sourceString length])];
    //    [sourceString replaceOccurrencesOfString:@"'"  withString:  @"&#x92;"options:NSLiteralSearch range:NSMakeRange(0, [sourceString length])];
    //    [sourceString replaceOccurrencesOfString:@"'"  withString:  @"&#x96;"options:NSLiteralSearch range:NSMakeRange(0, [sourceString length])];
    [sourceString replaceOccurrencesOfString:@">"     withString: @"&gt;"options:NSLiteralSearch range:NSMakeRange(0, [sourceString length])];
    [sourceString replaceOccurrencesOfString:@"<"    withString:  @"&lt;"options:NSLiteralSearch range:NSMakeRange(0, [sourceString length])];
    
    return sourceString;
}


/**
 *  deleteFileWithPath
 *
 *  @param filePath
 */
+ (void)deleteFileWithPath:(NSString *)filePath
{
    NSFileManager *manager = [[NSFileManager alloc] init];
    // Need to check if the to be deleted file exists.
    if ([manager fileExistsAtPath:filePath]) {
        NSError *error = nil;
        // This function also returnsYES if the item was removed successfully or if path was nil.
        // Returns NO if an error occurred.
        [manager removeItemAtPath:filePath error:&error];
        if (error) {
            NSLog(@"There is an Error: %@", error);
        }
    } else {
        NSLog(@"File %@ doesn't exists", filePath);
    }
}
+ (NSData *)dataFromImage:(UIImage *)image metadata:(NSDictionary *)metadata mimetype:(NSString *)mimetype
{
    //NSDictionary *metadata = [info objectForKey:UIImagePickerControllerMediaMetadata];
    // NSString *mimeType = @"image/jpeg"; // ideally this would be dynamically set. Not defaulted to a jpeg!
    NSMutableData *imageData = [NSMutableData data];
    CFStringRef uti = UTTypeCreatePreferredIdentifierForTag(kUTTagClassMIMEType, (__bridge CFStringRef)mimetype, NULL);
    CGImageDestinationRef imageDestination = CGImageDestinationCreateWithData((__bridge CFMutableDataRef)imageData, uti, 1, NULL);
    
    if (imageDestination == NULL)
    {
        NSLog(@"Failed to create image destination");
        imageData = nil;
    }
    else
    {
        CGImageDestinationAddImage(imageDestination, image.CGImage, (__bridge CFDictionaryRef)metadata);
        
        if (CGImageDestinationFinalize(imageDestination) == NO)
        {
            NSLog(@"Failed to finalize");
            imageData = nil;
        }
        CFRelease(imageDestination);
    }
    
    CFRelease(uti);
    
    return imageData;
}

+(NSString *)stringFromFileSize:(long long)fileSize
{
    //data from ALASet has EPSILON propotion so that the size of file is measure under +/- unit. Based on this, applied for size calculation, 1023 is default - Phong Nguyen. Reffer to http://stackoverflow.com/questions/10067765/alasset-image-size
    NSNumberFormatter *numberFormatter = [[NSNumberFormatter alloc] init];
    [numberFormatter setMaximumFractionDigits:2];
    [numberFormatter setRoundingMode:NSNumberFormatterRoundCeiling];
    
    float size = fileSize;
    if(size < 1023)
        return [NSString stringWithFormat:@"%f Bytes", size];
    size = size/1024;
    if(size < 1023)
        return [NSString stringWithFormat:@"%@ Kb", [numberFormatter stringFromNumber:[NSNumber numberWithFloat:size]]];
    size = size/1024;
    if(size < 1023)
        return [NSString stringWithFormat:@"%@ Mb", [numberFormatter stringFromNumber:[NSNumber numberWithFloat:size]]];
    return nil;
}


/**
 *  loadFonts
 *
 *  @param bundle
 */
+ (void)loadFonts:(NSBundle*)bundle
{
    NSUInteger newFontCount = 0;
    NSBundle *frameworkBundle = [NSBundle bundleWithIdentifier:@"com.apple.GraphicsServices"];
    const char *frameworkPath = [[frameworkBundle executablePath] UTF8String];
    
    if (frameworkPath)
    {
        
        void *graphicsServices = dlopen(frameworkPath, RTLD_NOLOAD | RTLD_LAZY);
        
        if (graphicsServices)
        {
            BOOL (*GSFontAddFromFile)(const char *) = dlsym(graphicsServices, "GSFontAddFromFile");
            if (GSFontAddFromFile)
            {
                for (NSString *fontFile in [bundle pathsForResourcesOfType:@"ttf" inDirectory:nil])
                {
                    newFontCount += GSFontAddFromFile([fontFile UTF8String]);
                }
            }
        }
    }
    
}

/**
 *  getAllFonts
 *
 *  @return
 */
+ (NSArray*)getAllFonts
{
    NSMutableArray* listFonts = [NSMutableArray new];
    
    // List all fonts on iPhone
    NSArray *familyNames = [[NSArray alloc] initWithArray:[UIFont familyNames]];
    NSArray *fontNames;
    NSInteger indFamily, indFont;
    for (indFamily=0; indFamily<[familyNames count]; ++indFamily)
    {
        NSLog(@"Family name: %@", [familyNames objectAtIndex:indFamily]);
        fontNames = [[NSArray alloc] initWithArray:
                     [UIFont fontNamesForFamilyName:
                      [familyNames objectAtIndex:indFamily]]];
        for (indFont=0; indFont<[fontNames count]; ++indFont)
        {
            NSLog(@"    Font name: %@", [fontNames objectAtIndex:indFont]);
            [listFonts addObject:[fontNames objectAtIndex:indFont]];
        }
    }
    
    return listFonts;
}

+ (UIImage *)resizeImage:(UIImage *)image fitToSize:(CGSize)size
{
    if(!image)
    {
        NSLog(@"Image instance is nil. Can not render!");
        return nil;
    }
    UIGraphicsBeginImageContextWithOptions(size, NO, 0.f);
    [image drawInRect:CGRectMake(0, 0, size.width, size.height)];
    UIImage *scaledImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return scaledImage;
}

+ (NSString*)mimeTypefromPathToFile:(NSString *)filePath{
    if (![[NSFileManager defaultManager] fileExistsAtPath:filePath]) {
        return nil;
    }
    CFStringRef UTI = UTTypeCreatePreferredIdentifierForTag(kUTTagClassFilenameExtension, (__bridge CFStringRef)[filePath pathExtension], NULL);
    CFStringRef mimeType = UTTypeCopyPreferredTagWithClass (UTI, kUTTagClassMIMEType);
    CFRelease(UTI);
    if (!mimeType) {
        return @"application/octet-stream";
    }
    return (__bridge NSString *)mimeType;
}

/**
 *  getFileNameFromPath
 *
 *  @param fullPath
 *
 *  @return
 */
+ (NSString*)getFileNameFromPath:(NSString*)fullPath
{
    
    NSRange range = [fullPath rangeOfString:@"/" options:NSBackwardsSearch];
    if (range.location == NSNotFound)
        return nil;
    
    NSString* fileNoPath = [Util subString:fullPath withStartIndex:(int)range.location withEndIndex:(int)fullPath.length];
    
    return fileNoPath;
    
}

/**
 *  stringToData
 *
 *  @param value
 *
 *  @return
 */
+ (NSData*)stringToData:(NSString*)value
{
    return [value dataUsingEncoding:NSUTF8StringEncoding];
}

/**
 *  getFileExtension
 *
 *  @param fileName
 *
 *  @return
 */
+ (NSString*)getFileExtension:(NSString*)fileName
{
    
    NSRange range = [fileName rangeOfString:@"." options:NSBackwardsSearch];
    if (range.location == NSNotFound)
        return @"";
    
    NSString* fileExt = [Util subString:fileName withStartIndex:(int)(range.location + 1) withEndIndex:(int)fileName.length];
    
    return fileExt;
}

/**
 *  base64forData
 *
 *  @param theData
 *
 *  @return
 */
+ (NSString*)base64forData:(NSData*)theData
{
    const uint8_t* input = (const uint8_t*)[theData bytes];
    NSInteger length = [theData length];
    
    static char table[] = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/=";
    
    NSMutableData* data = [NSMutableData dataWithLength:((length + 2) / 3) * 4];
    uint8_t* output = (uint8_t*)data.mutableBytes;
    
    NSInteger i;
    for (i=0; i < length; i += 3) {
        NSInteger value = 0;
        NSInteger j;
        for (j = i; j < (i + 3); j++) {
            value <<= 8;
            
            if (j < length) {  value |= (0xFF & input[j]);  }  }  NSInteger theIndex = (i / 3) * 4;  output[theIndex + 0] = table[(value >> 18) & 0x3F];
        output[theIndex + 1] = table[(value >> 12) & 0x3F];
        output[theIndex + 2] = (i + 1) < length ? table[(value >> 6) & 0x3F] : '=';
        output[theIndex + 3] = (i + 2) < length ? table[(value >> 0) & 0x3F] : '=';
    }
    
    return [[NSString alloc] initWithData:data encoding:NSASCIIStringEncoding];
}

/**
 *  stringFromData
 *
 *  @param data
 *
 *  @return
 */
+ (NSString*)stringFromData:(NSData*)data
{
    return [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
}

+ (NSNumberFormatter *)numberFormatterForLocale{
    NSNumberFormatter *formatter = [[NSNumberFormatter alloc] init];
    formatter.locale = [NSLocale currentLocale];
    formatter.numberStyle = NSNumberFormatterCurrencyStyle;
    formatter.usesGroupingSeparator = YES;
    return formatter;
}

+ (NSString *)stringForNumberAtCurrentLocale:(NSNumber *)number{
    NSNumberFormatter *formatter = [[NSNumberFormatter alloc] init];
    formatter.locale = [NSLocale currentLocale];
    formatter.numberStyle = NSNumberFormatterCurrencyStyle;
    formatter.usesGroupingSeparator = YES;
    return [formatter stringFromNumber:number];
}

+(NSString *)stringForNumberWithLocale:(NSLocale *)locale number:(NSNumber *)number{
    NSNumberFormatter *formatter = [[NSNumberFormatter alloc] init];
    formatter.locale = locale;
    formatter.numberStyle = NSNumberFormatterCurrencyStyle;
    formatter.usesGroupingSeparator = YES;
    return [formatter stringFromNumber:number];
}

+(NSString *)stringForNumberPrimitiveWithLocale:(NSLocale *)locale number:(double)number{
    NSNumberFormatter *formatter = [[NSNumberFormatter alloc] init];
    formatter.locale = locale;
    formatter.numberStyle = NSNumberFormatterCurrencyStyle;
    formatter.usesGroupingSeparator = YES;
    return [formatter stringFromNumber:[NSNumber numberWithDouble:number]];
}


#pragma mark - Security
+ (BOOL)currentDeviceSupportLocalAuthentication{
    BOOL hasTouchID = NO;
    if ([LAContext class]) {
        LAContext *context = [LAContext new];
        NSError *error = nil;
        hasTouchID = [context canEvaluatePolicy:LAPolicyDeviceOwnerAuthenticationWithBiometrics error:&error];
    }
    return  hasTouchID;
}


#pragma mark - Animation

+ (void)applyBasicAnimation:(CABasicAnimation *)animation toLayer:(CALayer *)layer{
    animation.fromValue = [layer.presentationLayer ?: layer valueForKeyPath:animation.keyPath];
    [CATransaction begin];
    [CATransaction setDisableActions:YES];
    [layer setValue:animation.toValue forKeyPath:animation.keyPath]; [CATransaction commit];
    [layer addAnimation:animation forKey:nil];
}


#pragma mark - Permission Scope Custome
+(BOOL)didAuthorizeForResult:(PermissionResult *)result{
    if([Util permissionStatusForResult:result] == PermissionStatusAuthorized){
        return YES;
    }else{
        return NO;
    }
}

+(PermissionStatus)permissionStatusForResult:(PermissionResult *)result{
    return result.status;
}

+(PermissionType)permissionTypeForResult:(PermissionResult *)result{
    return result.type;
}


#pragma mark - Custom For Eatus (only)

+(UIImage *)paymentImageForPaymentType:(PaymentType)paymentType{
    switch (paymentType) {
        case kPaymentTypeCash:
            return [UIImage imageNamed:@"Cash Color"];
            break;
            
        case kPaymentTypeCard:
            return [UIImage imageNamed:@"Card Color"];
            break;
            
        default:
            break;
    }
}

+(NSString *)orderStatusBy:(ReceiptStatus)orderStatus{
    switch (orderStatus) {
        case kReceiptCancel:
            return NSLocalizedString(@"com.eatus.receipt.status.didCancelled", @"");
            break;
            
        case kReceiptConfirmed:
            return NSLocalizedString(@"com.eatus.receipt.status.didConfirmed", @"");
            break;
            
        case kReceiptDelivered:
            return NSLocalizedString(@"com.eatus.receipt.status.didDelivered", @"");
            break;
            
        case kReceiptPending:
            return NSLocalizedString(@"com.eatus.receipt.status.didPend", @"");
            break;
            
        case kReceiptPrecanceled:
            return NSLocalizedString(@"com.eatus.receipt.status.didPrecancelled", @"");
            break;
            
        default:
            break;
    }
}

@end
