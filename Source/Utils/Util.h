//
//  Utils.h
//  MiPoBeacon
//
//  Created by Phong Nguyen on 8/29/15.
//  Copyright (c) 2015 MiPo Consolidate LLC. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <PermissionScope/PermissionScope-Swift.h>

#import "FavMacros.h"
#import "Reachability.h"
#import "MZFormSheetPresentationController.h"

#define VIETNAM_STANDARD_DATE_FORMAT        @"dd/MM/yyyy"
#define VIETNAM_DATE_FORMAT                 @"dd/MM/yyyy HH:mm"
#define STANDARD_DATE_FORMAT                @"MM/dd/yyyy"
#define STANDARD_DATE_TIME_FORMAT           @"MM/dd/yyyy hh:mm:ss"
#define STANDARD_DATE_UTC_FORMAT            @"yyyy-MM-dd hh:mm:ss"
#define STANDARD_DATE_TIME_UTC_FORMAT       @"yyyy-MM-dd hh:mm:ss.fff"
#define DATE_FORMAT_VPMS                    @"yyyy-MM-dd"

#define DURATION_TYPE_HOUR                  @"h"
#define DURATION_TYPE_MINUTE                @"m"
#define DURATION_TYPE_SECOND                @"s"

#define APPLICATION_SUPPORT_PATH            @"Application Support"
#define MENUS_DIRECTORY                     @"Menus"

@interface Util : NSObject


#pragma mark - View Manipulation
+ (void)dropShadowForView: (UIView *)view;
+ (UIImage*)getImageSyncFromURL:(NSString*)URL;
+ (void)getImageAsyncFromURL: (NSString *)URL completion: (void (^)(UIImage *image, NSError *error))completion;
+ (UIImage*) getImage:(NSString*)imageName;
+ (UIImage*)scaleImage:(UIImage*)image toWidth:(CGFloat)width andHeight:(CGFloat)height;
+ (UIImage*)modifyImage:(UIImage*)image toBytes:(int)bytes;
+ (UIColor*)colorWithHexString:(NSString*)hex;
+ (void)roundButton: (UIButton *)button background: (UIColor *)backgroundColor borderColor: (UIColor *)borderColor borderWith: (CGFloat)borderWidth;
+ (void)roundView: (UIView *)view background: (UIColor *)backgroundColor borderColor: (UIColor *)borderColor borderWidth: (CGFloat)borderWidth;
+ (void)roundViewAsCircle: (UIView *)view borderColor: (UIColor *)borderColor borderWith: (CGFloat)borderWidth;
+ (void)gradientBackgroundColorForView: (UIView *)view colors: (NSArray *)colorSet;


#pragma mark - Date Time Manipulation
+ (NSString*)dateToString:(NSDate*) date withFormat:(NSString*) format;
+ (NSString*)dateToString:(NSDate*) date withStyle:(NSDateFormatterStyle) style;
+ (NSString*)dateToStringFormatUTC:(NSDate*) date withFormat:(NSString*) format;
+ (NSDate*)stringToDate:(NSString*) date;
+ (NSDate*)stringToDateUTC:(NSString*)date format:(NSString*)format;
+ (NSString*)dateToString:(NSDate*) date;

+ (NSString *)dateToStringByLocale: (NSLocale *)locale date: (NSDate *)date format: (NSString *)format;
+ (NSString *)dateToStringByCurrentLocale: (NSDate *)date format: (NSString *)format;


#pragma mark - File & Directory Manipulation
+ (NSString*)getDocumentDir;
+ (NSString*)getTemporaryDir;
+ (NSString*)getMenusDir;
+ (NSString*)getLibraryDir;
+ (NSString*)getCacheDir;
+ (NSString*)getApplicationSupportDir;
+ (void)createDirIfNeeded:(NSString*)dir;
+ (void)deleteFileWithPath:(NSString *)filePath;
+ (NSString*)getAndCreatePath:(NSString*)newPath withExistingPath:(NSString*)existingPath;
+ (NSString*)getMenusDirWithMajor: (NSInteger)majorId;
+ (bool)isFileExisting:(NSString*)file;
+ (NSArray *)fileFromDirectory: (NSString *)directionPath;
+ (NSInteger)countFilesFromFolder:(NSString*)folderPath;
+ (NSDictionary*)splitParentFolder:(NSString*)fullPath;
+ (bool)copyFileIfNeeded:(NSString*)sourceFile targetFile:(NSString*) targetFile;


#pragma mark - String Manipulation
+ (NSString*)subString:(NSString*)sourceString withStartString:(NSString*)startString withEndString:(NSString*)endString;
+ (NSString*)subString:(NSString *)sourceString withStartIndex:(int)startIndex withEndIndex:(int)endIndex;
+ (BOOL)contains:(NSString*)sourceString withString:(NSString*)checkString;

+ (NetworkStatus)getNetworkStatus;

+ (NSError*)errorWithDomain:(NSString*)domain code:(long)errorCode message:(NSString*)message;

+ (NSDate*)stringToDateTime:(NSString*)date;
+ (NSDate*)stringToDateTime:(NSString*) date format:(NSString*)format;
+ (float)miliSecond:(NSDate*)fromDate toDate:(NSDate*)toDate;
+ (long long)getTimeIntervalSinceDate:(NSDate*)date ByType:(NSString *)type;
+ (long long)getTimeIntervalSinceDateInMilisecond:(NSDate*)date ByType:(NSString *)type;
+ (long long)getCurrentTimeStamp;
+ (NSDate*)getDateFromTimeStamp:(long long)timeStamp;
+ (long long)getTimeStamp:(NSDate*)date;
+ (NSDateComponents *)dateTimeComponentsAtLocale: (NSLocale *)locale date: (NSDate *)date;

//MZSheetModal
+ (MZFormSheetPresentationController *)presentMZModalWithViewController: (UIViewController *)viewController contentSize: (CGSize)contentSize;

#pragma mark - Data Manipulate
+ (NSString*)parseProperty:(NSMutableDictionary*)properties withKey:(NSString*)key;
+ (NSDictionary *)dictionaryWithData: (NSData *)data;
+ (NSData *)dataWithDictionary: (NSDictionary *)dictionary;

+ (NSString*)getDeviceInfo;
+ (NSString*)getDeviceUUID;

+ (NSIndexSet *)sectionAsIndexSetForSection: (NSInteger)path;

//+ (NSDate*)dateFromString:(NSString*)stringDate;
+ (NSString *)xmlEncode  :(NSString*)sourceData;
+ (NSString *)xmlDecode :(NSString*)sourceData;

//Convert Image to data
+ (NSData *)dataFromImage:(UIImage *)image metadata:(NSDictionary *)metadata mimetype:(NSString *)mimetype;

//Convert File Size
+ (NSString *)stringFromFileSize: (long long) fileSize;

// Dynamic Fonts
+ (void)loadFonts:(NSBundle*)bundle;
+ (NSArray*)getAllFonts;

//resize and fill image stretch to view
+ (UIImage *)resizeImage: (UIImage *)image fitToSize: (CGSize)size;

+ (NSString *)mimeTypefromPathToFile: (NSString *)filePath;

+ (NSString*)getFileNameFromPath:(NSString*)fullPath;

+ (NSData*)stringToData:(NSString*)value;

+ (NSString*)getFileExtension:(NSString*)fileName;

+ (NSString*)base64forData:(NSData*)theData;

+ (NSString*)stringFromData:(NSData*)data;

+ (NSNumberFormatter *)numberFormatterForLocale;

+ (NSString *)stringForNumberAtCurrentLocale: (NSNumber *)number;
+ (NSString *)stringForNumberWithLocale: (NSLocale *)locale number: (NSNumber *)number;
+ (NSString *)stringForNumberPrimitiveWithLocale: (NSLocale *)locale number: (double)number;


#pragma mark - Security
+ (BOOL)currentDeviceSupportLocalAuthentication;

#pragma mark - Animation
+ (void)applyBasicAnimation:(CABasicAnimation *)animation toLayer:(CALayer *)layer;


#pragma mark - Permission Scope Custom
+(BOOL)didAuthorizeForResult: (PermissionResult *)result;
+(PermissionStatus)permissionStatusForResult: (PermissionResult *)result;
+(PermissionType)permissionTypeForResult: (PermissionResult *)result;

#pragma mark - Custom
+(UIImage *)paymentImageForPaymentType: (PaymentType)paymentType;
+(NSString *)orderConfirmationBy: (OrderConfirmationStatus)confirmationStatus;
+(NSString *)orderStatusBy: (ReceiptStatus)orderStatus;

@end
