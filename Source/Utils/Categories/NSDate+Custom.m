//
//  NSDate+Custom.m
//  Light House
//
//  Created by phuc on 9/23/15.
//  Copyright (c) 2015 Triad Fox. All rights reserved.
//

#import "NSDate+Custom.h"

@implementation NSDate (Custom)

//Be careful when using NSDateFormatter.
//As Raywenderlich warning in http://www.raywenderlich.com/31166/25-ios-app-performance-tips-tricks
//NSDateFormat is expensive to create that object, also expensive to update new format
//Should use singleton for that purpose if possible

//something like @"MMM dd, yyyy, HH:mm:ss"
- (NSString*)toStringWithFormat:(NSString*)format {
    NSDateFormatter* formatter = [NSDateFormatter new];
    [formatter setDateFormat:format];
    return [formatter stringFromDate:self];
}

+ (NSDate*)dateFromString:(NSString *)dateString withFormat:(NSString*)format {
    NSDateFormatter* formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:format];
    return [formatter dateFromString:dateString];
}

- (NSDate*)getBeginningDate {
    //If we pass 05/04/2012 13:37:00, it will return 05/04/2012 00:00:00
    //gather current calendar
    NSCalendar *calendar = [NSCalendar currentCalendar];
    
    //gather date components from date
    NSDateComponents *dateComponents = [calendar components:NSCalendarUnitYear|NSCalendarUnitMonth|NSCalendarUnitDay fromDate:self];
    
    //set date components
    [dateComponents setHour:0];
    [dateComponents setMinute:0];
    [dateComponents setSecond:0];
    
    //return date relative from date
    return [calendar dateFromComponents:dateComponents];
}

- (NSDate*)getEndingDate {
    //If we pass 05/04/2012 13:37:00, it will return 05/04/2012 23:59:59
    //gather current calendar
    NSCalendar *calendar = [NSCalendar currentCalendar];
    
    //gather date components from date
    NSDateComponents *dateComponents = [calendar components:NSCalendarUnitYear|NSCalendarUnitMonth|NSCalendarUnitDay fromDate:self];
    
    //set date components
    [dateComponents setHour:23];
    [dateComponents setMinute:59];
    [dateComponents setSecond:59];
    
    //return date relative from date
    return [calendar dateFromComponents:dateComponents];
}

@end
