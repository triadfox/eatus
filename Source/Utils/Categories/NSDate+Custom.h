//
//  NSDate+Custom.h
//  Light House
//
//  Created by phuc on 9/23/15.
//  Copyright (c) 2015 Triad Fox. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSDate (Custom)

- (NSString*)toStringWithFormat:(NSString*)format;
- (NSDate*)getBeginningDate;
- (NSDate*)getEndingDate;

+ (NSDate*)dateFromString:(NSString *)dateString withFormat:(NSString*)format;

@end
