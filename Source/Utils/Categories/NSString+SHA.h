//
//  NSString+SHA.h
//  MiPoBeacon
//
//  Created by phuc on 9/2/15.
//  Copyright (c) 2015 MiPo Consolidate LLC. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSString (SHA)

@property (nonatomic, readonly) NSString *md5;
@property (nonatomic, readonly) NSString *sha1;
@property (nonatomic, readonly) NSString *sha224;
@property (nonatomic, readonly) NSString *sha256;
@property (nonatomic, readonly) NSString *sha384;
@property (nonatomic, readonly) NSString *sha512;

@end
