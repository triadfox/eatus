//
//  FavMacros.h
//  MiPoBeacon
//
//  Created by Phong Nguyen on 9/4/15.
//  Copyright (c) 2015 Triad Fox. All rights reserved.
//

#ifndef MiPoBeacon_FavMacros_h
#define MiPoBeacon_FavMacros_h

#define MONEY_CALCULATE_3_POSITIVE_DECIMAL(value)    [NSString stringWithFormat:@"%.3f", value]
#define MONEY_CALCULATE_3_NEGATIVE_DECIMAL(value)    [NSString stringWithFormat:@"-%.3f", value]

#define radForDeg(deg)                      (deg*M_PI/180)
#define nilToString(value)                  (value == nil ? @"" : value)
#define stringToBool(value)                 (value == nil ? NO : [value boolValue])
#define isNilOrEmpty(value)                 (value == nil || [value isEqualToString:@""])
#define dicIsNil(value)                     ([value count] == 0)
#define predicateArray(array,format,value)    [array filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:format,value]]
#define UIColorFromRGB(rgbValue) [UIColor \
colorWithRed:((float)((rgbValue & 0xFF0000) >> 16))/255.0 \
green:((float)((rgbValue & 0xFF00) >> 8))/255.0 \
blue:((float)(rgbValue & 0xFF))/255.0 alpha:1.0]
#define nilToUUIDString(value)  (value == nil ? [[NSUUID UUID] UUIDString]:value)
#define colorWithRGB(r, g, b)  [UIColor colorWithRed:((r) / 255.0) green:((g) / 255.0) blue:((b) / 255.0) alpha:1.0]
#define errorFunc(value)          [NSString stringWithFormat:@"%s: %@", __PRETTY_FUNCTION__, value]
#define NSLocalizedString(key, comment) [[NSBundle mainBundle] localizedStringForKey:(key) value:@"" table:nil]
#endif
