//
//  Constants.h
//  MiPoBeacon
//
//  Created by phuc on 9/2/15.
//  Copyright (c) 2015 Triad Fox. All rights reserved.
//

#ifndef MiPoBeacon_Constants_h
#define MiPoBeacon_Constants_h

/*********** Font ***********/
#pragma mark - Font

#define FONT_OPENSANS_LIGHT      @"OpenSans-Light"
#define FONT_OPENSANS            @"OpenSans"
#define FONT_OPENSANS_SEMI_BOLD  @"OpenSans-Semibold"
#define FONT_OPENSANS_BOLD       @"OpenSans-Bold"
#define FONT_OPENSANS_EXTRA_BOLD @"OpenSans-ExtraBold"
#define FONT_HELVETICANEUE       @"HelveticaNeue"

/*********** Detect Iphone Version ***********/
#pragma mark - Detect Iphone Version

#define IS_IPAD (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
#define IS_IPHONE (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone)
#define IS_RETINA ([[UIScreen mainScreen] scale] >= 2.0)

#define SCREEN_WIDTH ([[UIScreen mainScreen] bounds].size.width)
#define SCREEN_HEIGHT ([[UIScreen mainScreen] bounds].size.height)
#define SCREEN_MAX_LENGTH (MAX(SCREEN_WIDTH, SCREEN_HEIGHT))
#define SCREEN_MIN_LENGTH (MIN(SCREEN_WIDTH, SCREEN_HEIGHT))

#define IS_IPHONE_4_OR_LESS (IS_IPHONE && SCREEN_MAX_LENGTH < 568.0)
#define IS_IPHONE_5  (IS_IPHONE && SCREEN_MAX_LENGTH == 568.0)
#define IS_IPHONE_6  (IS_IPHONE && SCREEN_MAX_LENGTH == 667.0)
#define IS_IPHONE_6P (IS_IPHONE && SCREEN_MAX_LENGTH == 736.0)

/*********** Locale ***********/
#pragma mark - Locale
#define LOCALE_VIE_VN                                       @"vi_VN"
#define LOCALE_EN_US                                        @"en_US"
#define LOCALE_EN_EUROPE                                    @"eu"

#define priceByCurrency(value)                              [Util stringForNumberWithLocale:[[MenuEngineManager shareManager]   localeByCurrentCurrency] number:[NSNumber numberWithDouble:value]]
#define priceByCurrentAlongCurrentLocale(value)             [Util stringForNumberWithLocale:[NSLocale currentLocale] number:[NSNumber numberWithDouble:value]]
#define priceByCurrencyAlongCurrentReceipt(value)           [Util stringForNumberWithLocale:[NSLocale currentLocale] number:[NSNumber numberWithDouble:value]]



/*********** User Default & Keychain ***********/
#define kCardBrandNameWithLast4                                 @"brandNameWithLast4"
#define kCurrenCardLast4                                        @"paymentCardLast4"
#define kCurrenPaymentType                                      @"paymentType"
#define kCurrenPaymentCardInformation                           @"paymentCardInformation"
#define kFirstUseApp                                            @"firstTimeUseApp"
#define kFirstAccessToolTipsMainOrder                           @"firstAccessToolTipsMainOrder"
#define kShouldAuthTouchIdForPlaceOrder                         @"shouldAuthTouchIdForPlaceOrder"
#define kShouldSurveillanceBeacons                              @"shouldSurveillanceBeacons"



/*********** Notification ***********/
#pragma mark - Local Notification
#define kNotificationBeacon                                     @"keyBeaconNotifier"
#define kNotificationConfirmationOnOrder                        @"keyConfirmationOnOrder"

#define kNotificationChangeProfile                              @"keyNotiChangeProfile"
#define kNotificationRemoveProfile                              @"keyNotiRemoveProfile"
#define kNotificationSignOut                                    @"keyNotiSignOut"
#define kNotificationAddNewCard                                 @"keyNotiAddNewCard"
#define kNotificationSetPaymentPasscode                         @"keyNotiSetPaymentPasscode"
#define kNotificationEnableTouchIdForPlaceOrderDidChange        @"kNotiTouchIdForPlaceOrderChanged"
#define kNotificationEnableBeaconSurveillanceDidChange          @"kNotifiSurveillanceChanged"

//bluetooth, network + beacon
#define kNotificationNoBeaconsInRange                           @"keyNotiNoBeaconsInRange"
#define kNotificationBluetoothOff                               @"keyNotiBluetoothOff"
#define kNotificationBluetoothOn                                @"keyNotiBluetoothOn"
#define kNotificationNetworkOff                                 @"keyNotiNetworkOff"
#define kNotificationNetworkOn                                  @"keyNotiNetworkOn"


/*********** Payment ***********/
#pragma mark - Payment Type
#define PAYMENT_CASH                            @"Cash"
#define PAYMENT_CARD                            @"Card"
#define CARD_VISA                               @"VISA"
#define CARD_AMEX                               @"AMEX"
#define CARD_MASTER_CARD                        @"MASTERCARD"
#define CARD_DISCOVER                           @"DISCOVER"
#define CARD_JCB                                @"JCB"
#define CARD_DINERS_CLUB                        @"DINERS CLUB"
#define CARD_UNKNOWN                            @"UNKNOWN"

#define CARD_NUMBER_STAND_ENCRYPTED             @"\U0001F4B3"
#define CARD_NUMBER_ENCRYPTED(last4)            [NSString stringWithFormat:@"\U000025FE \U000025FE \U000025FE %@", last4];

/*********** Shortcuts ***********/
#pragma mark - Shortcuts

#define _AppDelegate ((AppDelegate *)[[UIApplication sharedApplication] delegate])
#define _Shared [SharedManager sharedInstance]
#define _NotificationCenter [NSNotificationCenter defaultCenter]
#define _UserDefault        [NSUserDefaults standardUserDefaults]
#define _WorkspaceNotification [[NSWorkspace sharedWorkspace] notificationCenter]
#define _DistributedNotification [NSDistributedNotificationCenter defaultCenter]
#define _ManagedObjectContext [[CoreDataManager sharedInstance] managedObjectContext]
#define RGBA(r,g,b,a) [NSColor colorWithCalibratedRed:r/255.f green:g/255.f blue:b/255.f alpha:a]

/*********** Database Contants ***********/
#pragma mark - Database Contants

#define kUser @"user"
#define kName @"name"
#define kPlace @"place"
#define kMapFile @"mapFile"
#define kAvatar @"avatar"
#define kPaymentTokenId @"paymentTokenId"
#define kPayment4LastId @"payment4LastId"

#define kBeacon @"beacon"
#define kBeaconBMajorId @"majorId"
#define kBeaconBMinorId @"minorId"
#define kProximityUUID @"proximityUUID"
#define kBeaconBfirmwareVersion @"firmwareVersion"
#define kBeaconBBatteryStatus @"batteryStatus"

#define kAdvertisementFrame @"advertisementFrame"
#define kIsExpired      @"isExpired"
#define kStartDate      @"startDate"
#define kEndDate        @"endDate"
#define kFacebookId     @"facebookId"
#define kSettingNotification @"enablePush"


/*********** Facebook ***************/
#define kFacebookPermissions @[@"user_friends", @"public_profile", @"email", @"user_birthday"]
#define kProfileParameters @{@"fields": @"id, name, birthday"}
#define kFBName @"name"
#define kBirthday @"birthday"
#define kFacebookPictureUrl @"https://graph.facebook.com/me/picture?type=large&return_ssl_resources=1&access_token=%@"


/*********** Enums ***********/
#pragma mark - Enums
enum {
    kContentTypeText = 1,
    kContentTypeUrl = 2,
    kContentTypeImage = 3
};


#pragma mark - UITableViewCell Naming
typedef NS_ENUM(NSInteger, MenuSideNaming){
    kMenuAvatar = 0,
    kMenuNearby = 1,
    kMenuReceipt = 2,
    kMenuPayment = 3,
    kMenuPromotion = 4,
    kMenuSignOut = 5
};

typedef NS_ENUM(NSInteger, OrderPlacingSection) {
    kOrderPlacingDishes = 0,
    kOrderPlacingAddItem = 1,
    kOrderPlacingReceipt = 2,
    kOrderPlacingPayment = 3
};

typedef NS_ENUM(NSInteger, MenuItemSection) {
    kItemProfilePix = 0,
    kItemOther
};

typedef NS_ENUM(NSInteger, FeedbackItemRow) {
    kFeedbackItemHappy,
    kFeedbackItemMeh
};

typedef NS_ENUM(NSInteger, OrderViewModes) {
    kOrderViewModeAdding,
    kOrderViewModeEditing
};

typedef NS_ENUM(NSUInteger, ReceiptStatus) {
    kReceiptPending = 1,
    kReceiptConfirmed = 2,
    kReceiptDelivered = 3,
    kReceiptPrecanceled = 4,
    kReceiptCancel = 5
};

typedef NS_ENUM(NSUInteger, OrderConfirmationStatus) {
//    kOrderPending = 1,
    kOrderConfirmed = 2,
//    kOrderDelivered = 3,
//    kOrderPrecancelled = 4,
    kOrderCancelled = 5
};

typedef NS_ENUM(NSUInteger, PaymentType) {
    kPaymentTypeCard = 0,
    kPaymentTypeCash = 1
};

typedef NS_ENUM(NSUInteger, StoreOpeningTiming) {
    kStoreDidClosed,
    kStoreDidOpened
};

/*********** Animation Key ***********/
#pragma mark - Animation
#define ANIMATION_KEY_OPTION_MOVE_IN                            @"optionsMoveIn"
#define ANIMATION_KEY_OPTION_MOVE_OUT                           @"optionsMoveOut"
#define ANIMATION_KEY_OPTION_MOVE_IN_IMPACT                     @"optionsMoveInImpact"

#define ANIMATION_KEY_ORDER_TABLE_MOVE_OUT                      @"orderMoveOut"
#define ANIMATION_KEY_ORDER_TABLE_MOVE_IN                       @"orderMoveIn"
#define ANIMATION_KEY_ORDERTABLE_MOVE_IN_IMPACT                 @"orderMoveInImpact"

#define ANIMATION_KEY_OPTION_TITLE_CHANGE_VALUE                 @"optionTitleChanged"

#define ANIMATION_KEY_BOTTOM_BUTTON_CHANGE_TO_OPTION            @"bottomToOptions"
#define ANIMATION_KEY_BOTTOM_BUTTON_BACK_TO_ORDER               @"bottomBackToOrder"

#define ANIMATION_KEY_VIEW_ORDER_MOVE_IN                        @"viewOrderMoveIn"
#define ANIMATION_KEY_VIEW_ORDER_MOVE_OUT                       @"viewOrderMoveOut"
#define ANIMATION_KEY_VIEW_ORDER_MOVE_IN_BACK                   @"viewOrderMoveInBack"


/*********** Them and Colouring ***********/
#pragma mark - Theme and Color
#define MAIN_THEME_COLOR                            @"ff7b00"
#define MAIN_THEME_COLOR_DARK                       @"ff6500"
#define MAIN_THEME_COLOR_LIGHT                      @"ffdc9d"
#define MAIN_THEME_COLOR_LIGHT_LV_1                 @"ff8514"
#define MAIN_THEME_COLOR_LIGHT_LV_2                 @"ff8f27"
#define MAIN_THEME_COLOR_LIGHT_LV_3                 @"ff993b"
#define MAIN_SCROLLING_COLOR_SILVER                 @"e6e6e6"
#define MAIN_HORIZONTAL_SEPARATOR_SILVER            @"a3a3a3"
#define MAIN_HORIZONTAL_SEPARATOR_SILVER_DARK       @"9b9b9b"

#define MAIN_FACEBOOK_PALETTE                       @"3b5998"
#define MAIN_SECURITY_GREEN                         @"27ae60"
#define MAIN_SUCCESS_GREEN                          @"14eb14"
#define MAIN_FAILURE_RED                            @"ff0015"
#define MAIN_PENDING_BLUE                           @"1e90ff"

#define MAIN_MENU_SIDE_BACKGROUND                   @"ff893b"
#define MAIN_MENU_ITEM_BACKGROUND                   @"ff993b"

#define COLOR_MAIN_MENU_SIDE_BACKGROUND             [Util colorWithHexString:MAIN_MENU_SIDE_BACKGROUND]
#define COLOR_MAIN_MENU_ITEM_BACKGROUND             [Util colorWithHexString:MAIN_MENU_ITEM_BACKGROUND]

#define COLOR_MAIN_THEME                            [Util colorWithHexString:MAIN_THEME_COLOR]
#define COLOR_MAIN_THEME_DARK                       [Util colorWithHexString:MAIN_THEME_COLOR_DARK]
#define COLOR_MAIN_THEME_LIGHT_LV_1                 [Util colorWithHexString:MAIN_THEME_COLOR_LIGHT_LV_1]
#define COLOR_MAIN_THEME_SILVER                     [Util colorWithHexString:MAIN_HORIZONTAL_SEPARATOR_SILVER]
#define COLOR_MAIN_THEME_SILVER_DARK                [Util colorWithHexString:MAIN_HORIZONTAL_SEPARATOR_SILVER_DARK]
#define COLOR_MAIN_THEME_SECURITY_GREEN             [Util colorWithHexString:MAIN_SECURITY_GREEN]

#define COLOR_MAIN_THEME_SUCCESS_GREEN              [Util colorWithHexString:OPEN_TIME_COLOR]
#define COLOR_MAIN_THEME_FAILURE_RED                [Util colorWithHexString:CLOSED_TIME_COLOR]
#define COLOR_MAIN_THEME_PENDING_BLUE               [Util colorWithHexString:MAIN_PENDING_BLUE]

#define CLOSED_TIME_COLOR                           @"e60000"
#define OPEN_TIME_COLOR                             @"00e600"

#define NAV_BAR_BACKGROUND_COLOR                    @"ff7b00"
#define NAV_BAR_BACKGROUND_BAR_TINT_COLOR           @"ff7b00"


/*********** Popover pre-define sizes ***********/
#pragma mark - Popover pre-defined sizes
#define POPOVER_CONTENT_SIZE_IPAD               CGSizeMake(320.0f, 135.0f)
#define POPOVER_CONTENT_SIZE_IPHONE             CGSizeMake(290.0f, 135.0f)


#pragma mark - Store Constant
#define MODAL_VIEW_SIZE                     CGSizeMake(self.view.bounds.size.width*0.9f, self.view.bounds.size.height*0.85f)
#define MODAL_VIEW_SIZE_ORDER_PLACING       CGSizeMake(self.view.bounds.size.width*0.9f, self.view.bounds.size.height*0.75f)
#define MODAL_VIEW_SIZE_FROM_CHILDVC        CGSizeMake(self.view.superview.bounds.size.width*0.9f, self.view.superview.bounds.size.height*0.95f)

#define SPACING_BETWEEN_SCROLLING_BUTTON    16.0f


#pragma mark - Common
#define EXTENSION_TMX       @"tmx"
#define EXTENSION_XML       @"xml"

#endif
